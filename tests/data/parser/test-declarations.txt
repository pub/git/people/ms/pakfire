a = 1
b = 2
c = %{a}%{b}

# Append something
d = A
d += BC

# Multi-line
lines
	line 1
	line 2
end

# Continued line
e = A \
	B \
	C

# Commands
command = %(echo "ABC")
