#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import pakfire

import tests

class ArchTest(tests.TestCase):
	"""
		This tests the architecture functions
	"""
	def test_supported_arches(self):
		arches = pakfire.supported_arches()

		# Ensure this is a list
		self.assertIsInstance(arches, list)

		# Check if we have all our supported arches
		self.assertIn("x86_64", arches)
		self.assertIn("aarch64", arches)
		self.assertIn("riscv64", arches)

if __name__ == "__main__":
	tests.main()
