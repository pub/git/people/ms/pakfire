#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import pakfire

import tests

class KeysTests(tests.TestCase):
	"""
		This tests the keys
	"""
	def setUp(self):
		self.pakfire = self.setup_pakfire()

	def test_generate(self):
		"""
			Generate a new key
		"""
		key = self.pakfire.generate_key(
			algorithm=pakfire.PAKFIRE_KEY_ALGO_ED25519, comment="Key 1")

		# Check if we got the correct type
		self.assertIsInstance(key, pakfire.Key)

		# Check that the ID is in integer
		self.assertIsInstance(key.id, int)

		# Check that the algorithm matches
		self.assertEqual(key.algorithm, "Ed255919")

	def _import(self, path):
		with self.open(path) as f:
			payload = f.read()

		return self.pakfire.import_key(payload)

	def test_import_public_key(self):
		# Import a public key
		key = self._import("keys/key1.pub")

		# Check for the correct key ID
		self.assertEqual(key.id, 13863674484496905947)

	def test_import_secret_key(self):
		# Import a secret key
		key = self._import("keys/key1.sec")

		# Check for the correct key ID
		self.assertEqual(key.id, 13863674484496905947)

	def test_sign(self):
		"""
			Generate a new key
		"""
		key = self.pakfire.generate_key(
			algorithm=pakfire.PAKFIRE_KEY_ALGO_ED25519, comment="Key 1")

		data = b"Pakfire"

		# Sign!
		signature = key.sign(data, comment="This is a comment")

		# Verify!
		self.assertTrue(key.verify(signature, data))


if __name__ == "__main__":
	tests.main()
