###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import logging
import pakfire
import os
import sys
import tempfile
import unittest

# Fetch TEST_DATA_DIR
TEST_DATA_DIR = os.environ.get("TEST_DATA_DIR")
if not TEST_DATA_DIR:
	raise RuntimeError("TEST_DATA_DIR is not set")

# Setup logging
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

class TestCase(unittest.TestCase):
	"""
		This is a TestCase class based on unittest.TestCase which has
		a couple of handy methods that we frequently need.
	"""
	def setup_ctx(self, path=None, **kwargs):
		"""
			Sets up a new Pakfire context
		"""
		if path is None:
			path = os.environ.get("TEST_CONFIG_FILE")

		# Return a new context
		return pakfire.Ctx(path=path, **kwargs)

	def setup_pakfire(self, ctx=None, path=None, config=None, stub=False, **kwargs):
		"""
			Sets up a new Pakfire environment
		"""
		# Create a context if none given
		if ctx is None:
			ctx = self.setup_ctx()

		if config is None:
			config = os.environ.get("TEST_CONFIG_FILE")

		if path is None:
			path = os.environ.get("TEST_STUB_ROOT")

		# If we are in stub mode, we need to reset the path
		if stub:
			path = None

		with open(config, "r") as f:
			config = f.read()

		return pakfire.Pakfire(ctx=ctx, path=path, config=config, stub=stub, **kwargs)

	def path(self, *args, **kwargs):
		"""
			Creates a path absolute to the test environment
		"""
		return os.path.join(TEST_DATA_DIR, *args, **kwargs)

	def open(self, path, *args, **kwargs):
		"""
			Opens a file in the test environment
		"""
		# Make the path absolute
		path = self.path(path)

		# Open the file
		return open(path, *args, **kwargs)

	def tempdir(self):
		"""
			Creates a temporary directory
		"""
		return tempfile.TemporaryDirectory(prefix="pakfire-test-")


# Overlay for now
main = unittest.main
