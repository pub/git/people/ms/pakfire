#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import pakfire

import tests

class VersionCompareTest(tests.TestCase):
	"""
		This tests the static version comparison functions
	"""
	def test_equality(self):
		self.assertTrue(pakfire.version_compare("a", "a") == 0)

	def test_sorting(self):
		self.assertTrue(pakfire.version_compare("a", "b") < 0)
		self.assertTrue(pakfire.version_compare("b", "a") > 0)
		self.assertTrue(pakfire.version_compare("a", "c") < 0)
		self.assertTrue(pakfire.version_compare("c", "a") > 0)

if __name__ == "__main__":
	tests.main()
