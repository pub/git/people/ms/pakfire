#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import logging
import pakfire

import tests

class CtxTests(tests.TestCase):
	"""
		This tests the pakfire context
	"""
	def test_version(self):
		self.assertIsInstance(pakfire.__version__, str)

	def test_create(self):
		"""
			Create a context without any further ado
		"""
		ctx = self.setup_ctx()

		self.assertIsInstance(ctx, pakfire.Ctx)

	# Cache Path

	def test_cache_path(self):
		ctx = self.setup_ctx()

		# Set the cache path
		ctx.cache_path = "/tmp"

		# Read the cache path
		self.assertEqual(ctx.cache_path, "/tmp")

	# Logging

	def test_log_default(self):
		"""
			Tests if there is any log output if the default logger is being used
		"""
		logger = logging.getLogger("pakfire")

		with self.assertLogs(logger, logging.DEBUG):
			self.setup_pakfire()

	def test_log_custom(self):
		"""
			Tests if log messages can be received using a custom logger
		"""
		# A simple logger that stores everything it receives in a buffer
		class Logger(object):
			def __init__(self):
				self.buffer = []

			def log(self, *args):
				self.buffer.append(args)

		logger = Logger()

		# Setup a new context
		ctx = self.setup_ctx()

		# Configure the logger
		ctx.set_logger(logger)

		# Create a new Pakfire instance to create some log messages
		self.setup_pakfire(ctx=ctx)

		# Check if we have received anything
		self.assertTrue(logger.buffer)


if __name__ == "__main__":
	tests.main()
