/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/pakfire.h>

#include "../testsuite.h"

static int test_init(const struct test* t) {
	LOG("Allocated at %p\n", t->pakfire);

	// Check if the path matches
	ASSERT_STRING_EQUALS(pakfire_get_path(t->pakfire), TEST_STUB_ROOT);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_stub(const struct test* t) {
	struct pakfire* pakfire = NULL;
	int r = EXIT_FAILURE;

	// Have some illegal calls
	ASSERT_ERROR(pakfire_create(&pakfire, t->ctx, t->config, "PATH", NULL, PAKFIRE_FLAGS_STUB), EINVAL);

	// Have a success call
	ASSERT_SUCCESS(pakfire_create(&pakfire, t->ctx, t->config, NULL, NULL, PAKFIRE_FLAGS_STUB));

	r = EXIT_SUCCESS;

FAIL:
	if (pakfire)
		pakfire_unref(pakfire);

	return r;
}

static int test_fail(const struct test* t) {
	struct pakfire_config* config = NULL;
	struct pakfire* pakfire = NULL;
	int r = EXIT_FAILURE;

	// Create a configuration
	ASSERT_SUCCESS(pakfire_config_create(&config));

	// Init without anything
	ASSERT_ERROR(pakfire_create(&pakfire, t->ctx, NULL, NULL, NULL, 0), EINVAL);

	// Invalid architecture
	ASSERT_ERROR(pakfire_create(&pakfire, t->ctx, config, NULL, "arch", 0), ENOTSUP);

	// Invalid path (must be absolute)
	ASSERT_ERROR(pakfire_create(&pakfire, t->ctx, config, "path", NULL, 0), EINVAL);

	// Cannot use snapshots with a path
	ASSERT_ERROR(pakfire_create(&pakfire, t->ctx, NULL, PAKFIRE_TMP_DIR "/test",
		NULL, PAKFIRE_USE_SNAPSHOT), EINVAL);

	r = EXIT_SUCCESS;

FAIL:
	if (config)
		pakfire_config_unref(config);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_init, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_stub, 0);
	testsuite_add_test(test_fail, 0);

	return testsuite_run(argc, argv);
}
