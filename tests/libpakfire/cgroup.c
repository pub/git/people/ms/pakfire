/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <unistd.h>

#include <pakfire/cgroup.h>
#include <pakfire/jail.h>

#include "../testsuite.h"

static int test_create_and_destroy(const struct test* t) {
	struct pakfire_cgroup* cgroup = NULL;
	struct pakfire_cgroup* child = NULL;
	int r = EXIT_FAILURE;

	// Open a new cgroup
	ASSERT_SUCCESS(pakfire_cgroup_create(&cgroup, t->ctx, NULL, "pakfire-test", 0));

	// Create a child cgroup
	ASSERT_SUCCESS(pakfire_cgroup_child(&child, cgroup, "child-group", 0));

	// Destroy the cgroup again
	ASSERT_SUCCESS(pakfire_cgroup_destroy(cgroup, PAKFIRE_CGROUP_DESTROY_RECURSIVE));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (cgroup)
		pakfire_cgroup_unref(cgroup);
	if (child)
		pakfire_cgroup_unref(child);

	return r;
}

static int test_recursive(const struct test* t) {
	struct pakfire_cgroup* cgroup = NULL;
	int r = EXIT_FAILURE;

	// Try to create some invalid cgroups
	ASSERT_ERROR(pakfire_cgroup_create(&cgroup, t->ctx, NULL, NULL, 0), EINVAL);
	ASSERT_ERROR(pakfire_cgroup_create(&cgroup, t->ctx, NULL, "pakfire-test/a/", 0), EINVAL);

	// Open a new cgroup
	ASSERT_SUCCESS(pakfire_cgroup_create(&cgroup, t->ctx, NULL, "pakfire-test/a/b/c", 0));

	// Destroy the cgroup again
	ASSERT_SUCCESS(pakfire_cgroup_destroy(cgroup, PAKFIRE_CGROUP_DESTROY_RECURSIVE));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (cgroup)
		pakfire_cgroup_unref(cgroup);

	return r;
}

static int test_child(const struct test* t) {
	struct pakfire_cgroup* cgroup = NULL;
	struct pakfire_cgroup* child = NULL;
	int r = EXIT_FAILURE;

	// Open a new cgroup
	ASSERT_SUCCESS(pakfire_cgroup_create(&cgroup, t->ctx, NULL, "pakfire-test", 0));

	// Create a child
	ASSERT_SUCCESS(pakfire_cgroup_child(&child, cgroup, "child-group", 0));

	// Destroy the cgroups again
	ASSERT_SUCCESS(pakfire_cgroup_destroy(child, PAKFIRE_CGROUP_DESTROY_RECURSIVE));
	ASSERT_SUCCESS(pakfire_cgroup_destroy(cgroup, PAKFIRE_CGROUP_DESTROY_RECURSIVE));

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (child)
		pakfire_cgroup_unref(child);
	if (cgroup)
		pakfire_cgroup_unref(cgroup);

	return r;
}

static int test_stats(const struct test* t) {
	struct pakfire_cgroup* cgroup = NULL;
	struct pakfire_jail* jail = NULL;
	int r = EXIT_FAILURE;

	struct pakfire_cgroup_stats stats;

	const char* argv[] = {
		"/command", "sleep", "1", NULL,
	};

	// Open the cgroup
	ASSERT_SUCCESS(pakfire_cgroup_create(&cgroup, t->ctx, NULL, "pakfire-test", 0));

	// Create a new jail
	ASSERT_SUCCESS(pakfire_jail_create(&jail, t->pakfire));

	// Connect jail to the cgroup
	ASSERT_SUCCESS(pakfire_jail_set_cgroup(jail, cgroup));

	// Run a few things
	for (unsigned int i = 0; i < 3; i++) {
		ASSERT_SUCCESS(pakfire_jail_exec_command(jail, argv, NULL, 0));
	}

	// Try reading the stats into some invalid space
	ASSERT_ERROR(pakfire_cgroup_stat(cgroup, NULL), EINVAL);

	// Read the stats
	ASSERT_SUCCESS(pakfire_cgroup_stat(cgroup, &stats));

	// We must have had some CPU usage
	ASSERT(stats.cpu.usage_usec > 0);

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (cgroup) {
		pakfire_cgroup_destroy(cgroup, PAKFIRE_CGROUP_DESTROY_RECURSIVE);
		pakfire_cgroup_unref(cgroup);
	}
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_create_and_destroy, 0);
	testsuite_add_test(test_recursive, 0);
	testsuite_add_test(test_child, 0);
	testsuite_add_test(test_stats, TEST_WANTS_PAKFIRE);

	return testsuite_run(argc, argv);
}
