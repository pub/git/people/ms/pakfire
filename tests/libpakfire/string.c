/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <pakfire/string.h>

#include "../testsuite.h"

static int test_string_set(const struct test* t) {
	// Allocate a small buffer
	char buffer[4];

	// Write a string into a buffer which just has just enough space
	ASSERT_SUCCESS(pakfire_string_set(buffer, "ABC"));
	ASSERT_STRING_EQUALS(buffer, "ABC");

	// Write a string which would not fit
	ASSERT_ERROR(pakfire_string_set(buffer, "1234"), ENOBUFS);
	ASSERT_STRING_EQUALS(buffer, "123");

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_setn(const struct test* t) {
	char buffer[8];

	// Some test data to copy
	const char* s1 = "ABCDEFG";
	const char* s2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	// Write a string into a buffer which just has just enough space
	ASSERT(pakfire_string_setn(buffer, s1, strlen(s1)) == strlen(s1));
	ASSERT_STRING_EQUALS(buffer, s1);

	// Write a string which would not fit
	ASSERT_ERROR(pakfire_string_setn(buffer, s2, strlen(s2)), ENOBUFS);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_startswith(const struct test* t) {
	ASSERT_TRUE(pakfire_string_startswith("ABC", "A"));
	ASSERT_FALSE(pakfire_string_startswith("ABC", "B"));

	// Check for invalid inputs
	ASSERT(pakfire_string_startswith("ABC", NULL) == -EINVAL);
	ASSERT(pakfire_string_startswith(NULL, "ABC") == -EINVAL);
	ASSERT(pakfire_string_startswith(NULL, NULL) == -EINVAL);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_endswith(const struct test* t) {
	ASSERT_TRUE(pakfire_string_endswith("ABC", "C"));
	ASSERT_FALSE(pakfire_string_endswith("ABC", "B"));

	// Check for invalid inputs
	ASSERT(pakfire_string_endswith("ABC", NULL) == -EINVAL);
	ASSERT(pakfire_string_endswith(NULL, "ABC") == -EINVAL);
	ASSERT(pakfire_string_endswith(NULL, NULL) == -EINVAL);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_matches(const struct test* t) {
	ASSERT_TRUE(pakfire_string_matches("ABC", "A"));
	ASSERT_TRUE(pakfire_string_matches("ABC", "B"));
	ASSERT_TRUE(pakfire_string_matches("ABC", "C"));
	ASSERT_TRUE(pakfire_string_matches("ABC", "AB"));
	ASSERT_TRUE(pakfire_string_matches("ABC", "BC"));
	ASSERT_TRUE(pakfire_string_matches("ABC", "ABC"));
	ASSERT_FALSE(pakfire_string_matches("ABC", "D"));
	ASSERT_FALSE(pakfire_string_matches("ABC", "ABCD"));

	// Check for invalid inputs
	ASSERT(pakfire_string_matches("ABC", NULL) == -EINVAL);
	ASSERT(pakfire_string_matches(NULL, "ABC") == -EINVAL);
	ASSERT(pakfire_string_matches(NULL, NULL) == -EINVAL);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_partition(const struct test* t) {
	char* part1;
	char* part2;

	// Regular case
	int r = pakfire_string_partition("ABC:DEF", ":", &part1, &part2);
	ASSERT(r == 0);
	ASSERT_STRING_EQUALS(part1, "ABC");
	ASSERT_STRING_EQUALS(part2, "DEF");

	free(part1);
	free(part2);

	// No delimiter
	r = pakfire_string_partition("ABCDEF", ":", &part1, &part2);
	ASSERT(r == 1);
	ASSERT(part1 == NULL);
	ASSERT(part2 == NULL);

	// Nothing after the delimiter
	r = pakfire_string_partition("ABC:", ":", &part1, &part2);
	ASSERT(r == 0);
	ASSERT_STRING_EQUALS(part1, "ABC");
	ASSERT_STRING_EQUALS(part2, "");

	free(part1);
	free(part2);

	// Nothing before the delimiter
	r = pakfire_string_partition(":ABC", ":", &part1, &part2);
	ASSERT(r == 0);
	ASSERT_STRING_EQUALS(part1, "");
	ASSERT_STRING_EQUALS(part2, "ABC");

	free(part1);
	free(part2);

	// Multi-character delimiter
	r = pakfire_string_partition("ABC:-:DEF", ":-:", &part1, &part2);
	ASSERT(r == 0);
	ASSERT_STRING_EQUALS(part1, "ABC");
	ASSERT_STRING_EQUALS(part2, "DEF");

	free(part1);
	free(part2);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_string_replace(const struct test* t) {
	int r = EXIT_FAILURE;

	char* result = pakfire_string_replace(
		"ABCABCABCABC", "AB", "CC"
	);
	ASSERT_STRING_EQUALS(result, "CCCCCCCCCCCC");

	// Success
	r = EXIT_SUCCESS;

FAIL:
	if (result)
		free(result);

	return r;
}

static int test_string_join(const struct test* t) {
	char* s = NULL;

	// Some test elements
	const char* elements1[] = {
		"A",
		"B",
		"C",
		NULL,
	};

	// Join with newline
	s = pakfire_string_join(elements1, "\n");
	ASSERT_STRING_EQUALS(s, "A\nB\nC");

	if (s) {
		free(s);
		s = NULL;
	}

	// Join with empty delimiter
	s = pakfire_string_join(elements1, "");
	ASSERT_STRING_EQUALS(s, "ABC");

	if (s) {
		free(s);
		s = NULL;
	}

	const char* elements2[] = {
		"",
		"",
		"",
		NULL,
	};

	// Join list with empty elements
	s = pakfire_string_join(elements2, "X");
	ASSERT_STRING_EQUALS(s, "XX");

	if (s) {
		free(s);
		s = NULL;
	}

	// Invalid inputs
	s = pakfire_string_join(NULL, NULL);
	ASSERT_ERRNO(!s, EINVAL);

	s = pakfire_string_join(elements1, NULL);
	ASSERT_ERRNO(!s, EINVAL);

	s = pakfire_string_join(NULL, "\n");
	ASSERT_ERRNO(!s, EINVAL);

	const char* elements3[] = {
		NULL,
	};

	// Join empty elements
	ASSERT_NULL(pakfire_string_join(elements3, "\n"));

	return EXIT_SUCCESS;

FAIL:
	if (s)
		free(s);

	return EXIT_FAILURE;
}

static int test_format_size(const struct test* t) {
	char buffer[128];
	char small_buffer[2];
	int r;

	ASSERT_SUCCESS(pakfire_format_size(buffer, 0));
	ASSERT_STRING_EQUALS(buffer, "0 ");

	ASSERT_SUCCESS(pakfire_format_size(buffer, 1024));
	ASSERT_STRING_EQUALS(buffer, "1k");

	ASSERT_SUCCESS(pakfire_format_size(buffer, 1024 * 1024) );
	ASSERT_STRING_EQUALS(buffer, "1.0M");

	ASSERT_ERROR(pakfire_format_size(small_buffer, 0), ENOBUFS);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_format_speed(const struct test* t) {
	char buffer[128];
	char small_buffer[2];
	int r;

	ASSERT_SUCCESS(pakfire_format_speed(buffer, 0));
	ASSERT_STRING_EQUALS(buffer, "   0B/s");

	ASSERT_SUCCESS(pakfire_format_speed(buffer, 1024));
	ASSERT_STRING_EQUALS(buffer, "   1kB/s");

	ASSERT_SUCCESS(pakfire_format_speed(buffer, 1536));
	ASSERT_STRING_EQUALS(buffer, "   2kB/s");

	ASSERT_SUCCESS(pakfire_format_speed(buffer, 1024 * 1024) );
	ASSERT_STRING_EQUALS(buffer, " 1.0MB/s");

	ASSERT_ERROR(pakfire_format_speed(small_buffer, 0), ENOBUFS);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_format_time(const struct test* t) {
	char buffer[128];

	// Values smaller than zero
	ASSERT_ERROR(pakfire_format_time(buffer, -1), EINVAL);

	// Values greater than a day
	ASSERT_SUCCESS(pakfire_format_time(buffer, 86400));
	ASSERT_STRING_EQUALS(buffer, "1d0h0m");

	// Values greater than an hour
	ASSERT_SUCCESS(pakfire_format_time(buffer, 3600));
	ASSERT_STRING_EQUALS(buffer, "1h0m0s");

	// Values greater than a minute
	ASSERT_SUCCESS(pakfire_format_time(buffer, 60));
	ASSERT_STRING_EQUALS(buffer, "1m0s");

	// Values below a minute
	ASSERT_SUCCESS(pakfire_format_time(buffer, 1));
	ASSERT_STRING_EQUALS(buffer, "1s");

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_parse_bytes(const struct test* t) {
	// Zero without unit
	ASSERT(pakfire_string_parse_bytes("0") == (size_t)0);

	// Zero with valid units
	ASSERT(pakfire_string_parse_bytes("0k") == (size_t)0);
	ASSERT(pakfire_string_parse_bytes("0M") == (size_t)0);
	ASSERT(pakfire_string_parse_bytes("0G") == (size_t)0);
	ASSERT(pakfire_string_parse_bytes("0T") == (size_t)0);

	// Zero with invalid units
	ASSERT_ERRNO(pakfire_string_parse_bytes("0X")  == (size_t)0, EINVAL);
	ASSERT_ERRNO(pakfire_string_parse_bytes("0XX") == (size_t)0, EINVAL);
	ASSERT_ERRNO(pakfire_string_parse_bytes("0MM") == (size_t)0, EINVAL);

	// One with valid units
	ASSERT(pakfire_string_parse_bytes("1")  == (size_t)1);
	ASSERT(pakfire_string_parse_bytes("1k") == (size_t)1024);
	ASSERT(pakfire_string_parse_bytes("1M") == (size_t)1024 * 1024);
	ASSERT(pakfire_string_parse_bytes("1G") == (size_t)1024 * 1024 * 1024);
	ASSERT(pakfire_string_parse_bytes("1T") == (size_t)1024 * 1024 * 1024 * 1024);

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_intervals(const struct test* t) {
	char buffer[1024];
	int r = EXIT_FAILURE;

	// Parse
	ASSERT(pakfire_string_parse_interval("0") == 0);
	ASSERT(pakfire_string_parse_interval("0s") == 0);
	ASSERT(pakfire_string_parse_interval("0m") == 0);

	ASSERT(pakfire_string_parse_interval("1") == 1);
	ASSERT(pakfire_string_parse_interval("1s") == 1);
	ASSERT(pakfire_string_parse_interval("1m") == 60);
	ASSERT(pakfire_string_parse_interval("1d") == 86400);
	ASSERT(pakfire_string_parse_interval("1w") == 604800);

	ASSERT(pakfire_string_parse_interval("2s") == 2);
	ASSERT(pakfire_string_parse_interval("2m") == 120);
	ASSERT(pakfire_string_parse_interval("2h") == 7200);

	// Invalid inputs
	ASSERT(pakfire_string_parse_interval(NULL) == -1);
	ASSERT(pakfire_string_parse_interval("abc") == -1);
	ASSERT(pakfire_string_parse_interval("abc0") == -1);
	ASSERT(pakfire_string_parse_interval("0abc") == -1);

	// Format
	ASSERT_SUCCESS(pakfire_string_format_interval(buffer, 0));
	ASSERT_STRING_EQUALS(buffer, "0s");
	ASSERT_SUCCESS(pakfire_string_format_interval(buffer, 1));
	ASSERT_STRING_EQUALS(buffer, "1s");
	ASSERT_SUCCESS(pakfire_string_format_interval(buffer, 5));
	ASSERT_STRING_EQUALS(buffer, "5s");
	ASSERT_SUCCESS(pakfire_string_format_interval(buffer, 60));
	ASSERT_STRING_EQUALS(buffer, "1m");
	ASSERT_SUCCESS(pakfire_string_format_interval(buffer, 61));
	ASSERT_STRING_EQUALS(buffer, "1m");
	ASSERT_SUCCESS(pakfire_string_format_interval(buffer, 3600));
	ASSERT_STRING_EQUALS(buffer, "1h");
	ASSERT_SUCCESS(pakfire_string_format_interval(buffer, 86400));
	ASSERT_STRING_EQUALS(buffer, "1d");
	ASSERT_SUCCESS(pakfire_string_format_interval(buffer, 604800));
	ASSERT_STRING_EQUALS(buffer, "1w");

	// Invalid inputs
	ASSERT_ERROR(pakfire_string_format_interval(buffer, -1), EINVAL);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	return r;
}

static int test_string_contains_whitespace(const struct test* t) {
	int r = EXIT_FAILURE;

	ASSERT_TRUE(pakfire_string_contains_whitespace("  "));
	ASSERT_TRUE(pakfire_string_contains_whitespace("ABC DEF"));
	ASSERT_TRUE(pakfire_string_contains_whitespace("ABC	DEF"));

	ASSERT_FALSE(pakfire_string_contains_whitespace("ABCDEF"));
	ASSERT_FALSE(pakfire_string_contains_whitespace(""));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	return r;
}

static int test_string_search(const struct test* t) {
	int r = EXIT_FAILURE;

	// Check if we find the pattern in the right place
	ASSERT_EQUALS(pakfire_string_search("ABCDEFGHIJKLMNOPQ", -1, "ABC", -1), 0);
	ASSERT_EQUALS(pakfire_string_search("ABCDEFGHIJKLMNOPQ", -1, "DEF", -1), 3);
	ASSERT_EQUALS(pakfire_string_search("ABCDEFGHIJKLMNOPQ", -1, "OPQ", -1), 14);

	// Check if don't find some false positive
	ASSERT_EQUALS(pakfire_string_search("ABCDEFGHIJKLMNOPQ", -1, "XYZ", -1), -1);

	// Check invalid inputs
	ASSERT_ERROR(pakfire_string_search(NULL, -1, NULL, -1), EINVAL);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	return r;
}

static int test_truncate(const struct test* t) {
	char s[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	int r = EXIT_FAILURE;

	// Truncate the string bit by bit

	pakfire_string_truncate(s, 24);
	ASSERT_STRING_EQUALS(s, "ABCDEFGHIJKLMNOPQRSTUVWX");

	pakfire_string_truncate(s, 3);
	ASSERT_STRING_EQUALS(s, "ABC");

	pakfire_string_truncate(s, 4);
	ASSERT_STRING_EQUALS(s, "ABC");

	pakfire_string_truncate(s, 0);
	ASSERT_STRING_EQUALS(s, "");

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_string_set, 0);
	testsuite_add_test(test_string_setn, 0);
	testsuite_add_test(test_string_startswith, 0);
	testsuite_add_test(test_string_endswith, 0);
	testsuite_add_test(test_string_matches, 0);
	testsuite_add_test(test_string_partition, 0);
	testsuite_add_test(test_string_replace, 0);
	testsuite_add_test(test_string_join, 0);
	testsuite_add_test(test_format_size, 0);
	testsuite_add_test(test_format_speed, 0);
	testsuite_add_test(test_format_time, 0);
	testsuite_add_test(test_parse_bytes, 0);
	testsuite_add_test(test_intervals, 0);
	testsuite_add_test(test_string_contains_whitespace, 0);
	testsuite_add_test(test_string_search, 0);
	testsuite_add_test(test_truncate, 0);

	return testsuite_run(argc, argv);
}
