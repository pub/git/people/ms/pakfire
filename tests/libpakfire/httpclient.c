/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>

#include <pakfire/httpclient.h>

#include "../testsuite.h"

#define DOWNLOAD_URL "file://" ABS_TOP_SRCDIR "/tests/data/beep-1.3-2.ip3.x86_64.pfm"

static int test_create(const struct test* t) {
	struct pakfire_httpclient* client = NULL;
	int r = EXIT_FAILURE;

	// Create a HTTP client
	ASSERT_SUCCESS(pakfire_httpclient_create(&client, t->ctx, NULL));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (client)
		pakfire_httpclient_unref(client);

	return r;
}

static int test_one(const struct test* t) {
	struct pakfire_xfer* xfer = NULL;
	char* buffer = NULL;
	size_t length = 0;
	int r = EXIT_FAILURE;

	// Create a new transfer
	ASSERT_SUCCESS(pakfire_xfer_create_simple(&xfer, t->ctx, DOWNLOAD_URL));

	// Set output buffer
	ASSERT_SUCCESS(pakfire_xfer_set_output_buffer(xfer, &buffer, &length));

	// Enqueue the transfer
	ASSERT_SUCCESS(pakfire_httpclient_enqueue(t->httpclient, xfer));

	// Run it!
	ASSERT_SUCCESS(pakfire_httpclient_run(t->httpclient, "Downloading..."));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (buffer)
		free(buffer);

	return r;
}

static int __test_create_transfer(const struct test* t, struct pakfire_xfer** xfer,
	char** buffer, size_t* length, const char* url, ...) __attribute__((format(printf, 5, 6)));

static int __test_create_transfer(const struct test* t, struct pakfire_xfer** xfer,
		char** buffer, size_t* length, const char* url, ...) {
	va_list args;
	int r;

	va_start(args, url);

	// Create a new transfer
	r = pakfire_xfer_create(xfer, t->ctx, url, args);
	if (r)
		goto ERROR;

	// Set the output buffer
	r = pakfire_xfer_set_output_buffer(*xfer, buffer, length);
	if (r)
		goto ERROR;

	// Enqueue the transfer
	r = pakfire_httpclient_enqueue(t->httpclient, *xfer);
	if (r)
		goto ERROR;

ERROR:
	va_end(args);

	return r;
}

static int test_multiple(const struct test* t) {
	struct pakfire_xfer* xfer1 = NULL;
	char* buffer1 = NULL;
	size_t length1 = 0;
	struct pakfire_xfer* xfer2 = NULL;
	char* buffer2 = NULL;
	size_t length2 = 0;
	struct pakfire_xfer* xfer3 = NULL;
	char* buffer3 = NULL;
	size_t length3 = 0;
	struct pakfire_xfer* xfer4 = NULL;
	char* buffer4 = NULL;
	size_t length4 = 0;
	int r = EXIT_FAILURE;

	ASSERT_SUCCESS(__test_create_transfer(t, &xfer1, &buffer1, &length1, "%s", DOWNLOAD_URL));
	ASSERT_SUCCESS(__test_create_transfer(t, &xfer2, &buffer2, &length2, "%s", DOWNLOAD_URL));
	ASSERT_SUCCESS(__test_create_transfer(t, &xfer3, &buffer3, &length3, "%s", DOWNLOAD_URL));
	ASSERT_SUCCESS(__test_create_transfer(t, &xfer4, &buffer4, &length4, "%s", DOWNLOAD_URL));

	// Run it!
	ASSERT_SUCCESS(pakfire_httpclient_run(t->httpclient, "Downloading..."));

	ASSERT(*buffer1);
	ASSERT(*buffer2);
	ASSERT(*buffer3);
	ASSERT(*buffer4);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (xfer1)
		pakfire_xfer_unref(xfer1);
	if (xfer2)
		pakfire_xfer_unref(xfer2);
	if (xfer3)
		pakfire_xfer_unref(xfer3);
	if (xfer4)
		pakfire_xfer_unref(xfer4);
	if (buffer1)
		free(buffer1);
	if (buffer2)
		free(buffer2);
	if (buffer3)
		free(buffer3);
	if (buffer4)
		free(buffer4);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_create, 0);
	testsuite_add_test(test_one, TEST_WANTS_HTTPCLIENT);
	testsuite_add_test(test_multiple, TEST_WANTS_HTTPCLIENT);

	return testsuite_run(argc, argv);
}
