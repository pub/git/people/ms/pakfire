/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/log_file.h>
#include <pakfire/path.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#include "../testsuite.h"

static int __test_simple(const struct test* t, int flags) {
	struct pakfire_log_file* file = NULL;
	char path[PATH_MAX] = "";
	int r = EXIT_FAILURE;

	// Create a new log file
	ASSERT_SUCCESS(pakfire_log_file_create(&file, t->ctx, NULL, "test.log", flags));

	// Store a copy of the path
	ASSERT_SUCCESS(pakfire_string_set(path, pakfire_log_file_path(file)));

	// Write something to the file
	for (int i = 0; i < 10; i++)
		ASSERT_SUCCESS(pakfire_log_file_write(file, LOG_INFO, "BUFFER DATA\n", -1));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (file)
		pakfire_log_file_unref(file);

	// Check if the log file was removed
	if (*path) {
		if (pakfire_path_exists(path)) {
			LOG("Log file was not cleaned up\n");
			return 1;
		}
	}

	return r;
}

static int test_uncompressed(const struct test* t) {
	return __test_simple(t, 0);
}

static int test_compressed(const struct test* t) {
	return __test_simple(t, PAKFIRE_LOG_FILE_COMPRESS);
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_uncompressed, 0);
	testsuite_add_test(test_compressed, 0);

	return testsuite_run(argc, argv);
}
