/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2019 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <limits.h>

#include <pakfire/path.h>
#include <pakfire/string.h>

#include "../testsuite.h"

static int test_path_normalize(const struct test* t) {
	char path[PATH_MAX];

	ASSERT_SUCCESS(pakfire_string_set(path, "/usr/bin/bash"));
	ASSERT_SUCCESS(pakfire_path_normalize(path));
	ASSERT_STRING_EQUALS(path, "/usr/bin/bash");

	ASSERT_SUCCESS(pakfire_string_set(path, "/"));
	ASSERT_SUCCESS(pakfire_path_normalize(path));
	ASSERT_STRING_EQUALS(path, "/");

	ASSERT_SUCCESS(pakfire_string_set(path, "/.."));
	ASSERT_SUCCESS(pakfire_path_normalize(path));
	ASSERT_STRING_EQUALS(path, "/");

	ASSERT_SUCCESS(pakfire_string_set(path, "/../.."));
	ASSERT_SUCCESS(pakfire_path_normalize(path));
	ASSERT_STRING_EQUALS(path, "/");

	ASSERT_SUCCESS(pakfire_string_set(path, "/usr/bin/bash"));
	ASSERT_SUCCESS(pakfire_path_normalize(path));
	ASSERT_STRING_EQUALS(path, "/usr/bin/bash");

	ASSERT_SUCCESS(pakfire_string_set(path, "/usr/bin/sh/../bash"));
	ASSERT_SUCCESS(pakfire_path_normalize(path));
	ASSERT_STRING_EQUALS(path, "/usr/bin/bash");

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_path_append(const struct test* t) {
	char path[PATH_MAX];

	ASSERT_SUCCESS(pakfire_path_append(path, "/usr/bin", "bash"));
	ASSERT_STRING_EQUALS(path, "/usr/bin/bash");

	ASSERT_SUCCESS(pakfire_path_append(path, "/usr/bin", "/bash"));
	ASSERT_STRING_EQUALS(path, "/usr/bin/bash");

	ASSERT_SUCCESS(pakfire_path_append(path, "/usr/bin/sh", "../bash"));
	ASSERT_STRING_EQUALS(path, "/usr/bin/bash");

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_path_merge(const struct test* t) {
	char path[PATH_MAX];

	ASSERT_SUCCESS(pakfire_path_merge(path, "/usr/bin/sh", "bash"));
	ASSERT_STRING_EQUALS(path, "/usr/bin/bash");

	ASSERT_SUCCESS(pakfire_path_merge(path, "/usr/bin", "/bash"));
	ASSERT_STRING_EQUALS(path, "/bash");

	ASSERT_SUCCESS(pakfire_path_merge(path, "/usr/bin/sh", "../bin/bash"));
	ASSERT_STRING_EQUALS(path, "/usr/bin/bash");

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_path_basename(const struct test* t) {
	char path[PATH_MAX];

	ASSERT_SUCCESS(pakfire_path_basename(path, "/usr/bin/bash"));
	ASSERT_STRING_EQUALS(path, "bash");

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_path_dirname(const struct test* t) {
	char path[PATH_MAX];

	ASSERT_SUCCESS(pakfire_path_dirname(path, "/usr/bin/bash"));
	ASSERT_STRING_EQUALS(path, "/usr/bin");

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_path_relative(const struct test* t) {
	char path[PATH_MAX];

	ASSERT_SUCCESS(pakfire_path_relative(path, "/", "/usr/bin/bash"));
	ASSERT_STRING_EQUALS(path, "usr/bin/bash");

	ASSERT_SUCCESS(pakfire_path_relative(path, "/usr", "/usr/bin/bash"));
	ASSERT_STRING_EQUALS(path, "bin/bash");

	ASSERT_SUCCESS(pakfire_path_relative(path, "/usr/bin", "/usr/bin/bash"));
	ASSERT_STRING_EQUALS(path, "bash");

	ASSERT_SUCCESS(pakfire_path_relative(path, "/usr/sbin", "/usr/bin/bash"));
	ASSERT_STRING_EQUALS(path, "../bin/bash");

	ASSERT_SUCCESS(pakfire_path_relative(path, "/dev", "/usr/bin/bash"));
	ASSERT_STRING_EQUALS(path, "../usr/bin/bash");

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

static int test_path_match(const struct test* t) {
	// Simple string match
	ASSERT_TRUE(pakfire_path_match("/abc", "/abc"));

	// Simple negative string match
	ASSERT_FALSE(pakfire_path_match("/abc", "/def"));

	// Simple star match
	ASSERT_TRUE(pakfire_path_match("/usr/*", "/usr/bin"));
	ASSERT_FALSE(pakfire_path_match("/usr/*", "/usr/bin/bash"));

	// Double star match
	ASSERT_TRUE(pakfire_path_match("/usr/**", "/usr/bin"));
	ASSERT_TRUE(pakfire_path_match("/usr/**", "/usr/bin/bash"));

	// Tripe star matches are invalid
	ASSERT_ERRNO(pakfire_path_match("/usr/***", "/usr/bin"), EINVAL);
	ASSERT_ERRNO(pakfire_path_match("/usr/***", "/usr/bin/bash"), EINVAL);

	// Partial paths shouldn't match
	ASSERT_FALSE(pakfire_path_match("/usr", "/usr/bin/bash"));

	// Check for stars in the middle
	ASSERT_TRUE(pakfire_path_match("/usr/lib64/*.so.*", "/usr/lib64/libblah.so.1"));
	ASSERT_TRUE(pakfire_path_match("/usr/lib64/**/*.so", "/usr/lib64/blah/module.so"));
	ASSERT_FALSE(pakfire_path_match("/usr/lib64/*.so.*", "/usr/lib64/beep"));

	// Relatve vs. absolute?
	ASSERT_FALSE(pakfire_path_match("/usr/lib/*.so", "relative-path"));

	return EXIT_SUCCESS;

FAIL:
	return EXIT_FAILURE;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_path_normalize, 0);
	testsuite_add_test(test_path_append, 0);
	testsuite_add_test(test_path_merge, 0);
	testsuite_add_test(test_path_basename, 0);
	testsuite_add_test(test_path_dirname, 0);
	testsuite_add_test(test_path_relative, 0);
	testsuite_add_test(test_path_match, 0);

	return testsuite_run(argc, argv);
}
