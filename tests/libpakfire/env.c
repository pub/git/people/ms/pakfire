/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/env.h>

#include "../testsuite.h"

static int test_set_get(const struct test* t) {
	struct pakfire_env* env = NULL;
	int r = EXIT_FAILURE;

	// Create the environment
	ASSERT_SUCCESS(pakfire_env_create(&env, t->ctx));

	// Add something
	ASSERT_SUCCESS(pakfire_env_set(env, "A", "1"));
	ASSERT_SUCCESS(pakfire_env_set(env, "B", "2"));

	// Fetch the values
	ASSERT_STRING_EQUALS(pakfire_env_get(env, "A"), "1");
	ASSERT_STRING_EQUALS(pakfire_env_get(env, "B"), "2");
	ASSERT_NULL(pakfire_env_get(env, "C"));

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (env)
		pakfire_env_unref(env);

	return r;
}

static int test_merge(const struct test* t) {
	struct pakfire_env* env1 = NULL;
	struct pakfire_env* env2 = NULL;
	int r = EXIT_FAILURE;

	// Create the environments
	ASSERT_SUCCESS(pakfire_env_create(&env1, t->ctx));
	ASSERT_SUCCESS(pakfire_env_create(&env2, t->ctx));

	// Add something to the first environment
	ASSERT_SUCCESS(pakfire_env_set(env1, "A", "1"));

	// Add something to the second environment
	ASSERT_SUCCESS(pakfire_env_set(env2, "B", "2"));

	// Check that we have set everything correctly
	ASSERT_STRING_EQUALS(pakfire_env_get(env1, "A"), "1");
	ASSERT_NULL(pakfire_env_get(env1, "B"));

	ASSERT_NULL(pakfire_env_get(env2, "A"));
	ASSERT_STRING_EQUALS(pakfire_env_get(env2, "B"), "2");

	// Merge
	ASSERT_SUCCESS(pakfire_env_merge(env1, env2));

	// env1 should now have B set, too
	ASSERT_STRING_EQUALS(pakfire_env_get(env1, "A"), "1");
	ASSERT_STRING_EQUALS(pakfire_env_get(env1, "B"), "2");

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (env1)
		pakfire_env_unref(env1);
	if (env2)
		pakfire_env_unref(env2);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_set_get, 0);
	testsuite_add_test(test_merge, 0);

	return testsuite_run(argc, argv);
}
