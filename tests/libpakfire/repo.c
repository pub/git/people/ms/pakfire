/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <pakfire/repo.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#include "../testsuite.h"

#define TEST_PKG1_PATH "data/beep-1.3-2.ip3.x86_64.pfm"

static int test_scan(const struct test* t) {
	struct pakfire_repo* repo = NULL;
	char baseurl[1024];
	int r = EXIT_FAILURE;
	FILE* f = NULL;

	pakfire_string_format(baseurl, "file://%s/data", TEST_SRC_PATH);

	ASSERT_SUCCESS(pakfire_repo_create(&repo, t->pakfire, "test"));
	ASSERT(repo);

	// Set the base URL (where to scan for files)
	ASSERT_SUCCESS(pakfire_repo_set_baseurl(repo, baseurl));

	// Scan for all files in this directory
	ASSERT_SUCCESS(pakfire_repo_scan(repo, 0));

	// There should be one package in this repository now
	ASSERT_EQUALS(pakfire_repo_count(repo), 1);

	// Allocate a temporary file
	f = test_mktemp(NULL);
	ASSERT(f);

	// Write SOLV database
	ASSERT_SUCCESS(pakfire_repo_write_solv(repo, f, 0));
	rewind(f);

	// Flush all information in memory
	pakfire_repo_clear(repo);

	// Read SOLV database again
	ASSERT_SUCCESS(pakfire_repo_read_solv(repo, f, 0));

	// There should still be exactly one package be in this repository
	ASSERT(pakfire_repo_count(repo) == 1);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (repo)
		pakfire_repo_unref(repo);
	if (f)
		fclose(f);

	return r;
}

static int test_compose(const struct test* t) {
	struct pakfire_repo* repo = NULL;
	int r = EXIT_FAILURE;

	// Create a temporary working directory
	char* path = test_mkdtemp();
	ASSERT(path);

	const char* files[] = {
		TEST_SRC_PATH TEST_PKG1_PATH,
		NULL,
	};

	// Compose the repository
	ASSERT_SUCCESS(
		pakfire_repo_compose(t->pakfire, path, NULL, files)
	);

	// Everything passed
	r = EXIT_SUCCESS;

FAIL:
	if (repo)
		pakfire_repo_unref(repo);
	if (path)
		free(path);

	return r;
}

int main(int argc, const char* argv[]) {
	testsuite_add_test(test_scan, TEST_WANTS_PAKFIRE);
	testsuite_add_test(test_compose, TEST_WANTS_PAKFIRE);

	return testsuite_run(argc, argv);
}
