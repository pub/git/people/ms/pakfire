/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>
#include <syslog.h>

#include <pakfire/ctx.h>

#include "lib/client-build.h"
#include "lib/command.h"
#include "lib/config.h"
#include "lib/pakfire.h"
#include "lib/progressbar.h"
#include "lib/repo.h"
#include "lib/upload.h"

const char* argp_program_version = PACKAGE_FULLVERSION;

static const struct command commands[] = {
	{ "build",  cli_client_build, 1, -1, 0 },
	{ "repo",   cli_repo_client, -1, -1, 0 },
	{ "upload", cli_upload,      -1, -1, 0 },
	{ NULL },
};

static const char* args_doc =
	"repo ...\n"
	"upload ...";

static const char* doc = "The Pakfire Build Service Client Tool";

enum {
	OPT_DEBUG  = 1,
	OPT_DISTRO = 2,
};

static struct argp_option options[] = {
	{ "debug",  OPT_DEBUG,      NULL, 0, "Run in debug mode",       0 },
	{ "distro", OPT_DISTRO, "DISTRO", 0, "Choose the distribution", 0 },
	{ NULL },
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_global_args* args = data;

	switch (key) {
		case OPT_DEBUG:
			pakfire_ctx_set_log_level(args->ctx, LOG_DEBUG);
			break;

		case OPT_DISTRO:
			args->distro = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

int main(int argc, char* argv[]) {
	struct pakfire_ctx* ctx = NULL;
	int r;

	// Setup the context
	r = pakfire_ctx_create(&ctx, NULL);
	if (r)
		goto ERROR;

	// Write logs to the console
	pakfire_ctx_set_log_callback(ctx, pakfire_log_stderr, NULL);

	// Setup progress callback
	pakfire_ctx_set_progress_callback(ctx, cli_setup_progressbar, NULL);

	struct cli_global_args args = {
		.ctx    = ctx,
		.distro = cli_get_default_distro(ctx),
	};

	// Parse the command line and run any commands
	r = cli_parse(options, commands, args_doc, doc,
		parse, 0, argc, argv, &args);

ERROR:
	if (ctx) {
		ctx = pakfire_ctx_unref(ctx);
		if (ctx) {
			fprintf(stderr, "Context was not freed\n");
			return 1;
		}
	}

	return r;
}
