/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include <pakfire/pakfire.h>

#include "command.h"
#include "dump.h"
#include "pakfire.h"
#include "provides.h"

static const char* args_doc = "PATTERNS...";

static const char* doc = "Search for packages that provide a certain pattern";

#define MAX_PATTERNS 256

struct cli_local_args {
	char* patterns[MAX_PATTERNS];
	unsigned int num_patterns;
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case ARGP_KEY_ARG:
			if (args->num_patterns >= MAX_PATTERNS)
				return -ENOBUFS;

			args->patterns[args->num_patterns++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

int cli_provides(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {};
	struct pakfire* pakfire = NULL;
	struct pakfire_packagelist* list = NULL;
	int r;

	// Parse the command line
	r = cli_parse(NULL, NULL, args_doc, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Setup pakfire
	r = cli_setup_pakfire(&pakfire, global_args);
	if (r)
		goto ERROR;

	// Allocate a new packagelist
	r = pakfire_packagelist_create(&list, global_args->ctx);
	if (r)
		goto ERROR;

	// Perform search
	for (unsigned int i = 0; i < local_args.num_patterns; i++) {
		r = pakfire_whatprovides(pakfire, local_args.patterns[i], 0, list);
		if (r)
			goto ERROR;
	}

	// Dump the packagelist
	r = cli_dump_packagelist(list, 0);

ERROR:
	if (list)
		pakfire_packagelist_unref(list);
	if (pakfire)
		pakfire_unref(pakfire);

	return r;
}
