/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_CLI_PAKFIRE_H
#define PAKFIRE_CLI_PAKFIRE_H

#include <pakfire/build.h>
#include <pakfire/buildservice.h>
#include <pakfire/config.h>
#include <pakfire/ctx.h>
#include <pakfire/pakfire.h>

#define MAX_REPOS 16

struct cli_global_args {
	struct pakfire_ctx* ctx;

	const char* distro;
	const char* config;
	const char* arch;
	const char* root;
	int flags;
	int yes;

	// Repos
	const char* enable_repos[MAX_REPOS];
	unsigned int num_enable_repos;
	const char* disable_repos[MAX_REPOS];
	unsigned int num_disable_repos;
};

int cli_setup_config(struct pakfire_config** config, struct cli_global_args* args);
int cli_setup_pakfire(struct pakfire** pakfire, struct cli_global_args* args);
int cli_setup_build(struct pakfire_build** build, struct cli_global_args* args, int flags);
int cli_setup_buildservice(struct pakfire_buildservice** service, struct cli_global_args* args);

#endif /* PAKFIRE_CLI_PAKFIRE_H */
