/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_CLI_TRANSACTION_H
#define PAKFIRE_CLI_TRANSACTION_H

#include <pakfire/pakfire.h>
#include <pakfire/transaction.h>

typedef int (*cli_transaction_callback)
	(struct pakfire_transaction* transaction, int argc, char* argv[], void* data);

int cli_transaction(struct pakfire* pakfire, int argc, char* argv[], int flags,
	cli_transaction_callback callback, void* data);

#endif /* PAKFIRE_CLI_TRANSACTION_H */
