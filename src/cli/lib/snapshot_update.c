/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include <pakfire/pakfire.h>

#include "command.h"
#include "pakfire.h"
#include "snapshot_update.h"

static const char* doc = "Update the snapshot";

int cli_snapshot_update(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct pakfire* pakfire = NULL;
	int r;

	// Parse the command line
	r = cli_parse(NULL, NULL, NULL, doc, NULL, 0, argc, argv, NULL);
	if (r)
		goto ERROR;

	// Setup pakfire
	r = cli_setup_pakfire(&pakfire, global_args);
	if (r)
		goto ERROR;

	// Make a new snapshot
	r = pakfire_update_snapshot(pakfire);

ERROR:
	if (pakfire)
		pakfire_unref(pakfire);

	return r;
}
