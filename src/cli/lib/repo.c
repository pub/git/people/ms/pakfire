/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include "command.h"
#include "repo.h"
#include "repo_compose.h"
#include "repo_create.h"
#include "repo_delete.h"
#include "repo_list.h"
#include "repo_show.h"

int cli_repo(void* data, int argc, char* argv[]) {
	static const struct command commands[] = {
		{ "compose", cli_repo_compose, 1, -1, 0 },
		{ NULL },
	};

	return cli_parse(NULL, commands, NULL, NULL, NULL, 0, argc, argv, data);
}

int cli_repo_client(void* data, int argc, char* argv[]) {
	static const struct command commands[] = {
		{ "create", cli_repo_create, 2, -1, 0 },
		{ "delete", cli_repo_delete, 2,  2, 0 },
		{ "list",   cli_repo_list,   1,  1, 0 },
		{ "show",   cli_repo_show,   2,  2, 0 },
		{ NULL },
	};

	return cli_parse(NULL, commands, NULL, NULL, NULL, 0, argc, argv, data);
}
