/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_CLI_COMMAND_H
#define PAKFIRE_CLI_COMMAND_H

#include <argp.h>

typedef error_t (*command_parse)(int key, char* arg, struct argp_state* state, void* data);

typedef enum cli_command_flags {
	CLI_REQUIRE_ROOT = (1 << 0),
} cli_command_flags_t;

struct command {
	const char* verb;
	int (*callback)(void* config, int argc, char* argv[]);
	int min_args;
	int max_args;
	enum {
		CLI_REQUIRE_ONE_ARGUMENT          = (1 << 0),
		CLI_REQUIRE_ONE_OR_MORE_ARGUMENTS = (1 << 1),
	} flags;
};

int cli_parse(const struct argp_option* options, const struct command* commands,
	const char* args_doc, const char* doc, command_parse parse, int flags,
	int argc, char** argv, void* data);

#endif /* PAKFIRE_CLI_COMMAND_H */
