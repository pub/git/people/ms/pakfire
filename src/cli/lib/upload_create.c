/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include <pakfire/buildservice.h>

#include "command.h"
#include "pakfire.h"
#include "upload_create.h"

static const char* args_doc = "FILES...";

static const char* doc = "Uploads a file";

#define MAX_FILES 32

struct cli_local_args {
	const char* files[MAX_FILES];
	unsigned int num_files;
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case ARGP_KEY_ARG:
			if (args->num_files >= MAX_FILES)
				return -ENOBUFS;

			args->files[args->num_files++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

int cli_upload_create(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {};
	struct pakfire_buildservice* service = NULL;
	char* uuid = NULL;
	int r;

	// Parse the command line
	r = cli_parse(NULL, NULL, args_doc, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Connect to the build service
	r = cli_setup_buildservice(&service, global_args);
	if (r < 0)
		goto ERROR;

	// List uploads
	for (unsigned int i = 0; i < local_args.num_files; i++) {
		r = pakfire_buildservice_upload(service, local_args.files[i], NULL, &uuid);
		if (r)
			goto ERROR;

		if (uuid) {
			printf("Uploaded %s as %s\n", local_args.files[i], uuid);
			free(uuid);
		}
	}

ERROR:
	if (service)
		pakfire_buildservice_unref(service);

	return r;
}
