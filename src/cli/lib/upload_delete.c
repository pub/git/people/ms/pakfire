/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include <pakfire/buildservice.h>
#include <pakfire/util.h>

#include "command.h"
#include "pakfire.h"
#include "upload_delete.h"

static const char* args_doc = "FILES...";

static const char* doc = "Deletes an uploaded file";

#define MAX_UPLOADS 32

struct cli_local_args {
	const char* uploads[MAX_UPLOADS];
	unsigned int num_uploads;
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case ARGP_KEY_ARG:
			if (args->num_uploads >= MAX_UPLOADS)
				return -ENOBUFS;

			// Validate the UUID
			if (!pakfire_uuid_is_valid(arg))
				argp_error(state, "Invalid UUID");

			args->uploads[args->num_uploads++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

int cli_upload_delete(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {};
	struct pakfire_buildservice* service = NULL;
	int r;

	// Parse the command line
	r = cli_parse(NULL, NULL, args_doc, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Connect to the build service
	r = cli_setup_buildservice(&service, global_args);
	if (r < 0)
		goto ERROR;

	// Delete uploads
	for (unsigned int i = 0; i < local_args.num_uploads; i++) {
		r = pakfire_buildservice_delete_upload(service, local_args.uploads[i]);
		if (r)
			goto ERROR;
	}

ERROR:
	if (service)
		pakfire_buildservice_unref(service);

	return r;
}
