/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include "command.h"
#include "snapshot.h"
#include "snapshot_update.h"

int cli_snapshot(void* data, int argc, char* argv[]) {
	static const struct command commands[] = {
		{ "update", cli_snapshot_update, 0, 0, 0 },
		{ NULL },
	};

	return cli_parse(NULL, commands, NULL, NULL, NULL, 0, argc, argv, data);
}
