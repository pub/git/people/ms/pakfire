/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <pakfire/i18n.h>
#include <pakfire/pakfire.h>
#include <pakfire/problem.h>
#include <pakfire/solution.h>

#include "terminal.h"

// Store terminal dimensions globally
static struct cli_term_dimensions {
	int rows;
	int cols;
	int __signal_registered;
} dimensions;

static void cli_term_update_dimensions(int signum) {
	struct winsize w;
	const int fd = STDOUT_FILENO;

	// Set defaults
	dimensions.rows = 80;
	dimensions.cols = 20;

	// Check if output file is a TTY
	int r = isatty(fd);
	if (r != 1)
		return;

	// Fetch the window size
	r = ioctl(fd, TIOCGWINSZ, &w);
	if (r)
		return;

	// Save result
	dimensions.rows = w.ws_row;
	dimensions.cols = w.ws_col;
}

int cli_term_get_dimensions(int* rows, int* cols) {
	// Update data if not done, yet
	if (!dimensions.rows || !dimensions.cols)
		cli_term_update_dimensions(SIGWINCH);

	// Register signal handler to update terminal size
	if (!dimensions.__signal_registered) {
		signal(SIGWINCH, cli_term_update_dimensions);

		// Store that we have registered the signal
		dimensions.__signal_registered = 1;
	}

	// Copy the dimensions
	*rows = dimensions.rows;
	*cols = dimensions.cols;

	return 0;
}

int cli_term_is_interactive(void) {
	return isatty(STDIN_FILENO) && isatty(STDOUT_FILENO) && isatty(STDERR_FILENO);
}

int cli_term_confirm(struct pakfire_ctx* ctx, struct pakfire* pakfire,
		void* data, const char* message, const char* question) {
	char* line = NULL;
	size_t length = 0;
	int r = 1;

	// Show the message if any
	if (message)
		printf("%s\n", message);

	for (;;) {
		// Print question
		printf("%s ", question);

		// Do not wait for any input if the terminal isn't interactive
		if (!cli_term_is_interactive())
			break;

		// Wait for the user to enter something
		ssize_t bytes_read = getline(&line, &length, stdin);
		if (bytes_read < 0) {
			r = bytes_read;
			goto END;

		// If we have read more than one character + newline, we will ask again
		} else if (bytes_read > 2)
			continue;

		// Check the response
		switch (*line) {
			// Yes!
			case 'Y':
			case 'y':
				r = 0;
				goto END;

			// No!
			case 'N':
			case 'n':
				r = 1;
				goto END;

			// Ask again for any other inputs
			default:
				continue;
		}
	}

END:
	if (line)
		free(line);

	return r;
}

int cli_term_confirm_yes(struct pakfire_ctx* ctx, struct pakfire* pakfire,
		void* data, const char* message, const char* question) {
	return 0;
}

static int cli_term_enter_number(const char* question, unsigned int* choice,
		unsigned int min, unsigned int max) {
	char* line = NULL;
	size_t length = 0;
	char* remainder = NULL;
	int r = 1;

	while (1) {
		// Print question
		printf("%s ", question);

		// Do not wait for any input if the terminal isn't interactive
		if (!cli_term_is_interactive())
			break;

		// Wait for the user to enter something
		ssize_t bytes_read = getline(&line, &length, stdin);
		if (bytes_read < 0)
			goto ERROR;

		// Convert input into an integer
		unsigned long int value = strtoul(line, &remainder, 10);

		// The remainder must point to newline
		if (!remainder || *remainder != '\n')
			goto AGAIN;

		// The value must be within bounds
		if (value < min || value > max)
			goto AGAIN;

		// Store choice
		*choice = value;
		r = 0;
		break;

AGAIN:
		printf("%s\n", _("Invalid value"));
	}

ERROR:
	if (line)
		free(line);

	return r;
}

static int cli_term_print_problem(struct pakfire_problem* problem, unsigned int* num_solutions) {
	struct pakfire_solution** solutions = NULL;

	// Show what the problem is
	printf("  * %s\n", pakfire_problem_to_string(problem));

	// Fetch the solutions
	solutions = pakfire_problem_get_solutions(problem);

	if (solutions) {
		// Show a little headline
		printf("    %s\n", _("Possible solutions:"));

		// Show all possible solutions
		for (struct pakfire_solution** s = solutions; *s; s++) {
			// Increment the solution counter
			(*num_solutions)++;

			// Show the solution
			printf("      [%u] %s\n", *num_solutions, pakfire_solution_to_string(*s));
		}
	}

	// Cleanup
	if (solutions) {
		for (struct pakfire_solution** s = solutions; *s; s++)
			pakfire_solution_unref(*s);

		free(solutions);
	}

	return 0;
}

static struct pakfire_solution* cli_term_find_solution(
		struct pakfire_problem** problems, const unsigned int choice) {
	struct pakfire_solution* selected_solution = NULL;
	struct pakfire_solution** solutions = NULL;

	// Count solutions
	unsigned int i = 1;

	for (struct pakfire_problem** problem = problems; *problem; problem++) {
		// Fetch solutions
		solutions = pakfire_problem_get_solutions(*problem);
		if (!solutions)
			continue;

		for (struct pakfire_solution** solution = solutions; *solution; solution++) {
			if (choice == i++)
				selected_solution = pakfire_solution_ref(*solution);

			pakfire_solution_unref(*solution);
		}
		free(solutions);

		// If we found a solution, we can skip the rest
		if (selected_solution)
			break;
	}

	return selected_solution;
}

int cli_term_pick_solution(struct pakfire_ctx* ctx, struct pakfire* pakfire, void* data,
		struct pakfire_transaction* transaction) {
	struct pakfire_problem** problems = NULL;
	struct pakfire_solution* solution = NULL;
	unsigned int num_solutions = 0;
	unsigned int choice = 0;
	int r = 0;

	// Fetch all problems
	problems = pakfire_transaction_get_problems(transaction);
	if (!problems)
		goto ERROR;

	for (;;) {
		// Reset solutions
		num_solutions = 0;

		// Print a headline
		printf("%s\n", _("One or more problems have occurred solving your request:"));

		// Show all problems
		for (struct pakfire_problem** p = problems; *p; p++) {
			r = cli_term_print_problem(*p, &num_solutions);
			if (r)
				goto ERROR;
		}

		// Let the user choose which solution they want
		r = cli_term_enter_number(
			_("Please select a solution:"), &choice, 1, num_solutions);
		if (r)
			goto ERROR;

		// Find the selected solution
		solution = cli_term_find_solution(problems, choice);
		if (!solution)
			goto ERROR;

		// Take the selected solution
		r = pakfire_transaction_take_solution(transaction, solution);
		if (r)
			goto ERROR;

		// Success!
		r = 0;
		break;
	}

ERROR:
	if (problems) {
		for (struct pakfire_problem** p = problems; *p; p++)
			pakfire_problem_unref(*p);

		free(problems);
	}
	if (solution)
		pakfire_solution_unref(solution);

	return r;
}
