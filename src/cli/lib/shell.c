/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>

#include "command.h"
#include "pakfire.h"
#include "shell.h"

#include <pakfire/build.h>
#include <pakfire/pakfire.h>

#define MAX_ARGS     128
#define MAX_PACKAGES 128

struct cli_local_args {
	enum {
		SHELL_ENABLE_SNAPSHOT = (1 << 0),
	} flags;

	// Arguments
	const char* argv[MAX_ARGS + 1];
	unsigned int argc;

	// Packages
	const char* packages[MAX_PACKAGES + 1];
	unsigned int num_packages;
};

static const char* doc =
	"Creates a build environment and launches an interactive shell in it";

enum {
	OPT_DISABLE_SNAPSHOT = 1,
	OPT_INSTALL          = 2,
};

static struct argp_option options[] = {
	{ "disable-snapshot", OPT_DISABLE_SNAPSHOT,      NULL, 0, "Do not use the snapshot", 0 },
	{ "install",          OPT_INSTALL,          "PACKAGE", 0, "Install this package", 0 },
	{ NULL },
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case OPT_DISABLE_SNAPSHOT:
			args->flags &= ~SHELL_ENABLE_SNAPSHOT;
			break;

		case OPT_INSTALL:
			if (args->num_packages >= MAX_PACKAGES)
				return -ENOBUFS;

			args->packages[args->num_packages++] = arg;
			break;

		case ARGP_KEY_ARG:
			if (args->argc >= MAX_ARGS)
				return -ENOBUFS;

			args->argv[args->argc++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

int cli_shell(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {
		.flags        = SHELL_ENABLE_SNAPSHOT,
		.argv         = {},
		.argc         = 0,
		.packages     = {},
		.num_packages = 0,
	};
	struct pakfire_build* build = NULL;
	int flags = PAKFIRE_BUILD_LOCAL | PAKFIRE_BUILD_INTERACTIVE;
	int r;

	// Parse the command line
	r = cli_parse(options, NULL, NULL, doc, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Enable snapshots?
	if (local_args.flags & SHELL_ENABLE_SNAPSHOT)
		flags |= PAKFIRE_BUILD_ENABLE_SNAPSHOT;

	// Setup the build environment
	r = cli_setup_build(&build, global_args, flags);
	if (r < 0)
		goto ERROR;

	// Run the command
	r = pakfire_build_shell(build,
		(local_args.argc) ? local_args.argv : NULL, local_args.packages);

ERROR:
	if (build)
		pakfire_build_unref(build);

	return r;
}
