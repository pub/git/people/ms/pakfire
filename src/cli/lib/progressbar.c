/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>
#include <sys/time.h>

#include <pakfire/i18n.h>
#include <pakfire/pakfire.h>
#include <pakfire/progress.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#include "progressbar.h"
#include "terminal.h"

#define DRAWS_PER_SECOND 20

static const struct timespec TIMER = {
	.tv_sec  = 0,
	.tv_nsec = 1000000000 / DRAWS_PER_SECOND,
};

struct cli_progressbar;
struct cli_progressbar_widget;

typedef ssize_t (*print_callback)(struct cli_progressbar* p,
	struct cli_progressbar_widget* widget, unsigned int width, void* data);
typedef void (*free_callback)(void* data);

struct cli_progressbar_widget {
	STAILQ_ENTRY(cli_progressbar_widget) nodes;

	int expandable;
	void* data;

	// Callbacks
	print_callback print;
	free_callback free;

	char buffer[LINE_MAX];
};

struct cli_progressbar {
	struct pakfire_progress* progress;

	// Output
	FILE* f;

	// Reference to the thread that is drawing the progress bar
	pthread_t renderer;
	volatile unsigned int is_running;

	// Widgets
	STAILQ_HEAD(widgets, cli_progressbar_widget) widgets;
	unsigned int num_widgets;
};

static int cli_progressbar_create(struct cli_progressbar** progressbar,
		struct pakfire_progress* progress, FILE* f) {
	struct cli_progressbar* p = NULL;

	// Allocate some memory
	p = calloc(1, sizeof(*p));
	if (!p)
		return -errno;

	// Keep a reference to progress
	p->progress = progress;

	// Default writing to stdout
	if (!f)
		f = stdout;

	// Where to write to?
	p->f = f;

	// Setup widgets
	STAILQ_INIT(&p->widgets);

	// Done
	*progressbar = p;

	return 0;
}

static void cli_progressbar_widget_free(struct cli_progressbar_widget* widget) {
	// Call own free method
	if (widget->free && widget->data)
		widget->free(widget->data);

	free(widget);
}

static void cli_progressbar_free_widgets(struct cli_progressbar* p) {
	struct cli_progressbar_widget* widget = NULL;

	// Free widgets
	while (!STAILQ_EMPTY(&p->widgets)) {
		widget = STAILQ_FIRST(&p->widgets);
		STAILQ_REMOVE_HEAD(&p->widgets, nodes);

		cli_progressbar_widget_free(widget);
	}
}

static void cli_progressbar_free(struct cli_progressbar* p) {
	cli_progressbar_free_widgets(p);
	free(p);
}

static void __cli_progressbar_free(struct pakfire_ctx* ctx,
		struct pakfire_progress* progress, void* data) {
	struct cli_progressbar* progressbar = data;

	cli_progressbar_free(progressbar);
}

static int cli_progressbar_draw(struct cli_progressbar* p) {
	struct cli_progressbar_widget* widget = NULL;
	int rows;
	int cols;
	int r;

	// Fetch terminal size
	r = cli_term_get_dimensions(&rows, &cols);
	if (r)
		return r;

	int cols_left = cols - p->num_widgets - 2;

	// Count how many widgets we have processed
	unsigned int widgets = 0;

	// Process all non-expandable widgets in the first pass
	STAILQ_FOREACH(widget, &p->widgets, nodes) {
		if (!widget->expandable) {
			r = widget->print(p, widget, 0, widget->data);
			if (r < 0)
				return r;

			// Take away used space
			cols_left -= strlen(widget->buffer);

			// Count the widget
			widgets++;
		}
	}

	// Print the expendable stuff only if there is space left
	if (cols_left > 0) {
		// How many expandable widgets are left?
		int num_expandables = p->num_widgets - widgets;

		// How much space do we allocate to each of them?
		int width = cols_left / num_expandables;

		// Process all expandable widgets
		STAILQ_FOREACH(widget, &p->widgets, nodes) {
			if (widget->expandable) {
				r = widget->print(p, widget, width, widget->data);
				if (r < 0)
					return r;
			}
		}
	}

	// Reset the line
	fputs("\r", p->f);

	// Print all buffers
	STAILQ_FOREACH(widget, &p->widgets, nodes) {
		if (!*widget->buffer)
			continue;

		fputs(" ", p->f);
		fputs(widget->buffer, p->f);
	}

	// Flush everything
	fflush(p->f);

	return 0;
}

static void* cli_progressbar_renderer(void* data) {
	struct cli_progressbar* progressbar = data;
	int r;

	do {
		// Redraw the progress bar
		r = cli_progressbar_draw(progressbar);
		if (r)
			goto ERROR;

		// Sleep for a short moment
		nanosleep(&TIMER, NULL);

	// Keep doing this for as long as we are running
	} while (progressbar->is_running);

	// Perform a final draw
	r = cli_progressbar_draw(progressbar);
	if (r)
		goto ERROR;

	// Finish line
	r = fputs("\n", progressbar->f);
	if (r <= 0 || r == EOF) {
		r = 1;
		goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	return (void *)(intptr_t)r;
}

static int cli_progressbar_start(struct pakfire_ctx* ctx, struct pakfire_progress* progress,
		void* data, unsigned long int max_value) {
	struct cli_progressbar* progressbar = data;

	// Set as running
	progressbar->is_running = 1;

	// Launch the renderer thread
	return pthread_create(&progressbar->renderer, NULL, cli_progressbar_renderer, progressbar);
}

static int cli_progressbar_finish(struct pakfire_ctx* ctx, struct pakfire_progress* progress,
		void* data) {
	struct cli_progressbar* progressbar = data;
	void* retval = NULL;
	int r;

	// We are no longer running
	progressbar->is_running = 0;

	// Wait until the render thread is done
	if (progressbar->renderer) {
		r = pthread_join(progressbar->renderer, &retval);
		if (r)
			return r;
	}

	return (intptr_t)retval;
}

static int cli_progressbar_add_widget(struct cli_progressbar* p,
		print_callback print, free_callback free, int expandable, void* data) {
	// Allocate the widget
	struct cli_progressbar_widget* widget = calloc(1, sizeof(*widget));
	if (!widget)
		return -ENOMEM;

	// Assign everything
	widget->print = print;
	widget->free = free;
	widget->expandable = expandable;
	widget->data = data;

	// Append it to the list
	STAILQ_INSERT_TAIL(&p->widgets, widget, nodes);
	p->num_widgets++;

	return 0;
}

static ssize_t cli_progressbar_title(struct cli_progressbar* p,
		struct cli_progressbar_widget* widget, unsigned int width, void* data) {
	const char* title = NULL;
	int r;

	title = pakfire_progress_get_title(p->progress);
	if (!title)
		return 0;

	r = pakfire_string_format(widget->buffer, "%s", title);
	if (r < 0)
		return r;

	return strlen(widget->buffer);
}

static int cli_progressbar_add_title(struct cli_progressbar* p) {
	return cli_progressbar_add_widget(p, cli_progressbar_title, NULL, 0, NULL);
}

static ssize_t cli_progressbar_counter(struct cli_progressbar* p,
		struct cli_progressbar_widget* widget, unsigned int width, void* data) {
	int r;

	// Fetch progress
	unsigned long int value = pakfire_progress_get_value(p->progress);
	unsigned long int max_value = pakfire_progress_get_value(p->progress);

	// Format the result
	r = pakfire_string_format(widget->buffer, "%lu/%lu", value, max_value);
	if (r < 0)
		return -errno;

	return strlen(widget->buffer);
}

static int cli_progressbar_add_counter(struct cli_progressbar* p) {
	return cli_progressbar_add_widget(p, cli_progressbar_counter, NULL, 0, NULL);
}

static ssize_t cli_progressbar_percentage(struct cli_progressbar* p,
		struct cli_progressbar_widget* widget, unsigned int width, void* data) {
	int r;

	// Fetch percentage
	double percentage = pakfire_progress_get_percentage(p->progress);

	// Format to string
	r = pakfire_string_format(widget->buffer, "%3.0f%%", percentage);
	if (r < 0)
		return -errno;

	return strlen(widget->buffer);
}

static int cli_progressbar_add_percentage(struct cli_progressbar* p) {
	return cli_progressbar_add_widget(p, cli_progressbar_percentage, NULL, 0, NULL);
}

static ssize_t cli_progressbar_bar(struct cli_progressbar* p,
		struct cli_progressbar_widget* widget, unsigned int width, void* data) {
	// This only works if we have at least one byte to fill
	if (!width)
		return -ENOBUFS;

	// Cap the widths to the buffer
	if (width >= sizeof(widget->buffer))
		width = sizeof(widget->buffer) - 1;

	// Remove the bar when we are finished so that the terminal is not so cluttered
	if (!p->is_running) {
		for (unsigned int i = 0; i < width; i++)
			widget->buffer[i] = ' ';
		widget->buffer[width] = '\0';

		return width;
	}

	unsigned int fill = pakfire_progress_get_percentage(p->progress) * (width - 2) / 100;

	// Write brackets
	widget->buffer[0] = '[';
	widget->buffer[width-1] = ']';

	// Write bar
	for (unsigned int i = 1; i < width - 1; i++) {
		if (i <= fill)
			widget->buffer[i] = '#';
		else
			widget->buffer[i] = '-';
	}

	// Terminate the string
	widget->buffer[width] = '\0';

	return width;
}

static int cli_progressbar_add_bar(struct cli_progressbar* p) {
	return cli_progressbar_add_widget(p, cli_progressbar_bar, NULL, 1, NULL);
}

static ssize_t cli_progressbar_elapsed_time(struct cli_progressbar* p,
		struct cli_progressbar_widget* widget, unsigned int width, void* data) {
	int r;

	// Fetch the elapsed time
	time_t t = pakfire_progress_get_elapsed_time(p->progress);
	if (t < 0)
		return -errno;

	// Format the time
	r = pakfire_string_format(widget->buffer, "%02ld:%02ld", t / 60, t % 60);
	if (r < 0)
		return -errno;

	return strlen(widget->buffer);
}

static int cli_progressbar_add_elapsed_time(struct cli_progressbar* p) {
	return cli_progressbar_add_widget(p, cli_progressbar_elapsed_time, NULL, 0, NULL);
}

static ssize_t cli_progressbar_eta(struct cli_progressbar* p,
		struct cli_progressbar_widget* widget, unsigned int width, void* data) {
	int r;

	// Show total time when finished
	if (!p->is_running)
		return cli_progressbar_elapsed_time(p, widget, width, data);

	// Fetch the ETA
	time_t t = pakfire_progress_get_eta(p->progress);

	// Print a placeholder when we have no ETA (yet)
	if (t <= 0) {
		r = pakfire_string_format(widget->buffer, "%-5s: --:--:--", _("ETA"));
		if (r < 0)
			return -errno;

	// Otherwise show the ETA
	} else {
		r = pakfire_string_format(widget->buffer, "%-5s: %02ld:%02ld", _("ETA"), t / 60, t % 60);
		if (r < 0)
			return -errno;
	}

	return strlen(widget->buffer);
}

static int cli_progressbar_add_eta(struct cli_progressbar* p) {
	return cli_progressbar_add_widget(p, cli_progressbar_eta, NULL, 0, NULL);
}

static ssize_t cli_progressbar_bytes_transferred(struct cli_progressbar* p,
		struct cli_progressbar_widget* widget, unsigned int width, void* data) {
	char buffer[16];
	int r;

	// Fetch value
	unsigned long int value = pakfire_progress_get_value(p->progress);

	// Format the size
	r = pakfire_format_size(buffer, value);
	if (r < 0)
		return r;

	// Add padding so that the string is always at least five characters long
	r = pakfire_string_format(widget->buffer, "%-5s", buffer);
	if (r < 0)
		return -errno;

	return strlen(widget->buffer);
}

static int cli_progressbar_add_bytes_transferred(struct cli_progressbar* p) {
	return cli_progressbar_add_widget(p, cli_progressbar_bytes_transferred, NULL, 0, NULL);
}

static ssize_t cli_progressbar_transfer_speed(struct cli_progressbar* p,
		struct cli_progressbar_widget* widget, unsigned int width, void* data) {
	int r;

	// Fetch the speed
	double speed = pakfire_progress_get_transfer_speed(p->progress);
	if (speed < 0)
		return -errno;

	// Format the speed
	r = pakfire_format_speed(widget->buffer, speed);
	if (r < 0)
		return r;

	return r;
}

static int cli_progressbar_add_transfer_speed(struct cli_progressbar* p) {
	return cli_progressbar_add_widget(p, cli_progressbar_transfer_speed, NULL, 0, NULL);
}

int cli_setup_progressbar(struct pakfire_ctx* ctx, void* data, struct pakfire_progress* p) {
	struct cli_progressbar* progressbar = NULL;
	int r;

	// Allocate a new progressbar
	r = cli_progressbar_create(&progressbar, p, NULL);
	if (r)
		return r;

	// Set callback data
	pakfire_progress_set_callback_data(p, progressbar);

	// Set start callback
	pakfire_progress_set_start_callback(p, cli_progressbar_start);

	// Set finish callback
	pakfire_progress_set_finish_callback(p, cli_progressbar_finish);

	// Set free callback
	pakfire_progress_set_free_callback(p, __cli_progressbar_free);

	// Add the title
	r = cli_progressbar_add_title(progressbar);
	if (r)
		goto ERROR;

	// Show the bar
	r = cli_progressbar_add_bar(progressbar);
	if (r)
		goto ERROR;

	// Show bytes transferred
	if (pakfire_progress_has_flag(p, PAKFIRE_PROGRESS_SHOW_BYTES_TRANSFERRED)) {
		r = cli_progressbar_add_bytes_transferred(progressbar);
		if (r)
			goto ERROR;
	}

	// Show transfer speed
	if (pakfire_progress_has_flag(p, PAKFIRE_PROGRESS_SHOW_TRANSFER_SPEED)) {
		r = cli_progressbar_add_transfer_speed(progressbar);
		if (r)
			goto ERROR;
	}

	// Show elapsed time
	if (pakfire_progress_has_flag(p, PAKFIRE_PROGRESS_SHOW_ELAPSED_TIME)) {
		r = cli_progressbar_add_elapsed_time(progressbar);
		if (r)
			goto ERROR;
	}

	// Show ETA
	if (pakfire_progress_has_flag(p, PAKFIRE_PROGRESS_SHOW_ETA)) {
		r = cli_progressbar_add_eta(progressbar);
		if (r)
			goto ERROR;
	}

	// Show counter
	if (pakfire_progress_has_flag(p, PAKFIRE_PROGRESS_SHOW_COUNTER)) {
		r = cli_progressbar_add_counter(progressbar);
		if (r)
			goto ERROR;
	}

	// Show percentage
	if (pakfire_progress_has_flag(p, PAKFIRE_PROGRESS_SHOW_PERCENTAGE)) {
		r = cli_progressbar_add_percentage(progressbar);
		if (r)
			goto ERROR;
	}

	return 0;

ERROR:
	if (progressbar)
		cli_progressbar_free(progressbar);

	return r;
}
