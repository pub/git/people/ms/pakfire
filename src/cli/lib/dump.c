/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include <json.h>

#include <pakfire/package.h>
#include <pakfire/packagelist.h>
#include <pakfire/pakfire.h>
#include <pakfire/repo.h>
#include <pakfire/repolist.h>

#include "dump.h"

int cli_dump_package(struct pakfire_package* package, int flags) {
	char* p = NULL;

	// Dump the package information
	p = pakfire_package_dump(package, flags);
	if (!p)
		return -errno;

	// Print it to the console
	printf("%s\n", p);
	free(p);

	return 0;
}

static int __cli_dump_package(struct pakfire_ctx* ctx, struct pakfire_package* package, void* p) {
	int flags = *(int*)p;

#if 0
	// Dump extra information when in build mode
	// XXX We don't have access to Pakfire any more
	if (pakfire_has_flag(pakfire, PAKFIRE_FLAGS_BUILD))
#endif
		flags |= PAKFIRE_PKG_DUMP_LONG;

	return cli_dump_package(package, flags);
}

int cli_dump_packagelist(struct pakfire_packagelist* list, int flags) {
	return pakfire_packagelist_walk(list, __cli_dump_package, &flags, 0);
}

int cli_dump_repolist(struct pakfire_repolist* list, int flags) {
	struct pakfire_repo* repo = NULL;
	int r;

	if (!list)
		return 0;

	// Print the header
	r = printf(" %-20s %8s %12s %12s \n", "Repository", "Enabled", "Priority", "Packages");
	if (r < 0)
		goto ERROR;

	size_t length = pakfire_repolist_size(list);

	for (unsigned int i = 0; i < length; i++) {
		repo = pakfire_repolist_get(list, i);
		if (!repo)
			break;

		r = printf(" %-20s %8s %12d %12d \n",
			pakfire_repo_get_name(repo),
			pakfire_repo_get_enabled(repo) ? "Yes" : "No",
			pakfire_repo_get_priority(repo),
			pakfire_repo_count(repo));
		pakfire_repo_unref(repo);
		if (r < 0)
			goto ERROR;
	}

ERROR:
	return r;
}

int cli_dump_json(struct json_object* object) {
	int r;

	// Serialize the object as a JSON string
	const char* buffer = json_object_to_json_string_ext(object,
		JSON_C_TO_STRING_PRETTY|JSON_C_TO_STRING_PRETTY_TAB);

	// Print to the console
	r = printf("%s\n", buffer);
	if (r < 0)
		return r;

	return 0;
}
