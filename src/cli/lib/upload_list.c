/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <json.h>

#include <pakfire/buildservice.h>

#include "command.h"
#include "dump.h"
#include "pakfire.h"
#include "upload_list.h"

static const char* doc = "Lists all uploads";

int cli_upload_list(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct pakfire_buildservice* service = NULL;
	struct json_object* uploads = NULL;
	int r;

	// Parse the command line
	r = cli_parse(NULL, NULL, NULL, doc, NULL, 0, argc, argv, NULL);
	if (r)
		goto ERROR;

	// Connect to the build service
	r = cli_setup_buildservice(&service, global_args);
	if (r < 0)
		goto ERROR;

	// List uploads
	r = pakfire_buildservice_list_uploads(service, &uploads);
	if (r)
		goto ERROR;

	// Dump everything
	r = cli_dump_json(uploads);
	if (r)
		goto ERROR;

ERROR:
	if (service)
		pakfire_buildservice_unref(service);
	if (uploads)
		json_object_put(uploads);

	return r;
}
