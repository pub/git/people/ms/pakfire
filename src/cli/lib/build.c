/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/syslog.h>

#include "build.h"
#include "color.h"
#include "command.h"
#include "pakfire.h"

#include <pakfire/archive.h>
#include <pakfire/build.h>
#include <pakfire/log_file.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/path.h>
#include <pakfire/repo.h>

#define MAX_MAKEFILES 32

struct cli_local_args {
	const char* distro;
	const char* target;
	enum {
		BUILD_INTERACTIVE     = (1 << 0),
		BUILD_ENABLE_CCACHE   = (1 << 1),
		BUILD_ENABLE_SNAPSHOT = (1 << 2),
		BUILD_ENABLE_TESTS    = (1 << 3),
	} flags;

	// Log file
	struct pakfire_log_file* log_file;
	const char* log_path;

	// Makefiles
	char* makefiles[MAX_MAKEFILES];
	unsigned int num_makefiles;
};

enum {
	OPT_DISABLE_CCACHE   = 1,
	OPT_DISABLE_SNAPSHOT = 2,
	OPT_DISABLE_TESTS    = 3,
	OPT_LOG_FILE         = 4,
	OPT_NON_INTERACTIVE  = 5,
	OPT_TARGET           = 6,
};

static struct argp_option options[] = {
	{ "disable-ccache",   OPT_DISABLE_CCACHE,      NULL, 0, "Disable the ccache",     0 },
	{ "disable-snapshot", OPT_DISABLE_SNAPSHOT,    NULL, 0, "Do not use the snapshot", 0 },
	{ "disable-tests",    OPT_DISABLE_TESTS,       NULL, 0, "Do not run tests",        0 },
	{ "log-file",         OPT_LOG_FILE,          "PATH", 0, "Writes the log to a file", 0 },
	{ "non-interactive",  OPT_NON_INTERACTIVE,     NULL, 0, "Run the build non-interactively", 0 },
	{ "target",           OPT_TARGET,          "TARGET", 0, "Output all packages into this directory", 0 },
	{ NULL },
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_local_args* args = data;

	switch (key) {
		case OPT_DISABLE_CCACHE:
			args->flags &= ~BUILD_ENABLE_CCACHE;
			break;

		case OPT_DISABLE_SNAPSHOT:
			args->flags &= ~BUILD_ENABLE_SNAPSHOT;
			break;

		case OPT_DISABLE_TESTS:
			args->flags &= ~BUILD_ENABLE_TESTS;
			break;

		case OPT_LOG_FILE:
			args->log_path = arg;
			break;

		case OPT_NON_INTERACTIVE:
			args->flags &= ~BUILD_INTERACTIVE;
			break;

		case OPT_TARGET:
			args->target = arg;
			break;

		case ARGP_KEY_ARG:
			if (args->num_makefiles >= MAX_MAKEFILES)
				return -ENOBUFS;

			args->makefiles[args->num_makefiles++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

static void log_callback(void* data, int priority, const char* file, int line,
	const char* function, const char* format, va_list args)
	__attribute__((format(printf, 6, 0)));

static void log_callback(void* data, int priority, const char* file, int line,
		const char* function, const char* format, va_list args) {
	const struct cli_local_args* local_args = data;
	char* buffer = NULL;
	ssize_t length;

	// Format the line
	length = vasprintf(&buffer, format, args);
	if (length < 0)
		return;

	switch (priority) {
		// Highlight any warnings
		case LOG_WARNING:
			fputs(color_yellow(), stderr);
			fprintf(stderr, "%s", buffer);
			fputs(color_reset(), stderr);
			break;

		// Highlight any error messages
		case LOG_ERR:
			fputs(color_highlight(), stderr);
			fprintf(stderr, "%s", buffer);
			fputs(color_reset(), stderr);
			break;

		// Print anything to standard output
		default:
			fprintf(stdout, "%s", buffer);
			break;
	}

	// Write to the log file
	if (local_args->log_file)
		pakfire_log_file_write(local_args->log_file, priority, buffer, length);

	if (buffer)
		free(buffer);
}

static int result_callback(struct pakfire_ctx* ctx, struct pakfire* pakfire,
		struct pakfire_build* build, struct pakfire_archive* archive, void* data) {
	const struct cli_local_args* local_args = data;
	struct pakfire_package* pkg = NULL;
	struct pakfire_repo* local = NULL;
	char path[PATH_MAX];
	int r;

	// Fetch the package metadata
	r = pakfire_archive_make_package(archive, NULL, &pkg);
	if (r < 0)
		goto ERROR;

	// Fetch NEVRA
	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);

	// Fetch the local repository & import the archive
	local = pakfire_get_repo(pakfire, PAKFIRE_REPO_LOCAL);
	if (local) {
		r = pakfire_repo_import_archive(local, archive, NULL);
		if (r < 0) {
			ERROR(ctx, "Could not import %s to the local repository: %s\n",
					nevra, strerror(-r));
			goto ERROR;
		}
	}

	// Copy to the target (if given)
	if (local_args->target) {
		// Fetch the filename
		const char* filename = pakfire_package_get_filename(pkg);

		// Make the absolute target path
		r = pakfire_path_append(path, local_args->target, filename);
		if (r < 0)
			goto ERROR;

		// Copy (or link) the package to the target path
		r = pakfire_archive_link_or_copy(archive, path);
		if (r < 0) {
			ERROR(ctx, "Could not copy %s to %s: %s\n",
					nevra, path, strerror(-r));
			goto ERROR;
		}
	}

ERROR:
	if (local)
		pakfire_repo_unref(local);
	if (pkg)
		pakfire_package_unref(pkg);

	return r;
}

int cli_build(void* data, int argc, char* argv[]) {
	struct cli_global_args* global_args = data;
	struct cli_local_args local_args = {
		.flags  =
			BUILD_INTERACTIVE |
			BUILD_ENABLE_CCACHE |
			BUILD_ENABLE_SNAPSHOT |
			BUILD_ENABLE_TESTS,
	};
	struct pakfire_build* build = NULL;
	int flags = PAKFIRE_BUILD_LOCAL;
	int r;

	// Parse the command line
	r = cli_parse(options, NULL, NULL, NULL, parse, 0, argc, argv, &local_args);
	if (r)
		goto ERROR;

	// Replace the logger
	pakfire_ctx_set_log_callback(global_args->ctx, log_callback, &local_args);

	// Use the snapshot?
	if (local_args.flags & BUILD_ENABLE_SNAPSHOT)
		flags |= PAKFIRE_BUILD_ENABLE_SNAPSHOT;

	// Is the build interactive?
	if (local_args.flags & BUILD_INTERACTIVE)
		flags |= PAKFIRE_BUILD_INTERACTIVE;

	// Enable ccache?
	if (!(local_args.flags & BUILD_ENABLE_CCACHE))
		flags |= PAKFIRE_BUILD_DISABLE_CCACHE;

	// Enable tests?
	if (!(local_args.flags & BUILD_ENABLE_TESTS))
		flags |= PAKFIRE_BUILD_DISABLE_TESTS;

	// Create the log file
	if (local_args.log_path) {
		r = pakfire_log_file_create(&local_args.log_file,
				global_args->ctx, local_args.log_path, NULL, 0);
		if (r < 0)
			goto ERROR;
	}

	// Setup the build environment
	r = cli_setup_build(&build, global_args, flags);
	if (r < 0)
		goto ERROR;

	// Process all packages
	for (unsigned int i = 0; i < local_args.num_makefiles; i++) {
		// Run the build
		r = pakfire_build_exec(build, local_args.makefiles[i], result_callback, &local_args);
		if (r) {
			fprintf(stderr, "Could not build %s\n", local_args.makefiles[i]);
			goto ERROR;
		}
	}

ERROR:
	if (local_args.log_file)
		pakfire_log_file_unref(local_args.log_file);
	if (build)
		pakfire_build_unref(build);

	return r;
}
