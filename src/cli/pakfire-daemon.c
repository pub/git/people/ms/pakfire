/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>
#include <syslog.h>

#include <pakfire/ctx.h>
#include <pakfire/daemon.h>
#include <pakfire/logging.h>
#include <pakfire/proctitle.h>

#include "lib/command.h"
#include "lib/daemon.h"

const char* argp_program_version = PACKAGE_FULLVERSION;

static const char* doc = "The Pakfire Build Daemon";

enum {
	OPT_DEBUG  = 1,
};

static struct argp_option options[] = {
	{ "debug", OPT_DEBUG, NULL, 0, "Run in debug mode", 0 },
	{ NULL },
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct pakfire_ctx* ctx = data;

	switch (key) {
		case OPT_DEBUG:
			// Set the log level to DEBUG
			pakfire_ctx_set_log_level(ctx, LOG_DEBUG);

			// Write logs to the console
			pakfire_ctx_set_log_callback(ctx, pakfire_log_stderr, NULL);
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

int main(int argc, char* argv[]) {
	struct pakfire_ctx* ctx = NULL;
	int r;

	// Initialize changing the process title
	r = pakfire_proctitle_init(argc, argv);
	if (r < 0)
		goto ERROR;

	// Setup the context
	r = pakfire_ctx_create(&ctx, NULL);
	if (r)
		goto ERROR;

	// Parse the command line and run any commands
	r = cli_parse(options, NULL, NULL, doc, parse,
		CLI_REQUIRE_ROOT, argc, argv, ctx);
	if (r)
		goto ERROR;

	// Do the work...
	r = cli_daemon_main(ctx);

ERROR:
	if (ctx) {
		ctx = pakfire_ctx_unref(ctx);
		if (ctx) {
			fprintf(stderr, "Context was not freed\n");
			return 1;
		}
	}

	return r;
}
