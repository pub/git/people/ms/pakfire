/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <argp.h>
#include <syslog.h>

#include "lib/clean.h"
#include "lib/command.h"
#include "lib/info.h"
#include "lib/install.h"
#include "lib/pakfire.h"
#include "lib/progressbar.h"
#include "lib/provides.h"
#include "lib/remove.h"
#include "lib/repolist.h"
#include "lib/requires.h"
#include "lib/search.h"
#include "lib/sync.h"
#include "lib/terminal.h"
#include "lib/update.h"
#include "lib/version.h"

#define MAX_REPOS 16

const char* argp_program_version = PACKAGE_FULLVERSION;

static const char* args_doc =
	"install  PACKAGES...\n"
	"remove   PACKAGES...\n"
	"update   [PACKAGES...]\n"
	"sync\n"
	"search   PATTERNS...\n"
	"provides PATTERNS...\n"
	"requires PATTERNS...\n"
	"repolist\n"
	"clean";

enum {
	OPT_CONFIG       = 1,
	OPT_DEBUG        = 2,
	OPT_OFFLINE      = 3,
	OPT_ROOT         = 4,
	OPT_YES          = 5,

	OPT_ENABLE_REPO  = 6,
	OPT_DISABLE_REPO = 7,
};

static struct argp_option options[] = {
	{ "config",       OPT_CONFIG,       "FILE", 0, "Use this configuration file", 0 },
	{ "debug",        OPT_DEBUG,          NULL, 0, "Run in debug mode",           0 },
	{ "offline",      OPT_OFFLINE,        NULL, 0, "Run in offline mode", 0 },
	{ "root",         OPT_ROOT,         "PATH", 0, "The path to operate in", 0 },
	{ "enable-repo",  OPT_ENABLE_REPO,  "REPO", 0, "Enable a repository", 0 },
	{ "disable-repo", OPT_DISABLE_REPO, "REPO", 0, "Disable a repository", 0 },
	{ "yes",          OPT_YES,            NULL, 0, "Answer all questions with yes", 0 },
	{ NULL },
};

static const struct command commands[] = {
	{ "clean",    cli_clean,    0,  0, 0 },
	{ "info",     cli_info,     1, -1, 0 },
	{ "install",  cli_install,  1, -1, 0 },
	{ "provides", cli_provides, 1, -1, 0 },
	{ "remove",   cli_remove,   1, -1, 0 },
	{ "repolist", cli_repolist, 0,  0, 0 },
	{ "requires", cli_requires, 1, -1, 0 },
	{ "search",   cli_search,   1, -1, 0 },
	{ "sync",     cli_sync,     0,  0, 0 },
	{ "update",   cli_update,   0, -1, 0 },
	{ NULL },
};

static error_t parse(int key, char* arg, struct argp_state* state, void* data) {
	struct cli_global_args* args = data;

	switch (key) {
		case OPT_CONFIG:
			args->config = arg;
			break;

		case OPT_DEBUG:
			pakfire_ctx_set_log_level(args->ctx, LOG_DEBUG);
			break;

		case OPT_OFFLINE:
			pakfire_ctx_set_flag(args->ctx, PAKFIRE_CTX_OFFLINE);
			break;

		case OPT_ROOT:
			args->root = arg;
			break;

		case OPT_YES:
			pakfire_ctx_set_confirm_callback(args->ctx, cli_term_confirm_yes, NULL);
			break;

		// Enable/Disable Repositories

		case OPT_ENABLE_REPO:
			if (args->num_enable_repos >= MAX_REPOS)
				return -ENOBUFS;

			args->enable_repos[args->num_enable_repos++] = arg;
			break;

		case OPT_DISABLE_REPO:
			if (args->num_disable_repos >= MAX_REPOS)
				return -ENOBUFS;

			args->disable_repos[args->num_disable_repos++] = arg;
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}


int main(int argc, char* argv[]) {
	struct pakfire_ctx* ctx = NULL;
	int r;

	// Setup the context
	r = pakfire_ctx_create(&ctx, NULL);
	if (r)
		goto ERROR;

	// Write logs to the console
	pakfire_ctx_set_log_callback(ctx, pakfire_log_stderr, NULL);

	// Make this all interactive
	pakfire_ctx_set_confirm_callback(ctx, cli_term_confirm, NULL);

	// Setup progress callback
	pakfire_ctx_set_progress_callback(ctx, cli_setup_progressbar, NULL);

	// Setup pick solution callback
	pakfire_ctx_set_pick_solution_callback(ctx, cli_term_pick_solution, NULL);

	struct cli_global_args args = {
		.ctx      = ctx,
		.config   = PAKFIRE_CONFIG_DIR "/pakfire.conf",
		.arch     = NULL,
		.root     = "/",
		.flags    = 0,
		.yes      = 0,
	};

	// Parse the command line and run any commands
	r = cli_parse(options, commands, args_doc, NULL, parse,
		CLI_REQUIRE_ROOT, argc, argv, &args);

ERROR:
	if (ctx) {
		ctx = pakfire_ctx_unref(ctx);
		if (ctx) {
			fprintf(stderr, "Context was not freed\n");
			return 1;
		}
	}

	return r;
}
