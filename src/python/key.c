/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <pakfire/key.h>
#include <pakfire/util.h>

#include "key.h"
#include "pakfire.h"
#include "util.h"

PyObject* new_key(PyTypeObject* type, struct pakfire_key* key) {
	KeyObject* self = (KeyObject *)type->tp_alloc(type, 0);
	if (self) {
		self->key = pakfire_key_ref(key);
	}

	return (PyObject *)self;
}

static PyObject* Key_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
	KeyObject* self = (KeyObject *)type->tp_alloc(type, 0);
	if (self) {
		self->key = NULL;
	}

	return (PyObject *)self;
}

static void Key_dealloc(KeyObject* self) {
	pakfire_key_unref(self->key);

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static uint64_t Key_id_to_int(const pakfire_key_id* id) {
	uint64_t i = 0;

	for (unsigned int j = 0; j < sizeof(*id); j++) {
		// Shift i by one byte & append the next byte
		i = (i << 8) | (*id)[j];
	}

	return i;
}

static PyObject* Key_repr(KeyObject* self) {
	const pakfire_key_id* id = pakfire_key_get_id(self->key);

	return PyUnicode_FromFormat("<_pakfire.Key (%lu)>", Key_id_to_int(id));
}

static PyObject* Key_str(KeyObject* self) {
	PyObject* ret = NULL;
	char* buffer = NULL;
	size_t length = 0;
	int r;

	// Dump the key
	r = pakfire_key_export_string(self->key, &buffer, &length);
	if (r < 0) {
		errno = -errno;
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	// Parse the string
	ret = PyUnicode_FromStringAndSize(buffer, length);
	free(buffer);

	return ret;
}

static PyObject* Key_get_id(KeyObject* self) {
	const pakfire_key_id* id = pakfire_key_get_id(self->key);

	uint64_t i = Key_id_to_int(id);

	return PyLong_FromUnsignedLong(i);
}

static PyObject* Key_get_algorithm(KeyObject* self) {
	const char* algorithm = pakfire_key_get_algo(self->key);

	// Raise an error on no input
	if (!algorithm) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	return PyUnicode_FromString(algorithm);
}

static PyObject* Key_get_comment(KeyObject* self) {
	const char* comment = pakfire_key_get_comment(self->key);
	if (!comment)
		Py_RETURN_NONE;

	return PyUnicode_FromString(comment);
}

static PyObject* Key_export(KeyObject* self, PyObject* args) {
	pakfire_key_export_mode_t mode = PAKFIRE_KEY_EXPORT_MODE_PUBLIC;
	PyObject* object = NULL;
	char* buffer = NULL;
	size_t length = 0;
	int secret = 0;

	if (!PyArg_ParseTuple(args, "|p", &secret))
		return NULL;

	// Create buffer to write the key to
	FILE* f = open_memstream(&buffer, &length);
	if (!f) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	if (secret)
		mode = PAKFIRE_KEY_EXPORT_MODE_PRIVATE;

	// Export the key
	int r = pakfire_key_export(self->key, f, mode);
	if (r < 0) {
		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	object = PyUnicode_FromStringAndSize(buffer, length);

ERROR:
	if (f)
		fclose(f);
	if (buffer)
		free(buffer);

	return object;
}

static PyObject* Key_sign(KeyObject* self, PyObject* args, PyObject* kwargs) {
	const char* kwlist[] = { "data", "comment", NULL };
	PyObject* object = NULL;
	const char* data = NULL;
	Py_ssize_t data_length = 0;
	const char* comment = NULL;
	char* signature = NULL;
	size_t signature_length = 0;
	int r;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "y#|z", (char**)kwlist,
			&data, &data_length, &comment))
		return NULL;

	// Create buffer to write the signature to
	FILE* f = open_memstream(&signature, &signature_length);
	if (!f) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	// Create the signature
	r = pakfire_key_sign_string(self->key, f, data, data_length, comment);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	object = PyUnicode_FromStringAndSize(signature, signature_length);

ERROR:
	if (f)
		fclose(f);
	if (signature)
		free(signature);

	return object;
}

static PyObject* Key_verify(KeyObject* self, PyObject* args) {
	char* signature = NULL;
	const char* data = NULL;
	Py_ssize_t data_length = 0;
	int r;

	if (!PyArg_ParseTuple(args, "sy#", &signature, &data, &data_length))
		return NULL;

	// Map the signature
	FILE* f = fmemopen(signature, strlen(signature), "r");
	if (!f) {
		PyErr_SetFromErrno(PyExc_OSError);
		r = 1;
		goto ERROR;
	}

	// Verify the signature
	r = pakfire_key_verify(self->key, f, data, data_length);
	if (r) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	// Success!
	r = 0;

ERROR:
	if (f)
		fclose(f);

	if (r)
		return NULL;

	Py_RETURN_TRUE;
}

static struct PyMethodDef Key_methods[] = {
	{
		"export",
		(PyCFunction)Key_export,
		METH_VARARGS,
		NULL,
	},
	{
		"sign",
		(PyCFunction)Key_sign,
		METH_VARARGS|METH_KEYWORDS,
		NULL,
	},
	{
		"verify",
		(PyCFunction)Key_verify,
		METH_VARARGS,
		NULL,
	},
	{ NULL },
};

static struct PyGetSetDef Key_getsetters[] = {
	{
		"algorithm",
		(getter)Key_get_algorithm,
		NULL,
		NULL,
		NULL,
	},
	{
		"comment",
		(getter)Key_get_comment,
		NULL,
		NULL,
		NULL,
	},
	{
		"id",
		(getter)Key_get_id,
		NULL,
		NULL,
		NULL,
	},
	{ NULL },
};

PyTypeObject KeyType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	.tp_name            = "pakfire.Key",
	.tp_basicsize       = sizeof(KeyObject),
	.tp_flags           = Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	.tp_new             = Key_new,
	.tp_dealloc         = (destructor)Key_dealloc,
	.tp_doc             = "Key Object",
	.tp_methods         = Key_methods,
	.tp_getset          = Key_getsetters,
	.tp_repr            = (reprfunc)Key_repr,
	.tp_str             = (reprfunc)Key_str,
};
