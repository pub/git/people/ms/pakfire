/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2011 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/hashes.h>
#include <pakfire/package.h>
#include <pakfire/repo.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#include "errors.h"
#include "package.h"
#include "pakfire.h"
#include "repo.h"
#include "util.h"

PyObject* new_package(PyTypeObject* type, struct pakfire_package* pkg) {
	PackageObject* self = (PackageObject *)type->tp_alloc(type, 0);
	if (self) {
		self->package = pakfire_package_ref(pkg);
	}

	return (PyObject *)self;
}

static PyObject* Package_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
	PackageObject* self = (PackageObject *)type->tp_alloc(type, 0);
	if (self) {
		self->package = NULL;
	}

	return (PyObject *)self;
}

static void Package_dealloc(PackageObject* self) {
	if (self->package)
		pakfire_package_unref(self->package);

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static int Package_init(PackageObject* self, PyObject* args, PyObject* kwds) {
	PakfireObject* pakfire = NULL;
	RepoObject* repo = NULL;
	const char* name = NULL;
	const char* evr = NULL;
	const char* arch = NULL;
	int r;

	if (!PyArg_ParseTuple(args, "O!O!sss",
			&PakfireType, &pakfire, &RepoType, &repo, &name, &evr, &arch))
		return -1;

	// Create the package object
	r = pakfire_package_create(&self->package, pakfire->pakfire, repo->repo, name, evr, arch);
	if (r < 0) {
		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);
		return -1;
	}

	return 0;
}

static Py_hash_t Package_hash(PackageObject* self) {
	return pakfire_package_id(self->package);
}

static PyObject* Package_repr(PackageObject* self) {
	const char* nevra = pakfire_package_get_string(self->package, PAKFIRE_PKG_NEVRA);
	if (!nevra) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	return PyUnicode_FromFormat("<_pakfire.Package %s>", nevra);
}

static PyObject* Package_str(PackageObject* self) {
	const char* nevra = pakfire_package_get_string(self->package, PAKFIRE_PKG_NEVRA);
	if (!nevra) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	return PyUnicode_FromString(nevra);
}

static PyObject* Package_richcompare(PackageObject* self, PyObject* o, int op) {
	PackageObject* other = (PackageObject *)o;

	// Check if the other object is of the correct type
	if (!PyType_IsSubtype(o->ob_type, &PackageType)) {
		PyErr_SetString(PyExc_TypeError, "Expected a Package object");
		return NULL;
	}

	long result = pakfire_package_cmp(self->package, other->package);

	switch (result) {
		case Py_EQ:
			if (result == 0)
				Py_RETURN_TRUE;
			break;

		case Py_NE:
			if (result != 0)
				Py_RETURN_TRUE;
			break;

		case Py_LE:
			if (result <= 0)
				Py_RETURN_TRUE;
			break;

		case Py_GE:
			if (result >= 0)
				Py_RETURN_TRUE;
			break;

		case Py_LT:
			if (result < 0)
				Py_RETURN_TRUE;
			break;

		case Py_GT:
			if (result > 0)
				Py_RETURN_TRUE;
			break;

		default:
			PyErr_BadArgument();
			return NULL;
	}

	Py_RETURN_FALSE;
}

static PyObject* Package_get_name(PackageObject* self) {
	const char* name = pakfire_package_get_string(self->package, PAKFIRE_PKG_NAME);
	if (!name)
		Py_RETURN_NONE;

	return PyUnicode_FromString(name);
}

static PyObject* Package_get_evr(PackageObject* self) {
	const char* evr = pakfire_package_get_string(self->package, PAKFIRE_PKG_EVR);
	if (!evr)
		Py_RETURN_NONE;

	return PyUnicode_FromString(evr);
}

static PyObject* Package_get_arch(PackageObject* self) {
	const char* arch = pakfire_package_get_string(self->package, PAKFIRE_PKG_ARCH);
	if (!arch)
		Py_RETURN_NONE;

	return PyUnicode_FromString(arch);
}

static PyObject* Package_get_uuid(PackageObject* self) {
	const char* uuid = pakfire_package_get_string(self->package, PAKFIRE_PKG_UUID);
	if (!uuid)
		Py_RETURN_NONE;

	return PyUnicode_FromString(uuid);
}

static PyObject* Package_get_checksum(PackageObject* self) {
	enum pakfire_hash_type type = PAKFIRE_HASH_UNDEFINED;
	const unsigned char* checksum = NULL;
	size_t checksum_length = 0;
	int r;

	// Fetch the checksum
	r = pakfire_package_get_checksum(self->package, &type, &checksum, &checksum_length);
	if (r < 0)
		return NULL;

	return Py_BuildValue("(sy#)", pakfire_hash_name(type), checksum, checksum_length);
}

static PyObject* Package_get_summary(PackageObject* self) {
	const char* summary = pakfire_package_get_string(self->package, PAKFIRE_PKG_SUMMARY);
	if (!summary)
		Py_RETURN_NONE;

	return PyUnicode_FromString(summary);
}

static PyObject* Package_get_description(PackageObject* self) {
	const char* description = pakfire_package_get_string(self->package, PAKFIRE_PKG_DESCRIPTION);
	if (!description)
		Py_RETURN_NONE;

	return PyUnicode_FromString(description);
}

static PyObject* Package_get_license(PackageObject* self) {
	const char* license = pakfire_package_get_string(self->package, PAKFIRE_PKG_LICENSE);
	if (!license)
		Py_RETURN_NONE;

	return PyUnicode_FromString(license);
}

static PyObject* Package_get_url(PackageObject* self) {
	const char* url = pakfire_package_get_string(self->package, PAKFIRE_PKG_URL);
	if (!url)
		Py_RETURN_NONE;

	return PyUnicode_FromString(url);
}

static PyObject* Package_get_groups(PackageObject* self) {
	char** groups = pakfire_package_get_strings(self->package, PAKFIRE_PKG_GROUPS);

	PyObject* ret = PyUnicodeList_FromStringArray((const char**)groups);

	// Cleanup
	if (groups)
		pakfire_strings_free(groups);

	return ret;
}

static PyObject* Package_get_vendor(PackageObject* self) {
	const char* vendor = pakfire_package_get_string(self->package, PAKFIRE_PKG_VENDOR);
	if (!vendor)
		Py_RETURN_NONE;

	return PyUnicode_FromString(vendor);
}

static PyObject* Package_get_distribution(PackageObject* self) {
	const char* distribution = pakfire_package_get_string(self->package, PAKFIRE_PKG_DISTRO);
	if (!distribution)
		Py_RETURN_NONE;

	return PyUnicode_FromString(distribution);
}

static PyObject* Package_get_packager(PackageObject* self) {
	const char* packager = pakfire_package_get_string(self->package, PAKFIRE_PKG_PACKAGER);
	if (!packager)
		Py_RETURN_NONE;

	return PyUnicode_FromString(packager);
}

static PyObject* Package_get_filename(PackageObject* self) {
	const char* filename = pakfire_package_get_string(self->package, PAKFIRE_PKG_FILENAME);
	if (!filename)
		Py_RETURN_NONE;

	return PyUnicode_FromString(filename);
}

static PyObject* Package_get_downloadsize(PackageObject* self) {
	unsigned long long size = pakfire_package_get_num(self->package, PAKFIRE_PKG_DOWNLOADSIZE, 0);

	return PyLong_FromUnsignedLongLong(size);
}

static PyObject* Package_get_installsize(PackageObject* self) {
	unsigned long long size = pakfire_package_get_num(self->package, PAKFIRE_PKG_INSTALLSIZE, 0);

	return PyLong_FromUnsignedLongLong(size);
}

static PyObject* Package_get_size(PackageObject* self) {
	size_t size = pakfire_package_get_size(self->package);

	return PyLong_FromUnsignedLongLong(size);
}

static PyObject* Package_get_build_arches(PackageObject* self) {
	// Fetch all supported arches
	char** build_arches = pakfire_package_get_strings(self->package, PAKFIRE_PKG_BUILD_ARCHES);

	// Make a new list
	PyObject* ret = PyUnicodeList_FromStringArray((const char**)build_arches);

	// Cleanup
	if (build_arches)
		pakfire_strings_free(build_arches);

	return ret;
}

static PyObject* Package_get_build_id(PackageObject* self) {
	const char* build_id = pakfire_package_get_string(self->package, PAKFIRE_PKG_BUILD_ID);
	if (!build_id)
		Py_RETURN_NONE;

	return PyUnicode_FromString(build_id);
}

static PyObject* Package_get_buildhost(PackageObject* self) {
	const char* build_host = pakfire_package_get_string(self->package, PAKFIRE_PKG_BUILD_HOST);
	if (!build_host)
		Py_RETURN_NONE;

	return PyUnicode_FromString(build_host);
}

static PyObject* Package_get_buildtime(PackageObject* self) {
	time_t build_time = pakfire_package_get_num(self->package, PAKFIRE_PKG_BUILD_TIME, 0);

	return PyLong_FromUnsignedLongLong(build_time);
}

static PyObject* Package_get_repo(PackageObject* self) {
	struct pakfire_repo* repo = pakfire_package_get_repo(self->package);
	if (!repo)
		Py_RETURN_NONE;

	PyObject* obj = new_repo(&RepoType, repo);
	pakfire_repo_unref(repo);

	return obj;
}

static PyObject* Package_get_path(PackageObject* self) {
	const char* path = pakfire_package_get_string(self->package, PAKFIRE_PKG_PATH);
	if (!path) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	return PyUnicode_FromString(path);
}

static PyObject* Package_get_deps(PackageObject* self, int what) {
	char** deps = NULL;

	// Fetch dependencies
	deps = pakfire_package_get_deps(self->package, what);

	// Convert it all into Python stuff
	PyObject* list = PyUnicodeList_FromStringArray((const char**)deps);

	// Cleanup
	if (deps)
		pakfire_strings_free(deps);

	return list;
}

static PyObject* Package_get_provides(PackageObject* self) {
	return Package_get_deps(self, PAKFIRE_PKG_PROVIDES);
}

static PyObject* Package_get_prerequires(PackageObject* self) {
	return Package_get_deps(self, PAKFIRE_PKG_PREREQUIRES);
}

static PyObject* Package_get_requires(PackageObject* self) {
	return Package_get_deps(self, PAKFIRE_PKG_REQUIRES);
}

static PyObject* Package_get_obsoletes(PackageObject* self) {
	return Package_get_deps(self, PAKFIRE_PKG_OBSOLETES);
}

static PyObject* Package_get_conflicts(PackageObject* self) {
	return Package_get_deps(self, PAKFIRE_PKG_CONFLICTS);
}

static PyObject* Package_get_recommends(PackageObject* self) {
	return Package_get_deps(self, PAKFIRE_PKG_RECOMMENDS);
}

static PyObject* Package_get_suggests(PackageObject* self) {
	return Package_get_deps(self, PAKFIRE_PKG_SUGGESTS);
}

static PyObject* Package_get_reverse_requires(PackageObject* self) {
	struct pakfire_packagelist* list = NULL;
	PyObject* ret = NULL;
	int r;

	// Search for all reverse requires
	r = pakfire_package_get_reverse_requires(self->package, &list);
	if (r < 0) {
		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	ret = PyList_FromPackageList(list);

ERROR:
	if (list)
		pakfire_packagelist_unref(list);

	return ret;
}

static PyObject* Package_get_filelist(PackageObject* self, PyObject* args) {
	PyObject* list = PyList_New(0);
	if (list == NULL)
		return NULL;

	struct pakfire_filelist* filelist = pakfire_package_get_filelist(self->package);
	for (unsigned int i = 0; i < pakfire_filelist_length(filelist); i++) {
		struct pakfire_file* file = pakfire_filelist_get(filelist, i);
		const char* path = pakfire_file_get_path(file);
		pakfire_file_unref(file);

		PyObject* obj = PyUnicode_FromString(path);

		int ret = PyList_Append(list, obj);
		Py_DECREF(obj);

		if (ret) {
			pakfire_filelist_unref(filelist);
			Py_DECREF(list);

			return NULL;
		}
	}

	pakfire_filelist_unref(filelist);

	return list;
}

static PyObject* Package_dump(PackageObject* self, PyObject *args, PyObject* kwds) {
	const char* kwlist[] = { "long", "filelist", NULL };

	int long_format = 0;
	int filelist = 0;

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "|ii", (char**)kwlist, &long_format, &filelist))
		return NULL;

	int flags = 0;
	if (long_format)
		flags |= PAKFIRE_PKG_DUMP_LONG;

	if (filelist)
		flags |= PAKFIRE_PKG_DUMP_FILELIST;

	char* dump = pakfire_package_dump(self->package, flags);
	if (!dump)
		Py_RETURN_NONE;

	PyObject* ret = PyUnicode_FromString(dump);
	free(dump);

	return ret;
}

static PyObject* Package_get_source_package(PackageObject* self) {
	const char* source_package = pakfire_package_get_string(self->package, PAKFIRE_PKG_SOURCE_PKG);
	if (!source_package)
		Py_RETURN_NONE;

	return PyUnicode_FromString(source_package);
}

static PyObject* Package_get_source_name(PackageObject* self) {
	const char* source_name = pakfire_package_get_string(self->package, PAKFIRE_PKG_SOURCE_NAME);
	if (!source_name)
		Py_RETURN_NONE;

	return PyUnicode_FromString(source_name);
}

static PyObject* Package_get_source_evr(PackageObject* self) {
	const char* source_evr = pakfire_package_get_string(self->package, PAKFIRE_PKG_SOURCE_EVR);
	if (!source_evr)
		Py_RETURN_NONE;

	return PyUnicode_FromString(source_evr);
}

static PyObject* Package_get_source_arch(PackageObject* self) {
	const char* source_arch = pakfire_package_get_string(self->package, PAKFIRE_PKG_SOURCE_ARCH);
	if (!source_arch)
		Py_RETURN_NONE;

	return PyUnicode_FromString(source_arch);
}

static PyObject* Package_installcheck(PackageObject* self) {
	char* problem = NULL;
	int r;

	// Perform the installcheck
	r = pakfire_package_installcheck(self->package, &problem, 0);

	// Success!
	if (r == 0)
		Py_RETURN_NONE;

	// We did not get the problem
	else if (!problem) {
		PyErr_SetFromErrno(PyExc_OSError);

	// Otherwise raise a dependency error
	} else {
		PyErr_SetString(PyExc_DependencyError, problem);
		free(problem);
	}

	return NULL;
}

static struct PyMethodDef Package_methods[] = {
	{
		"dump",
		(PyCFunction)Package_dump,
		METH_VARARGS|METH_KEYWORDS,
		NULL,
	},
	{
		"installcheck",
		(PyCFunction)Package_installcheck,
		METH_NOARGS,
		NULL,
	},
	{ NULL },
};

static struct PyGetSetDef Package_getsetters[] = {
	{
		"name",
		(getter)Package_get_name,
		NULL,
		NULL,
		NULL
	},
	{
		"evr",
		(getter)Package_get_evr,
		NULL,
		NULL,
		NULL
	},
	{
		"arch",
		(getter)Package_get_arch,
		NULL,
		NULL,
		NULL
	},
	{
		"uuid",
		(getter)Package_get_uuid,
		NULL,
		NULL,
		NULL
	},
	{
		"checksum",
		(getter)Package_get_checksum,
		NULL,
		NULL,
		NULL,
	},
	{
		"summary",
		(getter)Package_get_summary,
		NULL,
		NULL,
		NULL
	},
	{
		"description",
		(getter)Package_get_description,
		NULL,
		NULL,
		NULL
	},
	{
		"license",
		(getter)Package_get_license,
		NULL,
		NULL,
		NULL
	},
	{
		"url",
		(getter)Package_get_url,
		NULL,
		NULL,
		NULL
	},
	{
		"groups",
		(getter)Package_get_groups,
		NULL,
		NULL,
		NULL
	},
	{
		"vendor",
		(getter)Package_get_vendor,
		NULL,
		NULL,
		NULL
	},
	{
		"distribution",
		(getter)Package_get_distribution,
		NULL,
		NULL,
		NULL
	},
	{
		"packager",
		(getter)Package_get_packager,
		NULL,
		NULL,
		NULL
	},
	{
		"filename",
		(getter)Package_get_filename,
		NULL,
		NULL,
		NULL
	},
	{
		"downloadsize",
		(getter)Package_get_downloadsize,
		NULL,
		NULL,
		NULL
	},
	{
		"installsize",
		(getter)Package_get_installsize,
		NULL,
		NULL,
		NULL
	},
	{
		"size",
		(getter)Package_get_size,
		NULL,
		NULL,
		NULL
	},
	{
		"build_id",
		(getter)Package_get_build_id,
		NULL,
		NULL,
		NULL,
	},
	{
		"buildhost",
		(getter)Package_get_buildhost,
		NULL,
		NULL,
		NULL
	},
	{
		"buildtime",
		(getter)Package_get_buildtime,
		NULL,
		NULL,
		NULL
	},
	{
		"build_arches",
		(getter)Package_get_build_arches,
		NULL,
		NULL,
		NULL,
	},
	{
		"source_package",
		(getter)Package_get_source_package,
		NULL,
		NULL,
		NULL
	},
	{
		"source_name",
		(getter)Package_get_source_name,
		NULL,
		NULL,
		NULL
	},
	{
		"source_evr",
		(getter)Package_get_source_evr,
		NULL,
		NULL,
		NULL
	},
	{
		"source_arch",
		(getter)Package_get_source_arch,
		NULL,
		NULL,
		NULL
	},

	// Dependencies
	{
		"provides",
		(getter)Package_get_provides,
		NULL,
		NULL,
		NULL
	},
	{
		"prerequires",
		(getter)Package_get_prerequires,
		NULL,
		NULL,
		NULL
	},
	{
		"requires",
		(getter)Package_get_requires,
		NULL,
		NULL,
		NULL
	},
	{
		"obsoletes",
		(getter)Package_get_obsoletes,
		NULL,
		NULL,
		NULL
	},
	{
		"conflicts",
		(getter)Package_get_conflicts,
		NULL,
		NULL,
		NULL
	},
	{
		"recommends",
		(getter)Package_get_recommends,
		NULL,
		NULL,
		NULL
	},
	{
		"suggests",
		(getter)Package_get_suggests,
		NULL,
		NULL,
		NULL
	},
	{
		"reverse_requires",
		(getter)Package_get_reverse_requires,
		NULL,
		NULL,
		NULL
	},

	// Repository
	{
		"repo",
		(getter)Package_get_repo,
		NULL,
		NULL,
		NULL
	},
	{
		"path",
		(getter)Package_get_path,
		NULL,
		NULL,
		NULL
	},

	// Filelist
	{
		"filelist",
		(getter)Package_get_filelist,
		NULL,
		NULL,
		NULL
	},

	{ NULL }
};

PyTypeObject PackageType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	.tp_name            = "pakfire.Package",
	.tp_basicsize       = sizeof(PackageObject),
	.tp_flags           = Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	.tp_new             = Package_new,
	.tp_dealloc         = (destructor)Package_dealloc,
	.tp_init            = (initproc)Package_init,
	.tp_doc             = "Package Object",
	.tp_methods         = Package_methods,
	.tp_getset          = Package_getsetters,
	.tp_hash            = (hashfunc)Package_hash,
	.tp_repr            = (reprfunc)Package_repr,
	.tp_str             = (reprfunc)Package_str,
	.tp_richcompare     = (richcmpfunc)Package_richcompare,
};
