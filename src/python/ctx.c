/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <syslog.h>

#include <pakfire/ctx.h>

#include "ctx.h"

static PyObject* Ctx_new(PyTypeObject* type, PyObject* args, PyObject* kwds) {
	CtxObject* self = (CtxObject *)type->tp_alloc(type, 0);
	if (self)
		self->ctx = NULL;

	return (PyObject *)self;
}

static void Ctx_log_callback(void* data, int level, const char* file, int line,
	const char* fn, const char* format, va_list args) __attribute__((format(printf, 6, 0)));

static void Ctx_log_callback(void* data, int level, const char* file, int line,
		const char* fn, const char* format, va_list args) {
	PyObject* logger = (PyObject*)data;
	PyObject* result = NULL;
	char* buffer = NULL;
	int r;

	PyObject* exception = NULL;
	PyObject* type = NULL;
	PyObject* value = NULL;
	PyObject* traceback = NULL;

	// Do nothing if the logger does not exist
	if (!logger)
		return;

	// Translate priority to Python logging priorities
	switch (level) {
		case LOG_DEBUG:
			level = 10;
			break;

		case LOG_INFO:
			level = 20;
			break;

		case LOG_WARNING:
			level = 30;
			break;

		case LOG_ERR:
			level = 40;
			break;

		// Drop messages of an unknown level
		default:
			return;
	}

	PyGILState_STATE state = PyGILState_Ensure();

	// Format the log line
	r = vasprintf(&buffer, format, args);
	if (r < 0)
		goto ERROR;

	// Remove trailing newline
	if (buffer[r - 1] == '\n')
		r--;

	// Call the logger
	result = PyObject_CallMethod(logger, "log", "is#", level, buffer, r);
	if (!result)
		goto ERROR;

ERROR:
	/*
		We cannot really catch any Python errors here, since we cannot send
		any error codes up the chain.

		So, in order to debug the problem, We will check if an exception has
		occurred and if so, print it to the console.
	*/
	exception = PyErr_Occurred();
	if (exception) {
		PyErr_Print();

		// Fetch the exception and destroy it
		PyErr_Fetch(&type, &value, &traceback);

		Py_XDECREF(type);
		Py_XDECREF(value);
		Py_XDECREF(traceback);
	}

	if (buffer)
		free(buffer);
	Py_XDECREF(result);

	// Release the GIL
	PyGILState_Release(state);
}

static void Ctx___set_logger(CtxObject* self, PyObject* logger) {
	// Dereference the old logger
	Py_XDECREF(self->logger);

	// Store the new logger
	self->logger = logger;
	Py_XINCREF(self->logger);

	// Set the logger
	pakfire_ctx_set_log_callback(self->ctx, Ctx_log_callback, self->logger);
}

static int Ctx_setup_logging(CtxObject* self) {
	PyObject* logging = NULL;
	PyObject* logger = NULL;
	int r = -1;

	// import logging
	logging = PyImport_ImportModule("logging");
	if (!logging)
		goto ERROR;

	// logging.getLogger("pakfire")
	logger = PyObject_CallMethod(logging, "getLogger", "s", "pakfire");
	if (!logger)
		goto ERROR;

	// Set default logger
	Ctx___set_logger(self, logger);

	// Success
	r = 0;

ERROR:
	Py_XDECREF(logging);
	Py_XDECREF(logger);

	return r;
}

static int Ctx_init(CtxObject* self, PyObject* args, PyObject* kwargs) {
	char* kwlist[] = { (char*)"path", NULL };
	const char* path = NULL;
	int r;

	// Parse arguments
	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "|s", kwlist, &path))
		return -1;

	// Create context
	r = pakfire_ctx_create(&self->ctx, path);
	if (r) {
		errno = -r;

		PyErr_SetFromErrno(PyExc_OSError);
		return -1;
	}

	// Set the log level to DEBUG
	pakfire_ctx_set_log_level(self->ctx, LOG_DEBUG);

	// Setup the default logger
	r = Ctx_setup_logging(self);
	if (r)
		return r;

	return 0;
}

static void Ctx_dealloc(CtxObject* self) {
	if (self->ctx) {
		// Reset the logger
		pakfire_ctx_set_log_callback(self->ctx, NULL, NULL);

		pakfire_ctx_unref(self->ctx);
	}

	Py_XDECREF(self->logger);

	Py_TYPE(self)->tp_free((PyObject *)self);
}

// Logging

static PyObject* Ctx_set_logger(CtxObject* self, PyObject* args) {
	PyObject* logger = NULL;

	if (!PyArg_ParseTuple(args, "O", &logger))
		return NULL;

	// Set the logger
	Ctx___set_logger(self, logger);

	Py_RETURN_NONE;
}

static PyObject* Ctx_get_cache_path(CtxObject* self) {
	const char* path = NULL;

	// Fetch the path
	path = pakfire_ctx_get_cache_path(self->ctx);
	if (!path) {
		PyErr_SetFromErrno(PyExc_OSError);
		return NULL;
	}

	return PyUnicode_FromString(path);
}

static int Ctx_set_cache_path(CtxObject* self, PyObject* value) {
	const char* path = NULL;
	int r;

	// Fetch the path
	path = PyUnicode_AsUTF8(value);
	if (!path)
		return -1;

	// Set the cache path
	r = pakfire_ctx_set_cache_path(self->ctx, path);
	if (r < 0) {
		errno = -r;
		PyErr_SetFromErrno(PyExc_OSError);

		return r;
	}

	return 0;
}

static struct PyMethodDef Ctx_methods[] = {
	{
		"set_logger",
		(PyCFunction)Ctx_set_logger,
		METH_VARARGS,
		NULL,
	},
	{ NULL },
};

static struct PyGetSetDef Ctx_getsetters[] = {
	{
		"cache_path",
		(getter)Ctx_get_cache_path,
		(setter)Ctx_set_cache_path,
		NULL,
		NULL,
	},
    { NULL },
};

PyTypeObject CtxType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	.tp_name            = "pakfire.Ctx",
	.tp_basicsize       = sizeof(CtxObject),
	.tp_flags           = Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	.tp_new             = Ctx_new,
	.tp_dealloc         = (destructor)Ctx_dealloc,
	.tp_init            = (initproc)Ctx_init,
	.tp_doc             = "Ctx Object",
	.tp_methods         = Ctx_methods,
	.tp_getset          = Ctx_getsetters,
};
