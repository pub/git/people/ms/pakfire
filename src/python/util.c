/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2011 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <datetime.h>

#include <errno.h>
#include <stdio.h>

#include <pakfire/filelist.h>
#include <pakfire/package.h>
#include <pakfire/packagelist.h>

#include "file.h"
#include "package.h"
#include "util.h"

PyObject* PyUnicodeList_FromStringArray(const char** l) {
	PyObject* list = NULL;
	PyObject* s = NULL;
	int r;

	list = PyList_New(0);
	if (!list)
		goto ERROR;

	for (; l && *l; l++) {
		s = PyUnicode_FromString(*l);
		if (!s)
			goto ERROR;

		r = PyList_Append(list, s);
		Py_DECREF(s);
		if (r)
			goto ERROR;
	}

	return list;

ERROR:
	if (list)
		Py_DECREF(list);

	return NULL;
}

PyObject* PyList_FromPackageList(struct pakfire_packagelist* packagelist) {
	struct pakfire_package* package = NULL;
	PyObject* item = NULL;

	// Determine the length of the list
	const size_t length = pakfire_packagelist_length(packagelist);

	// Create a new list
	PyObject* list = PyList_New(length);

	for (size_t i = 0; i < length; i++) {
		package = pakfire_packagelist_get(packagelist, i);

		// Make a new package
		item = new_package(&PackageType, package);
		pakfire_package_unref(package);

		// Fail if we could not create a package
		if (!item)
			goto ERROR;

		// Add it to the list
		PyList_SET_ITEM(list, i, item);
	}

	return list;

ERROR:
	Py_XDECREF(list);
	return NULL;
}

PyObject* PyList_FromFileList(struct pakfire_filelist* filelist) {
	struct pakfire_file* file = NULL;
	PyObject* f = NULL;
	int r;

	PyObject* list = PyList_New(0);
	if (!list)
		goto ERROR;

	// Fetch size
	const size_t size = pakfire_filelist_length(filelist);

	for (unsigned int i = 0; i < size; i++) {
		file = pakfire_filelist_get(filelist, i);
		if (!file)
			goto ERROR;

		// Create a new File object
		f = new_file(&FileType, file);
		if (!f)
			goto ERROR;

		// Append the new object to the list
		r = PyList_Append(list, f);
		Py_DECREF(f);

		// If we could not append to the list, we will break
		if (r)
			goto ERROR;

		// Free file
		pakfire_file_unref(file);
	}

	return list;

ERROR:
	Py_XDECREF(list);
	return NULL;
}

PyObject* PyDateTime_FromTime_t(const time_t* t) {
	struct tm buffer;

	struct tm* tm = gmtime_r(t, &buffer);
	if (!tm)
		return NULL;

	PyDateTime_IMPORT;

	return PyDateTime_FromDateAndTime(
		1900 + tm->tm_year,
		1 + tm->tm_mon,
		tm->tm_mday,
		tm->tm_hour,
		tm->tm_min,
		tm->tm_sec,
		0
	);
}
