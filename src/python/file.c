/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <pakfire/hashes.h>
#include <pakfire/file.h>

#include "file.h"
#include "pakfire.h"
#include "util.h"

PyObject* new_file(PyTypeObject* type, struct pakfire_file* file) {
	FileObject* self = (FileObject *)type->tp_alloc(type, 0);
	if (self) {
		self->file = pakfire_file_ref(file);
	}

	return (PyObject *)self;
}

static void File_dealloc(FileObject* self) {
	if (self->file)
		pakfire_file_unref(self->file);

	Py_TYPE(self)->tp_free((PyObject *)self);
}

static PyObject* File_repr(FileObject* self) {
	const char* path = pakfire_file_get_path(self->file);

	return PyUnicode_FromFormat("<_pakfire.File %s>", path);
}

static PyObject* File_get_path(FileObject* self) {
	const char* path = pakfire_file_get_path(self->file);
	if (!path)
		Py_RETURN_NONE;

	return PyUnicode_FromString(path);
}

static PyObject* File_get_size(FileObject* self) {
	const size_t size = pakfire_file_get_size(self->file);

	return PyLong_FromSize_t(size);
}

static PyObject* File_get_type(FileObject* self) {
	int type = pakfire_file_get_type(self->file);

	return PyLong_FromLong(type);
}

static PyObject* File_get_uname(FileObject* self) {
	const char* uname = pakfire_file_get_uname(self->file);
	if (!uname)
		Py_RETURN_NONE;

	return PyUnicode_FromString(uname);
}

static PyObject* File_get_gname(FileObject* self) {
	const char* gname = pakfire_file_get_gname(self->file);
	if (!gname)
		Py_RETURN_NONE;

	return PyUnicode_FromString(gname);
}

static PyObject* File_get_mode(FileObject* self) {
	const mode_t mode = pakfire_file_get_mode(self->file);

	return PyLong_FromLong(mode);
}

static PyObject* File_get_ctime(FileObject* self) {
	const time_t t = pakfire_file_get_ctime(self->file);

	return PyDateTime_FromTime_t(&t);
}

static PyObject* File_get_mtime(FileObject* self) {
	const time_t t = pakfire_file_get_mtime(self->file);

	return PyDateTime_FromTime_t(&t);
}

static PyObject* File_checksum(FileObject* self, PyObject* args) {
	const unsigned char* checksum = NULL;
	size_t length = 0;
	int r;

	const char* name = NULL;

	if (!PyArg_ParseTuple(args, "s", &name))
		return NULL;

	// Fetch the type
	const enum pakfire_hash_type type = pakfire_hash_by_name(name);

	// Raise ValueError if we could not find the type
	if (!type) {
		PyErr_Format(PyExc_ValueError, "Unknown hash type: %s", name);
		return NULL;
	}

	// Fetch the checksum
	r = pakfire_file_get_checksum(self->file, type, &checksum, &length);
	if (r < 0)
		Py_RETURN_NONE;

	return PyBytes_FromStringAndSize((const char*)checksum, length);
}

static PyObject* File_get_mimetype(FileObject* self) {
	const char* mimetype = pakfire_file_get_mimetype(self->file);
	if (!mimetype)
		Py_RETURN_NONE;

	return PyUnicode_FromString(mimetype);
}

static PyObject* File_get_capabilities(FileObject* self) {
	PyObject* result = NULL;
	char* caps = NULL;

	// No capabilities
	if (!pakfire_file_has_caps(self->file))
		Py_RETURN_NONE;

	// Fetch capabilities
	caps = pakfire_file_get_caps(self->file);
	if (!caps) {
		PyErr_SetFromErrno(PyExc_OSError);
		goto ERROR;
	}

	// Convert to Python string object
	result = PyUnicode_FromString(caps);

ERROR:
	if (caps)
		free(caps);

	return result;
}

static struct PyMethodDef File_methods[] = {
	{
		"checksum",
		(PyCFunction)File_checksum,
		METH_VARARGS,
		NULL,
	},
	{ NULL },
};

static struct PyGetSetDef File_getsetters[] = {
	{
		"capabilities",
		(getter)File_get_capabilities,
		NULL,
		NULL,
		NULL,
	},
	{
		"ctime",
		(getter)File_get_ctime,
		NULL,
		NULL,
		NULL,
	},
	{
		"gname",
		(getter)File_get_gname,
		NULL,
		NULL,
		NULL,
	},
	{
		"mimetype",
		(getter)File_get_mimetype,
		NULL,
		NULL,
		NULL,
	},
	{
		"mode",
		(getter)File_get_mode,
		NULL,
		NULL,
		NULL,
	},
	{
		"mtime",
		(getter)File_get_mtime,
		NULL,
		NULL,
		NULL,
	},
	{
		"path",
		(getter)File_get_path,
		NULL,
		NULL,
		NULL,
	},
	{
		"size",
		(getter)File_get_size,
		NULL,
		NULL,
		NULL,
	},
	{
		"type",
		(getter)File_get_type,
		NULL,
		NULL,
		NULL,
	},
	{
		"uname",
		(getter)File_get_uname,
		NULL,
		NULL,
		NULL,
	},
	{ NULL },
};

PyTypeObject FileType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	.tp_name            = "pakfire.File",
	.tp_basicsize       = sizeof(FileObject),
	.tp_flags           = Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE,
	.tp_dealloc         = (destructor)File_dealloc,
	.tp_doc             = "File object",
	.tp_methods         = File_methods,
	.tp_getset          = File_getsetters,
	.tp_repr            = (reprfunc)File_repr,
	.tp_str             = (reprfunc)File_get_path,
};
