/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/sendfile.h>
#include <sys/stat.h>

#include <elf.h>

#include <pakfire/elf.h>
#include <pakfire/filelist.h>
#include <pakfire/hex.h>
#include <pakfire/i18n.h>
#include <pakfire/pakfire.h>
#include <pakfire/path.h>
#include <pakfire/string.h>
#include <pakfire/stripper.h>
#include <pakfire/util.h>

struct pakfire_stripper {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Pakfire
	struct pakfire* pakfire;

	// Jail
	struct pakfire_jail* jail;

	// Path
	char path[PATH_MAX];

	// Filelist
	struct pakfire_filelist* filelist;

	// Sources
	struct pakfire_filelist* sources;

	// Source Directory
	int sourcesfd;
};

static int pakfire_stripper_open_sources(struct pakfire_stripper* self) {
	// Open the source directory
	self->sourcesfd = pakfire_openat(self->pakfire, BUILD_SRC_DIR, O_DIRECTORY|O_PATH);
	if (self->sourcesfd < 0) {
		switch (errno) {
			case ENOENT:
				break;

			default:
				ERROR(self->ctx, "Could not open %s: %m\n", BUILD_SRC_DIR);
				return -errno;
		}
	}

	return 0;
}

int pakfire_stripper_create(struct pakfire_stripper** stripper,
		struct pakfire* pakfire, struct pakfire_jail* jail, const char* path) {
	struct pakfire_stripper* self = NULL;
	int r;

	// Allocate a new object
	self = calloc(1, sizeof(*self));
	if (!self)
		return -errno;

	// Store a reference to the context
	self->ctx = pakfire_ctx(pakfire);

	// Store a reference to Pakfire
	self->pakfire = pakfire_ref(pakfire);

	// Store a reference to the jail
	self->jail = pakfire_jail_ref(jail);

	// Initialize the reference counter
	self->nrefs = 1;

	// Initialize file descriptors
	self->sourcesfd = -EBADF;

	// Store the path
	r = pakfire_string_set(self->path, path);
	if (r < 0)
		goto ERROR;

	// Create a filelist
	r = pakfire_filelist_create(&self->filelist, self->pakfire);
	if (r < 0)
		goto ERROR;

	// Create a list for the sources
	r = pakfire_filelist_create(&self->sources, self->pakfire);
	if (r < 0)
		goto ERROR;

	// Open the source directory
	r = pakfire_stripper_open_sources(self);
	if (r < 0)
		goto ERROR;

	// Return the pointer
	*stripper = pakfire_stripper_ref(self);

ERROR:
	if (self)
		pakfire_stripper_unref(self);

	return r;
}

static void pakfire_stripper_free(struct pakfire_stripper* self) {
	if (self->sources)
		pakfire_filelist_unref(self->sources);
	if (self->sourcesfd)
		close(self->sourcesfd);
	if (self->filelist)
		pakfire_filelist_unref(self->filelist);
	if (self->pakfire)
		pakfire_unref(self->pakfire);
	if (self->jail)
		pakfire_jail_unref(self->jail);
	if (self->ctx)
		pakfire_ctx_unref(self->ctx);
	free(self);
}

struct pakfire_stripper* pakfire_stripper_ref(struct pakfire_stripper* self) {
	++self->nrefs;

	return self;
}

struct pakfire_stripper* pakfire_stripper_unref(struct pakfire_stripper* self) {
	if (--self->nrefs > 0)
		return self;

	pakfire_stripper_free(self);
	return NULL;
}

static int pakfire_stripper_find_elf(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	struct pakfire_stripper* self = data;
	int r;

	// Never try to strip any firmware
	if (pakfire_file_matches(file, "/usr/lib/firmware/**"))
		return 0;

	// Add ELF files to the filelist
	if (pakfire_file_matches_class(file, PAKFIRE_FILE_ELF)) {
		r = pakfire_filelist_add(self->filelist, file);
		if (r < 0)
			return r;
	}

	return 0;
}

/*
	Scan all files in path and identify ELF files
*/
static int pakfire_stripper_scan(struct pakfire_stripper* self) {
	struct pakfire_filelist* filelist = NULL;
	int r;

	// Create a new filelist
	r = pakfire_filelist_create(&filelist, self->pakfire);
	if (r < 0)
		goto ERROR;

	// Scan path for all files
	r = pakfire_filelist_scan(filelist, self->path, NULL, NULL, 0);
	if (r < 0)
		goto ERROR;

	// Walk through all files to find ELF files
	r = pakfire_filelist_walk(filelist, pakfire_stripper_find_elf, self, 0, NULL);
	if (r < 0)
		goto ERROR;

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

static int pakfire_stripper_collect_sources(
		struct pakfire_ctx* ctx, struct pakfire_elf* elf, const char* filename, void* data) {
	struct pakfire_stripper* self = data;
	struct pakfire_file* file = NULL;
	int r;

	// If the source file is not in the right path, we ignore it
	if (!pakfire_string_startswith(filename, DEBUG_SRC_DIR))
		return 0;

	// Don't add files more than once
	if (pakfire_filelist_contains(self->sources, filename))
		return 0;

	// Create a new file object
	r = pakfire_file_create(&file, self->pakfire, filename);
	if (r < 0)
		goto ERROR;

	// Add the file to the list
	r = pakfire_filelist_add(self->sources, file);
	if (r < 0)
		goto ERROR;

ERROR:
	if (file)
		pakfire_file_unref(file);

	return r;
}

static int pakfire_stripper_copy_sources(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	struct pakfire_stripper* self = data;
	struct stat st = {};
	char p[PATH_MAX];
	FILE* dst = NULL;
	int fd = -EBADF;
	int r;

	// Fetch the path
	const char* path = pakfire_file_get_path(file);
	if (!path)
		return -EINVAL;

	// Remove the original source path
	r = pakfire_path_relative(p, DEBUG_SRC_DIR, path);
	if (r < 0)
		goto ERROR;

	// Open the source file
	fd = openat(self->sourcesfd, p, O_RDONLY);
	if (fd < 0) {
		switch (errno) {
			// If the source file does not exist, we cannot copy anything
			case ENOENT:
				goto ERROR;

			default:
				ERROR(self->ctx, "Could not open /%s: %m\n", p);
				r = -errno;
				goto ERROR;
		}
	}

	// Stat the source file
	r = fstat(fd, &st);
	if (r < 0) {
		ERROR(self->ctx, "Could not stat /%s: %m\n", p);
		r = -errno;
		goto ERROR;
	}

	// If the source is not a regular file, we will skip it
	if (!S_ISREG(st.st_mode))
		goto ERROR;

	// Add the buildroot
	r = pakfire_path_append(p, self->path, path);
	if (r < 0)
		goto ERROR;

	// Create all directories
	r = pakfire_mkparentdir(p, 0755);
	if (r < 0)
		goto ERROR;

	// Open the destination file
	dst = fopen(p, "wx");
	if (!dst) {
		ERROR(self->ctx, "Could not open %s: %m\n", p);
		r = -errno;
		goto ERROR;
	}

	// Copy all content
	ssize_t bytes_written = sendfile(fileno(dst), fd, 0, st.st_size);
	if (bytes_written < st.st_size) {
		ERROR(self->ctx, "Failed to copy source file %s: %m\n", path);
		r = -errno;
		goto ERROR;
	}

	// Change permissions
	r = fchmod(fileno(dst), 0444);
	if (r < 0) {
		ERROR(self->ctx, "Could not change permissions of %s: %m\n", path);
		r = -errno;
		goto ERROR;
	}

ERROR:
	if (dst)
		fclose(dst);
	if (fd >= 0)
		close(fd);

	return r;
}

#define pakfire_stripper_debug_path(stripper, s, filename, build_id) \
	__pakfire_stripper_debug_path(stripper, s, sizeof(s), filename, build_id)

static int __pakfire_stripper_debug_path(struct pakfire_stripper* self,
		char* buffer, size_t length, struct pakfire_file* file, const char* build_id) {
	char debugpath[PATH_MAX];
	int r;

	// Make the path to the debug directory
	r = pakfire_path_append(debugpath, self->path, "usr/lib/debug");
	if (r < 0)
		return r;

	// If we have a Build ID, we will make the path using that
	if (build_id)
		return __pakfire_string_format(buffer, length,
			"%s/.build-id/%c%c/%s.debug", debugpath, build_id[0], build_id[1], build_id + 2);

	// Fetch the path of the file
	const char* path = pakfire_file_get_path(file);
	if (!path)
		return -EINVAL;

	// Otherwise we will have to fall pack to the name of the file
	return __pakfire_string_format(buffer, length, "%s/%s.debug", debugpath, path);
}

static int pakfire_stripper_extract_debuginfo(struct pakfire_stripper* self,
		struct pakfire_file* file, struct pakfire_elf* elf, const char* build_id_path) {
	int r;

	const char* path =  pakfire_file_get_abspath(file);

	const char* argv[] = {
		"objcopy",
		"--only-keep-debug",

		// (Re-)compress everything using Zstandard
		"--compress-debug-sections=zstd",

		// Source File
		path,

		// Debug Info File
		build_id_path,
		NULL,
	};

	// Create the directories
	r = pakfire_mkparentdir(build_id_path, 0755);
	if (r < 0)
		return r;

	// Run the command
	r = pakfire_jail_exec_command(self->jail, argv, NULL, 0);
	if (r < 0) {
		ERROR(self->ctx, "Could not extract debug sections from %s: %s\n",
			pakfire_file_get_path(file), strerror(-r));
		return r;

	// The command returned an error
	} else if (r > 0) {
		ERROR(self->ctx, "Could not extract debug sections from %s: %d\n",
			pakfire_file_get_path(file), r);
		return -ENOTSUP;
	}

	return 0;
}

static int pakfire_stripper_strip_debuginfo(struct pakfire_stripper* self,
		struct pakfire_file* file, struct pakfire_elf* elf) {
	char debugpath[PATH_MAX];
	char tmppath[PATH_MAX] = "";
	char** argv = NULL;
	FILE* f = NULL;
	int r;

	int extract_debuginfo = 1;

	// Fetch the path
	const char* path = pakfire_file_get_abspath(file);

	// Fetch the Build ID
	const char* build_id = pakfire_elf_build_id(elf);

	// Check whether we should extract debug information
	switch (pakfire_elf_type(elf)) {
		// Relocatable objects
		case ET_REL:
			if (pakfire_file_matches(file, "**.o"))
				extract_debuginfo = 0;
			break;

		default:
			break;
	}

	// pakfire_stripper_debug_path
	r = pakfire_stripper_debug_path(self, debugpath, file, build_id);
	if (r < 0) {
		ERROR(self->ctx, "Could not make the debug path for %s: %s\n", path, strerror(-r));
		goto ERROR;
	}

	// Make a path for a new temporary file
	r = pakfire_path(self->pakfire, tmppath, "%s", PAKFIRE_TMP_DIR "/.pakfire-strip.XXXXXXX");
	if (r < 0)
		goto ERROR;

	// Create a new temporary file
	f = pakfire_mktemp(tmppath, 0644);
	if (!f) {
		r = -errno;
		goto ERROR;
	}

	const char* strip[] = {
		"objcopy",

		// Ask to strip all debug information
		"--strip-debug",

		// Strip anything unneeded
		"--strip-unneeded",

		// Remove any LTO stuff
		"--remove-section=.gnu.lto_*",
		"--remove-section=.gnu.debuglto_*",
		"--strip-symbol=__gnu_lto_v1",

		// Remove any previous debuglinks
		"--remove-section=.gnu_debuglink",

		// The source file
		path,

		// The stripped output
		pakfire_relpath(self->pakfire, tmppath),
		NULL,
	};

	// Compose the commandline
	r = pakfire_strings_appendm(&argv, strip);
	if (r < 0)
		goto ERROR;

	// Extract the debug information
	if (extract_debuginfo) {
		r = pakfire_stripper_extract_debuginfo(self, file, elf, debugpath);
		if (r < 0)
			goto ERROR;

		// Add the debuglink
		r = pakfire_strings_appendf(&argv, "--add-gnu-debuglink=%s", debugpath);
		if (r < 0)
			goto ERROR;
	}

	// Run the command
	r = pakfire_jail_exec_command(self->jail, (const char**)argv, NULL, 0);
	if (r < 0) {
		ERROR(self->ctx, "Could not strip debug sections from %s: %s\n",
			pakfire_file_get_path(file), strerror(-r));
		goto ERROR;

	// The command returned an error
	} else if (r > 0) {
		ERROR(self->ctx, "Could not strip debug sections from %s\n",
			pakfire_file_get_path(file));
		r = -ENOTSUP;
		goto ERROR;
	}

	// Replace the original file with the stripped version
	r = pakfire_file_consume(file, fileno(f));
	if (r < 0)
		goto ERROR;

ERROR:
	if (argv)
		pakfire_strings_free(argv);
	if (*tmppath)
		unlink(tmppath);
	if (f)
		fclose(f);

	return r;
}

static int pakfire_stripper_strip(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	struct pakfire_stripper* self = data;
	struct pakfire_elf* elf = NULL;
	int r;

	// Open the ELF file
	r = pakfire_elf_open_file(&elf, self->ctx, file);
	if (r < 0)
		goto ERROR;

	// Only strip if there is actually some debug information
	if (!pakfire_elf_is_stripped(elf)) {
		// Collect sources
		r = pakfire_elf_foreach_source_file(elf,
				pakfire_stripper_collect_sources, self);
		if (r < 0)
			goto ERROR;

		// Strip debug information
		r = pakfire_stripper_strip_debuginfo(self, file, elf);
		if (r < 0)
			goto ERROR;
	}

ERROR:
	if (elf)
		pakfire_elf_unref(elf);

	return r;
}

int pakfire_stripper_run(struct pakfire_stripper* self) {
	int r;

	// Scan for all ELF files in path
	r = pakfire_stripper_scan(self);
	if (r < 0)
		return r;

	// If the filelist is empty, there is nothing to do
	if (pakfire_filelist_is_empty(self->filelist))
		return 0;

	// Strip all files
	r = pakfire_filelist_walk(self->filelist, pakfire_stripper_strip,
			self, PAKFIRE_FILELIST_SHOW_PROGRESS, _("Stripping Files..."));
	if (r < 0)
		return r;

	// Copy sources
	r = pakfire_filelist_walk(self->sources, pakfire_stripper_copy_sources,
			self, PAKFIRE_FILELIST_SHOW_PROGRESS, _("Copying Sources..."));
	if (r < 0)
		return r;

	return 0;
}
