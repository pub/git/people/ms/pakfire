/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_HTTPCLIENT_H
#define PAKFIRE_HTTPCLIENT_H

// systemd
#include <systemd/sd-event.h>

struct pakfire_httpclient;

#include <pakfire/ctx.h>
#include <pakfire/xfer.h>

int pakfire_httpclient_create(struct pakfire_httpclient** client,
	struct pakfire_ctx* ctx, sd_event* loop);

struct pakfire_httpclient* pakfire_httpclient_ref(struct pakfire_httpclient* self);
struct pakfire_httpclient* pakfire_httpclient_unref(struct pakfire_httpclient* self);

int pakfire_httpclient_enqueue(struct pakfire_httpclient* self, struct pakfire_xfer* xfer);
int pakfire_httpclient_dequeue(struct pakfire_httpclient* self, struct pakfire_xfer* xfer);

int pakfire_httpclient_run(struct pakfire_httpclient* self, const char* title);

#endif /* PAKFIRE_HTTPCLIENT_H */
