/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <pakfire/string.h>
#include <pakfire/util.h>

int __pakfire_string_format(char* s, const size_t length, const char* format, ...) {
	va_list args;
	int r;

	// Call __pakfire_string_vformat
	va_start(args, format);
	r = __pakfire_string_vformat(s, length, format, args);
	va_end(args);

	return r;
}

int __pakfire_string_vformat(char* s, const size_t length, const char* format, va_list args) {
	// Write string to buffer
	const ssize_t required = vsnprintf(s, length, format, args);

	// Catch any errors
	if (unlikely(required < 0))
		return -errno;

	// Check if the entire string could be written
	if (unlikely((size_t)required >= length))
		return -ENOBUFS;

	// Success
	return 0;
}

int __pakfire_string_set(char* s, const size_t length, const char* value) {
	// If value is NULL or an empty, we will overwrite the buffer with just zeros
	if (unlikely(!value)) {
		*s = '\0';
		return 0;
	}

	// Copy everything until we hit the end of the input string
	for (unsigned int i = 0; i < length; i++) {
		s[i] = value[i];

		// Done
		if (value[i] == '\0')
			return 0;
	}

	// It seems that the buffer was not large enough. Terminate and return an error.
	s[length - 1] = '\0';

	return -ENOBUFS;
}

int __pakfire_string_setn(char* s, const size_t length, const char* value, const size_t l) {
	// Fail if we don't have enough space
	if (unlikely(l >= length))
		return -ENOBUFS;

	// Copy the data
	if (likely(l > 0))
		memcpy(s, value, l);

	// Terminate the result buffer
	s[l] = '\0';

	return l;
}

int pakfire_string_startswith(const char* s, const char* prefix) {
	// Validate input
	if (!s || !prefix)
		return -EINVAL;

	return !strncmp(s, prefix, strlen(prefix));
}

int pakfire_string_endswith(const char* s, const char* suffix) {
	// Validate input
	if (!s || !suffix)
		return -EINVAL;

	return !strcmp(s + strlen(s) - strlen(suffix), suffix);
}

void pakfire_string_lstrip(char* s) {
	if (!s)
		return;

	size_t l = strlen(s);
	if (!l)
		return;

	// Remove leading space
	while (isspace(s[0]))
		memmove(s, s + 1, l--);
}

void pakfire_string_rstrip(char* s) {
	if (!s)
		return;

	size_t l = strlen(s);

	// Remove trailing space
	while (l > 0 && isspace(s[l - 1]))
		s[l-- - 1] = '\0';
}

void pakfire_string_strip(char* s) {
	// Strip everything on the left side
	pakfire_string_lstrip(s);

	// Strip everything on the right side
	pakfire_string_rstrip(s);
}

void pakfire_string_unquote(char* s) {
	size_t length = 0;

	// Nothing to do
	if (!s || !*s)
		return;

	// Determine the length of the string
	length = strlen(s);

	// Check if the first character is "
	if (s[0] != '"')
		return;

	// Check if the last characters is "
	if (s[length - 1] != '"')
		return;

	// Remove the quotes
	memmove(s, s + 1, length - 2);

	// Terminate the string
	s[length - 2] = '\0';
}

int pakfire_string_matches(const char* s, const char* pattern) {
	// Validate input
	if (!s || !pattern)
		return -EINVAL;

	return !!strstr(s, pattern);
}

void pakfire_string_truncate(char* s, const size_t l) {
	// Determine the length of the string
	size_t length = strlen(s);

	// Return if the string isn't long enough
	if (length < l)
		return;

	// Truncate
	s[l] = '\0';
}

int pakfire_string_partition(const char* s, const char* delim, char** s1, char** s2) {
	char* p = strstr(s, delim);

	// Delim was not found
	if (!p) {
		*s1 = NULL;
		*s2 = NULL;
		return 1;
	}

	// Length of string before delim
	size_t l = p - s;

	char* buffer = malloc(l + 1);
	if (!buffer)
		return 1;

	// Copy first part
	*s1 = memcpy(buffer, s, l);
	buffer[l] = '\0';

	// Copy second part
	*s2 = strdup(p + strlen(delim));

	return 0;
}

static int __pakfire_string_replace(char** buffer, const char* s, const size_t l) {
	int r;

	// Check how long the string is that we are holding
	size_t length = (buffer && *buffer) ? strlen(*buffer) : 0;

	// Resize the buffer
	*buffer = pakfire_realloc(*buffer, length + l + 1);
	if (!*buffer)
		return -errno;

	// Append the new string
	r = snprintf(*buffer + length, l + 1, "%s", s);
	if (r < 0)
		return r;

	return 0;
}

char* pakfire_string_replace(const char* s, const char* pattern, const char* repl) {
	const char* m = NULL;
	char* buffer = NULL;
	size_t l = 0;
	int r;

	// Check inputs
	if (!s || !pattern) {
		errno = EINVAL;
		return NULL;
	}

	// Accept NULL for repl and replace with an empty string
	if (!repl)
		repl = "";

	const size_t pattern_length = strlen(pattern);
	const size_t repl_length = strlen(repl);

	// Walk through the string...
	for (const char* p = s; *p;) {
		// Search for the pattern
		m = strstr(p, pattern);

		if (!m) {
			// Copy the remaining string
			r = __pakfire_string_replace(&buffer, p, strlen(p));
			if (r < 0)
				goto ERROR;

			break;
		}

		// Determine the length of the string up to the match
		l = m - p;

		// Copy the read string up to pattern
		r = __pakfire_string_replace(&buffer, p, l);
		if (r < 0)
			goto ERROR;

		r = __pakfire_string_replace(&buffer, repl, repl_length);
		if (r < 0)
			goto ERROR;

		// Advance p
		p += l + pattern_length;
	}

	// Return the buffer
	return buffer;

ERROR:
	if (buffer)
		free(buffer);

	return NULL;
}

void pakfire_string_remove_linebreaks(char* src) {
	char* dst = src;

	while (*src) {
		// Check for "\" at the end of a line
		if (src[0] == '\\' && src[1] == '\n') {
			// Skip the string
			src += strlen("\\\n");

			// Consume any following whitespace
			while (*src && isspace(*src))
				src++;

		// Otherwise copy the entire string
		} else {
			*dst++ = *src++;
		}
	}

	// Ensure the string is terminated
	*dst = '\0';
}

char* pakfire_string_join(const char** list, const char* delim) {
	// Validate input
	if (!list || !delim) {
		errno = EINVAL;
		return NULL;
	}

	size_t length = 0;
	unsigned int elements = 0;

	// Count the number of elements and the total length
	for (const char** item = list; *item; item++) {
		length += strlen(*item);
		elements++;
	}

	// Empty list?
	if (!elements)
		return NULL;

	// Add the delimiters
	length += strlen(delim) * (elements - 1);

	// Allocate the result string
	char* string = malloc(length + 1);
	if (!string)
		return NULL;

	// Pointer to where we are writing
	char* p = string;

	size_t bytes_left = length + 1;
	size_t bytes_written;

	for (const char** item = list; *item; item++) {
		bytes_written = snprintf(p, bytes_left, "%s", *item);

		bytes_left -= bytes_written;
		p += bytes_written;

		// Write the delimiter
		if (bytes_left) {
			bytes_written = snprintf(p, bytes_left, "%s", delim);

			bytes_left -= bytes_written;
			p += bytes_written;
		}
	}

	return string;
}

/*
	Append the given string to the buffer
*/
int __pakfire_string_append(char* buffer, size_t length, const char* appendix) {
	// Check inputs
	if (!buffer || !appendix)
		return -EINVAL;

	// How long is the existing string?
	const size_t l = strlen(buffer);

	return __pakfire_string_set(buffer + l, length - l, appendix);
}

int __pakfire_string_appendf(char* buffer, size_t length, const char* format, ...) {
	char* appendix = NULL;
	va_list args;
	int r;

	va_start(args, format);
	r = vasprintf(&appendix, format, args);
	va_end(args);

	// Return on error
	if (r < 0)
		return r;

	// Append the string
	r = __pakfire_string_append(buffer, length, appendix);
	free(appendix);

	return r;
}

int pakfire_string_search(const char* haystack, ssize_t lhaystack,
		const char* needle, ssize_t lneedle) {
	// Check inputs
	if (!haystack || !needle || !lhaystack || !lneedle)
		return -EINVAL;

	// Fill lengths
	if (lhaystack < 0)
		lhaystack = strlen(haystack);

	if (lneedle < 0)
		lneedle = strlen(needle);

	// Alphabet size
	const int d = 256;

	// A prime number
	const int q = 101;

	// Hash constant
	int c = 1;

	// Hash value for the haystack
	int h = 0;

	// Hash value for the needle
	int n = 0;

	// Compute the hash constant
	for (int i = 0; i < lneedle - 1; i++)
		c = (c * d) % q;

	// Calculate the hash value of pattern and first window
	for (int i = 0; i < lneedle; i++) {
		h = (d * h + haystack[i]) % q;
		n = (d * n + needle[i]) % q;
    }

	// Slide the text over the pattern...
	for (int i = 0; i <= lhaystack - lneedle; i++) {
		// Check the hash values of the current window.
		// If they match, then check then check if we actually found a match
		if (h == n) {
			// Assume there was a match
			int match = 1;

			for (int j = 0; j < lneedle; j++) {
				// Skip the parts that match
				if (haystack[i + j] == needle[j])
					continue;

				// If a character does not match, we reset and continue
				match = 0;
				break;
			}

			// We found a match and are done
			if (match)
				return i;
		}

		// Calculate the hash value for the next window
		if (i < lhaystack - lneedle) {
			h = (d * (h - haystack[i] * c) + haystack[i + lneedle]) % q;

			// If we have underflowed, we make the hash positive again
			if (h < 0)
				h += q;
		}
	}

	return -1;
}

size_t pakfire_strings_length(char** array) {
	size_t length = 0;

	// If we have received NULL, we will return zero
	if (!array)
		return 0;

	// Count all elements
	for (char** s = array; *s; s++)
		length++;

	return length;
}

void pakfire_strings_free(char** array) {
	for (char** s = array; *s; s++)
		free(*s);

	free(array);
}

int pakfire_strings_contain(char** array, const char* string) {
	int r;

	for (char** s = array; *s; s++) {
		r = pakfire_string_equals(*s, string);
		if (r)
			return r;
	}

	return 0;
}

int pakfire_strings_append(char*** array, const char* s) {
	if (!array)
		return -EINVAL;

	// Copy the string
	char* p = strdup(s);
	if (!p)
		goto ERROR;

	// Fetch the length
	size_t length = pakfire_strings_length(*array);

	// Resize the array
	*array = reallocarray(*array, length + 2, sizeof(**array));
	if (!*array)
		goto ERROR;

	// Store the string
	(*array)[length++] = p;

	// Terminate the array
	(*array)[length] = NULL;

	// Return the total length of the array
	return 0;

ERROR:
	if (p)
		free(p);

	return -errno;
}

int pakfire_strings_appendf(char*** array, const char* format, ...) {
	char* p = NULL;
	va_list args;
	int r;

	// Format the string
	va_start(args, format);
	r = vasprintf(&p, format, args);
	va_end(args);

	// Break on any errors
	if (r < 0)
		return -errno;

	// Append it to the array
	r = pakfire_strings_append(array, p);
	free(p);

	return r;
}

/*
	Copies all strings into the array
*/
int pakfire_strings_appendm(char*** array, const char** strings) {
	int r = 0;

	// Check inputs
	if (!strings)
		return -EINVAL;

	for (const char** s = strings; *s; s++) {
		r = pakfire_strings_append(array, *s);
		if (r < 0)
			return r;
	}

	return r;
}

/*
	This function dumps the string array to the console. This is useful for debugging only.
*/
int pakfire_strings_dump(char** array) {
	int r;

	// Check inputs
	if (!array)
		return -EINVAL;

	// Determine the length of the array
	size_t length = pakfire_strings_length(array);

	// If the array is empty, we return a message
	if (!length) {
		printf("Empty string array\n");
		return 0;
	}

	// Dump the array
	for (unsigned int i = 0; i < length; i++) {
		r = printf("array[%u] : %s\n", i, array[i]);
		if (r < 0)
			return r;
	}

	return 0;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
int __pakfire_format_size(char* dst, size_t length, double value) {
	const char* units[] = {
		"%.0f ",
		"%.0fk",
		"%.1fM",
		"%.1fG",
		"%.1fT",
		NULL
	};
	const char** unit = units;

	while (*(unit + 1) && value >= 1024.0) {
		value /= 1024.0;
		unit++;
	}

	return __pakfire_string_format(dst, length, *unit, value);
}

int __pakfire_format_speed(char* dst, size_t length, double value) {
	const char* units[] = {
		"%4.0fB/s",
		"%4.0fkB/s",
		"%4.1fMB/s",
		"%4.1fGB/s",
		"%4.1fTB/s",
		NULL
	};
	const char** unit = units;

	while (*(unit + 1) && value >= 1024.0) {
		value /= 1024.0;
		unit++;
	}

	return __pakfire_string_format(dst, length, *unit, value);
}

int __pakfire_strftime(char* buffer, const size_t length,
		const char* format, const time_t t) {
	int r;

	struct tm* tm = gmtime(&t);

	// Format time
	r = strftime(buffer, length, format, tm);
	if (r < 0)
		return -errno;

	return 0;
}

int __pakfire_strftime_now(char* buffer, size_t length, const char* format) {
	// Fetch the current time
	const time_t t = time(NULL);
	if (t < 0)
		return -errno;

	return __pakfire_strftime(buffer, length, format, t);
}
#pragma GCC diagnostic pop

int __pakfire_format_time(char* buffer, const size_t length, const time_t t) {
	// Values smaller than zero for t are invalid
	if (t < 0)
		return -EINVAL;

	int d = t / 86400;
	int h = t % 86400 / 3600;
	int m = t % 3600 / 60;
	int s = t % 60;

	if (d)
		return __pakfire_string_format(buffer, length, "%dd%dh%dm", d, h, m);
	else if (h)
		return __pakfire_string_format(buffer, length, "%dh%dm%ds", h, m, s);
	else if (m)
		return __pakfire_string_format(buffer, length, "%dm%ds", m, s);
	else
		return __pakfire_string_format(buffer, length, "%ds", s);
}

int __pakfire_timeval_to_iso8601(char* buffer, const size_t length, const struct timeval* t) {
	struct tm tm = {};

	// Convert to broken down time
	if (!gmtime_r(&t->tv_sec, &tm))
		return -errno;

	return __pakfire_string_format(buffer, length, "%04d-%02d-%02dT%02d:%02d:%02d.%03ldZ",
		tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec,
		US_TO_MS(t->tv_usec));
}

int __pakfire_string_format_interval(char* buffer, size_t length, const time_t t) {
	int r;

	if (t < 0)
		return -EINVAL;

	// Weeks
	else if (t >= 604800)
		r = __pakfire_string_format(buffer, length, "%ldw", t / 604800);

	// Days
	else if (t >= 86400)
		r = __pakfire_string_format(buffer, length, "%ldd", t / 86400);

	// Hours
	else if (t >= 3600)
		r = __pakfire_string_format(buffer, length, "%ldh", t / 3600);

	// Minutes
	else if (t >= 60)
		r = __pakfire_string_format(buffer, length, "%ldm", t / 60);

	// Seconds
	else
		r = __pakfire_string_format(buffer, length, "%lds", t);

	// Handle any errors
	if (r < 0)
		return r;

	return 0;
}

time_t pakfire_string_parse_interval(const char* buffer) {
	char* unit = NULL;

	// Check inputs
	if (!buffer)
		return -1;

	// Try reading some numeric value
	time_t t = strtoul(buffer, &unit, 10);

	// Convert any units
	if (unit && *unit) {
		switch (*unit) {
			// Weeks
			case 'w':
				t *= 604800;
				break;

			// Days
			case 'd':
				t *= 86400;
				break;

			// Hours
			case 'h':
				t *= 3600;
				break;

			// Minutes
			case 'm':
				t *= 60;
				break;

			// Seconds
			case 's':
				break;

			// Unknown unit
			default:
				return -1;
		}

		// Move forward
		unit++;

		// If there are extra characters after the unit we will have to fail
		if (*unit)
			return -1;
	}

	return t;
}

size_t pakfire_string_parse_bytes(const char* s) {
	const char* units = "kMGT";
	char* unit = NULL;

	// Read the numeric value
	size_t bytes = strtoul(s, &unit, 10);

	// Return bytes if there is no unit
	if (!unit || !*unit)
		return bytes;

	// Is the unit of the correct length?
	const size_t length = strlen(unit);
	if (length > 1)
		goto ERROR;

	// Walk through all units and find a match
	for (const char* u = units; *u; u++) {
		bytes *= 1024;

		// End if the unit matches
		if (*unit == *u)
			return bytes;
	}

ERROR:
	errno = EINVAL;
	return 0;
}

int pakfire_string_is_url(const char* s) {
	static const char* known_schemas[] = {
		"https://",
		"http://",
		"file://",
		NULL,
	};

	for (const char** schema = known_schemas; *schema; schema++) {
		if (pakfire_string_startswith(s, *schema))
			return 1;
	}

	return 0;
}
