/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_SOLUTION_H
#define PAKFIRE_SOLUTION_H

// libsolv
#include <solv/pooltypes.h>

struct pakfire_solution;

#include <pakfire/pakfire.h>
#include <pakfire/problem.h>

struct pakfire_solution* pakfire_solution_ref(struct pakfire_solution* solution);
struct pakfire_solution* pakfire_solution_unref(struct pakfire_solution* solution);

const char* pakfire_solution_to_string(struct pakfire_solution* solution);

int pakfire_solution_create(struct pakfire_solution** solution, struct pakfire* pakfire,
	struct pakfire_problem* problem, Id id);
struct pakfire_problem* pakfire_solution_get_problem(struct pakfire_solution* solution);
Id pakfire_solution_get_id(struct pakfire_solution* solution);

#endif /* PAKFIRE_SOLUTION_H */
