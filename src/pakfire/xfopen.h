/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_XFOPEN_H
#define PAKFIRE_XFOPEN_H

#include <stdio.h>

// Automatically detect
FILE* pakfire_xfopen(FILE* f, const char* mode);

// gzip
FILE* pakfire_gzfopen(FILE* f, const char* mode);

// XZ
FILE* pakfire_xzfopen(FILE* f, const char* mode);

// ZSTD
FILE* pakfire_zstdfopen(FILE* f, const char* mode);

#endif /* PAKFIRE_XFOPEN_H */
