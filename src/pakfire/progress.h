/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PROGRESS_H
#define PAKFIRE_PROGRESS_H

#include <time.h>

struct pakfire_progress;

#include <pakfire/ctx.h>

enum pakfire_progress_flags {
	PAKFIRE_PROGRESS_NO_PROGRESS            = (1 << 0),
	PAKFIRE_PROGRESS_SHOW_PERCENTAGE        = (1 << 1),
	PAKFIRE_PROGRESS_SHOW_BYTES_TRANSFERRED = (1 << 2),
	PAKFIRE_PROGRESS_SHOW_COUNTER           = (1 << 3),
	PAKFIRE_PROGRESS_SHOW_ELAPSED_TIME      = (1 << 4),
	PAKFIRE_PROGRESS_SHOW_ETA               = (1 << 5),
	PAKFIRE_PROGRESS_SHOW_TRANSFER_SPEED    = (1 << 6),
};

int pakfire_progress_has_flag(struct pakfire_progress* p, int flag);

/*
	Callbacks
*/
typedef int (*pakfire_progress_start_callback)
	(struct pakfire_ctx* ctx, struct pakfire_progress* progress, void* data, unsigned long int value);
typedef int (*pakfire_progress_finish_callback)
	(struct pakfire_ctx* ctx, struct pakfire_progress* progress, void* data);
typedef int (*pakfire_progress_update_callback)
	(struct pakfire_ctx* ctx, struct pakfire_progress* progress, void* data, unsigned long int value);
typedef void (*pakfire_progress_free_callback)
	(struct pakfire_ctx* ctx, struct pakfire_progress* progress, void* data);

void pakfire_progress_set_callback_data(struct pakfire_progress* p, void* data);

void pakfire_progress_set_start_callback(
	struct pakfire_progress* p, pakfire_progress_start_callback callback);
void pakfire_progress_set_finish_callback(
	struct pakfire_progress* p, pakfire_progress_finish_callback callback);
void pakfire_progress_set_update_callback(
	struct pakfire_progress* p, pakfire_progress_update_callback callback);
void pakfire_progress_set_free_callback(
	struct pakfire_progress* p, pakfire_progress_free_callback callback);

struct pakfire_progress* pakfire_progress_get_parent(struct pakfire_progress* p);
unsigned long int pakfire_progress_get_value(struct pakfire_progress* p);
unsigned long int pakfire_progress_get_max_value(struct pakfire_progress* p);
const char* pakfire_progress_get_title(struct pakfire_progress* p);
double pakfire_progress_get_percentage(struct pakfire_progress* p);
time_t pakfire_progress_get_elapsed_time(struct pakfire_progress* p);
time_t pakfire_progress_get_eta(struct pakfire_progress* p);
double pakfire_progress_get_transfer_speed(struct pakfire_progress* p);

int pakfire_progress_create(struct pakfire_progress** progress,
	struct pakfire_ctx* ctx, int flags, struct pakfire_progress* parent);

struct pakfire_progress* pakfire_progress_ref(struct pakfire_progress* p);
struct pakfire_progress* pakfire_progress_unref(struct pakfire_progress* p);

int pakfire_progress_start(struct pakfire_progress* p, unsigned long int value);
int pakfire_progress_finish(struct pakfire_progress* p);
int pakfire_progress_update(struct pakfire_progress* p, unsigned long int value);
int pakfire_progress_increment(struct pakfire_progress* p, unsigned long int value);
int pakfire_progress_reset(struct pakfire_progress* p);

int pakfire_progress_set_title(struct pakfire_progress* p, const char* format, ...)
	__attribute__((format(printf, 2, 3)));
void pakfire_progress_set_max_value(struct pakfire_progress* p, unsigned long int value);

#endif /* PAKFIRE_PROGRESS_H */
