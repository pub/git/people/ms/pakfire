/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_CTX_H
#define PAKFIRE_CTX_H

struct pakfire_ctx;

#include <pakfire/config.h>
#include <pakfire/logging.h>
#include <pakfire/os.h>
#include <pakfire/pakfire.h>
#include <pakfire/progress.h>
#include <pakfire/transaction.h>

int pakfire_ctx_create(struct pakfire_ctx** ctx, const char* path);

struct pakfire_ctx* pakfire_ctx_ref(struct pakfire_ctx* ctx);
struct pakfire_ctx* pakfire_ctx_unref(struct pakfire_ctx* ctx);

// Flags

enum pakfire_ctx_flags {
	PAKFIRE_CTX_OFFLINE = (1 << 0),
	PAKFIRE_CTX_IN_JAIL = (1 << 1),
};

int pakfire_ctx_has_flag(struct pakfire_ctx* ctx, int flag);
int pakfire_ctx_set_flag(struct pakfire_ctx* ctx, int flag);

// Config

struct pakfire_config* pakfire_ctx_get_config(struct pakfire_ctx* ctx);

// Logging

int pakfire_ctx_get_log_level(struct pakfire_ctx* ctx);
void pakfire_ctx_set_log_level(struct pakfire_ctx* ctx, int level);

void pakfire_ctx_set_log_callback(struct pakfire_ctx* ctx,
	pakfire_log_callback callback, void* data);

// Paths

const char* pakfire_ctx_get_cache_path(struct pakfire_ctx* ctx);
int pakfire_ctx_set_cache_path(struct pakfire_ctx* ctx, const char* path);

// Confirm

typedef int (*pakfire_confirm_callback)(struct pakfire_ctx* ctx, struct pakfire* pakfire,
	void* data, const char* message, const char* question);

void pakfire_ctx_set_confirm_callback(struct pakfire_ctx* ctx,
	pakfire_confirm_callback callback, void* data);

// Progress

typedef int (*pakfire_progress_callback)(struct pakfire_ctx* ctx,
	void* data, struct pakfire_progress* progress);

void pakfire_ctx_set_progress_callback(struct pakfire_ctx* ctx,
	pakfire_progress_callback callback, void* data);

// Pick Solution

typedef int (*pakfire_pick_solution_callback)(struct pakfire_ctx* ctx,
	struct pakfire* pakfire, void* data, struct pakfire_transaction* transaction);

void pakfire_ctx_set_pick_solution_callback(struct pakfire_ctx* ctx,
	pakfire_pick_solution_callback callback, void* data);

// Distro
const struct pakfire_distro* pakfire_ctx_get_distro(struct pakfire_ctx* ctx);

// Logging

void pakfire_ctx_log(struct pakfire_ctx* ctx, int level, const char* file, int line,
	const char* fn, const char* format, ...) __attribute__((format(printf, 6, 7)));

// Confirm

int pakfire_ctx_confirm(struct pakfire_ctx* ctx, struct pakfire* pakfire,
	const char* message, const char* question);

// Progress

int pakfire_ctx_setup_progress(struct pakfire_ctx* ctx, struct pakfire_progress* progress);

// Pick Solution

int pakfire_ctx_pick_solution(struct pakfire_ctx* ctx, struct pakfire* pakfire,
	struct pakfire_transaction* transaction);

// cURL

#include <curl/curl.h>

CURLSH* pakfire_ctx_curl_share(struct pakfire_ctx* ctx);

// Magic

#include <magic.h>

magic_t pakfire_ctx_magic(struct pakfire_ctx* ctx);

#endif /* PAKFIRE_CTX_H */
