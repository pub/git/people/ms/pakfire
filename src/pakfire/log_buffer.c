/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <string.h>
#include <sys/queue.h>
#include <sys/time.h>

#include <pakfire/ctx.h>
#include <pakfire/log_buffer.h>

struct pakfire_log_line {
	STAILQ_ENTRY(pakfire_log_line) nodes;

	struct timeval timestamp;
	int priority;
	char* line;
	size_t length;
};

struct pakfire_log_buffer {
	struct pakfire_ctx* ctx;
	int nrefs;

	STAILQ_HEAD(lines, pakfire_log_line) lines;

	size_t length;
	size_t max_length;
};

static void pakfire_log_line_free(struct pakfire_log_line* line) {
	if (line->line)
		free(line->line);
	free(line);
}

static void pakfire_log_buffer_free(struct pakfire_log_buffer* buffer) {
	struct pakfire_log_line* line = NULL;

	// Free all entries
	while (!STAILQ_EMPTY(&buffer->lines)) {
		line = STAILQ_FIRST(&buffer->lines);
		STAILQ_REMOVE_HEAD(&buffer->lines, nodes);

		pakfire_log_line_free(line);
	}

	if (buffer->ctx)
		pakfire_ctx_unref(buffer->ctx);

	free(buffer);
}

int pakfire_log_buffer_create(struct pakfire_log_buffer** buffer, struct pakfire_ctx* ctx, size_t max_length) {
	struct pakfire_log_buffer* b = NULL;

	// Allocate a new object
	b = calloc(1, sizeof(*b));
	if (!b)
		return -errno;

	// Initialize the reference counter
	b->nrefs = 1;

	// Store a reference to the context
	b->ctx = pakfire_ctx_ref(ctx);

	// Store the maximum length
	b->max_length = max_length;

	// Initialize lines
	STAILQ_INIT(&b->lines);

	// Return the pointer
	*buffer = b;

	return 0;
}

struct pakfire_log_buffer* pakfire_log_buffer_ref(struct pakfire_log_buffer* buffer) {
	++buffer->nrefs;

	return buffer;
}

struct pakfire_log_buffer* pakfire_log_buffer_unref(struct pakfire_log_buffer* buffer) {
	if (--buffer->nrefs > 0)
		return buffer;

	pakfire_log_buffer_free(buffer);
	return NULL;
}

static int pakfire_log_buffer_drop(struct pakfire_log_buffer* self) {
	struct timeval timestamp = {};
	int priority = 0;
	char* line = NULL;
	int r;

	// Dequeue one line
	r = pakfire_log_buffer_dequeue(self, &timestamp, &priority, &line, NULL);
	if (r < 0)
		return r;

	// Free the line
	free(line);

	return 0;
}

int pakfire_log_buffer_enqueue(struct pakfire_log_buffer* buffer, int priority, const char* line, ssize_t length) {
	struct pakfire_log_line* l = NULL;
	int r;

	// Check input
	if (priority <= 0 || !line)
		return -EINVAL;

	// Drop a previously inserted message if the buffer is full
	while ((buffer->max_length > 0) && (buffer->length >= buffer->max_length)) {
		r = pakfire_log_buffer_drop(buffer);
		if (r < 0)
			return r;
	}

	// Automatically determine the length
	if (length < 0)
		length = strlen(line);

	// Allocate a new line
	l = calloc(1, sizeof(*l));
	if (!l)
		return -errno;

	// Store the current time
	r = gettimeofday(&l->timestamp, NULL);
	if (r < 0) {
		ERROR(buffer->ctx, "Could not determine the current time: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Store the priority
	l->priority = priority;

	// Store the line
	l->line = strndup(line, length);
	if (!l->line) {
		ERROR(buffer->ctx, "Failed to allocate memory: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Store the length
	l->length = length;

	// Append to the queue
	STAILQ_INSERT_TAIL(&buffer->lines, l, nodes);

	// The buffer is now longer
	buffer->length++;

	return 0;

ERROR:
	if (l)
		pakfire_log_line_free(l);

	return r;
}

int pakfire_log_buffer_dequeue(struct pakfire_log_buffer* buffer,
		struct timeval* timestamp, int* priority, char** line, size_t* length) {
	struct pakfire_log_line* l = NULL;

	// Timestamp, priority & line must be set, length is optional
	if (!timestamp || !priority || !line)
		return -EINVAL;

	// Fetch the first line
	l = STAILQ_FIRST(&buffer->lines);
	if (!l) {
		// Reset all values
		(*timestamp).tv_sec = (*timestamp).tv_usec = 0;
		*priority = -1;
		*line = NULL;

		if (length)
			*length = 0;

		return 0;
	}

	// Return the timestamp
	*timestamp = l->timestamp;

	// Return the priority
	*priority = l->priority;

	// Return the line
	*line = l->line;

	// Reset so we won't free the buffer
	l->line = NULL;

	// Return the length
	if (length)
		*length = l->length;

	// Remove the line
	STAILQ_REMOVE_HEAD(&buffer->lines, nodes);

	// The buffer is now shorter
	buffer->length--;

	// Free the line
	pakfire_log_line_free(l);

	return 0;
}

int pakfire_log_buffer_dump(struct pakfire_log_buffer* buffer, char** result, size_t* length) {
	struct pakfire_log_line* line = NULL;
	char* s = NULL;
	size_t l = 0;

	// Check inputs
	if (!result || !length)
		return -EINVAL;

	// Compute the total length of the buffer that we will need
	STAILQ_FOREACH(line, &buffer->lines, nodes) {
		l += line->length;
	}

	// Nothing to do if the buffer is empty
	if (!l)
		return 0;

	// Allocate a buffer that can fit the entire content
	s = malloc(l);
	if (!s)
		return -errno;

	char* p = s;

	// Copy the entire content into the buffer
	STAILQ_FOREACH(line, &buffer->lines, nodes) {
		memcpy(p, line->line, line->length);

		p += line->length;
	}

	// Return the buffer
	*result = s;
	*length = l;

	return 0;
}
