/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2014 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>

// libarchive
#include <archive.h>

#include <pakfire/ctx.h>
#include <pakfire/file.h>
#include <pakfire/filelist.h>
#include <pakfire/i18n.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/path.h>
#include <pakfire/progress.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

struct pakfire_filelist {
	struct pakfire_ctx* ctx;
	struct pakfire* pakfire;
	int nrefs;

	struct pakfire_file** files;
	unsigned int num_files;
};

int pakfire_filelist_create(struct pakfire_filelist** list, struct pakfire* pakfire) {
	struct pakfire_filelist* l = NULL;

	// Allocate a new object
	l = calloc(1, sizeof(*l));
	if (!l)
		return -errno;

	// Store a reference to the context
	l->ctx = pakfire_ctx(pakfire);

	// Store a reference to Pakfire
	l->pakfire = pakfire_ref(pakfire);

	// Initialize the reference counter
	l->nrefs = 1;

	// Return the pointer
	*list = l;

	return 0;
}

static void pakfire_filelist_free(struct pakfire_filelist* list) {
	pakfire_filelist_clear(list);
	if (list->pakfire)
		pakfire_unref(list->pakfire);
	if (list->ctx)
		pakfire_ctx_unref(list->ctx);
	free(list);
}

struct pakfire_filelist* pakfire_filelist_ref(struct pakfire_filelist* list) {
	list->nrefs++;

	return list;
}

struct pakfire_filelist* pakfire_filelist_unref(struct pakfire_filelist* list) {
	if (--list->nrefs > 0)
		return list;

	pakfire_filelist_free(list);
	return NULL;
}

size_t pakfire_filelist_length(struct pakfire_filelist* list) {
	return list->num_files;
}

size_t pakfire_filelist_total_size(struct pakfire_filelist* list) {
	size_t size = 0;

	for (unsigned int i = 0; i < list->num_files; i++)
		size += pakfire_file_get_size(list->files[i]);

	return size;
}

int pakfire_filelist_is_empty(struct pakfire_filelist* list) {
	return list->num_files == 0;
}

void pakfire_filelist_clear(struct pakfire_filelist* list) {
	if (list->files) {
		for (unsigned int i = 0; i < list->num_files; i++)
			pakfire_file_unref(list->files[i]);

		// Free the array
		free(list->files);
		list->files = NULL;

		// Reset number of files on the list
		list->num_files = 0;
	}
}

struct pakfire_file* pakfire_filelist_get(struct pakfire_filelist* list, size_t index) {
	// Check that index is in range
	if (index >= list->num_files) {
		errno = ERANGE;
		return NULL;
	}

	return pakfire_file_ref(list->files[index]);
}

/*
	Checks if the file is already on the list
*/
static int pakfire_filelist_has_file(struct pakfire_filelist* list, struct pakfire_file* file) {
	for (unsigned int i = 0; i < list->num_files; i++) {
		if (list->files[i] == file)
			return i;
	}

	// Not found
	return -1;
}

static int pakfire_filelist_search(struct pakfire_filelist* list, struct pakfire_file* file) {
	int i;
	int r;

	// Set starting points
	int lo = 0;
	int hi = list->num_files;

	while (lo < hi) {
		// Find the middle
		i = (lo + hi) / 2;

		// Compare the files
		r = pakfire_file_cmp(list->files[i], file);

		// If the file is greater than the middle, we only need to search in the left half
		if (r >= 0)
			hi = i;

		// Otherwise we need to search in the right half
		else
			lo = i + 1;
	}

	return lo;
}

int pakfire_filelist_add(struct pakfire_filelist* list, struct pakfire_file* file) {
	// Do not do anything if the file is already on the list
	if (pakfire_filelist_has_file(list, file) >= 0)
		return 0;

	// Find the index where the file should go
	int pos = pakfire_filelist_search(list, file);

	// Make space
	list->files = reallocarray(list->files, list->num_files + 1, sizeof(*list->files));
	if (!list->files) {
		ERROR(list->ctx, "Could not allocate filelist: %m\n");
		return -errno;
	}

	// Move everything up one slot
	for (int i = list->num_files; i > pos; i--)
		list->files[i] = list->files[i - 1];

	// Store the file
	list->files[pos] = pakfire_file_ref(file);

	// The list is now longer
	++list->num_files;

	return 0;
}

static int pakfire_filelist_remove(struct pakfire_filelist* list, struct pakfire_file* file) {
	// Check if the file is on the list
	int pos = pakfire_filelist_has_file(list, file);

	// Do nothing if the file is not on the list
	if (pos < 0)
		return 0;

	// Remove the reference
	pakfire_file_unref(list->files[pos]);

	// Fill the gap
	for (unsigned int i = pos; i < list->num_files - 1; i++)
		list->files[i] = list->files[i + 1];

	// The list is now shorter
	--list->num_files;

	// Downsize the array
	list->files = reallocarray(list->files, list->num_files, sizeof(*list->files));
	if (!list->files) {
		ERROR(list->ctx, "Could not shrink the filelist: %m\n");
		return -errno;
	}

	return 0;
}

static int __pakfire_filelist_remove_one(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	struct pakfire_filelist* list = data;

	// Remove the file from the given filelist
	return pakfire_filelist_remove(list, file);
}

int pakfire_filelist_remove_all(
		struct pakfire_filelist* list, struct pakfire_filelist* removees) {
	return pakfire_filelist_walk(removees, __pakfire_filelist_remove_one, list, 0, NULL);
}

static int pakfire_filelist_match_patterns(const char* path,
		const char** patterns, const int flags) {
	char buffer[PATH_MAX];
	int r;

	// Check inputs
	if (!path || !patterns)
		return -EINVAL;

	for (const char** pattern = patterns; *pattern; pattern++) {
		// Try to find a simple match
		if (pakfire_path_match(*pattern, path))
			return 1;

		// If requested perform extended matching
		if (flags & PAKFIRE_FILELIST_EXTENDED_MATCHING) {
			// / matches everything
			if (strcmp(*pattern, "/") == 0)
				return 1;

			// Try to match any sub-directories and files
			r = pakfire_string_format(buffer, "%s/**", *pattern);
			if (r < 0)
				return r;

			if (pakfire_path_match(buffer, path))
				return 1;
		}
	}

	// No match
	return 0;
}

struct pakfire_filelist_matches {
	const char* root;
	const char** includes;
	const char** excludes;
	const int flags;
};

static int pakfire_filelist_scan_filter(struct archive* archive, void* p,
		struct archive_entry* entry) {
	const struct pakfire_filelist_matches* matches = p;

	// Descend if possible
	if (archive_read_disk_can_descend(archive))
		archive_read_disk_descend(archive);

	// Fetch the path
	const char* path = archive_entry_pathname(entry);

	// Make the path relative to the root
	path = pakfire_path_relpath(matches->root, path);

	// Skip the root
	if (!path || !*path)
		return 0;

	// Should we skip any directories?
	if (matches->flags & PAKFIRE_FILELIST_NO_DIRECTORIES) {
		mode_t filetype = archive_entry_filetype(entry);

		if (filetype == S_IFDIR)
			return 0;
	}

	// Skip excludes
	if (matches->excludes &&
			pakfire_filelist_match_patterns(path, matches->excludes, matches->flags))
		return 0;

	// Skip what is not included
	if (matches->includes &&
			!pakfire_filelist_match_patterns(path, matches->includes, matches->flags))
		return 0;

	// Store the new path
	archive_entry_set_pathname(entry, path);

	return 1;
}

int pakfire_filelist_scan(struct pakfire_filelist* list, const char* root,
		const char** includes, const char** excludes, int flags) {
	struct archive* reader = NULL;
	struct pakfire_file* file = NULL;
	struct archive_entry* entry = NULL;
	struct pakfire_filelist_matches matches = {
		.root     = root,
		.includes = includes,
		.excludes = excludes,
		.flags    = flags,
	};
	int r = 1;

	// Root must be absolute
	if (!pakfire_path_is_absolute(root)) {
		errno = EINVAL;
		return 1;
	}

	DEBUG(list->ctx, "Scanning %s...\n", root);

	if (includes) {
		DEBUG(list->ctx, "  Includes:\n");

		for (const char** include = includes; *include; include++)
			DEBUG(list->ctx, "    %s\n", *include);
	}

	if (excludes) {
		DEBUG(list->ctx, "  Excludes:\n");

		for (const char** exclude = excludes; *exclude; exclude++)
			DEBUG(list->ctx, "    %s\n", *exclude);
	}

	// Check if the path exists
	if (!pakfire_path_exists(root)) {
		DEBUG(list->ctx, "Path to scan (%s) does not exist\n", root);
		r = 0;
		goto ERROR;
	}

	// Create a new disk reader
	reader = pakfire_get_disk_reader(list->pakfire);
	if (!reader)
		goto ERROR;

	// Start reading from here
	r = archive_read_disk_open(reader, root);
	if (r) {
		ERROR(list->ctx, "Could not open %s: %s\n",
			root, archive_error_string(reader));
		goto ERROR;
	}

	// Configure filter function
	r = archive_read_disk_set_metadata_filter_callback(reader,
		pakfire_filelist_scan_filter, &matches);
	if (r) {
		ERROR(list->ctx, "Could not set filter callback: %s\n",
			archive_error_string(reader));
		goto ERROR;
	}

	// Walk through all files
	for (;;) {
		r = archive_read_next_header(reader, &entry);

		// Handle the return code
		switch (r) {
			// Fall through if everything is okay
			case ARCHIVE_OK:
				break;

			// Return OK when we reached the end of the archive
			case ARCHIVE_EOF:
				r = 0;
				goto ERROR;

			// Raise any other errors
			default:
				ERROR(list->ctx, "Could not read next file: %s\n",
					archive_error_string(reader));
				goto ERROR;
		}

		// Create file
		r = pakfire_file_create_from_archive_entry(&file, list->pakfire, entry);
		if (r)
			goto ERROR;

		// Append it to the list
		r = pakfire_filelist_add(list, file);
		if (r) {
			pakfire_file_unref(file);
			goto ERROR;
		}

		pakfire_file_unref(file);
	}

ERROR:
	if (reader)
		archive_read_free(reader);

	return r;
}

static int pakfire_filelist_contains_path(
		struct pakfire_filelist* list, const char* path) {
	const char* p = NULL;
	int i;
	int r;

	// Set starting points
	int lo = 0;
	int hi = list->num_files - 1;

	while (lo <= hi) {
		// Find the middle
		i = lo + (hi - lo) / 2;

		// Fetch the package path
		p = pakfire_file_get_path(list->files[i]);
		if (!p)
			return -EINVAL;

		// Compare the paths
		r = strcmp(p, path);

		// We found a match
		if (r == 0)
			return 1;

		// Search the right half
		else if (r < 0)
			lo = i + 1;

		// Search the left half
		else if (r > 0)
			hi = i - 1;
	}

	// No match
	return 0;
}

static int pakfire_filelist_contains_pattern(
		struct pakfire_filelist* list, const char* pattern) {
	for (unsigned int i = 0; i < list->num_files; i++) {
		const char* path = pakfire_file_get_path(list->files[i]);
		if (!path)
			return -errno;

		if (pakfire_path_match(pattern, path))
			return 1;
	}

	return 0;
}

int pakfire_filelist_contains(struct pakfire_filelist* list, const char* pattern) {
	if (!pattern)
		return -EINVAL;

	// If this is a pattern, we perform a naive search
	if (pakfire_path_is_pattern(pattern))
		return pakfire_filelist_contains_pattern(list, pattern);

	// Otherwise we perform a binary search to be faster
	return pakfire_filelist_contains_path(list, pattern);
}

int pakfire_filelist_walk(struct pakfire_filelist* list,
		pakfire_filelist_walk_callback callback, void* data, int flags, const char* title) {
	struct pakfire_progress* progress = NULL;
	int r = 0;

	// Show progress when iterating over the filelist
	if (flags & PAKFIRE_FILELIST_SHOW_PROGRESS) {
		r = pakfire_progress_create(&progress, list->ctx,
			PAKFIRE_PROGRESS_SHOW_PERCENTAGE|PAKFIRE_PROGRESS_SHOW_ETA, NULL);
		if (r)
			goto ERROR;

		// Set a default title
		if (!title)
			title = _("Processing Files...");

		// Set title
		r = pakfire_progress_set_title(progress, "%s", title);
		if (r)
			goto ERROR;

		const size_t length = pakfire_filelist_length(list);

		// Start progress
		r = pakfire_progress_start(progress, length);
		if (r)
			goto ERROR;
	}

	// Call the callback once for every element on the list
	for (unsigned int i = 0; i < list->num_files; i++) {
		// Call the callback
		r = callback(list->ctx, list->files[i], data);
		if (r)
			goto ERROR;

		// Increment the progress
		if (progress)
			pakfire_progress_increment(progress, 1);
	}

	// Done!
	if (progress) {
		r = pakfire_progress_finish(progress);
		if (r)
			goto ERROR;
	}

ERROR:
	// Finish progress
	if (progress)
		pakfire_progress_unref(progress);

	return r;
}

static int __pakfire_filelist_dump(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	int* flags = data;

	char* s = pakfire_file_dump(file, *flags);
	if (s) {
		INFO(ctx, "%s\n", s);
		free(s);
	}

	return 0;
}

int pakfire_filelist_dump(struct pakfire_filelist* list, int flags) {
	return pakfire_filelist_walk(list, __pakfire_filelist_dump, &flags, 0, NULL);
}

/*
	Verifies all files on the filelist
*/
int pakfire_filelist_verify(struct pakfire_filelist* list, struct pakfire_filelist* errors) {
	struct pakfire_progress* progress = NULL;
	int status;
	int r;

	const size_t length = pakfire_filelist_length(list);

	DEBUG(list->ctx, "Verifying filelist (%zu file(s))...\n", length);

	// Setup progress
	r = pakfire_progress_create(&progress, list->ctx,
		PAKFIRE_PROGRESS_SHOW_PERCENTAGE|PAKFIRE_PROGRESS_SHOW_ETA, NULL);
	if (r)
		goto ERROR;

	// Add title
	r = pakfire_progress_set_title(progress, "%s", _("Verifying Files..."));
	if (r)
		goto ERROR;

	// Start the progress indicator
	r = pakfire_progress_start(progress, length);
	if (r)
		goto ERROR;

	// Iterate over the entire list
	for (unsigned int i = 0; i < list->num_files; i++) {
		// Verify the file
		r = pakfire_file_verify(list->files[i], &status);
		if (r)
			goto ERROR;

		// If the verification failed, we append it to the errors list
		if (status) {
			// Append the file to the error list
			r = pakfire_filelist_add(errors, list->files[i]);
			if (r)
				goto ERROR;
		}

		// Increment progress
		r = pakfire_progress_increment(progress, 1);
		if (r)
			goto ERROR;
	}

	// Finish
	r = pakfire_progress_finish(progress);
	if (r)
		goto ERROR;

ERROR:
	if (progress)
		pakfire_progress_unref(progress);

	return r;
}

int pakfire_filelist_cleanup(struct pakfire_filelist* list, int flags) {
	int r;

	// Nothing to do if the filelist is empty
	if (pakfire_filelist_is_empty(list))
		return 0;

	// Walk through the list backwards
	for (int i = list->num_files - 1; i >= 0; i--) {
		r = pakfire_file_cleanup(list->files[i], flags);
		if (r)
			return r;
	}

	return 0;
}

static int __pakfire_filelist_matches_class(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	int* class = data;

	return pakfire_file_matches_class(file, *class);
}

/*
	Returns true if any file on the list matches class
*/
int pakfire_filelist_matches_class(struct pakfire_filelist* list, int class) {
	return pakfire_filelist_walk(list, __pakfire_filelist_matches_class, &class, 0, NULL);
}
