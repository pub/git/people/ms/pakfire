/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_TRANSACTION_H
#define PAKFIRE_TRANSACTION_H

// libsolv
#include <solv/solver.h>

struct pakfire_transaction;

#include <pakfire/key.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/problem.h>
#include <pakfire/solution.h>

enum pakfire_transaction_flags {
	PAKFIRE_TRANSACTION_WITHOUT_RECOMMENDED = 1 << 1,
	PAKFIRE_TRANSACTION_ALLOW_UNINSTALL     = 1 << 2,
	PAKFIRE_TRANSACTION_ALLOW_DOWNGRADE     = 1 << 3,
};

enum pakfire_transaction_solve_flags {
	PAKFIRE_SOLVE_SHOW_SOLUTIONS            = 1 << 0,
};

enum pakfire_job_action {
	PAKFIRE_JOB_INSTALL,
	PAKFIRE_JOB_ERASE,
	PAKFIRE_JOB_UPDATE,
	PAKFIRE_JOB_UPDATE_ALL,
	PAKFIRE_JOB_SYNC,
	PAKFIRE_JOB_LOCK,
	PAKFIRE_JOB_VERIFY,
};

enum pakfire_job_flags {
	PAKFIRE_JOB_KEEP_DEPS           = 1 << 0,
	PAKFIRE_JOB_KEEP_ORPHANED       = 1 << 1,
	PAKFIRE_JOB_ESSENTIAL           = 1 << 2,
	PAKFIRE_JOB_BEST                = 1 << 3,
};

int pakfire_transaction_create(struct pakfire_transaction** transaction,
	struct pakfire* pakfire, int flags);

struct pakfire_transaction* pakfire_transaction_ref(struct pakfire_transaction* transaction);
struct pakfire_transaction* pakfire_transaction_unref(struct pakfire_transaction* transaction);

int pakfire_transaction_solve(struct pakfire_transaction* transaction, int flags, char** problems);

int pakfire_transaction_request(struct pakfire_transaction* transaction,
	const enum pakfire_job_action action, const char* what, int flags);
int pakfire_transaction_request_package(struct pakfire_transaction* transaction,
	const enum pakfire_job_action action, struct pakfire_package* package, int flags);

struct pakfire_problem** pakfire_transaction_get_problems(struct pakfire_transaction* transaction);
int pakfire_transaction_take_solution(
	struct pakfire_transaction* transaction, struct pakfire_solution* solution);

size_t pakfire_transaction_count(struct pakfire_transaction* transaction);
char* pakfire_transaction_dump(struct pakfire_transaction* transaction, size_t width);

int pakfire_transaction_run(struct pakfire_transaction* transaction);

int pakfire_transaction_download(struct pakfire_transaction* transaction);

// XXX needs removal
enum pakfire_request_solve_flags {
	PAKFIRE_REQ_SOLVE_INTERACTIVE,
};

Solver* pakfire_transaction_get_solver(struct pakfire_transaction* transaction);

int pakfire_transaction_compose_repo(struct pakfire_transaction* transaction,
	struct pakfire_key* key, const char* path);

#endif /* PAKFIRE_TRANSACTION_H */
