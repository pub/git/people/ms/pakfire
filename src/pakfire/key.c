/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

// OpenSSL
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rand.h>

#include <pakfire/base64.h>
#include <pakfire/constants.h>
#include <pakfire/ctx.h>
#include <pakfire/i18n.h>
#include <pakfire/key.h>
#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

// The maximum length of the comment
#define COMMENT_MAX		1024

// Size of the buffer to allocate for error messages
#define ERROR_MAX		1024

struct pakfire_key {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Algorithm
	pakfire_key_algo_t algo;

	// Key ID
	pakfire_key_id id;

	// The public (or private) key
	EVP_PKEY* pkey;

	// Comment
	char comment[COMMENT_MAX];
};

struct pakfire_key_public_key {
	unsigned char sig_algo[2];
	pakfire_key_id id;
	unsigned char pubkey[32];
};

struct pakfire_key_private_key {
	unsigned char sig_algo[2];
    unsigned char kdf_algo[2];
	uint32_t kdf_rounds;
    unsigned char kdf_salt[16];
	unsigned char checksum[8];
	pakfire_key_id id;
	struct {
		unsigned char secret[32];
		unsigned char public[32];
	} keys;
};

struct pakfire_key_signature {
	unsigned char sig_algo[2];
	pakfire_key_id key_id;
	unsigned char signature[64];
};

static int pakfire_key_id_equals(const pakfire_key_id* id1, const pakfire_key_id* id2) {
	return !memcmp(*id1, *id2, sizeof(*id1));
}

static int pakfire_key_create(struct pakfire_key** key, struct pakfire_ctx* ctx,
		const pakfire_key_algo_t algo, const pakfire_key_id id, EVP_PKEY* pkey, const char* comment) {
	struct pakfire_key* k = NULL;
	int r = 0;

	if (!pkey)
		return -EINVAL;

	// Allocate memory
	k = calloc(1, sizeof(*k));
	if (!k)
		return -errno;

	// Store a reference to the context
	k->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	k->nrefs = 1;

	// Store the algorithm
	switch (algo) {
		case PAKFIRE_KEY_ALGO_ED25519:
			k->algo = algo;
			break;

		default:
			ERROR(k->ctx, "Unsupported key algorithm %u\n", algo);
			r = -ENOTSUP;
			goto ERROR;
	}

	// Store the key ID
	memcpy(k->id, id, sizeof(k->id));

	// Keep a reference to this key
	EVP_PKEY_up_ref(pkey);
	k->pkey = pkey;

	// Store the comment
	if (comment) {
		r = pakfire_string_set(k->comment, comment);
		if (r < 0)
			goto ERROR;

		// Remove any trailing newline
		pakfire_string_rstrip(k->comment);
	}

	// Return the pointer
	*key = pakfire_key_ref(k);

ERROR:
	if (k)
		pakfire_key_unref(k);

	return r;
}

static void pakfire_key_free(struct pakfire_key* key) {
	// Free the key
	if (key->pkey)
		EVP_PKEY_free(key->pkey);
	if (key->ctx)
		pakfire_ctx_unref(key->ctx);
	free(key);
}

struct pakfire_key* pakfire_key_ref(struct pakfire_key* key) {
	++key->nrefs;

	return key;
}

struct pakfire_key* pakfire_key_unref(struct pakfire_key* key) {
	if (--key->nrefs > 0)
		return key;

	pakfire_key_free(key);
	return NULL;
}

pakfire_key_id* pakfire_key_get_id(struct pakfire_key* key) {
	return &key->id;
}

const char* pakfire_key_get_algo(struct pakfire_key* key) {
	switch (key->algo) {
		case PAKFIRE_KEY_ALGO_ED25519:
			return "Ed255919";

		case PAKFIRE_KEY_ALGO_NULL:
			break;
	}

	return NULL;
}

const char* pakfire_key_get_comment(struct pakfire_key* key) {
	return key->comment;
}

int pakfire_key_generate(struct pakfire_key** key, struct pakfire_ctx* ctx,
		const pakfire_key_algo_t algo, const char* comment) {
	EVP_PKEY* pkey = NULL;
	EVP_PKEY_CTX* pctx = NULL;
	pakfire_key_id key_id;
	char error[ERROR_MAX];
	int id;
	int r;

	switch (algo) {
		case PAKFIRE_KEY_ALGO_ED25519:
			id = EVP_PKEY_ED25519;
			break;

		default:
			ERROR(ctx, "Invalid key algorithm %u\n", algo);
			return -EINVAL;
	}

	// Generate a random key ID
	r = RAND_bytes((unsigned char*)&key_id, sizeof(key_id));
	if (r < 0) {
		ERROR(ctx, "Could not generate the key ID\n");
		r = -EINVAL;
		goto ERROR;
	}

	// Setup the context
	pctx = EVP_PKEY_CTX_new_id(id, NULL);
	if (!pctx) {
		ERROR(ctx, "Could not allocate the OpenSSL context: %m\n");
		r = -ENOMEM;
		goto ERROR;
	}

	// Prepare the context for key generation
	r = EVP_PKEY_keygen_init(pctx);
	if (r < 1) {
		ERR_error_string_n(r, error, sizeof(error));

		ERROR(ctx, "Could not prepare the context: %s\n", error);
		r = -EINVAL;
		goto ERROR;
	}

	// Generate the key
	r = EVP_PKEY_keygen(pctx, &pkey);
	if (r < 1) {
		ERR_error_string_n(r, error, sizeof(error));

		ERROR(ctx, "Could not generate the key: %s\n", error);
		r = -EINVAL;
		goto ERROR;
	}

	// Create a key object
	r = pakfire_key_create(key, ctx, algo, key_id, pkey, comment);
	if (r < 0)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	if (pctx)
		EVP_PKEY_CTX_free(pctx);
	if (pkey)
		EVP_PKEY_free(pkey);

	return r;
}

/*
	Import
*/

static int pakfire_key_import_secret_key(struct pakfire_key** key,
		struct pakfire_ctx* ctx, const char* comment,
		const struct pakfire_key_private_key* buffer) {
	const pakfire_key_algo_t algo = PAKFIRE_KEY_ALGO_ED25519;
	EVP_PKEY* pkey = NULL;
	char error[ERROR_MAX];
	int r;

	unsigned char checksum[64];
	unsigned int length = sizeof(checksum);

	// Check for input
	if (!buffer) {
		errno = EINVAL;
		return 1;
	}

	// Check the signature algorithm
	if (buffer->sig_algo[0] != 'E' || buffer->sig_algo[1] != 'd') {
		ERROR(ctx, "Unsupported signature algorithm\n");
		r = -ENOTSUP;
		goto ERROR;
	}

	// Check the KDF algorithm
	if (buffer->kdf_algo[0] != 'B' || buffer->kdf_algo[1] != 'K') {
		ERROR(ctx, "Unsupported KDF algorithm\n");
		r = -ENOTSUP;
		goto ERROR;
	}

	// We don't support encrypted keys here
	if (buffer->kdf_rounds) {
		ERROR(ctx, "Encrypted keys are not supported\n");
		r = -ENOTSUP;
		goto ERROR;
	}

	// Compute a SHA512 checksum over the key material
	r = EVP_Digest(&buffer->keys, sizeof(buffer->keys), checksum, &length, EVP_sha512(), NULL);
	if (r < 0) {
		ERROR(ctx, "Could not compute the checksum: %m\n");
		goto ERROR;
	}

	// Compare the checksum
	if (memcmp(buffer->checksum, checksum, sizeof(buffer->checksum)) != 0) {
		ERROR(ctx, "Checksum mismatch\n");
		r = -EINVAL;
		goto ERROR;
	}

	// Load the private key
	pkey = EVP_PKEY_new_raw_private_key(EVP_PKEY_ED25519, NULL,
		buffer->keys.secret, sizeof(buffer->keys.secret));
	if (!pkey) {
		ERR_error_string_n(ERR_get_error(), error, sizeof(error));

		ERROR(ctx, "Could not load secret key: %s\n", error);
		r = -EINVAL;
		goto ERROR;
	}

	// Create a new key object
	r = pakfire_key_create(key, ctx, algo, buffer->id, pkey, comment);
	if (r < 0)
		goto ERROR;

ERROR:
	if (pkey)
		EVP_PKEY_free(pkey);

	return r;
}

static int pakfire_key_import_public_key(struct pakfire_key** key,
		struct pakfire_ctx* ctx, const char* comment,
		const struct pakfire_key_public_key* buffer) {
	const pakfire_key_algo_t algo = PAKFIRE_KEY_ALGO_ED25519;
	EVP_PKEY* pkey = NULL;
	char error[ERROR_MAX];
	int r;

	// Check for input
	if (!buffer)
		return -EINVAL;

	// Check the signature algorithm
	if (buffer->sig_algo[0] != 'E' || buffer->sig_algo[1] != 'd') {
		ERROR(ctx, "Unsupported signature algorithm\n");
		r = -ENOTSUP;
		goto ERROR;
	}

	// Load the public key
	pkey = EVP_PKEY_new_raw_public_key(EVP_PKEY_ED25519, NULL,
		buffer->pubkey, sizeof(buffer->pubkey));
	if (!pkey) {
		ERR_error_string_n(ERR_get_error(), error, sizeof(error));

		ERROR(ctx, "Could not load public key: %s\n", error);
		r = -EINVAL;
		goto ERROR;
	}

	// Create a new key object
	r = pakfire_key_create(key, ctx, algo, buffer->id, pkey, comment);
	if (r < 0)
		goto ERROR;

ERROR:
	if (pkey)
		EVP_PKEY_free(pkey);

	return r;
}

int pakfire_key_import(struct pakfire_key** key,
		struct pakfire_ctx* ctx, FILE* f) {
	void* buffer = NULL;
	size_t buffer_length = 0;
	int r;

	char* line = NULL;
	size_t length = 0;
	size_t lineno = 0;

	char comment[COMMENT_MAX];

	for (;;) {
		ssize_t bytes_read = getline(&line, &length, f);
		if (bytes_read < 0)
			break;

		// Increment the line counter
		lineno++;

		switch (lineno) {
			// The first line must start with "untrusted comment:"
			case 1:
				if (!pakfire_string_startswith(line, "untrusted comment: ")) {
					ERROR(ctx, "The first line must start with 'untrusted comment: '"
						" and not %s\n", line);
					r = -EINVAL;
					goto ERROR;
				}

				// Copy the comment
				r = pakfire_string_set(comment, line + strlen("untrusted comment: "));
				if (r < 0) {
					ERROR(ctx, "Could not copy comment: %m\n");
					goto ERROR;
				}

				break;

			// The second line should hold the key
			case 2:
				// Decode the key
				r = pakfire_b64decode(ctx, &buffer, &buffer_length, line);
				if (r) {
					ERROR(ctx, "Could not decode the key: %m\n");
					r = -EINVAL;
					goto ERROR;
				}

				// What kind of key do we have?
				switch (buffer_length) {
					// Public Key
					case sizeof(struct pakfire_key_public_key):
						r = pakfire_key_import_public_key(key, ctx, comment,
							(struct pakfire_key_public_key*)buffer);
						if (r < 0)
							goto ERROR;
						break;

					// Private Key
					case sizeof(struct pakfire_key_private_key):
						r = pakfire_key_import_secret_key(key, ctx, comment,
							(struct pakfire_key_private_key*)buffer);
						if (r < 0)
							goto ERROR;
						break;

					// Unknown key
					default:
						ERROR(ctx, "Unsupported key type\n");
						r = -ENOTSUP;
						goto ERROR;
				}
				break;

			// Ignore any further data
			default:
				break;
		}
	}

	// Success!
	r = 0;

ERROR:
	if (buffer) {
		OPENSSL_cleanse(buffer, buffer_length);
		free(buffer);
	}
	if (line)
		free(line);

	return r;
}

int pakfire_key_import_from_string(struct pakfire_key** key,
		struct pakfire_ctx* ctx, const char* data, const size_t length) {
	FILE* f = NULL;
	int r;

	// Map the data to a file
	f = fmemopen((char*)data, length, "r");
	if (!f) {
		ERROR(ctx, "Could not map the key to file: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Import the key
	r = pakfire_key_import(key, ctx, f);

ERROR:
	if (f)
		fclose(f);

	return r;
}

static int pakfire_key_get_public_key(struct pakfire_key* key,
		unsigned char* buffer, const size_t length) {
	char error[ERROR_MAX];
	int r;

	size_t l = length;

	// Extract the raw public key
	r = EVP_PKEY_get_raw_public_key(key->pkey, buffer, &l);
	if (r < 0) {
		ERR_error_string_n(ERR_get_error(), error, sizeof(error));

		ERROR(key->ctx, "Could not extract the public key: %s\n", error);
		return -EINVAL;
	}

	if (l > length) {
		ERROR(key->ctx, "The buffer was too small to write the public key\n");
		return -ENOBUFS;
	}

	return 0;
}

static int pakfire_key_get_secret_key(struct pakfire_key* key,
		unsigned char* buffer, const size_t length) {
	char error[ERROR_MAX];
	int r;

	size_t l = length;

	// Extract the raw secret key
	r = EVP_PKEY_get_raw_private_key(key->pkey, buffer, &l);
	if (r < 0) {
		ERR_error_string_n(ERR_get_error(), error, sizeof(error));

		ERROR(key->ctx, "Could not extract the secret key: %s\n", error);
		return -EINVAL;
	}

	if (l > length) {
		ERROR(key->ctx, "The buffer was too small to write the secret key\n");
		return -ENOBUFS;
	}

	return 0;
}

/*
	Export
*/

static int pakfire_key_export_private_key(struct pakfire_key* key,
		struct pakfire_key_private_key* buffer) {
	unsigned char checksum[64];
	unsigned int length = sizeof(checksum);
	int r;

	// Write the algorithm
	switch (key->algo) {
		case PAKFIRE_KEY_ALGO_ED25519:
			// Signature Algortihm
			buffer->sig_algo[0] = 'E';
			buffer->sig_algo[1] = 'd';

			// KDF Algorithm
			buffer->kdf_algo[0] = 'B';
			buffer->kdf_algo[1] = 'K';
			break;

		default:
			ERROR(key->ctx, "Unknown algorithm\n");
			return -ENOTSUP;
	}

	// Generate a salt
	r = RAND_bytes(buffer->kdf_salt, sizeof(buffer->kdf_salt));
	if (r < 1) {
		ERROR(key->ctx, "Could not generate salt\n");
		return -EINVAL;
	}

	// Copy the key ID
	memcpy(buffer->id, key->id, sizeof(buffer->id));

	// Write the public key
	r = pakfire_key_get_public_key(key, buffer->keys.public, sizeof(buffer->keys.public));
	if (r < 0) {
		ERROR(key->ctx, "Could not export the public key: %m\n");
		return r;
	}

	// Write the secret key
	r = pakfire_key_get_secret_key(key, buffer->keys.secret, sizeof(buffer->keys.secret));
	if (r < 0) {
		ERROR(key->ctx, "Could not export the secret key: %m\n");
		return r;
	}

	// Compute a SHA512 checksum over the key material
	r = EVP_Digest(&buffer->keys, sizeof(buffer->keys), checksum, &length, EVP_sha512(), NULL);
	if (r < 0) {
		ERROR(key->ctx, "Could not compute the checksum: %m\n");
		return r;
	}

	// Copy the first couple of bytes of the checksum
	memcpy(buffer->checksum, checksum, sizeof(buffer->checksum));

	// Success!
	return 0;
}

static int pakfire_key_export_public_key(struct pakfire_key* key,
		struct pakfire_key_public_key* buffer) {
	int r;

	// Write the algorithm
	switch (key->algo) {
		case PAKFIRE_KEY_ALGO_ED25519:
			buffer->sig_algo[0] = 'E';
			buffer->sig_algo[1] = 'd';
			break;

		default:
			ERROR(key->ctx, "Unknown algorithm\n");
			return -ENOTSUP;
	}

	// Copy the key ID
	memcpy(buffer->id, key->id, sizeof(buffer->id));

	// Write the public key
	r = pakfire_key_get_public_key(key, buffer->pubkey, sizeof(buffer->pubkey));
	if (r < 0) {
		ERROR(key->ctx, "Could not export the public key: %m\n");
		return r;
	}

	// Success!
	return 0;
}

int pakfire_key_export(struct pakfire_key* key, FILE* f,
		const pakfire_key_export_mode_t mode) {
	struct pakfire_key_public_key public_key = { 0 };
	struct pakfire_key_private_key private_key = { 0 };
	int r;

	BIO* bio = NULL;
	BIO* b64 = NULL;

	// Write the comment
	if (*key->comment) {
		r = fprintf(f, "untrusted comment: %s\n", key->comment);
		if (r < 0) {
			ERROR(key->ctx, "Could not write comment: %m\n");
			r = 1;
			goto ERROR;
		}
	}

	// Setup the base64 encoder
	b64 = BIO_new(BIO_f_base64());
	if (!b64) {
		ERROR(key->ctx, "Could not setup the base64 encoder\n");
		r = 1;
		goto ERROR;
	}

	// Disable line breaks and a trailing newline
	BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

	bio = BIO_new_fp(f, BIO_NOCLOSE);
	if (!bio) {
		ERROR(key->ctx, "Could not open BIO\n");
		r = 1;
		goto ERROR;
	}

	BIO_push(b64, bio);

	switch (mode) {
		case PAKFIRE_KEY_EXPORT_MODE_PUBLIC:
			r = pakfire_key_export_public_key(key, &public_key);
			if (r)
				goto ERROR;

			// Write the output
			r = BIO_write(b64, &public_key, sizeof(public_key));
			if (r < 0) {
				ERROR(key->ctx, "Could not write the public key\n");
				r = 1;
				goto ERROR;
			}
			break;

		case PAKFIRE_KEY_EXPORT_MODE_PRIVATE:
			r = pakfire_key_export_private_key(key, &private_key);
			if (r)
				goto ERROR;

			// Write the output
			r = BIO_write(b64, &private_key, sizeof(private_key));
			if (r < 0) {
				ERROR(key->ctx, "Could not write the private key\n");
				goto ERROR;
			}
			break;

		default:
			r = -EINVAL;
			goto ERROR;
	}

	// Flush all buffers
	BIO_flush(b64);

	// Append a trailing newline
	fprintf(f, "\n");

	// Success!
	r = 0;

ERROR:
	if (b64)
		BIO_free(b64);
	if (bio)
		BIO_free(bio);

	return r;
}

int pakfire_key_export_string(struct pakfire_key* self, char** buffer, size_t* length) {
	FILE* f = NULL;
	int r;

	// Create a file handle to write to the buffer
	f = open_memstream(buffer, length);
	if (!f) {
		ERROR(self->ctx, "Could not open a new memstream: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Export the public part of the key
	r = pakfire_key_export(self, f, PAKFIRE_KEY_EXPORT_MODE_PUBLIC);
	if (r < 0) {
		ERROR(self->ctx, "Could not export key: %m\n");
		goto ERROR;
	}

ERROR:
	if (f)
		fclose(f);

	return r;
}

/*
	Sign
*/

static int __pakfire_key_sign(struct pakfire_key* key,
		struct pakfire_key_signature* signature, const void* data, const size_t length) {
	EVP_MD_CTX* mdctx = NULL;
	char error[ERROR_MAX];
	int r;

	// Check inputs
	if (!signature || !data || !length)
		return -EINVAL;

	// Set algorithm
	signature->sig_algo[0] = 'E';
	signature->sig_algo[1] = 'd';

	// Set the key ID
	memcpy(signature->key_id, key->id, sizeof(signature->key_id));

	// Create a message digest context
	mdctx = EVP_MD_CTX_new();
	if (!mdctx) {
		ERROR(key->ctx, "Could not initialize the message digest context: %m\n");
		r = -ENOMEM;
		goto ERROR;
	}

	// Setup the context
	r = EVP_DigestSignInit(mdctx, NULL, NULL, NULL, key->pkey);
	if (r < 1) {
		ERROR(key->ctx, "Could not setup context\n");
		r = -EINVAL;
		goto ERROR;
	}

	size_t signature_length = sizeof(signature->signature);

	// Create a signature over the digest
	r = EVP_DigestSign(mdctx, signature->signature, &signature_length, data, length);
	if (r < 1) {
		ERR_error_string_n(ERR_get_error(), error, sizeof(error));

		ERROR(key->ctx, "Could not sign content: %s\n", error);
		goto ERROR;
	}

	// Success!
	r = 0;

ERROR:
	if (mdctx)
		EVP_MD_CTX_free(mdctx);

	return r;
}

int pakfire_key_sign_string(struct pakfire_key* key,
		FILE* f, const void* data, const size_t length, const char* comment) {
	struct pakfire_key_signature signature = { 0 };
	char* buffer = NULL;
	int r;

	// Create a signature
	r = __pakfire_key_sign(key, &signature, data, length);
	if (r)
		return r;

	// Write the comment
	if (comment) {
		r = fprintf(f, "untrusted comment: %s\n", comment);
		if (r < 0) {
			ERROR(key->ctx, "Could not write comment: %m\n");
			r = -errno;
			goto ERROR;
		}
	}

	// Encode the signature to base64
	r = pakfire_b64encode(key->ctx, &buffer, &signature, sizeof(signature));
	if (r < 0)
		goto ERROR;

	// Write the signature
	r = fprintf(f, "%s\n", buffer);
	if (r < 0) {
		ERROR(key->ctx, "Could not write the signature: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Ensure the entire signature is being written
	fflush(f);

	// Success!
	r = 0;

ERROR:
	if (buffer)
		free(buffer);

	return r;
}

int pakfire_key_sign(struct pakfire_key* key, FILE* s, FILE* f, const char* comment) {
	char* buffer = NULL;
	size_t length = 0;
	int r;

	// Map the entire content into memory
	r = pakfire_mmap(fileno(f), &buffer, &length);
	if (r < 0)
		goto ERROR;

	// Sign!
	r = pakfire_key_sign_string(key, s, buffer, length, comment);

ERROR:
	if (buffer)
		munmap(buffer, length);

	return r;
}

static int pakfire_key_read_signature(struct pakfire_key* key,
		struct pakfire_key_signature* signature, FILE* f) {
	void* buffer = NULL;
	size_t buffer_length = 0;
	int r;

	char* line = NULL;
	size_t length = 0;
	size_t lineno = 0;

	for (;;) {
		ssize_t bytes_read = getline(&line, &length, f);
		if (bytes_read < 0)
			break;

		// Increment the line counter
		lineno++;

		// Don't parse any comments
		if (pakfire_string_startswith(line, "untrusted comment:"))
			continue;

		// Decode the signature
		r = pakfire_b64decode(key->ctx, &buffer, &buffer_length, line);
		if (r) {
			ERROR(key->ctx, "Could not decode the signature: %m\n");
			r = -EINVAL;
			goto ERROR;
		}

		// What kind of signature do we have?
		switch (buffer_length) {
			case sizeof(*signature):
				// Copy the buffer to the signature
				memcpy(signature, buffer, sizeof(*signature));

				// Check if we support the signature type
				if (signature->sig_algo[0] != 'E' || signature->sig_algo[1] != 'd') {
					ERROR(key->ctx, "Unknown signature type\n");
					r = -ENOTSUP;
					goto ERROR;
				}

				// Done
				r = 0;
				goto ERROR;

			default:
				ERROR(key->ctx, "Unknown signature type\n");
				r = -ENOTSUP;
				goto ERROR;
		}

		// Only ever parse one signature
		break;
	}

	// Fail if we had nothing to read
	r = -ENODATA;

ERROR:
	if (buffer)
		free(buffer);
	if (line)
		free(line);

	return r;
}

static int pakfire_key_verify_signature(struct pakfire_key* key,
		const struct pakfire_key_signature* signature, const void* data, const size_t length) {
	EVP_MD_CTX* mdctx = NULL;
	int r;

	DEBUG(key->ctx, "Verifying signature...\n");

	// Check the key ID
	if (!pakfire_key_id_equals(&key->id, &signature->key_id)) {
		ERROR(key->ctx, "The signature has been created with a different key\n");
		r = -EBADMSG;
		goto ERROR;
	}

	// Create message digest context
	mdctx = EVP_MD_CTX_new();
	if (!mdctx) {
		ERROR(key->ctx, "Could not create the message digest context\n");
		r = -ENOMEM;
		goto ERROR;
	}

	// Setup the context for verification
	r = EVP_DigestVerifyInit(mdctx, NULL, NULL, NULL, key->pkey);
	if (r < 1) {
		ERROR(key->ctx, "Could not setup the verification context\n");
		goto ERROR;
	}

	// Verify the signature
	r = EVP_DigestVerify(mdctx, signature->signature, sizeof(signature->signature),
			data, length);
	switch (r) {
		// Fail
		case 0:
			ERROR(key->ctx, "Signature verification failed\n");
			r = -EBADMSG;
			break;

		// Success
		case 1:
			DEBUG(key->ctx, "Signature verification successful\n");
			r = 0;
			break;

		// Error
		default:
			ERROR(key->ctx, "Could not perform signature verification\n");
			r = -ENOTSUP;
			goto ERROR;
	}

ERROR:
	if (mdctx)
		EVP_MD_CTX_free(mdctx);

	return r;
}

int pakfire_key_verify(struct pakfire_key* key, FILE* f,
		const void* data, const size_t length) {
	struct pakfire_key_signature signature = { 0 };
	int r;

	// Read the signature
	r = pakfire_key_read_signature(key, &signature, f);
	if (r < 0) {
		ERROR(key->ctx, "Could not read signature: %s\n", strerror(-r));
		return r;
	}

	// Verify signature
	r = pakfire_key_verify_signature(key, &signature, data, length);
	if (r < 0) {
		ERROR(key->ctx, "Could not verify signature: %s\n", strerror(-r));
		return r;
	}

	return 0;
}
