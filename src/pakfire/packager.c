/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

// libarchive
#include <archive.h>
#include <archive_entry.h>

#include <json.h>

#include <pakfire/archive.h>
#include <pakfire/archive_writer.h>
#include <pakfire/constants.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/packager.h>
#include <pakfire/pakfire.h>
#include <pakfire/path.h>
#include <pakfire/pwd.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

struct pakfire_packager {
	struct pakfire_ctx* ctx;
	struct pakfire* pakfire;
	int nrefs;

	time_t time_created;

	struct pakfire_package* pkg;
	char filename[PATH_MAX];

	// Reader
	struct archive* reader;

	// Payload
	struct pakfire_filelist* filelist;

	// Scriptlets
	struct pakfire_scriptlet** scriptlets;
	unsigned int num_scriptlets;

	// Digests
	unsigned int digests;
};

static void pakfire_packager_free(struct pakfire_packager* packager) {
	if (packager->reader)
		archive_read_free(packager->reader);

	// Scriptlets
	if (packager->scriptlets) {
		for (unsigned int i = 0; i < packager->num_scriptlets; i++)
			pakfire_scriptlet_unref(packager->scriptlets[i]);
		free(packager->scriptlets);
	}

	if (packager->filelist)
		pakfire_filelist_unref(packager->filelist);
	if (packager->pkg)
		pakfire_package_unref(packager->pkg);
	if (packager->pakfire)
		pakfire_unref(packager->pakfire);
	if (packager->ctx)
		pakfire_ctx_unref(packager->ctx);
	free(packager);
}

int pakfire_packager_create(struct pakfire_packager** packager,
		struct pakfire* pakfire, struct pakfire_package* pkg) {
	struct pakfire_packager* p = NULL;
	char hostname[HOST_NAME_MAX];
	int r = 1;

	// Allocate the packager object
	p = calloc(1, sizeof(*p));
	if (!p)
		return -errno;

	// Save creation time
	p->time_created = time(NULL);

	// Initialize reference counting
	p->nrefs = 1;

	// Store a reference to the context
	p->ctx = pakfire_ctx(pakfire);

	// Store a reference to Pakfire
	p->pakfire = pakfire_ref(pakfire);

	// Store a reference to the package
	p->pkg = pakfire_package_ref(pkg);

	// Use the default digests
	p->digests = PAKFIRE_PACKAGER_HASHES;

	// Set distribution
	const char* tag = pakfire_get_distro_tag(p->pakfire);
	if (!tag) {
		ERROR(p->ctx, "Distribution tag is not configured: %m\n");
		goto ERROR;
	}

	pakfire_package_set_string(pkg, PAKFIRE_PKG_DISTRO, tag);

	// Fetch the hostname
	r = gethostname(hostname, sizeof(hostname));
	if (r) {
		ERROR(p->ctx, "Could not determine the hostname: %m\n");
		goto ERROR;
	}

	// Set build host
	r = pakfire_package_set_string(pkg, PAKFIRE_PKG_BUILD_HOST, hostname);
	if (r) {
		ERROR(p->ctx, "Could not set the hostname: %s\n", strerror(r));
		goto ERROR;
	}

	// Set build time
	pakfire_package_set_num(pkg, PAKFIRE_PKG_BUILD_TIME, p->time_created);

	// Create reader
	p->reader = pakfire_get_disk_reader(p->pakfire);
	if (!p->reader)
		goto ERROR;

	// Create filelist
	r = pakfire_filelist_create(&p->filelist, p->pakfire);
	if (r)
		goto ERROR;

	// Add a requirement for the cryptographic algorithms we are using
	if (p->digests & PAKFIRE_HASH_SHA3_512) {
		r = pakfire_package_add_dep(p->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(Digest-SHA3-512)");
		if (r)
			goto ERROR;
	}
	if (p->digests & PAKFIRE_HASH_SHA3_256) {
		r = pakfire_package_add_dep(p->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(Digest-SHA3-256)");
		if (r)
			goto ERROR;
	}
	if (p->digests & PAKFIRE_HASH_BLAKE2B512) {
		r = pakfire_package_add_dep(p->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(Digest-BLAKE2b512)");
		if (r)
			goto ERROR;
	}
	if (p->digests & PAKFIRE_HASH_BLAKE2S256) {
		r = pakfire_package_add_dep(p->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(Digest-BLAKE2s256)");
		if (r)
			goto ERROR;
	}
	if (p->digests & PAKFIRE_HASH_SHA2_512) {
		r = pakfire_package_add_dep(p->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(Digest-SHA2-512)");
		if (r)
			goto ERROR;
	}
	if (p->digests & PAKFIRE_HASH_SHA2_256) {
		r = pakfire_package_add_dep(p->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(Digest-SHA2-256)");
		if (r)
			goto ERROR;
	}

	// Return a reference
	*packager = pakfire_packager_ref(p);

ERROR:
	if (p)
		pakfire_packager_unref(p);

	return r;
}

struct pakfire_packager* pakfire_packager_ref(
		struct pakfire_packager* packager) {
	++packager->nrefs;

	return packager;
}

struct pakfire_packager* pakfire_packager_unref(
		struct pakfire_packager* packager) {
	if (--packager->nrefs > 0)
		return packager;

	pakfire_packager_free(packager);
	return NULL;
}

char* pakfire_packager_filename(struct pakfire_packager* packager) {
	const char* filename = NULL;

	// Fetch the filename from the package
	filename = pakfire_package_get_string(packager->pkg, PAKFIRE_PKG_FILENAME);
	if (!filename)
		return NULL;

	// Copy the string
	return strdup(filename);
}

static int pakfire_packager_write_format(struct pakfire_packager* packager,
		struct pakfire_archive_writer* writer) {
	const char format[] = TO_STRING(PACKAGE_FORMAT) "\n";
	int r;

	DEBUG(packager->ctx, "Writing package format\n");

	// Write the file
	r = pakfire_archive_writer_create_file(writer,
			"pakfire-format", 0444, format, strlen(format));
	if (r < 0) {
		ERROR(packager->ctx, "Failed to write pakfire-format: %s\n", strerror(-r));
		return r;
	}

	// Add package format marker
	r = pakfire_package_add_dep(packager->pkg, PAKFIRE_PKG_REQUIRES,
			"pakfire(PackageFormat-%d)", PACKAGE_FORMAT);
	if (r < 0)
		return r;

	return 0;
}

static int pakfire_packager_write_metadata(struct pakfire_packager* packager,
		struct pakfire_archive_writer* writer) {
	struct json_object* md = NULL;
	int r;

	// Convert all package metadata to JSON
	md = pakfire_package_to_json(packager->pkg);
	if (!md) {
		r = -errno;
		goto ERROR;
	}

	// Write metadata
	r = pakfire_archive_writer_create_file_from_json(writer, ".PKGINFO", 0444, md);
	if (r < 0) {
		ERROR(packager->ctx, "Failed to write package metadata: %s\n", strerror(-r));
		goto ERROR;
	}

ERROR:
	if (md)
		json_object_put(md);

	return r;
}

static int pakfire_packager_write_scriptlet(struct pakfire_packager* packager,
		struct pakfire_archive_writer* writer, struct pakfire_scriptlet* scriptlet) {
	char filename[PATH_MAX];
	size_t size;
	int r;

	// Fetch type
	const char* type = pakfire_scriptlet_get_type(scriptlet);

	DEBUG(packager->ctx, "Writing scriptlet '%s' to package\n", type);

	// Make filename
	r = pakfire_string_format(filename, ".scriptlets/%s", type);
	if (r < 0)
		return r;

	// Fetch scriptlet
	const char* data = pakfire_scriptlet_get_data(scriptlet, &size);

	// Write file
	return pakfire_archive_writer_create_file(writer, filename, 0544, data, size);
}

/*
	This function is being called at the end when all data has been added to the package.

	It will create a new archive and write the package to the given file descriptor.
*/
int pakfire_packager_finish(struct pakfire_packager* packager, FILE* f) {
	struct pakfire_archive_writer* writer = NULL;
	int r;

	// Fetch nevra
	const char* nevra = pakfire_package_get_string(packager->pkg, PAKFIRE_PKG_NEVRA);

	// Add requires feature markers
	if (pakfire_package_has_rich_deps(packager->pkg)) {
		r = pakfire_package_add_dep(packager->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(RichDependencies)");
		if (r)
			goto ERROR;
	}

	// Add feature marker
	pakfire_package_add_dep(packager->pkg,
		PAKFIRE_PKG_REQUIRES, "pakfire(Compress-Zstandard)");

	// Add filelist
	r = pakfire_package_set_filelist(packager->pkg, packager->filelist);
	if (r)
		goto ERROR;

	const size_t installsize = pakfire_filelist_total_size(packager->filelist);

	// Store total install size
	r = pakfire_package_set_num(packager->pkg, PAKFIRE_PKG_INSTALLSIZE, installsize);
	if (r)
		goto ERROR;

	// Create a new archive writer
	r = pakfire_archive_writer_create(&writer, packager->pakfire,
			PAKFIRE_FORMAT_SOURCE_ARCHIVE, f);
	if (r < 0)
		goto ERROR;

	// Set the title
	r = pakfire_archive_writer_set_title(writer, "%s", nevra);
	if (r < 0)
		goto ERROR;

	// Start with the format file
	r = pakfire_packager_write_format(packager, writer);
	if (r < 0) {
		ERROR(packager->ctx, "Could not add format file to archive: %s\n", strerror(-r));
		goto ERROR;
	}

	// Write the metadata
	r = pakfire_packager_write_metadata(packager, writer);
	if (r < 0) {
		ERROR(packager->ctx, "Could not add metadata file to archive: %s\n", strerror(-r));
		goto ERROR;
	}

	// Write scriptlets
	for (unsigned int i = 0; i < packager->num_scriptlets; i++) {
		r = pakfire_packager_write_scriptlet(packager, writer, packager->scriptlets[i]);
		if (r < 0) {
			ERROR(packager->ctx, "Could not add scriptlet to the archive: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Write the payload
	r = pakfire_archive_writer_write_files(writer, packager->filelist);
	if (r < 0)
		goto ERROR;

ERROR:
	if (writer)
		pakfire_archive_writer_unref(writer);

	return r;
}

int pakfire_packager_write_archive(struct pakfire_packager* self,
		struct pakfire_archive** archive) {
	FILE* f = NULL;
	char path[PATH_MAX];
	int r;

	// Fetch the filename of the package
	const char* filename = pakfire_package_get_string(self->pkg, PAKFIRE_PKG_FILENAME);
	if (!filename) {
		r = -EINVAL;
		goto ERROR;
	}

	// Compose the path for a temporary file
	r = pakfire_path_format(path, "%s/.%s.XXXXXX", PAKFIRE_TMP_DIR, filename);
	if (r < 0)
		goto ERROR;

	// Create the temporary file
	f = pakfire_mktemp(path, 0644);
	if (!f) {
		ERROR(self->ctx, "Could not create temporary file: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Write everything to the temporary file
	r = pakfire_packager_finish(self, f);
	if (r < 0)
		goto ERROR;

	// Close the file
	fclose(f);
	f = NULL;

	// Open the archive
	r = pakfire_archive_open(archive, self->pakfire, path);
	if (r < 0) {
		ERROR(self->ctx, "Could not open the generated archive at %s: %s\n",
			path, strerror(-r));
		goto ERROR;
	}

ERROR:
	if (f)
		fclose(f);

	return r;
}

int pakfire_packager_add_file(struct pakfire_packager* packager, struct pakfire_file* file) {
	int r;

	// Check input
	if (!file) {
		errno = EINVAL;
		return 1;
	}

	// Fetch path
	const char* path = pakfire_file_get_path(file);

	// Files cannot have an empty path
	if (!path || !*path) {
		ERROR(packager->ctx, "Cannot add a file with an empty path\n");
		errno = EPERM;
		return 1;

	// Hidden files cannot be added
	} else if (*path == '.') {
		ERROR(packager->ctx, "Hidden files cannot be added to a package: %s\n", path);
		errno = EPERM;
		return 1;
	}

	DEBUG(packager->ctx, "Adding file to payload: %s\n", path);

	// Detect the MIME type
	r = pakfire_file_detect_mimetype(file);
	if (r)
		return r;

	// Overwrite a couple of things for source archives
	if (pakfire_package_is_source(packager->pkg)) {
		// Reset permissions
		pakfire_file_set_perms(file, 0644);

		// Reset file ownership
		pakfire_file_set_uname(file, "root");
		pakfire_file_set_gname(file, "root");
	}

	// Handle systemd sysusers
	if (pakfire_file_matches(file, "/usr/lib/sysusers.d/*.conf")) {
		pakfire_package_add_dep(packager->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(systemd-sysusers)");

		// Ask to pre-install /usr/bin/systemd-sysusers
		pakfire_package_add_dep(packager->pkg,
			PAKFIRE_PKG_PREREQUIRES, "/usr/bin/systemd-sysusers");
	}

	// Handle systemd tmpfiles
	if (pakfire_file_matches(file, "/usr/lib/tmpfiles.d/*.conf")) {
		pakfire_package_add_dep(packager->pkg,
			PAKFIRE_PKG_REQUIRES, "pakfire(systemd-tmpfiles)");

		// Ask to pre-install systemd
		pakfire_package_add_dep(packager->pkg,
			PAKFIRE_PKG_PREREQUIRES, "systemd");
	}

	// Append the file to the filelist
	return pakfire_filelist_add(packager->filelist, file);
}

int pakfire_packager_add(struct pakfire_packager* packager,
		const char* sourcepath, const char* path) {
	struct archive_entry* entry = NULL;
	struct pakfire_file* file = NULL;
	int r;

	// Check inputs
	if (!sourcepath || !path)
		return -EINVAL;

	// Create a new archive entry
	entry = archive_entry_new();
	if (!entry)
		return -errno;

	// Set the path
	archive_entry_copy_pathname(entry, path);

	// Set the absolute path
	archive_entry_copy_sourcepath(entry, sourcepath);

	// Read everything
	r = archive_read_disk_entry_from_file(packager->reader, entry, -1, NULL);
	if (r) {
		ERROR(packager->ctx, "Could not read %s: %s\n",
			sourcepath, archive_error_string(packager->reader));
		r = -errno;
		goto ERROR;
	}

	// Create a new file object from the archive entry
	r = pakfire_file_create_from_archive_entry(&file, packager->pakfire, entry);
	if (r < 0) {
		ERROR(packager->ctx, "Could not create file object: %s\n", strerror(-r));
		goto ERROR;
	}

	// Call the main function
	r = pakfire_packager_add_file(packager, file);

ERROR:
	if (file)
		pakfire_file_unref(file);
	if (entry)
		archive_entry_free(entry);

	return r;
}

static int __pakfire_packager_add_files(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	struct pakfire_packager* packager = data;

	return pakfire_packager_add_file(packager, file);
}

int pakfire_packager_add_files(
		struct pakfire_packager* packager, struct pakfire_filelist* filelist) {
	// Add all files on the filelist
	return pakfire_filelist_walk(filelist, __pakfire_packager_add_files, packager, 0, NULL);
}

int pakfire_packager_add_scriptlet(struct pakfire_packager* packager,
		struct pakfire_scriptlet* scriptlet) {
	if (!scriptlet) {
		errno = EINVAL;
		return 1;
	}

	// Extend array
	packager->scriptlets = reallocarray(packager->scriptlets,
		packager->num_scriptlets + 1, sizeof(*packager->scriptlets));
	if (!packager->scriptlets)
		return 1;

	// Append scriptlet
	packager->scriptlets[packager->num_scriptlets++] = pakfire_scriptlet_ref(scriptlet);

	return 0;
}

/*
	Removes all files on the filelist
*/
int pakfire_packager_cleanup(struct pakfire_packager* packager) {
	// Delete all files on the filelist
	return pakfire_filelist_cleanup(packager->filelist, PAKFIRE_FILE_CLEANUP_TIDY);
}
