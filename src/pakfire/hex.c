/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <pakfire/hex.h>

char* __pakfire_hexlify(const unsigned char* digest, const size_t length) {
	const char* hexdigits = "0123456789abcdef";

	char* s = malloc((length * 2) + 1);
	if (!s)
		return NULL;

	for (unsigned int i = 0, j = 0; i < length; i++) {
		char b = digest[i];

		s[j++] = hexdigits[(b >> 4) & 0xf];
		s[j++] = hexdigits[(b & 0x0f)];
	}

	// Terminate result
	s[length * 2] = '\0';

	return s;
}

static int parse_nibble(char nibble) {
	// Handle digits
	if (nibble >= '0' && nibble <= '9')
		return nibble - '0';

	// Handle lowercase letters
	if (nibble >= 'a' && nibble <= 'f')
		return 10 + nibble - 'a';

	// Handle uppercase letters
	if (nibble >= 'A' && nibble <= 'F')
		return 10 + nibble - 'A';

	// Error
	return -1;
}

int __pakfire_unhexlify(unsigned char* dst, const size_t l, const char* src) {
	if (!src)
		return -EINVAL;

	// Determine input length
	const size_t length = strlen(src);

	for (unsigned int i = 0, j = 0; i < length && j < l; i += 2, j++) {
		int high = parse_nibble(src[i]);
		int low  = parse_nibble(src[i+1]);

		// Check for invalid input
		if (high < 0 || low < 0)
			return -EINVAL;

		// Store result
		dst[j] = high << 4 | low;
	}

	return 0;
}
