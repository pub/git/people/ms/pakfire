/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_MOUNT_H
#define PAKFIRE_MOUNT_H

#include <pakfire/ctx.h>
#include <pakfire/pakfire.h>

typedef enum pakfire_mntns {
	PAKFIRE_MNTNS_INNER = (1 << 0),
	PAKFIRE_MNTNS_OUTER = (2 << 0),
} pakfire_mntns_t;

int pakfire_mount_change_propagation(struct pakfire_ctx* ctx, const char* path, int propagation);

int pakfire_mount_make_mounpoint(struct pakfire_ctx* ctx, const char* path);

int pakfire_bind(struct pakfire_ctx* ctx, struct pakfire* pakfire,
	const char* src, const char* dst, int flags);

int pakfire_mount_list(struct pakfire_ctx* ctx);

int pakfire_populate_dev(struct pakfire_ctx* ctx, struct pakfire* pakfire, int flags);

int pakfire_mount_interpreter(struct pakfire_ctx* ctx, struct pakfire* pakfire);

enum pakfire_mount_flags {
	PAKFIRE_MOUNT_LOOP_DEVICES = (1 << 0),
};

int pakfire_mount_all(struct pakfire_ctx* ctx, struct pakfire* pakfire,
	pakfire_mntns_t ns, int flags);

#endif /* PAKFIRE_MOUNT_H */
