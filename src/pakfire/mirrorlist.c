/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>

#include <pakfire/ctx.h>
#include <pakfire/json.h>
#include <pakfire/logging.h>
#include <pakfire/mirror.h>
#include <pakfire/mirrorlist.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

struct pakfire_mirrorlist {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Mirrors
	struct pakfire_mirror** mirrors;
	unsigned int num_mirrors;
};

static void pakfire_mirrorlist_clear(struct pakfire_mirrorlist* list) {
	if (list->mirrors) {
		for (unsigned int i = 0; i < list->num_mirrors; i++)
			pakfire_mirror_unref(list->mirrors[i]);

		free(list->mirrors);

		list->mirrors = NULL;
		list->num_mirrors = 0;
	}
}

static void pakfire_mirrorlist_free(struct pakfire_mirrorlist* list) {
	pakfire_mirrorlist_clear(list);

	if (list->ctx)
		pakfire_ctx_unref(list->ctx);
	free(list);
}

int pakfire_mirrorlist_create(struct pakfire_mirrorlist** list, struct pakfire_ctx* ctx) {
	struct pakfire_mirrorlist* l = NULL;

	// Allocate a new list
	l = calloc(1, sizeof(*l));
	if (!l)
		return -errno;

	// Store a reference to the context
	l->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	l->nrefs = 1;

	// Return the pointer
	*list = l;

	return 0;
}

struct pakfire_mirrorlist* pakfire_mirrorlist_ref(struct pakfire_mirrorlist* list) {
	++list->nrefs;

	return list;
}

struct pakfire_mirrorlist* pakfire_mirrorlist_unref(struct pakfire_mirrorlist* list) {
	if (--list->nrefs > 0)
		return list;

	pakfire_mirrorlist_free(list);
	return NULL;
}

static int pakfire_mirrorlist_check_mirrorlist(struct pakfire_mirrorlist* list,
		struct json_object* root) {
	struct json_object* typeobj = NULL;
	int r;

	r = json_object_object_get_ex(root, "type", &typeobj);
	if (!r) {
		ERROR(list->ctx, "mirrorlist does not have a 'type' attribute\n");
		return -EBADMSG;
	}

	const char* type = json_object_get_string(typeobj);
	if (!type) {
		ERROR(list->ctx, "mirrorlist has an empty or unknown 'type' attribute\n");
		return -EBADMSG;
	}

	if (!pakfire_string_equals(type, "mirrorlist")) {
		ERROR(list->ctx, "Unexpected type: %s\n", type);
		return -EBADMSG;
	}

	return 0;
}

static int pakfire_mirrorlist_add_mirror_from_url(
		struct pakfire_mirrorlist* list, const char* url) {
	struct pakfire_mirror* mirror = NULL;
	int r;

	// Create a new mirror object
	r = pakfire_mirror_create(&mirror, list->ctx, url);
	if (r)
		goto ERROR;

	// Add the mirror to the list
	r = pakfire_mirrorlist_add_mirror(list, mirror);
	if (r)
		goto ERROR;

ERROR:
	if (mirror)
		pakfire_mirror_unref(mirror);

	return r;
}

int pakfire_mirrorlist_read(struct pakfire_mirrorlist* list, const char* path) {
	int r;

	if (!path || !*path) {
		errno = EINVAL;
		return 1;
	}

	DEBUG(list->ctx, "Reading mirrorlist from %s\n", path);

	struct json_object* json = pakfire_json_parse_from_file(list->ctx, path);
	if (!json) {
		// Ignore if path does not exist
		if (errno == ENOENT)
			return 0;

		ERROR(list->ctx, "Could not parse mirrorlist from %s: %m\n", path);
		return 1;
	}

	struct json_object* mirrors = NULL;

	// Check if we found a valid mirrorlist
	r = pakfire_mirrorlist_check_mirrorlist(list, json);
	if (r)
		goto ERROR;

	// Clear all existing mirrors
	pakfire_mirrorlist_clear(list);

	// Add the new mirrors
	r = json_object_object_get_ex(json, "mirrors", &mirrors);
	if (!r) {
		DEBUG(list->ctx, "Mirrorlist has no mirrors\n");
		r = 0;
		goto ERROR;
	}

	size_t num_mirrors = json_object_array_length(mirrors);

	for (unsigned int i = 0; i < num_mirrors; i++) {
		struct json_object* mirror = json_object_array_get_idx(mirrors, i);
		if (!mirror)
			continue;

		// Find URL
		struct json_object* urlobj;
		r = json_object_object_get_ex(mirror, "url", &urlobj);
		if (!r)
			goto ERROR;

		const char* url = json_object_get_string(urlobj);

		// Add the mirror to the downloader
		r = pakfire_mirrorlist_add_mirror_from_url(list, url);
		if (r) {
			ERROR(list->ctx, "Could not add mirror %s: %m\n", url);
			goto ERROR;
		}
	}

	// Success
	r = 0;

ERROR:
	if (json)
		json_object_put(json);

	return r;
}

int pakfire_mirrorlist_add_mirror(struct pakfire_mirrorlist* list, struct pakfire_mirror* mirror) {
	// Check input
	if (!mirror)
		return -EINVAL;

	// Grow the array
	list->mirrors = reallocarray(list->mirrors, list->num_mirrors + 1, sizeof(*list->mirrors));
	if (!list->mirrors)
		return -errno;

	// Store the mirror
	list->mirrors[list->num_mirrors++] = pakfire_mirror_ref(mirror);

	return 0;
}

int pakfire_mirrorlist_empty(struct pakfire_mirrorlist* list) {
	return list->num_mirrors == 0;
}

/*
	Return the first mirror that isn't disabled
*/
struct pakfire_mirror* pakfire_mirrorlist_get_first(struct pakfire_mirrorlist* list) {
	for (unsigned int i = 0; i < list->num_mirrors; i++) {
		if (!pakfire_mirror_is_enabled(list->mirrors[i]))
			continue;

		return pakfire_mirror_ref(list->mirrors[i]);
	}

	return NULL;
}

struct pakfire_mirror* pakfire_mirrorlist_get_next(
		struct pakfire_mirrorlist* list, struct pakfire_mirror* mirror) {
	// Return NULL if we have reached the end of the list
	if (!mirror)
		return NULL;

	int found = 0;

	// Walk through all mirrors until we have found the current one
	for (unsigned int i = 0; i < list->num_mirrors; i++) {
		if (!found) {
			// Set found once we found our mirror
			if (list->mirrors[i] == mirror)
				found = 1;

			continue;
		}

		// Skip any disabled mirrors
		if (!pakfire_mirror_is_enabled(list->mirrors[i]))
			continue;

		// Return the mirror
		return pakfire_mirror_ref(list->mirrors[i]);
	}

	return NULL;
}
