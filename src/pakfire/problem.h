/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PROBLEM_H
#define PAKFIRE_PROBLEM_H

// libsolv
#include <solv/pooltypes.h>

struct pakfire_problem;

#include <pakfire/solution.h>
#include <pakfire/transaction.h>

struct pakfire_problem* pakfire_problem_ref(struct pakfire_problem* problem);
struct pakfire_problem* pakfire_problem_unref(struct pakfire_problem* problem);

const char* pakfire_problem_to_string(struct pakfire_problem* problem);

struct pakfire_solution** pakfire_problem_get_solutions(
	struct pakfire_problem* problem);

int pakfire_problem_create(struct pakfire_problem** problem, struct pakfire* pakfire,
	struct pakfire_transaction* transaction, Id id);

struct pakfire_transaction* pakfire_problem_get_transaction(struct pakfire_problem* problem);

Id pakfire_problem_get_id(struct pakfire_problem* problem);

#endif /* PAKFIRE_PROBLEM_H */
