/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fts.h>
#include <fcntl.h>
#include <glob.h>
#include <stddef.h>
#include <stdlib.h>

#include <pakfire/arch.h>
#include <pakfire/dist.h>
#include <pakfire/i18n.h>
#include <pakfire/logging.h>
#include <pakfire/mirror.h>
#include <pakfire/mirrorlist.h>
#include <pakfire/package.h>
#include <pakfire/packager.h>
#include <pakfire/pakfire.h>
#include <pakfire/parser.h>
#include <pakfire/path.h>
#include <pakfire/repo.h>
#include <pakfire/string.h>
#include <pakfire/util.h>
#include <pakfire/xfer.h>

#define PAKFIRE_MACROS_DIR				"/usr/lib/pakfire/macros"
#define PAKFIRE_MACROS_GLOB_PATTERN		PAKFIRE_MACROS_DIR "/*.macro"

static const char* pakfire_dist_excludes[] = {
	// Don't package any backup files
	"**~",
	"**.bak",
	"**.swp",

	// Don't package any other package files
	"**.pfm",

	NULL,
};

static int pakfire_makefile_set_defaults(struct pakfire* pakfire,
		struct pakfire_parser* parser, const char* path) {
	char dirname[PATH_MAX];
	char buffer[1024];
	int r;

	// Set epoch
	pakfire_parser_set(parser, NULL, "epoch", "0", 0);

	// Set EVR
	pakfire_parser_set(parser, NULL, "evr", "%{epoch}:%{version}-%{_release}", 0);

	// Set vendor
	pakfire_parser_set(parser, NULL, "vendor", "%{DISTRO_VENDOR}", 0);

	// Set arch
	const char* arch = pakfire_get_arch(pakfire);
	if (arch) {
		r = pakfire_parser_set(parser, NULL, "arch", arch, 0);
		if (r)
			return r;
	}

	// Set DISTRO_NAME
	const char* name = pakfire_get_distro_name(pakfire);
	if (name)
		pakfire_parser_set(parser, NULL, "DISTRO_NAME", name, 0);

	// Set DISTRO_SNAME
	const char* id = pakfire_get_distro_id(pakfire);
	if (id)
		pakfire_parser_set(parser, NULL, "DISTRO_SNAME", name, 0);

	// Set DISTRO_RELEASE
	const char* version_id = pakfire_get_distro_version_id(pakfire);
	if (version_id)
		pakfire_parser_set(parser, NULL, "DISTRO_RELEASE", version_id, 0);

	// Set DISTRO_DISTTAG
	const char* tag = pakfire_get_distro_tag(pakfire);
	if (tag)
		pakfire_parser_set(parser, NULL, "DISTRO_DISTTAG", tag, 0);

	// Set DISTRO_VENDOR
	const char* vendor = pakfire_get_distro_vendor(pakfire);
	if (vendor)
		pakfire_parser_set(parser, NULL, "DISTRO_VENDOR", vendor, 0);

	// Set DISTRO_ARCH
	const char* effective_arch = pakfire_get_effective_arch(pakfire);
	if (effective_arch) {
		pakfire_parser_set(parser, NULL, "DISTRO_ARCH", effective_arch, 0);

		const char* platform = pakfire_arch_platform(effective_arch);
		if (platform)
			pakfire_parser_set(parser, NULL, "DISTRO_PLATFORM", platform, 0);

		if (vendor) {
			// Set DISTRO_MACHINE
			r = pakfire_arch_machine(buffer, effective_arch, vendor);
			if (!r)
				pakfire_parser_set(parser, NULL, "DISTRO_MACHINE", buffer, 0);

			// Set DISTRO_BUILDTARGET
			r = pakfire_arch_buildtarget(buffer, effective_arch, vendor);
			if (!r)
				pakfire_parser_set(parser, NULL, "DISTRO_BUILDTARGET", buffer, 0);
		}
	}

	// Set BASEDIR
	r = pakfire_path_dirname(dirname, path);
	if (r)
		return r;

	if (*dirname) {
		const char* root = pakfire_get_path(pakfire);

		pakfire_parser_set(parser, NULL, "BASEDIR",
			pakfire_path_relpath(root, dirname), 0);
	}

	long processors_online = sysconf(_SC_NPROCESSORS_ONLN);

	// Set PARALLELISMFLAGS
	if (processors_online) {
		pakfire_string_format(buffer, "-j%ld", processors_online);
		pakfire_parser_set(parser, "build", "PARALLELISMFLAGS", buffer, 0);
	}

	return 0;
}

int pakfire_read_makefile(struct pakfire_parser** parser, struct pakfire* pakfire,
		const char* path, struct pakfire_parser_error** error) {
	int r;

	// Fetch context
	struct pakfire_ctx* ctx = pakfire_ctx(pakfire);

	// Create a new parser
	r = pakfire_parser_create(parser, pakfire, NULL, NULL, PAKFIRE_PARSER_FLAGS_EXPAND_COMMANDS);
	if (r < 0)
		goto ERROR;

	// Set defaults
	r = pakfire_makefile_set_defaults(pakfire, *parser, path);
	if (r < 0)
		goto ERROR;

	// Find all macros

	DEBUG(ctx, "Searching for macros in %s\n", PAKFIRE_MACROS_GLOB_PATTERN);

	glob_t globmacros;
	r = glob(PAKFIRE_MACROS_GLOB_PATTERN, 0, NULL, &globmacros);

	// Handle any errors
	switch (r) {
		case 0:
		case GLOB_NOMATCH:
			break;

		case GLOB_NOSPACE:
			r = -ENOMEM;
			goto ERROR;

		case GLOB_ABORTED:
			goto ERROR;

		default:
			ERROR(ctx, "glob() returned an unhandled error: %d\n", r);
			r = -ENOTSUP;
			goto ERROR;
	}

	DEBUG(ctx, "Found %zu macro(s)\n", globmacros.gl_pathc);

	// Read all macros
	for (unsigned int i = 0; i < globmacros.gl_pathc; i++) {
		// Parse the file
		r = pakfire_parser_read_file(*parser, globmacros.gl_pathv[i], error);
		if (r < 0)
			goto ERROR;
	}

	globfree(&globmacros);

	// Finally, parse the makefile
	r = pakfire_parser_read_file(*parser, path, error);
	if (r < 0) {
		ERROR(ctx, "Could not read makefile %s: %m\n", path);
		goto ERROR;
	}

	if (ctx)
		pakfire_ctx_unref(ctx);

	return 0;

ERROR:
	globfree(&globmacros);

	if (*parser) {
		pakfire_parser_unref(*parser);
		*parser = NULL;
	}

	if (ctx)
		pakfire_ctx_unref(ctx);

	return r;
}

static int pakfire_dist_get_mirrorlist(struct pakfire_ctx* ctx, struct pakfire* pakfire,
		struct pakfire_parser* makefile, struct pakfire_mirrorlist** list) {
	struct pakfire_mirrorlist* m = NULL;
	struct pakfire_mirror* mirror = NULL;
	char* p = NULL;
	int r;

	// Fetch source_dl
	char* source_dl = pakfire_parser_get(makefile, NULL, "source_dl");

	// We do not need to create a mirrorlist if this isn't set
	if (!source_dl)
		return 0;

	// Create mirrorlist
	r = pakfire_mirrorlist_create(&m, ctx);
	if (r) {
		ERROR(ctx, "Could not create the mirrorlist\n");
		goto ERROR;
	}

	// Add all mirrors
	const char* url = strtok_r(source_dl, " ", &p);
	while (url) {
		// Create a new mirror
		r = pakfire_mirror_create(&mirror, ctx, url);
		if (r)
			goto ERROR;

		// Add the mirror to the mirrorlist
		r = pakfire_mirrorlist_add_mirror(m, mirror);
		pakfire_mirror_unref(mirror);
		if (r)
			goto ERROR;

		url = strtok_r(NULL, " ", &p);
	}

	// Success
	*list = pakfire_mirrorlist_ref(m);

ERROR:
	if (m)
		pakfire_mirrorlist_unref(m);
	if (source_dl)
		free(source_dl);

	return r;
}

static int pakfire_dist_download_source(struct pakfire_ctx* ctx,
	struct pakfire_mirrorlist* mirrorlist, const char* cache_path, const char* filename, ...)
	__attribute__((format(printf, 4, 5)));

static int pakfire_dist_download_source(struct pakfire_ctx* ctx,
		struct pakfire_mirrorlist* mirrorlist, const char* cache_path, const char* filename, ...) {
	struct pakfire_xfer* xfer = NULL;
	va_list args;
	int r;

	// Do not download if the file exists
	if (pakfire_path_exists(cache_path))
		return 0;

	va_start(args, filename);

	// Create a new transfer
	r = pakfire_xfer_create(&xfer, ctx, filename, args);
	if (r)
		goto ERROR;

	// Set the mirrorlist
	r = pakfire_xfer_set_mirrorlist(xfer, mirrorlist);
	if (r)
		goto ERROR;

	// Set the output path
	r = pakfire_xfer_set_output_path(xfer, cache_path);
	if (r)
		goto ERROR;

	// Run the download
	r = pakfire_xfer_run(xfer, 0);
	if (r)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	va_end(args);

	return r;
}

static int pakfire_dist_add_source(struct pakfire* pakfire, struct pakfire_packager* packager,
		struct pakfire_package* pkg, struct pakfire_ctx* ctx,
		struct pakfire_mirrorlist* mirrorlist, const char* filename) {
	char archive_path[PATH_MAX];
	char cache_path[PATH_MAX];
	int r;

	const char* name = pakfire_package_get_string(pkg, PAKFIRE_PKG_NAME);

	// Compose path
	r = pakfire_cache_path(pakfire, cache_path, "sources/%s/%s", name, filename);
	if (r)
		return r;

	// Download the source file
	r = pakfire_dist_download_source(ctx, mirrorlist, cache_path, "%s", filename);
	if (r)
		return r;

	r = pakfire_string_format(archive_path, "files/%s", filename);
	if (r)
		return r;

	// Add file to package
	return pakfire_packager_add(packager, cache_path, archive_path);
}

static int pakfire_dist_add_sources(struct pakfire_ctx* ctx, struct pakfire* pakfire,
		struct pakfire_packager* packager, struct pakfire_package* pkg, struct pakfire_parser* makefile) {
	struct pakfire_mirrorlist* mirrorlist = NULL;
	char* sources = NULL;
	char* p = NULL;
	int r;

	// Fetch sources
	sources = pakfire_parser_get(makefile, NULL, "sources");

	// Nothing to do if this variable is empty
	if (!sources)
		return 0;

	// Fetch the mirrorlist
	r = pakfire_dist_get_mirrorlist(ctx, pakfire, makefile, &mirrorlist);
	if (r)
		goto ERROR;

	// Add all files one by one
	// Add all mirrors
	const char* source = strtok_r(sources, " ", &p);
	while (source) {
		DEBUG(ctx, "Adding source file %s\n", source);

		r = pakfire_dist_add_source(pakfire, packager, pkg, ctx, mirrorlist, source);
		if (r) {
			ERROR(ctx, "Could not add '%s' to package: %m\n", source);
			goto ERROR;
		}

		source = strtok_r(NULL, " ", &p);
	}

	// Success
	r = 0;

ERROR:
	if (mirrorlist)
		pakfire_mirrorlist_unref(mirrorlist);
	if (sources)
		free(sources);

	return r;
}

#define pakfire_dist_find_root(root, file) \
	__pakfire_dist_find_root(root, sizeof(root), file)

static int __pakfire_dist_find_root(char* root, const size_t length, const char* file) {
	char path[PATH_MAX];

	// Find the absolute path of the makefile
	char* p = realpath(file, path);
	if (!p)
		return -errno;

	// Return the parent directory
	return __pakfire_path_dirname(root, length, p);
}

static int pakfire_dist_add_files(struct pakfire_ctx* ctx, struct pakfire* pakfire,
		struct pakfire_packager* packager, const char* file) {
	struct pakfire_filelist* filelist = NULL;
	char root[PATH_MAX];
	int r;

	// Find the package directory
	r = pakfire_dist_find_root(root, file);
	if (r) {
		ERROR(ctx, "Could not find package root directory: %s\n", strerror(r));
		return r;
	}

	DEBUG(ctx, "Adding all files in '%s' to package...\n", root);

	// Create a new filelist
	r = pakfire_filelist_create(&filelist, pakfire);
	if (r)
		goto ERROR;

	// Scan for any files
	r = pakfire_filelist_scan(filelist, root, NULL, pakfire_dist_excludes,
		PAKFIRE_FILELIST_NO_DIRECTORIES);
	if (r)
		goto ERROR;

	// Add all files to the package
	r = pakfire_packager_add_files(packager, filelist);
	if (r)
		goto ERROR;

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

int pakfire_dist(struct pakfire* pakfire, const char* path,
		struct pakfire_archive** archive, char** filename) {
	struct pakfire_packager* packager = NULL;
	struct pakfire_package* pkg = NULL;
	struct pakfire_parser* makefile = NULL;
	struct pakfire_parser_error* error = NULL;
	struct pakfire_archive* a = NULL;
	int r;

	// Fetch context
	struct pakfire_ctx* ctx = pakfire_ctx(pakfire);

	// Load makefile
	r = pakfire_read_makefile(&makefile, pakfire, path, &error);
	if (r < 0) {
		if (error)
			pakfire_parser_error_unref(error);
		else
			ERROR(ctx, "Could not read makefile: %m\n");

		goto ERROR;
	}

	// The architecture is always "src"
	r = pakfire_parser_set(makefile, NULL, "arch", "src", 0);
	if (r) {
		ERROR(ctx, "Could not set architecture to 'src': %m\n");
		goto ERROR;
	}

	// Get the package object
	r = pakfire_parser_create_package(makefile, &pkg, NULL, NULL, "src");
	if (r)
		goto ERROR;

	// Create a packager
	r = pakfire_packager_create(&packager, pakfire, pkg);
	if (r)
		goto ERROR;

	// Add all files in the directory
	r = pakfire_dist_add_files(ctx, pakfire, packager, path);
	if (r)
		goto ERROR;

	// Add all source files (which might need to be downloaded)
	r = pakfire_dist_add_sources(ctx, pakfire, packager, pkg, makefile);
	if (r)
		goto ERROR;

	// Write the archive
	r = pakfire_packager_write_archive(packager, &a);
	if (r < 0)
		goto ERROR;

	// Link the archive
	r = pakfire_archive_lint(a, NULL, NULL);
	if (r < 0)
		goto ERROR;

	// Return the filename
	if (filename)
		*filename = pakfire_packager_filename(packager);

	// Return a reference to the archive
	*archive = pakfire_archive_ref(a);

ERROR:
	if (packager)
		pakfire_packager_unref(packager);
	if (pkg)
		pakfire_package_unref(pkg);
	if (makefile)
		pakfire_parser_unref(makefile);
	if (a)
		pakfire_archive_unref(a);
	if (ctx)
		pakfire_ctx_unref(ctx);

	return r;
}
