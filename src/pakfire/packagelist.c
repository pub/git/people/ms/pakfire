/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>

#include <solv/queue.h>

#include <pakfire/ctx.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/packagelist.h>
#include <pakfire/pakfire.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

struct pakfire_packagelist {
	struct pakfire_ctx* ctx;
	int nrefs;

	struct pakfire_package** packages;
	unsigned int num_packages;
};

static void pakfire_packagelist_clear(struct pakfire_packagelist* self) {
	if (self->packages) {
		for (unsigned int i = 0; i < self->num_packages; i++)
			pakfire_package_unref(self->packages[i]);

		// Free the array
		free(self->packages);
		self->packages = NULL;

		// Reset number of files on the list
		self->num_packages = 0;
	}
}

static void pakfire_packagelist_free(struct pakfire_packagelist* list) {
	pakfire_packagelist_clear(list);

	if (list->ctx)
		pakfire_ctx_unref(list->ctx);
	free(list);
}

int pakfire_packagelist_create(struct pakfire_packagelist** list, struct pakfire_ctx* ctx) {
	struct pakfire_packagelist* self = NULL;

	// Allocate some memory
	self = calloc(1, sizeof(*self));
	if (!self)
		return -errno;

	// Store a reference to the context
	self->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	self->nrefs = 1;

	// Return the pointer
	*list = self;

	return 0;
}

struct pakfire_packagelist* pakfire_packagelist_ref(struct pakfire_packagelist* self) {
	self->nrefs++;

	return self;
}

struct pakfire_packagelist* pakfire_packagelist_unref(struct pakfire_packagelist* self) {
	if (--self->nrefs > 0)
		return self;

	pakfire_packagelist_free(self);
	return NULL;
}

size_t pakfire_packagelist_length(struct pakfire_packagelist* self) {
	return self->num_packages;
}

struct pakfire_package* pakfire_packagelist_get(struct pakfire_packagelist* self, unsigned int index) {
	// Check that index is in range
	if (index >= self->num_packages) {
		errno = ERANGE;
		return NULL;
	}

	return pakfire_package_ref(self->packages[index]);
}

static int pakfire_packagelist_search(struct pakfire_packagelist* self, struct pakfire_package* pkg) {
	int i;
	int r;

	// Set starting points
	int lo = 0;
	int hi = self->num_packages;

	while (lo < hi) {
		// Find the middle
		i = (lo + hi) / 2;

		// Compare the packages
		r = pakfire_package_cmp(self->packages[i], pkg);

		// If the file is greater than the middle, we only need to search in the left half
		if (r >= 0)
			hi = i;

		// Otherwise we need to search in the right half
		else
			lo = i + 1;
	}

	return lo;
}

int pakfire_packagelist_add(struct pakfire_packagelist* self, struct pakfire_package* pkg) {
	// Find the index where the package should go
	int pos = pakfire_packagelist_search(self, pkg);

	// Make space
	self->packages = reallocarray(self->packages, self->num_packages + 1, sizeof(*self->packages));
	if (!self->packages) {
		ERROR(self->ctx, "Could not allocate packagelist: %m\n");
		return -errno;
	}

	// Move everything up one slot
	for (int i = self->num_packages; i > pos; i--)
		self->packages[i] = self->packages[i - 1];

	// Store the package
	self->packages[pos] = pakfire_package_ref(pkg);

	// The list is now longer
	++self->num_packages;

	return 0;
}

int pakfire_packagelist_add_list(struct pakfire_packagelist* self, struct pakfire_packagelist* other) {
	int r;

	// Add all packages from the other list
	for (unsigned int i = 0; i < other->num_packages; i++) {
		r = pakfire_packagelist_add(self, other->packages[i]);
		if (r < 0)
			return r;
	}

	return 0;
}

int pakfire_packagelist_walk(struct pakfire_packagelist* self,
		pakfire_packagelist_walk_callback callback, void* data, int flags) {
	int kept_going = 0;
	int r = 0;

	// Call the callback once for every element on the list
	for (unsigned int i = 0; i < self->num_packages; i++) {
		// Call the callback
		r = callback(self->ctx, self->packages[i], data);

		// Continue if there was no error
		if (r == 0)
			continue;

		// Exit on error
		else if (r < 0)
			return r;

		// If have been asked to keep going, we
		else if (flags & PAKFIRE_PACKAGELIST_KEEPGOING)
			kept_going = r;

		else if (r > 0)
			return r;
	}

	return (kept_going > 0) ? kept_going : r;
}

int pakfire_packagelist_import_solvables(struct pakfire_packagelist* self,
		struct pakfire* pakfire, Queue* q) {
	struct pakfire_package* pkg = NULL;
	int r;

	// Walk through all elements on the queue
	for (int i = 0; i < q->count; i++) {
		r = pakfire_package_create_from_solvable(&pkg, pakfire, NULL, q->elements[i]);
		if (r)
			return r;

		r = pakfire_packagelist_add(self, pkg);
		pakfire_package_unref(pkg);
		if (r)
			return r;
	}

	return 0;
}

static int __pakfire_packagelist_has_path(
		struct pakfire_ctx* ctx, struct pakfire_package* pkg, void* data) {
	const char* path = data;

	// Fetch the path where we would expect this package to be
	const char* pkg_path = pakfire_package_get_path(pkg);
	if (unlikely(!pkg_path))
		return -EINVAL;

	return pakfire_string_equals(pkg_path, path);
}

int pakfire_packagelist_has_path(struct pakfire_packagelist* self, const char* path) {
	return pakfire_packagelist_walk(self, __pakfire_packagelist_has_path, (char*)path, 0);
}
