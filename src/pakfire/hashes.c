/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdio.h>

#include <openssl/crypto.h>

#include <pakfire/ctx.h>
#include <pakfire/hashes.h>
#include <pakfire/hex.h>
#include <pakfire/logging.h>
#include <pakfire/string.h>

const char* pakfire_hash_name(const enum pakfire_hash_type hash) {
	switch (hash) {
		// SHA-3
		case PAKFIRE_HASH_SHA3_512:
			return "sha3-512";

		case PAKFIRE_HASH_SHA3_256:
			return "sha3-256";

		// BLAKE2
		case PAKFIRE_HASH_BLAKE2B512:
			return "blake2b512";

		case PAKFIRE_HASH_BLAKE2S256:
			return "blake2s256";

		// SHA-2
		case PAKFIRE_HASH_SHA2_512:
			return "sha2-512";

		case PAKFIRE_HASH_SHA2_256:
			return "sha2-256";

		// Undefined
		case PAKFIRE_HASH_UNDEFINED:
			break;
	}

	return NULL;
}

enum pakfire_hash_type pakfire_hash_by_name(const char* hash) {
	// SHA-3
	if (pakfire_string_equals(hash, "sha3-512"))
		return PAKFIRE_HASH_SHA3_512;

	else if (pakfire_string_equals(hash, "sha3-256"))
		return PAKFIRE_HASH_SHA3_256;

	// BLAKE2
	else if (pakfire_string_equals(hash, "blake2b512"))
		return PAKFIRE_HASH_BLAKE2B512;

	else if (pakfire_string_equals(hash, "blake2s256"))
		return PAKFIRE_HASH_BLAKE2S256;

	// SHA-2
	else if (pakfire_string_equals(hash, "sha2-512"))
		return PAKFIRE_HASH_SHA2_512;

	else if (pakfire_string_equals(hash, "sha2-256"))
		return PAKFIRE_HASH_SHA2_256;

	return PAKFIRE_HASH_UNDEFINED;
}

void pakfire_hashes_reset(struct pakfire_hashes* hashes) {
	memset(hashes, 0, sizeof(*hashes));
}

int pakfire_hashes_import(struct pakfire_hashes* dst, const struct pakfire_hashes* src) {
	memcpy(dst, src, sizeof(*dst));

	return 0;
}

int pakfire_hashes_has(const struct pakfire_hashes* hashes, const enum pakfire_hash_type type) {
	return (hashes->types & type);
}

int pakfire_hashes_get(const struct pakfire_hashes* hashes,
		const enum pakfire_hash_type type, const unsigned char** hash, size_t* length) {

	// Return NULL if we don't have the hash
	if (!pakfire_hashes_has(hashes, type))
		return 0;

	switch (type) {
		// SHA-3
		case PAKFIRE_HASH_SHA3_512:
			if (length)
				*length = sizeof(hashes->sha3_512);

			*hash = hashes->sha3_512;
			break;

		case PAKFIRE_HASH_SHA3_256:
			if (length)
				*length = sizeof(hashes->sha3_256);

			*hash = hashes->sha3_256;
			break;

		// BLAKE2
		case PAKFIRE_HASH_BLAKE2B512:
			if (length)
				*length = sizeof(hashes->blake2b512);

			*hash = hashes->blake2b512;
			break;

		case PAKFIRE_HASH_BLAKE2S256:
			if (length)
				*length = sizeof(hashes->blake2s256);

			*hash = hashes->blake2s256;
			break;

		// SHA-2
		case PAKFIRE_HASH_SHA2_512:
			if (length)
				*length = sizeof(hashes->sha2_512);

			*hash = hashes->sha2_512;
			break;

		case PAKFIRE_HASH_SHA2_256:
			if (length)
				*length = sizeof(hashes->sha2_256);

			*hash = hashes->sha2_256;
			break;

		default:
			return -EINVAL;
	}

	return 0;
}

int pakfire_hashes_set(struct pakfire_hashes* hashes,
		const enum pakfire_hash_type type, const unsigned char* hash, const size_t length) {
	switch (type) {
		// SHA-3
		case PAKFIRE_HASH_SHA3_512:
			if (length != sizeof(hashes->sha3_512))
				return -EINVAL;

			memcpy(hashes->sha3_512, hash, sizeof(hashes->sha3_512));
			break;

		case PAKFIRE_HASH_SHA3_256:
			if (length != sizeof(hashes->sha3_256))
				return -EINVAL;

			memcpy(hashes->sha3_256, hash, sizeof(hashes->sha3_256));
			break;

		// BLAKE2
		case PAKFIRE_HASH_BLAKE2B512:
			if (length != sizeof(hashes->blake2b512))
				return -EINVAL;

			memcpy(hashes->blake2b512, hash, sizeof(hashes->blake2b512));
			break;

		case PAKFIRE_HASH_BLAKE2S256:
			if (length != sizeof(hashes->blake2s256))
				return -EINVAL;

			memcpy(hashes->blake2s256, hash, sizeof(hashes->blake2s256));
			break;

		// SHA-2
		case PAKFIRE_HASH_SHA2_512:
			if (length != sizeof(hashes->sha2_512))
				return -EINVAL;

			memcpy(hashes->sha2_512, hash, sizeof(hashes->sha2_512));
			break;

		case PAKFIRE_HASH_SHA2_256:
			if (length != sizeof(hashes->sha2_256))
				return -EINVAL;

			memcpy(hashes->sha2_256, hash, sizeof(hashes->sha2_256));
			break;

		default:
			return -EINVAL;
	}

	// Store the type
	hashes->types |= type;

	return 0;
}

int pakfire_hashes_get_hex(const struct pakfire_hashes* hashes,
		const enum pakfire_hash_type type, char** hexdigest) {
	const unsigned char* hash = NULL;
	size_t length = 0;
	int r;

	// Fetch the hash
	r = pakfire_hashes_get(hashes, type, &hash, &length);
	if (r < 0)
		return r;

	// Return immediately without a hash
	if (!hash)
		return 0;

	// Return the hexdigest
	*hexdigest = __pakfire_hexlify(hash, length);
	if (!*hexdigest)
		return -errno;

	return 0;
}

int pakfire_hashes_set_hex(struct pakfire_hashes* hashes,
		const enum pakfire_hash_type type, const char* hexdigest) {
	int r;

	switch (type) {
		case PAKFIRE_HASH_SHA3_512:
			r = pakfire_unhexlify(hashes->sha3_512, hexdigest);
			if (r < 0)
				return r;
			break;

		case PAKFIRE_HASH_SHA3_256:
			r = pakfire_unhexlify(hashes->sha3_256, hexdigest);
			if (r < 0)
				return r;
			break;

		case PAKFIRE_HASH_BLAKE2B512:
			r = pakfire_unhexlify(hashes->blake2b512, hexdigest);
			if (r < 0)
				return r;
			break;

		case PAKFIRE_HASH_BLAKE2S256:
			r = pakfire_unhexlify(hashes->blake2s256, hexdigest);
			if (r < 0)
				return r;
			break;

		case PAKFIRE_HASH_SHA2_512:
			r = pakfire_unhexlify(hashes->sha2_512, hexdigest);
			if (r < 0)
				return r;
			break;

		case PAKFIRE_HASH_SHA2_256:
			r = pakfire_unhexlify(hashes->sha2_256, hexdigest);
			if (r < 0)
				return r;
			break;

		default:
			return -EINVAL;
	}

	// Store the type
	hashes->types |= type;

	return 0;
}

static int __pakfire_hashes_dump(struct pakfire_ctx* ctx,
		const struct pakfire_hashes* hashes, const enum pakfire_hash_type hash, int level) {
	char* hexdigest = NULL;
	int r;

	// Don't dump if hash is not set
	if (!pakfire_hashes_has(hashes, hash))
		return 0;

	// Fetch the hexdigest
	r = pakfire_hashes_get_hex(hashes, hash, &hexdigest);
	if (r < 0)
		goto ERROR;

	// Send to the logger
	pakfire_ctx_log_condition(ctx, level, "  %s: %s\n", pakfire_hash_name(hash), hexdigest);

ERROR:
	if (hexdigest)
		free(hexdigest);

	return r;
}

int pakfire_hashes_dump(struct pakfire_ctx* ctx, const struct pakfire_hashes* hashes, int level) {
	enum pakfire_hash_type hash = PAKFIRE_HASH_UNDEFINED;
	int r;

	PAKFIRE_HASHES_FOREACH(hash) {
		r = __pakfire_hashes_dump(ctx, hashes, hash, level);
		if (r < 0)
			return r;
	}

	return 0;
}

static int __pakfire_hashes_compare(struct pakfire_ctx* ctx, const enum pakfire_hash_type hash,
		const struct pakfire_hashes* hashes1, const struct pakfire_hashes* hashes2) {
	int r;

	// Skip this if not both hashes have this one set
	if (!pakfire_hashes_has(hashes1, hash) || !pakfire_hashes_has(hashes2, hash))
		return 0;

	switch (hash) {
		// SHA-3
		case PAKFIRE_HASH_SHA3_512:
			r = CRYPTO_memcmp(hashes1->sha3_512, hashes2->sha3_512, sizeof(hashes1->sha3_512));
			break;

		case PAKFIRE_HASH_SHA3_256:
			r = CRYPTO_memcmp(hashes1->sha3_256, hashes2->sha3_256, sizeof(hashes1->sha3_256));
			break;

		// BLAKE2
		case PAKFIRE_HASH_BLAKE2B512:
			r = CRYPTO_memcmp(hashes1->blake2b512, hashes2->blake2b512, sizeof(hashes1->blake2b512));
			break;

		case PAKFIRE_HASH_BLAKE2S256:
			r = CRYPTO_memcmp(hashes1->blake2s256, hashes2->blake2s256, sizeof(hashes1->blake2s256));
			break;

		// SHA-2
		case PAKFIRE_HASH_SHA2_512:
			r = CRYPTO_memcmp(hashes1->sha2_512, hashes2->sha2_512, sizeof(hashes1->sha2_512));
			break;

		case PAKFIRE_HASH_SHA2_256:
			r = CRYPTO_memcmp(hashes1->sha2_256, hashes2->sha2_256, sizeof(hashes1->sha2_256));
			break;

		case PAKFIRE_HASH_UNDEFINED:
		default:
			r = -EINVAL;
			break;
	}

	return r;
}

int pakfire_hashes_compare(struct pakfire_ctx* ctx,
		const struct pakfire_hashes* hashes1, const struct pakfire_hashes* hashes2) {
	enum pakfire_hash_type hash = PAKFIRE_HASH_UNDEFINED;
	int r;

	// If either has nothing set, we cannot run this
	if (!hashes1->types || !hashes2->types) {
		ERROR(ctx, "At least one input hashes object is empty\n");
		return -ENOTSUP;
	}

	// Check if there are any overlapping hashes
	if (!(hashes1->types & hashes2->types)) {
		ERROR(ctx, "The hashes don't share any common types\n");
		return -ENOTSUP;
	}

	PAKFIRE_HASHES_FOREACH(hash) {
		r = __pakfire_hashes_compare(ctx, hash, hashes1, hashes2);
		if (r)
			return r;
	}

	return 0;
}
