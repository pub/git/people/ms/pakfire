/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <sys/queue.h>

#include <curl/curl.h>

#include <systemd/sd-event.h>

#include <pakfire/httpclient.h>
#include <pakfire/logging.h>
#include <pakfire/progress.h>
#include <pakfire/string.h>
#include <pakfire/xfer.h>
#include <pakfire/util.h>

// The number of concurrent downloads
#define DEFAULT_MAX_PARALLEL			4

struct pakfire_httpclient_xfer {
	TAILQ_ENTRY(pakfire_httpclient_xfer) nodes;

	struct pakfire_xfer* xfer;

	// Store the status
	enum pakfire_httpclient_xfer_status {
		PAKFIRE_XFER_QUEUED,
		PAKFIRE_XFER_RUNNING,
		PAKFIRE_XFER_FINISHED,
	} status;
};

struct pakfire_httpclient {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Flags
	enum {
		PAKFIRE_HTTPCLIENT_STANDALONE = (1 << 0),
	} flags;

	// Base URL
	char baseurl[PATH_MAX];

	// Event Loop
	sd_event* loop;
	int still_running;

	// Timer
	sd_event_source* timer;

	// Progress
	struct pakfire_progress* progress;

	// How many transfers in parallel?
	long unsigned max_parallel;

	// cURL multi handle
	CURLM* curl;

	// Referenced xfers
	TAILQ_HEAD(xfers, pakfire_httpclient_xfer) xfers;

	// How many xfers do we have queued?
	unsigned int total_xfers;

	// The total downloadsize
	unsigned int total_downloadsize;
};

static int pakfire_httpclient_has_flag(struct pakfire_httpclient* self, int flags) {
	return self->flags & flags;
}

static int pakfire_httpclient_xfer_create(
		struct pakfire_httpclient_xfer** x, struct pakfire_xfer* xfer) {
	struct pakfire_httpclient_xfer* e = NULL;

	// Allocate some space
	e = calloc(1, sizeof(*e));
	if (!e)
		return -errno;

	// Store a reference to the xfer
	e->xfer = pakfire_xfer_ref(xfer);

	// Start as queued
	e->status = PAKFIRE_XFER_QUEUED;

	// Return the pointer
	*x = e;

	return 0;
}

static void pakfire_httpclient_xfer_free(struct pakfire_httpclient_xfer* x) {
	if (x->xfer)
		pakfire_xfer_unref(x->xfer);
	free(x);
}

static size_t pakfire_httpclient_count_xfers(
		struct pakfire_httpclient* self, const enum pakfire_httpclient_xfer_status status) {
	struct pakfire_httpclient_xfer* e = NULL;
	size_t counter = 0;

	TAILQ_FOREACH(e, &self->xfers, nodes) {
		if (e->status == status)
			counter++;
	}

	return counter;
}

static size_t pakfire_httpclient_num_running_xfers(struct pakfire_httpclient* self) {
	return pakfire_httpclient_count_xfers(self, PAKFIRE_XFER_RUNNING);
}

static size_t pakfire_httpclient_num_queued_xfers(struct pakfire_httpclient* self) {
	return pakfire_httpclient_count_xfers(self, PAKFIRE_XFER_QUEUED);
}

/*
	Returns the next queued xfer or NULL
*/
static struct pakfire_httpclient_xfer* pakfire_httpclient_get_queued_xfer(struct pakfire_httpclient* self) {
	struct pakfire_httpclient_xfer* e = NULL;

	TAILQ_FOREACH(e, &self->xfers, nodes) {
		if (e->status == PAKFIRE_XFER_QUEUED)
			return e;
	}

	return NULL;
}

static struct pakfire_httpclient_xfer* pakfire_httpclient_xfer_find(
		struct pakfire_httpclient* self, struct pakfire_xfer* xfer) {
	struct pakfire_httpclient_xfer* e = NULL;

	TAILQ_FOREACH(e, &self->xfers, nodes) {
		if (e->xfer == xfer)
			return e;
	}

	return NULL;
}

static int pakfire_httpclient_remove(
		struct pakfire_httpclient* self, struct pakfire_httpclient_xfer* e) {
	int r;

	// Nothing to do if not running
	switch (e->status) {
		case PAKFIRE_XFER_RUNNING:
			break;

		case PAKFIRE_XFER_QUEUED:
		case PAKFIRE_XFER_FINISHED:
			return 0;
	}

	// Remove the handle
	r = curl_multi_remove_handle(self->curl, pakfire_xfer_handle(e->xfer));
	if (r) {
		ERROR(self->ctx, "Could not remove the handle: %s\n", curl_multi_strerror(r));
		return -ENOTSUP;
	}

	// Mark as finished
	e->status = PAKFIRE_XFER_FINISHED;

	return 0;
}

static int pakfire_httpclient_launch_one(
		struct pakfire_httpclient* self, struct pakfire_httpclient_xfer* e) {
	int r;

	// Remove the handle if we ware launching again
	switch (e->status) {
		case PAKFIRE_XFER_RUNNING:
			r = pakfire_httpclient_remove(self, e);
			if (r < 0)
				return r;
			break;

		default:
			break;
	}

	// Prepare the xfer
	r = pakfire_xfer_prepare(e->xfer, self->progress, 0);
	if (r < 0)
		return r;

	// Add the handle to cURL
	r = curl_multi_add_handle(self->curl, pakfire_xfer_handle(e->xfer));
	if (r) {
		ERROR(self->ctx, "Adding handle failed: %s\n", curl_multi_strerror(r));
		return -ENOTSUP;
	}

	// Mark as running
	e->status = PAKFIRE_XFER_RUNNING;

	// Increment the still running counter. This is a bit hacky, but it prevents
	// that the event loop will terminate too early.
	self->still_running++;

	return 0;
}

static int pakfire_httpclient_launch(struct pakfire_httpclient* self) {
	struct pakfire_httpclient_xfer* e = NULL;
	int r;

	while (pakfire_httpclient_num_running_xfers(self) < self->max_parallel) {
		// Fetch the next queued xfer
		e = pakfire_httpclient_get_queued_xfer(self);
		if (!e)
			break;

		// Launch it
		r = pakfire_httpclient_launch_one(self, e);
		if (r < 0)
			return r;
	}

	return 0;
}

static int pakfire_httpclient_check(struct pakfire_httpclient* self) {
	struct pakfire_httpclient_xfer* e = NULL;
	struct pakfire_xfer* xfer = NULL;
	int r;

	CURLMsg* msg = NULL;
	int msgs_left = 0;

	for (;;) {
		// Read the next message
		msg = curl_multi_info_read(self->curl, &msgs_left);
		if (!msg)
			break;

		switch (msg->msg) {
			case CURLMSG_DONE:
				// Update reference to transfer
				curl_easy_getinfo(msg->easy_handle, CURLINFO_PRIVATE, &xfer);

				// Find the queue element
				e = pakfire_httpclient_xfer_find(self, xfer);
				if (!e) {
					ERROR(self->ctx, "Could not find queued transfer\n");
					return -EINVAL;
				}

				// Call the done callback
				r = pakfire_xfer_done(xfer, self->loop, msg->data.result);
				if (r < 0) {
					switch (-r) {
						case EAGAIN:
							// Launch it again
							r = pakfire_httpclient_launch_one(self, e);
							if (r < 0)
								return r;

							// Move to the next message
							continue;

						// Handle signal that the handle should not be dequeued, yet
						case EINPROGRESS:
							continue;

						// Raise any errors
						default:
							return r;
					}
				}

				// Remove the xfer from the queue
				pakfire_httpclient_dequeue(self, xfer);

				break;

			default:
				ERROR(self->ctx, "Received unhandled cURL message %u\n", msg->msg);
				break;
		}
	}

	// Launch some more transfers
	r = pakfire_httpclient_launch(self);
	if (r < 0)
		return r;

	// If we control the loop, terminate it if there are no more transfers left
	if (pakfire_httpclient_has_flag(self, PAKFIRE_HTTPCLIENT_STANDALONE)) {
		if (self->still_running <= 0)
			return sd_event_exit(self->loop, 0);
	}

	return 0;
}

static int __pakfire_httpclient_timer(sd_event_source* s, uint64_t usec, void* data) {
	struct pakfire_httpclient* self = data;
	int r;

	DEBUG(self->ctx, "cURL timer fired\n");

	r = curl_multi_socket_action(self->curl, CURL_SOCKET_TIMEOUT, 0, &self->still_running);
	if (r)
		return r;

	// Check for any messages
	r = pakfire_httpclient_check(self);
	if (r)
		return r;

	return 0;
}

static int pakfire_httpclient_timer(CURLM* multi, long timeout_ms, void* data) {
	struct pakfire_httpclient* self = data;
	int r;

	// A negative value indicates that we should remove the timer
	if (timeout_ms < 0) {
		if (self->timer) {
			r = sd_event_source_set_enabled(self->timer, SD_EVENT_OFF);
			if (r < 0)
				ERROR(self->ctx, "Could not disarm the timer: %s\n", strerror(-r));
		}

		return 0;
	}

	// Set the timer
	r = sd_event_source_set_time_relative(self->timer, timeout_ms * 1000);
	if (r < 0) {
		ERROR(self->ctx, "Could not set timer: %s\n", strerror(-r));

		return r;
	}

	// Have the timer fire once
	r = sd_event_source_set_enabled(self->timer, SD_EVENT_ONESHOT);
	if (r < 0) {
		ERROR(self->ctx, "Could not enable the timer: %s\n", strerror(-r));

		return r;
	}

	DEBUG(self->ctx, "cURL set a timer for %ldms\n", timeout_ms);

	return 0;
}

static int __pakfire_httpclient_socket(sd_event_source* s, int fd, uint32_t events, void* data) {
	struct pakfire_httpclient* self = data;
	int r;

	int action = 0;

	if (events & EPOLLIN)
		action |= CURL_CSELECT_IN;

	if (events & EPOLLOUT)
		action |= CURL_CSELECT_OUT;

	//DEBUG(self->ctx, "cURL has activity on socket %d\n", fd);

	// Inform cURL about some socket activity
	r = curl_multi_socket_action(self->curl, fd, action, &self->still_running);
	if (r)
		return r;

	// Check for any messages
	r = pakfire_httpclient_check(self);
	if (r)
		return r;

	// Disarm the timer
	if (self->still_running <= 0) {
		if (self->timer) {
			r = sd_event_source_set_enabled(self->timer, SD_EVENT_OFF);
			if (r < 0) {
				ERROR(self->ctx, "Could not disarm the timer: %s\n", strerror(-r));

				return r;
			}
		}
	}

	return 0;
}

static int pakfire_httpclient_socket(CURL* e, curl_socket_t fd, int what, void* data, void* data2) {
	struct pakfire_httpclient* self = data;
	sd_event_source* s = data2;
	uint32_t events = 0;
	int r;

	// Remove the socket?
	if (what == CURL_POLL_REMOVE) {
		// Disable the event
		r = sd_event_source_set_enabled(s, SD_EVENT_OFF);
		if (r < 0)
			ERROR(self->ctx, "Could not disable fd %d: %s\n", fd, strerror(-r));

		DEBUG(self->ctx, "cURL deregistered socket %d\n", fd);

		return r;
	}

	// Do we need to read from this socket?
	if (what & CURL_POLL_IN)
		events |= EPOLLIN;

	// Do we need to write to this socket?
	if (what & CURL_POLL_OUT)
		events |= EPOLLOUT;

	// Change events?
	if (s) {
		r = sd_event_source_set_io_events(s, events);
		if (r < 0) {
			ERROR(self->ctx, "Could not change events for socket %d: %s\n",
				fd, strerror(-r));

			return r;
		}

		DEBUG(self->ctx, "cURL changed socket %d\n", fd);

		return 0;
	}

	// Add the socket to the event loop
	r = sd_event_add_io(self->loop, &s, fd, events, __pakfire_httpclient_socket, self);
	if (r < 0) {
		ERROR(self->ctx, "Could not register socket %d: %s\n", fd, strerror(-r));

		goto ERROR;
	}

	// Store the event source
	curl_multi_assign(self->curl, fd, sd_event_source_ref(s));

	DEBUG(self->ctx, "cURL registered socket %d\n", fd);

ERROR:
	if (s)
		sd_event_source_unref(s);

	return r;
}

static int pakfire_httpclient_setup_loop(struct pakfire_httpclient* self, sd_event* loop) {
	int r;

	// Use the given loop
	if (loop) {
		self->loop = sd_event_ref(loop);

	// Otherwise create a new loop
	} else {
		r = sd_event_new(&self->loop);
		if (r < 0) {
			ERROR(self->ctx, "Could not setup event loop: %s\n", strerror(-r));

			return r;
		}

		// We are no in standalone-mode
		self->flags |= PAKFIRE_HTTPCLIENT_STANDALONE;
	}

	// Create a new timer
	r = sd_event_add_time_relative(self->loop, &self->timer, CLOCK_MONOTONIC, 0, 0,
		__pakfire_httpclient_timer, self);
	if (r < 0) {
		ERROR(self->ctx, "Could not set timer: %s\n", strerror(-r));

		return r;
	}

	return 0;
}

static int pakfire_httpclient_setup_curl(struct pakfire_httpclient* self) {
	int r;

	// Initialize cURL
	r = curl_global_init(CURL_GLOBAL_DEFAULT);
	if (r) {
		ERROR(self->ctx, "Could not initialize cURL: %d\n", r);
		return r;
	}

	// Create a new multi handle
	self->curl = curl_multi_init();
	if (!self->curl) {
		ERROR(self->ctx, "Could not create cURL multi handle\n");
		return 1;
	}

	// Register with the event loop
	r = curl_multi_setopt(self->curl, CURLMOPT_TIMERFUNCTION, pakfire_httpclient_timer);
	if (r) {
		ERROR(self->ctx, "Could not register the timer function: %s\n",
			curl_multi_strerror(r));

		return r;
	}

	r = curl_multi_setopt(self->curl, CURLMOPT_TIMERDATA, self);
	if (r) {
		ERROR(self->ctx, "Could not register the timer data: %s\n",
			curl_multi_strerror(r));

		return r;
	}

	r = curl_multi_setopt(self->curl, CURLMOPT_SOCKETFUNCTION, pakfire_httpclient_socket);
	if (r) {
		ERROR(self->ctx, "Could not register the socket function: %s\n",
			curl_multi_strerror(r));

		return r;
	}

	r = curl_multi_setopt(self->curl, CURLMOPT_SOCKETDATA, self);
	if (r) {
		ERROR(self->ctx, "Could not register the socket data: %s\n",
			curl_multi_strerror(r));

		return r;
	}

	// Set the number of open idle connections
	r = curl_multi_setopt(self->curl, CURLMOPT_MAXCONNECTS, 16);
	if (r) {
		ERROR(self->ctx, "Could not set max parallel transfers: %s\n",
			curl_multi_strerror(r));

		return r;
	}

	// Do not open more than eight connections to the same host
	r = curl_multi_setopt(self->curl, CURLMOPT_MAX_HOST_CONNECTIONS, 8);
	if (r) {
		ERROR(self->ctx, "Could not set max host connections: %s\n",
			curl_multi_strerror(r));

		return r;
	}

	// Limit total number of open connections
	r = curl_multi_setopt(self->curl, CURLMOPT_MAX_TOTAL_CONNECTIONS, 64);
	if (r) {
		ERROR(self->ctx, "Could not set max total connections: %s\n",
			curl_multi_strerror(r));

		return r;
	}

	return 0;
}

static int pakfire_httpclient_setup_progress(struct pakfire_httpclient* self) {
	int r;

	const int flags =
		PAKFIRE_PROGRESS_SHOW_PERCENTAGE |
		PAKFIRE_PROGRESS_SHOW_ETA |
		PAKFIRE_PROGRESS_SHOW_BYTES_TRANSFERRED |
		PAKFIRE_PROGRESS_SHOW_TRANSFER_SPEED;

	// Create a new progress indicator
	r = pakfire_progress_create(&self->progress, self->ctx, flags, NULL);
	if (r)
		return r;

	return 0;
}

static void pakfire_httpclient_free(struct pakfire_httpclient* self) {
	struct pakfire_httpclient_xfer* e = NULL;

	// Free any xfers that we still hold
	while (!TAILQ_EMPTY(&self->xfers)) {
		e = TAILQ_LAST(&self->xfers, xfers);
		TAILQ_REMOVE(&self->xfers, e, nodes);

		pakfire_httpclient_xfer_free(e);
	}

	if (self->progress)
		pakfire_progress_unref(self->progress);
	if (self->curl)
		curl_multi_cleanup(self->curl);
	if (self->timer)
		sd_event_source_unref(self->timer);
	if (self->loop)
		sd_event_unref(self->loop);
	if (self->ctx)
		pakfire_ctx_unref(self->ctx);
	free(self);
}

int pakfire_httpclient_create(struct pakfire_httpclient** client,
		struct pakfire_ctx* ctx, sd_event* loop) {
	struct pakfire_httpclient* self = NULL;
	int r;

	// Allocate a new object
	self = calloc(1, sizeof(*self));
	if (!self)
		return -errno;

	// Store reference to the context
	self->ctx = pakfire_ctx_ref(ctx);

	// Initialize reference counting
	self->nrefs = 1;

	// Set parallelism
	self->max_parallel = DEFAULT_MAX_PARALLEL;

	// Init the xfer queue
	TAILQ_INIT(&self->xfers);

	// Setup event loop
	r = pakfire_httpclient_setup_loop(self, loop);
	if (r)
		goto ERROR;

	// Setup cURL
	r = pakfire_httpclient_setup_curl(self);
	if (r)
		goto ERROR;

	// Setup progress
	r = pakfire_httpclient_setup_progress(self);
	if (r)
		goto ERROR;

	// Return the pointer
	*client = pakfire_httpclient_ref(self);

ERROR:
	if (self)
		pakfire_httpclient_unref(self);

	return r;
}

struct pakfire_httpclient* pakfire_httpclient_ref(struct pakfire_httpclient* self) {
	++self->nrefs;

	return self;
}

struct pakfire_httpclient* pakfire_httpclient_unref(struct pakfire_httpclient* self) {
	if (--self->nrefs > 0)
		return self;

	pakfire_httpclient_free(self);
	return NULL;
}

int pakfire_httpclient_enqueue(struct pakfire_httpclient* self, struct pakfire_xfer* xfer) {
	struct pakfire_httpclient_xfer* e = NULL;
	int r;

	// Create a new object
	r = pakfire_httpclient_xfer_create(&e, xfer);
	if (r < 0)
		return r;

	// Update the total download size
	self->total_downloadsize += pakfire_xfer_get_size(xfer);

	// Keep a reference to the xfer
	TAILQ_INSERT_TAIL(&self->xfers, e, nodes);

	// We are done if we are running in standalone mode
	if (pakfire_httpclient_has_flag(self, PAKFIRE_HTTPCLIENT_STANDALONE))
		return 0;

	// Otherwise we launch the transfer straight away
	return pakfire_httpclient_launch_one(self, e);
}

int pakfire_httpclient_dequeue(struct pakfire_httpclient* self, struct pakfire_xfer* xfer) {
	struct pakfire_httpclient_xfer* e = NULL;
	int r;

	// Find reference
	e = pakfire_httpclient_xfer_find(self, xfer);
	if (!e)
		return 0;

	// Remove the transfer
	r = pakfire_httpclient_remove(self, e);
	if (r < 0)
		return r;

	// Reduce the total download size
	self->total_downloadsize -= pakfire_xfer_get_size(xfer);

	TAILQ_REMOVE(&self->xfers, e, nodes);
	pakfire_httpclient_xfer_free(e);

	return 0;
}

int pakfire_httpclient_run(struct pakfire_httpclient* self, const char* title) {
	int r = 0;

	// This can only be run in standalone-mode
	if (!pakfire_httpclient_has_flag(self, PAKFIRE_HTTPCLIENT_STANDALONE)) {
		ERROR(self->ctx, "Trying to launch HTTP client that is not in standalone-mode\n");
		return -ENOTSUP;
	}

	// Cannot run without any transfers
	if (TAILQ_EMPTY(&self->xfers)) {
		DEBUG(self->ctx, "Skipping running HTTP client without any transfers\n");
		return 0;
	}

	// Set the title
	r = pakfire_progress_set_title(self->progress, "%s", title);
	if (r)
		goto ERROR;

	// Start the progress
	r = pakfire_progress_start(self->progress, self->total_downloadsize);
	if (r)
		goto ERROR;

	// Launch some transfers
	r = pakfire_httpclient_launch(self);
	if (r < 0)
		goto ERROR;

	// Run the event loop
	r = sd_event_loop(self->loop);
	if (r < 0) {
		ERROR(self->ctx, "Event loop failed: %s\n", strerror(-r));
		goto ERROR;
	}

	if (pakfire_httpclient_num_running_xfers(self)) {
		ERROR(self->ctx, "HTTP client ended with running transfers\n");
		r = -ECANCELED;
		goto ERROR;
	}

	if (pakfire_httpclient_num_queued_xfers(self)) {
		ERROR(self->ctx, "Not all queued items have been downloaded\n");
		r = -ECANCELED;
		goto ERROR;
	}

ERROR:
	// We are finished!
	pakfire_progress_finish(self->progress);

	return r;
}
