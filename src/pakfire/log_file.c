/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>

#include <pakfire/ctx.h>
#include <pakfire/log_file.h>
#include <pakfire/string.h>
#include <pakfire/util.h>
#include <pakfire/xfopen.h>

#define MAX_LINE_LENGTH 4096

struct pakfire_log_file {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Filename
	char filename[PATH_MAX];

	// Flags
	int flags;

	// Internal Flags
	enum pakfire_log_file_internal_flags {
		PAKFIRE_LOG_FILE_UNLINK = (1 << 0),
	} internal_flags;

	// Path
	char path[PATH_MAX];

	// File Handle
	FILE* f;
};

static void pakfire_log_file_free(struct pakfire_log_file* self) {
	// Close the log file
	pakfire_log_file_close(self);

	// Unlink the file
	if (self->internal_flags & PAKFIRE_LOG_FILE_UNLINK) {
		if (*self->path)
			unlink(self->path);
	}

	if (self->ctx)
		pakfire_ctx_unref(self->ctx);
	free(self);
}

int pakfire_log_file_create(struct pakfire_log_file** file, struct pakfire_ctx* ctx,
		const char* path, const char* filename, int flags) {
	struct pakfire_log_file* self = NULL;
	int r;

	// Allocate some memory
	self = calloc(1, sizeof(*self));
	if (!self)
		return -errno;

	// Store the context
	self->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	self->nrefs = 1;

	// Store flags
	self->flags = flags;

	// Store the filename
	r = pakfire_string_set(self->filename, filename);
	if (r < 0)
		goto ERROR;

	// Store the path
	if (path) {
		r = pakfire_string_set(self->path, path);
		if (r < 0)
			goto ERROR;

		// Open the file
		self->f = fopen(self->path, "w");
		if (!self->f) {
			ERROR(self->ctx, "Failed to open %s for writing: %m\n", self->path);
			goto ERROR;
		}

	// If no path has been given, we create a new temporary file
	} else {
		// Make some path
		r = pakfire_string_set(self->path, PAKFIRE_TMP_DIR "/pakfire-log.XXXXXX");
		if (r < 0)
			goto ERROR;

		// Create a new temporary file
		self->f = pakfire_mktemp(self->path, 0600);
		if (!self->f) {
			r = -errno;
			goto ERROR;
		}

		// Remove the temporary file afterwards
		self->internal_flags |= PAKFIRE_LOG_FILE_UNLINK;
	}

	// If we want to write the log file compressed, we replace the file handle
	if (self->flags & PAKFIRE_LOG_FILE_COMPRESS) {
		self->f = pakfire_gzfopen(self->f, "w");
		if (!self->f) {
			ERROR(self->ctx, "Could not enable compression for log file: %m\n");
			r = -errno;
			goto ERROR;
		}

		// Add a suffix to the filename
		r = pakfire_string_append(self->filename, ".gz");
		if (r < 0)
			goto ERROR;
	}

	DEBUG(self->ctx, "Created log file %s (%s)\n", self->filename, self->path);

	// Return the pointer
	*file = self;
	return 0;

ERROR:
	pakfire_log_file_free(self);

	return r;
}

struct pakfire_log_file* pakfire_log_file_ref(struct pakfire_log_file* self) {
	self->nrefs++;

	return self;
}

struct pakfire_log_file* pakfire_log_file_unref(struct pakfire_log_file* self) {
	if (--self->nrefs > 0)
		return self;

	pakfire_log_file_free(self);
	return NULL;
}

const char* pakfire_log_file_filename(struct pakfire_log_file* self) {
	return self->filename;
}

const char* pakfire_log_file_path(struct pakfire_log_file* self) {
	return self->path;
}

int pakfire_log_file_close(struct pakfire_log_file* self) {
	int r;

	if (self->f) {
		r = fclose(self->f);
		self->f = NULL;

		if (r < 0) {
			ERROR(self->ctx, "Could not close log file: %s\n", strerror(-r));
			return r;
		}

		DEBUG(self->ctx, "Closed log file %s\n", self->path);
	}

	return 0;
}

int pakfire_log_file_write(struct pakfire_log_file* self, int priority, const char* buffer, ssize_t length) {
	int r;

	// Check inputs
	if (unlikely(!buffer))
		return -EINVAL;

	// Check if the file is still open
	if (unlikely(!self->f))
		return -EBADF;

	// Don't log any debugging stuff to the file
	switch (priority) {
		case LOG_DEBUG:
			return 0;

		default:
			break;
	}

	// Automatically determine the length
	if (unlikely(length < 0))
		length = strlen(buffer);

	// Write the buffer to the file
	r = fwrite(buffer, 1, length, self->f);
	if (unlikely(r < length)) {
		ERROR(self->ctx, "Could not write to log file: %m\n");
		return -errno;
	}

	return 0;
}
