/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2014 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_FILELIST_H
#define PAKFIRE_FILELIST_H

#include <stdio.h>

struct pakfire_filelist;

#include <pakfire/ctx.h>
#include <pakfire/file.h>
#include <pakfire/pakfire.h>

int pakfire_filelist_create(struct pakfire_filelist** list, struct pakfire* pakfire);

struct pakfire_filelist* pakfire_filelist_ref(struct pakfire_filelist* list);
struct pakfire_filelist* pakfire_filelist_unref(struct pakfire_filelist* list);

size_t pakfire_filelist_length(struct pakfire_filelist* list);
int pakfire_filelist_is_empty(struct pakfire_filelist* list);
void pakfire_filelist_clear(struct pakfire_filelist* list);

struct pakfire_file* pakfire_filelist_get(struct pakfire_filelist* list, size_t index);

int pakfire_filelist_add(struct pakfire_filelist* list, struct pakfire_file* file);

size_t pakfire_filelist_total_size(struct pakfire_filelist* list);

int pakfire_filelist_remove_all(struct pakfire_filelist* list,
	struct pakfire_filelist* removees);

enum pakfire_filelist_scan_flags {
	PAKFIRE_FILELIST_EXTENDED_MATCHING = (1 << 0),
	PAKFIRE_FILELIST_NO_DIRECTORIES    = (1 << 1),
};

int pakfire_filelist_scan(struct pakfire_filelist* list, const char* root,
		const char** includes, const char** excludes, int flags);

int pakfire_filelist_contains(struct pakfire_filelist* list, const char* pattern);

typedef int (*pakfire_filelist_walk_callback)
	(struct pakfire_ctx* ctx, struct pakfire_file* file, void* data);

enum pakfire_filelist_walk_flags {
	PAKFIRE_FILELIST_SHOW_PROGRESS = (1 << 0),
};

int pakfire_filelist_walk(struct pakfire_filelist* list,
	pakfire_filelist_walk_callback callback, void* data, int flags, const char* title);
int pakfire_filelist_dump(struct pakfire_filelist* list, int flags);

int pakfire_filelist_verify(struct pakfire_filelist* list, struct pakfire_filelist* errors);

int pakfire_filelist_cleanup(struct pakfire_filelist* list, int flags);

int pakfire_filelist_matches_class(struct pakfire_filelist* list, int class);

#endif /* PAKFIRE_FILELIST_H */
