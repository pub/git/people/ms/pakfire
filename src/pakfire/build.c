/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/mount.h>
#include <time.h>
#include <unistd.h>
#include <uuid/uuid.h>

#include <pakfire/build.h>
#include <pakfire/cgroup.h>
#include <pakfire/config.h>
#include <pakfire/ctx.h>
#include <pakfire/deps.h>
#include <pakfire/dist.h>
#include <pakfire/elf.h>
#include <pakfire/env.h>
#include <pakfire/file.h>
#include <pakfire/i18n.h>
#include <pakfire/jail.h>
#include <pakfire/logging.h>
#include <pakfire/mount.h>
#include <pakfire/package.h>
#include <pakfire/packager.h>
#include <pakfire/parser.h>
#include <pakfire/path.h>
#include <pakfire/problem.h>
#include <pakfire/repo.h>
#include <pakfire/scriptlet.h>
#include <pakfire/solution.h>
#include <pakfire/string.h>
#include <pakfire/stripper.h>
#include <pakfire/transaction.h>
#include <pakfire/util.h>

#define CCACHE_DIR "/var/cache/ccache"

// We guarantee 2 GiB of memory to every build container
#define PAKFIRE_BUILD_MEMORY_GUARANTEED		(size_t)2 * 1024 * 1024 * 1024

// We allow only up to 2048 processes/threads for every build container
#define PAKFIRE_BUILD_PID_LIMIT				(size_t)2048

// A list of packages that is installed by default
static const char* PAKFIRE_BUILD_PACKAGES[] = {
	"build-essential",
	NULL,
};

struct pakfire_build {
	struct pakfire_ctx* ctx;
	struct pakfire* pakfire;
	int nrefs;

	// Flags
	int flags;

	// Build ID
	uuid_t id;
	char _id[UUID_STR_LEN];

	// Times
	struct timespec time_start;

	// cgroup
	struct pakfire_cgroup* cgroup;

	// Jail
	struct pakfire_jail* jail;

	// Environment
	struct pakfire_env* env;

	// The build repository
	struct pakfire_repo* repo;

	// Buildroot
	char buildroot[PATH_MAX];

	// ccache path
	char ccache_path[PATH_MAX];
};

#define TEMPLATE \
	"#!/bin/bash --login\n" \
	"\n" \
	"set -e\n" \
	"set -x\n" \
	"\n" \
	"%%{_%s}\n" \
	"\n" \
	"exit 0\n"

static int pakfire_build_has_flag(struct pakfire_build* build, int flag) {
	return build->flags & flag;
}

static double pakfire_build_duration(struct pakfire_build* build) {
	struct timespec now;
	int r;

	// What time is it now?
	r = clock_gettime(CLOCK_MONOTONIC, &now);
	if (r < 0)
		return r;

	return pakfire_timespec_delta(&now, &build->time_start);
}

static int pakfire_build_output_callback(
		struct pakfire_ctx* ctx, void* data, const char* buffer, size_t length) {
	struct pakfire_build* build = data;

	// Get the runtime of the build
	const double t = pakfire_build_duration(build);
	if (t < 0)
		return t;

	const unsigned int h  = (unsigned int)t / 3600;
	const unsigned int m  = (unsigned int)t % 3600 / 60;
	const unsigned int s  = (unsigned int)t % 60;
	const unsigned int ms = (unsigned int)(t * 1000.0) % 1000;

	if (h)
		INFO(ctx, "[%02u:%02u:%02u.%04u] %.*s", h, m, s, ms, (int)length, buffer);

	else if (m)
		INFO(ctx, "[   %02u:%02u.%04u] %.*s", m, s, ms, (int)length, buffer);

	else
		INFO(ctx, "[      %02u.%04u] %.*s", s, ms, (int)length, buffer);

	return length;
}

static int __pakfire_build_setup_repo(struct pakfire* pakfire,
		struct pakfire_repo* repo, void* p) {
	char path[PATH_MAX];
	FILE* f = NULL;
	int r;

	struct pakfire_build* build = (struct pakfire_build*)p;

	// Skip processing the installed repository
	if (pakfire_repo_is_installed_repo(repo))
		return 0;

	// Skip processing any other internal repositories
	if (pakfire_repo_is_internal(repo))
		return 0;

	const char* name = pakfire_repo_get_name(repo);

	DEBUG(build->ctx, "Exporting repository configuration for '%s'\n", name);

	// Make path for configuration file
	r = pakfire_path(build->pakfire, path, PAKFIRE_CONFIG_DIR "/repos/%s.repo", name);
	if (r) {
		ERROR(build->ctx, "Could not make repository configuration path for %s: %m\n", name);
		goto ERROR;
	}

	// Create the parent directory
	r = pakfire_mkparentdir(path, 0755);
	if (r)
		goto ERROR;

	// Open the repository configuration
	f = fopen(path, "w");
	if (!f) {
		ERROR(build->ctx, "Could not open %s for writing: %m\n", path);
		goto ERROR;
	}

	// Write repository configuration
	r = pakfire_repo_write_config(repo, f);
	if (r < 0) {
		ERROR(build->ctx, "Could not write repository configuration for %s: %s\n",
			name, strerror(-r));
		goto ERROR;
	}

	// Bind-mount any local repositories
	if (pakfire_repo_is_local(repo)) {
		const char* _path = pakfire_repo_get_path(repo);

		// Bind-mount the repository data read-only
		if (pakfire_path_exists(_path)) {
			r = pakfire_jail_bind(build->jail, _path, _path, MS_RDONLY);
			if (r) {
				ERROR(build->ctx, "Could not bind-mount the repository at %s: %m\n", _path);
				goto ERROR;
			}
		}
	}

ERROR:
	if (f)
		fclose(f);

	return r;
}

/*
	This function enables the local repository so that it can be accessed by
	a pakfire instance running inside the chroot.
*/
static int pakfire_build_enable_repos(struct pakfire_build* build) {
	return pakfire_repo_walk(build->pakfire, __pakfire_build_setup_repo, build);
}

static int pakfire_build_read_script(struct pakfire_build* build,
		const char* filename, char** buffer, size_t* length) {
	char path[PATH_MAX];
	FILE* f = NULL;
	int r;

	// Compose the source path
	r = pakfire_path_append(path, PAKFIRE_SCRIPTS_DIR, filename);
	if (r) {
		ERROR(build->ctx, "Could not compose path for script '%s': %m\n", filename);
		goto ERROR;
	}

	DEBUG(build->ctx, "Reading script from %s...\n", path);

	// Open the file
	f = fopen(path, "r");
	if (!f) {
		ERROR(build->ctx, "Could not open script %s: %m\n", path);
		goto ERROR;
	}

	// Map the script into the buffer
	r = pakfire_mmap(fileno(f), buffer, length);
	if (r < 0)
		goto ERROR;

ERROR:
	if (f)
		fclose(f);

	return r;
}

static int pakfire_build_run_script(
		struct pakfire_build* build,
		const char* filename,
		const char* args[],
		pakfire_pty_stdin_callback stdin_callback, void* stdin_data,
		pakfire_pty_stdout_callback stdout_callback, void* stdout_data) {
	char* script = NULL;
	size_t length = 0;
	int r;

	DEBUG(build->ctx, "Running build script '%s'...\n", filename);

	// Read the script
	r = pakfire_build_read_script(build, filename, &script, &length);
	if (r < 0) {
		ERROR(build->ctx, "Could not read script %s: %s\n", filename, strerror(-r));
		goto ERROR;
	}

	// Execute the script
	r = pakfire_jail_exec_script(build->jail, script, length, args, build->env,
			stdin_callback, stdin_data, stdout_callback, stdout_data);
	if (r)
		ERROR(build->ctx, "Script '%s' failed with status %d\n", filename, r);

ERROR:
	if (script)
		munmap(script, length);

	return r;
}

struct pakfire_find_deps_ctx {
	struct pakfire_build* build;
	struct pakfire_package* pkg;
	struct pakfire_filelist* filelist;
};

static int pakfire_build_process_pkgconfig_dep(struct pakfire_package* pkg,
		const enum pakfire_package_key key, const char* line, const size_t length) {
	char buffer[PATH_MAX];
	int r;

	// Copy the line to a buffer
	r = pakfire_string_set(buffer, line);
	if (r < 0)
		return r;

	// Remove any whitespace
	pakfire_string_strip(buffer);

	const char* name = NULL;
	const char* op = NULL;
	const char* version = NULL;
	const char* token = NULL;
	char* p = NULL;
	int i = 0;

	// Try splitting the string
	token = strtok_r(buffer, " ", &p);

	while (token) {
		switch (i++) {
			case 0:
				name = token;
				break;

			case 1:
				op = token;
				break;

			case 2:
				version = token;
				break;

			default:
				break;
		};

		token = strtok_r(NULL, " ", &p);
	}

	// Add everything if we have all the information
	if (name && op && version) {
		r = pakfire_package_add_dep(pkg, key, "pkgconfig(%s) %s %s", name, op, version);
		if (r < 0)
			return r;

	// Otherwise we add the entire buffer and hope for the best
	} else {
		r = pakfire_package_add_dep(pkg, key, "pkgconfig(%s)", buffer);
		if (r < 0)
			return r;
	}

	return length;
}

static int pakfire_build_process_pkgconfig_provides(
		struct pakfire_ctx* ctx, void* data, const char* line, const size_t length) {
	struct pakfire_find_deps_ctx* deps = data;

	return pakfire_build_process_pkgconfig_dep(deps->pkg, PAKFIRE_PKG_PROVIDES, line, length);
}

static int pakfire_build_process_pkgconfig_requires(
		struct pakfire_ctx* ctx, void* data, const char* line, const size_t length) {
	struct pakfire_find_deps_ctx* deps = data;

	return pakfire_build_process_pkgconfig_dep(deps->pkg, PAKFIRE_PKG_REQUIRES, line, length);
}

static int pakfire_build_find_pkgconfig_provides(
		struct pakfire_ctx* ctx, struct pakfire_file* file, struct pakfire_find_deps_ctx* deps) {
	int r;

	// Fetch the absolute path
	const char* path = pakfire_file_get_abspath(file);
	if (!path)
		return -EINVAL;

	const char* argv[] = {
		"pkg-config",
		"--print-provides",
		path,
		NULL,
	};

	// Run pkg-config and process the output
	r = pakfire_jail_communicate(deps->build->jail, argv, NULL, 0, NULL, NULL,
			pakfire_build_process_pkgconfig_provides, deps);
	if (r < 0)
		return r;

	return 0;
}

static int pakfire_build_find_pkgconfig_requires(
		struct pakfire_ctx* ctx, struct pakfire_file* file, struct pakfire_find_deps_ctx* deps) {
	struct pakfire_env* env = NULL;
	int r;

	// Fetch the absolute path
	const char* path = pakfire_file_get_abspath(file);
	if (!path)
		return -EINVAL;

	// This package now requires pkgconfig
	r = pakfire_package_add_dep(deps->pkg, PAKFIRE_PKG_REQUIRES, "pkgconfig");
	if (r < 0)
		goto ERROR;

	// Create a new environment
	r = pakfire_env_create(&env, ctx);
	if (r < 0)
		goto ERROR;

	const char* argv[] = {
		"pkg-config",
		"--print-requires",
		"--print-requires-private",
		path,
		NULL,
	};

	// Tell pkg-config to look inside BUILDROOT
	const char* search_paths[] = {
		"/usr/lib64/pkgconfig",
		"/usr/lib/pkgconfig",
		"/usr/share/pkgconfig",
		NULL,
	};

	for (const char** search_path = search_paths; *search_path; search_path++) {
		r = pakfire_env_append(env, "PKG_CONFIG_PATH", "%s%s", deps->build->buildroot, *search_path);
		if (r < 0)
			goto ERROR;
	}

	// Run pkg-config and process the output
	r = pakfire_jail_communicate(deps->build->jail, argv, env, 0, NULL, NULL,
			pakfire_build_process_pkgconfig_requires, deps);
	if (r < 0)
		goto ERROR;

ERROR:
	if (env)
		pakfire_env_unref(env);

	return r;
}

static int pakfire_build_find_elf_provides(
		struct pakfire_ctx* ctx, struct pakfire_file* file, struct pakfire_find_deps_ctx* deps) {
	struct pakfire_elf* elf = NULL;
	char** provides = NULL;
	int r;

	// Skip files that are not executable
	if (!pakfire_file_is_executable(file))
		return 0;

	// Only handle .so files
	if (!pakfire_file_matches(file, "**.so") && !pakfire_file_matches(file, "**.so.*"))
		return 0;

	// Try to open the ELF file
	r = pakfire_elf_open_file(&elf, ctx, file);
	if (r < 0) {
		switch (-r) {
			// This does not seem to be an ELF file
			case ENOTSUP:
				return 0;

			// Raise any other errors
			default:
				goto ERROR;
		}
	}

	// Fetch all provides
	r = pakfire_elf_provides(elf, &provides);
	if (r < 0)
		goto ERROR;

	// Add them all to the package
	if (provides) {
		for (char** p = provides; *p; p++) {
			r = pakfire_package_add_dep(deps->pkg, PAKFIRE_PKG_PROVIDES, "%s", *p);
			if (r < 0)
				goto ERROR;
		}
	}

ERROR:
	if (provides)
		pakfire_strings_free(provides);
	if (elf)
		pakfire_elf_unref(elf);

	return r;
}

static int pakfire_build_find_elf_requires(
		struct pakfire_ctx* ctx, struct pakfire_file* file, struct pakfire_find_deps_ctx* deps) {
	struct pakfire_elf* elf = NULL;
	char** requires = NULL;
	int r;

	// Files must be executable
	if (!pakfire_file_is_executable(file))
		return 0;

	// Try to open the ELF file
	r = pakfire_elf_open_file(&elf, ctx, file);
	if (r < 0) {
		switch (-r) {
			// This does not seem to be an ELF file
			case ENOTSUP:
				return 0;

			// Raise any other errors
			default:
				goto ERROR;
		}
	}

	// Fetch the interpreter
	const char* interpreter = pakfire_elf_interpreter(elf);
	if (interpreter) {
		r = pakfire_package_add_dep(deps->pkg, PAKFIRE_PKG_REQUIRES, "%s", interpreter);
		if (r < 0)
			goto ERROR;
	}

	// Fetch all requires
	r = pakfire_elf_requires(elf, &requires);
	if (r < 0)
		goto ERROR;

	// Add them all to the package
	if (requires) {
		for (char** s = requires; *s; s++) {
			r = pakfire_package_add_dep(deps->pkg, PAKFIRE_PKG_REQUIRES, "%s", *s);
			if (r < 0)
				goto ERROR;
		}
	}

ERROR:
	if (requires)
		pakfire_strings_free(requires);
	if (elf)
		pakfire_elf_unref(elf);

	return r;
}

static int pakfire_build_find_python_abi_requires(
		struct pakfire_ctx* ctx, struct pakfire_file* file, struct pakfire_find_deps_ctx* deps) {
	char basename[PATH_MAX];
	const char* p = NULL;
	int r;

	// Fetch the path
	const char* path = pakfire_file_get_path(file);
	if (!path)
		return -EINVAL;

	// Take the basename
	r = pakfire_path_basename(basename, path);
	if (r < 0)
		return r;

	// The basename must start with python
	if (!pakfire_string_startswith(basename, "python"))
		return -EINVAL;

	// Skip "python"
	p = basename + strlen("python");

	// Terminate if this was the entire string
	if (!*p)
		return 0;

	// Add the dependency
	return pakfire_package_add_dep(deps->pkg, PAKFIRE_PKG_REQUIRES, "python-abi = %s", p);
}

static int pakfire_build_find_symlink_destinations(
		struct pakfire_ctx* ctx, struct pakfire_file* file, struct pakfire_find_deps_ctx* deps) {
	const char* symlink = NULL;
	const char* path = NULL;
	char buffer[PATH_MAX];
	int r;

	// Fetch the path
	path = pakfire_file_get_path(file);
	if (!path)
		return -EINVAL;

	// Fetch the destination
	symlink = pakfire_file_get_symlink(file);
	if (!symlink)
		return -EINVAL;

	// Resolve the symlink
	r = pakfire_path_merge(buffer, path, symlink);
	if (r < 0)
		return r;

	// Ignore if the destination is also part of this package
	if (pakfire_filelist_contains(deps->filelist, buffer))
		return 0;

	// Otherwise add it as dependency
	return pakfire_package_add_dep(deps->pkg, PAKFIRE_PKG_REQUIRES, "%s", buffer);
}

static int pakfire_build_find_provides(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	struct pakfire_find_deps_ctx* deps = data;
	int r;

	// Skip debug files
	if (pakfire_file_matches(file, "/usr/lib/debug/**"))
		return 0;

	// Skip debug sources
	else if (pakfire_file_matches(file, "/usr/src/debug/**"))
		return 0;

	// Handle pkg-config files
	else if (pakfire_file_matches(file ,"**.pc"))
		return pakfire_build_find_pkgconfig_provides(ctx, file, deps);

	// Split certain file types differently
	switch (pakfire_file_get_type(file)) {
		// Regular files
		case S_IFREG:
			// Handle ELF files
			r = pakfire_build_find_elf_provides(ctx, file, deps);
			if (r < 0)
				return r;
			break;
	}

	return 0;
}

static int pakfire_build_find_requires(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	struct pakfire_find_deps_ctx* deps = data;
	int r;

	// Skip debug files
	if (pakfire_file_matches(file, "/usr/lib/debug/**"))
		return 0;

	// Skip debug sources
	else if (pakfire_file_matches(file, "/usr/src/debug/**"))
		return 0;

	// Handle pkg-config files
	else if (pakfire_file_matches(file, "**.pc"))
		return pakfire_build_find_pkgconfig_requires(ctx, file, deps);

	// Split certain file types differently
	switch (pakfire_file_get_type(file)) {
		// Regular files
		case S_IFREG:
			// Handle ELF files
			r = pakfire_build_find_elf_requires(ctx, file, deps);
			if (r < 0)
				return r;
			break;

		// Directories
		case S_IFDIR:
			// Handle Python ABI
			if (pakfire_file_matches(file, "/usr/lib*/python*")) {
				r = pakfire_build_find_python_abi_requires(ctx, file, deps);
				if (r < 0)
					return r;
			}
			break;

		// Symlinks
		case S_IFLNK:
			r = pakfire_build_find_symlink_destinations(ctx, file, deps);
			if (r < 0)
				return r;
			break;
	}

	return 0;
}

/*
	Perl Dependency Stuff
*/

static int pakfire_build_find_perl_files(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	struct pakfire_filelist* perlfiles = data;

	// Add all perl files
	if (pakfire_file_matches_class(file, PAKFIRE_FILE_PERL))
		return pakfire_filelist_add(perlfiles, file);

	return 0;
}

static int pakfire_build_add_perl_dep(struct pakfire_ctx* ctx, struct pakfire_package* pkg,
		const enum pakfire_package_key key, const char* line, size_t length) {
	int r;

	// Add the dependency
	r = pakfire_package_add_dep(pkg, key, "%.*s", (int)length, line);
	if (r < 0)
		return r;

	return length;
}

static int pakfire_build_add_perl_provides(struct pakfire_ctx* ctx,
		void* data, const char* line, size_t length) {
	struct pakfire_package* pkg = data;

	return pakfire_build_add_perl_dep(ctx, pkg, PAKFIRE_PKG_PROVIDES, line, length);
}

static int pakfire_build_add_perl_requires(struct pakfire_ctx* ctx,
		void* data, const char* line, size_t length) {
	struct pakfire_package* pkg = data;

	return pakfire_build_add_perl_dep(ctx, pkg, PAKFIRE_PKG_REQUIRES, line, length);
}

static int pakfire_build_find_perl_deps(struct pakfire_build* build,
		struct pakfire_package* pkg, struct pakfire_filelist* filelist) {
	struct pakfire_filelist* perlfiles = NULL;
	const char* args[] = {
		build->buildroot,
		NULL,
	};
	int r;

	// If we don't have any Perl files at all, there is nothing to do
	if (!pakfire_filelist_matches_class(filelist, PAKFIRE_FILE_PERL))
		return 0;

	// Create a new filelist
	r = pakfire_filelist_create(&perlfiles, build->pakfire);
	if (r < 0)
		goto ERROR;

	// Select all Perl files
	r = pakfire_filelist_walk(filelist, pakfire_build_find_perl_files, perlfiles, 0, NULL);
	if (r < 0)
		goto ERROR;

	struct pakfire_pty_filelist input = {
		.filelist = perlfiles,
	};

	// Find provides
	r = pakfire_build_run_script(build, "perl.prov", args,
			pakfire_pty_send_filelist, &input, pakfire_build_add_perl_provides, pkg);
	if (r < 0)
		goto ERROR;

	// Find requires
	r = pakfire_build_run_script(build, "perl.req", args,
			pakfire_pty_send_filelist, &input, pakfire_build_add_perl_requires, pkg);
	if (r < 0)
		goto ERROR;

ERROR:
	if (perlfiles)
		pakfire_filelist_unref(perlfiles);

	return r;
}

static int pakfire_build_find_dependencies(struct pakfire_build* build,
		struct pakfire_parser* makefile, const char* namespace,
		struct pakfire_package* pkg, struct pakfire_filelist* filelist) {
	char* filter_provides = NULL;
	char* filter_requires = NULL;
	int r;

	struct pakfire_find_deps_ctx deps = {
		.build    = build,
		.pkg      = pkg,
		.filelist = filelist,
	};

	// Fetch the provides filter
	filter_provides = pakfire_parser_get(makefile, namespace, "filter_provides");
	if (filter_provides) {
		r = pakfire_package_add_dep_filter(pkg, PAKFIRE_PKG_PROVIDES, filter_provides);
		if (r < 0)
			goto ERROR;
	}

	// Fetch the requires filter
	filter_requires = pakfire_parser_get(makefile, namespace, "filter_requires");
	if (filter_requires) {
		r = pakfire_package_add_dep_filter(pkg, PAKFIRE_PKG_REQUIRES, filter_requires);
		if (r < 0)
			goto ERROR;
	}

	// Find all provides
	r = pakfire_filelist_walk(filelist, pakfire_build_find_provides, &deps, 0, NULL);
	if (r < 0)
		goto ERROR;

	// Find all requires
	r = pakfire_filelist_walk(filelist, pakfire_build_find_requires, &deps, 0, NULL);
	if (r < 0)
		goto ERROR;

	// Find Perl dependencies
	r = pakfire_build_find_perl_deps(build, pkg, filelist);
	if (r < 0)
		goto ERROR;

ERROR:
	if (filter_provides) {
		pakfire_package_add_dep_filter(pkg, PAKFIRE_PKG_PROVIDES, NULL);
		free(filter_provides);
	}

	if (filter_requires) {
		pakfire_package_add_dep_filter(pkg, PAKFIRE_PKG_REQUIRES, NULL);
		free(filter_requires);
	}

	return r;
}

static int __pakfire_build_package_mark_config_files(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	char** configfiles = data;
	int r;

	// Skip anything that isn't a regular file
	if (pakfire_file_get_type(file) == S_IFREG)
		return 0;

	// Fetch path
	const char* path = pakfire_file_get_path(file);

	// Check if any configfile entry matches against this
	for (char** configfile = configfiles; *configfile; configfile++) {
		if (pakfire_string_startswith(path, *configfile)) {
			r = pakfire_file_set_flags(file, PAKFIRE_FILE_CONFIG);
			if (r)
				return r;

			// Done
			break;
		}
	}

	return 0;
}

static int pakfire_build_package_mark_config_files(struct pakfire_build* build,
		struct pakfire_filelist* filelist, struct pakfire_parser* makefile, const char* namespace) {
	char** configfiles = NULL;
	int r;

	// Fetch configfiles from makefile
	r = pakfire_parser_get_filelist(makefile, namespace, "configfiles", &configfiles, NULL);
	if (r < 0)
		goto ERROR;

	// If configfiles is not set, there is nothing to do
	if (!configfiles)
		goto ERROR;

	// Walk through the filelist
	r = pakfire_filelist_walk(filelist, __pakfire_build_package_mark_config_files, configfiles, 0, NULL);
	if (r < 0)
		goto ERROR;

ERROR:
	if (configfiles)
		pakfire_strings_free(configfiles);

	return r;
}

static int pakfire_build_package_add_files(struct pakfire_build* build,
		struct pakfire_parser* makefile, const char* buildroot, const char* namespace,
		struct pakfire_package* pkg, struct pakfire_packager* packager) {
	struct pakfire_filelist* filelist = NULL;
	char** includes = NULL;
	char** excludes = NULL;
	int r;

	// Fetch filelist from makefile
	r = pakfire_parser_get_filelist(makefile, namespace, "files", &includes, &excludes);
	if (r < 0)
		goto ERROR;

	// No files to package?
	if (!includes && !excludes)
		goto ERROR;

	// Allocate a new filelist
	r = pakfire_filelist_create(&filelist, build->pakfire);
	if (r)
		goto ERROR;

	// Scan for files
	r = pakfire_filelist_scan(filelist, build->buildroot,
		(const char**)includes, (const char**)excludes, PAKFIRE_FILELIST_EXTENDED_MATCHING);
	if (r < 0)
		goto ERROR;

	DEBUG(build->ctx, "%zu file(s) found\n", pakfire_filelist_length(filelist));

	// Nothing to do if the filelist is empty
	if (pakfire_filelist_is_empty(filelist))
		goto ERROR;

	// Dump the filelist
	pakfire_filelist_dump(filelist, PAKFIRE_FILE_DUMP_FULL);

	// Find dependencies
	r = pakfire_build_find_dependencies(build, makefile, namespace, pkg, filelist);
	if (r < 0) {
		ERROR(build->ctx, "Finding dependencies failed: %s\n", strerror(-r));
		goto ERROR;
	}

	// Mark configuration files
	r = pakfire_build_package_mark_config_files(build, filelist, makefile, namespace);
	if (r)
		goto ERROR;

	// Add all files to the package
	r = pakfire_packager_add_files(packager, filelist);
	if (r)
		goto ERROR;

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);
	if (includes)
		pakfire_strings_free(includes);
	if (excludes)
		pakfire_strings_free(excludes);

	return r;
}

static int pakfire_build_extract_command(
		char* command, size_t lcommand, const char* line, const size_t lline) {
	const char* s = NULL;
	const char* e = NULL;

	// Line must at least have a certain length
	if (lline <= strlen("executable()"))
		return -EINVAL;

	// Check if we begin with "executable("
	if (!pakfire_string_startswith(line, "executable("))
		return -EINVAL;

	// Find the closing bracket
	e = memrchr(line, ')', lline);
	if (!e)
		return -EINVAL;

	// Find the opening bracket
	s = memchr(line, '(', lline);
	if (!s)
		return -EINVAL;

	// Skip the opening bracket
	s++;

	// Check if we have seen the opening bracket first and then the closing bracket
	ssize_t l = e - s;
	if (l <= 0)
		return -EINVAL;

	// Copy the text to the buffer
	return __pakfire_string_setn(command, lcommand, s, l);
}

static int pakfire_build_process_scriptlet_dep(
		struct pakfire_ctx* ctx, void* data, const char* line, const size_t length) {
	struct pakfire_find_deps_ctx* deps = data;
	char command[PATH_MAX];
	char path[PATH_MAX];
	int r;

	// Try to extract the command
	r = pakfire_build_extract_command(command, sizeof(command), line, length);
	if (r < 0) {
		switch (-r) {
			// Ignore if the string was presumed to be invalid (some other format)
			case EINVAL:
				DEBUG(ctx, "Ignoring invalid line %.*s\n", (int)length, line);

				// Consume the entire line
				return length;

			default:
				return r;
		}
	}

	DEBUG(ctx, "Found '%s' as a dependency of the scriptlet\n", command);

	// Add absolute paths just like that
	if (pakfire_path_is_absolute(command)) {
		r = pakfire_package_add_dep(deps->pkg, PAKFIRE_PKG_PREREQUIRES, "%s", command);
		if (r < 0)
			return r;

	// Otherwise, we will have to resolve the command
	} else {
		r = pakfire_which(deps->build->pakfire, path, command);
		if (r < 0)
			return r;

		// If we could resolve the command, we add the full path
		if (*path) {
			r = pakfire_package_add_dep(deps->pkg, PAKFIRE_PKG_PREREQUIRES, "%s", path);
			if (r < 0)
				return r;
		}
	}

	// Consume the entire line
	return length;
}

static int pakfire_build_add_scriptlet_requires(struct pakfire_build* build,
		struct pakfire_package* pkg, struct pakfire_scriptlet* scriptlet) {
	struct pakfire_pty_buffer buffer = {};
	int r;

	struct pakfire_find_deps_ctx ctx = {
		.build = build,
		.pkg   = pkg,
	};

	// Fetch the scriptlet data
	buffer.data = pakfire_scriptlet_get_data(scriptlet, &buffer.length);
	if (!buffer.data)
		return -errno;

	// To execute the scriptlet we require /bin/bash
	r = pakfire_package_add_dep(pkg, PAKFIRE_PKG_PREREQUIRES, "/bin/sh");
	if (r < 0)
		return r;

	// We pass /dev/stdin as a script name to prevent bash from running in interactive mode
	const char* argv[] = {
		"bash", "--rpm-requires", "/dev/stdin", NULL,
	};

	return pakfire_jail_communicate(build->jail, argv, NULL, 0,
			pakfire_pty_send_buffer, &buffer, pakfire_build_process_scriptlet_dep, &ctx);
}

static int pakfire_build_package_add_scriptlet(struct pakfire_build* build,
		struct pakfire_package* pkg, struct pakfire_packager* packager,
		const char* type, const char* data) {
	struct pakfire_scriptlet* scriptlet = NULL;
	char* shell = NULL;
	int r;

	// Wrap scriptlet into a shell script
	r = asprintf(&shell, "#!/bin/sh\n\nset -e\n\n%s\n\nexit 0\n", data);
	if (r < 0)
		goto ERROR;

	// Create a scriptlet
	r = pakfire_scriptlet_create(&scriptlet, build->ctx, type, shell, 0);
	if (r)
		goto ERROR;

	// Add it to the package
	r = pakfire_packager_add_scriptlet(packager, scriptlet);
	if (r) {
		ERROR(build->ctx, "Could not add scriptlet %s\n", type);
		goto ERROR;
	}

	// Add scriptlet requirements
	r = pakfire_build_add_scriptlet_requires(build, pkg, scriptlet);
	if (r) {
		ERROR(build->ctx, "Could not add scriptlet requirements: %m\n");
		goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	if (scriptlet)
		pakfire_scriptlet_unref(scriptlet);
	if (shell)
		free(shell);

	return r;
}

static int pakfire_build_package_add_scriptlets(struct pakfire_build* build,
		struct pakfire_parser* makefile, const char* namespace,
		struct pakfire_package* pkg, struct pakfire_packager* packager) {
	char name[NAME_MAX];
	int r;

	for (const char** type = pakfire_scriptlet_types; *type; type++) {
		r = pakfire_string_format(name, "scriptlet:%s", *type);
		if (r)
			return r;

		// Fetch the scriptlet
		char* data = pakfire_parser_get(makefile, namespace, name);
		if (!data)
			continue;

		// Add it to the package
		r = pakfire_build_package_add_scriptlet(build, pkg, packager, *type, data);
		if (r) {
			free(data);
			return r;
		}

		free(data);
	}

	return 0;
}

static int pakfire_build_package(struct pakfire_build* build, struct pakfire_parser* makefile,
		const char* buildroot, const char* namespace) {
	struct pakfire_archive* archive = NULL;
	struct pakfire_package* pkg = NULL;
	struct pakfire_packager* packager = NULL;

	int r = 1;

	// Expand the handle into the package name
	char* name = pakfire_parser_expand(makefile, "packages", namespace);
	if (!name) {
		ERROR(build->ctx, "Could not get package name: %m\n");
		goto ERROR;
	}

	INFO(build->ctx, "Building package '%s'...\n", name);
	DEBUG(build->ctx, "  buildroot = %s\n", buildroot);

	// Fetch build architecture
	const char* arch = pakfire_get_arch(build->pakfire);
	if (!arch)
		goto ERROR;

	// Fetch package from makefile
	r = pakfire_parser_create_package(makefile, &pkg, NULL, namespace, arch);
	if (r) {
		ERROR(build->ctx, "Could not create package from makefile: %m\n");
		goto ERROR;
	}

	// Set distribution
	const char* distribution = pakfire_parser_get(makefile, NULL, "DISTRO_NAME");
	if (distribution) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_DISTRO, distribution);
		if (r)
			goto ERROR;
	}

	// Set build ID
	pakfire_package_set_uuid(pkg, PAKFIRE_PKG_BUILD_ID, build->id);

	// Set source package
	const char* source_name = pakfire_parser_get(makefile, NULL, "name");
	if (source_name) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_NAME, source_name);
		if (r)
			goto ERROR;
	}

	// Set source EVR
	const char* source_evr = pakfire_parser_get(makefile, NULL, "evr");
	if (source_evr) {
		r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_EVR, source_evr);
		if (r)
			goto ERROR;
	}

	// Set source arch
	r = pakfire_package_set_string(pkg, PAKFIRE_PKG_SOURCE_ARCH, "src");
	if (r)
		goto ERROR;

	// Create a packager
	r = pakfire_packager_create(&packager, build->pakfire, pkg);
	if (r)
		goto ERROR;

	// Add files
	r = pakfire_build_package_add_files(build, makefile, buildroot, namespace,
		pkg, packager);
	if (r)
		goto ERROR;

	// Add scriptlets
	r = pakfire_build_package_add_scriptlets(build, makefile, namespace,
		pkg, packager);
	if (r)
		goto ERROR;

	// Write the archive
	r = pakfire_packager_write_archive(packager, &archive);
	if (r < 0) {
		ERROR(build->ctx, "pakfire_packager_finish() failed: %s\n", strerror(-r));
		goto ERROR;
	}

	// Cleanup all packaged files
	r = pakfire_packager_cleanup(packager);
	if (r)
		goto ERROR;

	// Import the package into the repository
	r = pakfire_repo_import_archive(build->repo, archive, NULL);
	if (r < 0)
		goto ERROR;

ERROR:
	if (archive)
		pakfire_archive_unref(archive);
	if (packager)
		pakfire_packager_unref(packager);
	if (pkg)
		pakfire_package_unref(pkg);
	if (name)
		free(name);

	return r;
}

static int pakfire_build_package_dump(struct pakfire_ctx* ctx,
		struct pakfire_package* pkg, void* p) {
	struct pakfire_build* build = p;

	char* dump = pakfire_package_dump(pkg, PAKFIRE_PKG_DUMP_LONG);
	if (!dump)
		return 1;

	INFO(build->ctx, "%s\n", dump);
	free(dump);

	return 0;
}

static int pakfire_build_packages(struct pakfire_build* build,
		struct pakfire_parser* makefile) {
	INFO(build->ctx, "Creating packages...");
	int r = 1;

	// Fetch a list all all packages
	char** packages = pakfire_parser_list_namespaces(makefile, "packages.package:*");
	if (!packages) {
		ERROR(build->ctx, "Could not find any packages: %m\n");
		goto ERROR;
	}

	unsigned int num_packages = 0;

	// Count how many packages we have
	for (char** package = packages; *package; package++)
		num_packages++;

	DEBUG(build->ctx, "Found %u package(s)\n", num_packages);

	// Build packages in reverse order
	for (int i = num_packages - 1; i >= 0; i--) {
		r = pakfire_build_package(build, makefile, build->buildroot, packages[i]);
		if (r)
			goto ERROR;
	}

	// Dump them all
	r = pakfire_repo_walk_packages(build->repo, pakfire_build_package_dump, build, 0);
	if (r)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	if (packages)
		pakfire_strings_free(packages);

	return r;
}

static int pakfire_build_stage(struct pakfire_build* build,
		struct pakfire_parser* makefile, const char* stage) {
	struct pakfire_env* env = NULL;
	char* script = NULL;
	char template[1024];
	int r;

	// Prepare template for this stage
	r = pakfire_string_format(template, TEMPLATE, stage);
	if (r < 0)
		goto ERROR;

	// Create a new environment
	r = pakfire_env_create(&env, build->ctx);
	if (r < 0)
		goto ERROR;

	// Import the build environment
	r = pakfire_env_merge(env, build->env);
	if (r < 0)
		goto ERROR;

	// Fetch the environment
	r = pakfire_parser_set_env(makefile, env);
	if (r < 0)
		goto ERROR;

	// Create the build script
	script = pakfire_parser_expand(makefile, "build", template);
	if (!script) {
		ERROR(build->ctx, "Could not generate the build script for stage '%s': %m\n", stage);
		goto ERROR;
	}

	INFO(build->ctx, "Running build stage '%s'\n", stage);

	// Run the script
	r = pakfire_jail_exec_script(build->jail, script, strlen(script), NULL, env,
			NULL, NULL, pakfire_build_output_callback, build);
	if (r)
		ERROR(build->ctx, "Build stage '%s' failed with status %d\n", stage, r);

ERROR:
	if (env)
		pakfire_env_unref(env);
	if (script)
		free(script);

	return r;
}

enum {
	PAKFIRE_BUILD_CLEANUP_FILES         = (1 << 0),
	PAKFIRE_BUILD_ERROR_IF_NOT_EMPTY    = (1 << 1),
	PAKFIRE_BUILD_SHOW_PROGRESS         = (1 << 2),
};

struct pakfire_build_post_process_state {
	struct pakfire_build* build;

	// Filelist with files to remove
	struct pakfire_filelist* removees;
};

/*
	This helper function takes a callback which is expected to add any files
	to the given filelist which will optionally be all removed after.
*/
static int pakfire_build_post_process_files(struct pakfire_build* build,
		struct pakfire_filelist* filelist, const char* description,
		pakfire_filelist_walk_callback callback, int flags) {
	struct pakfire_filelist* removees = NULL;
	int r;

	// Create a filelist with objects that need to be removed
	r = pakfire_filelist_create(&removees, build->pakfire);
	if (r)
		goto ERROR;

	struct pakfire_build_post_process_state state = {
		.build    = build,
		.removees = removees,
	};

	// Find all files that need to be removed
	r = pakfire_filelist_walk(filelist, callback, &state,
		(flags & PAKFIRE_BUILD_SHOW_PROGRESS) ? PAKFIRE_FILELIST_SHOW_PROGRESS : 0, NULL);
	if (r)
		goto ERROR;

	if (!pakfire_filelist_is_empty(removees)) {
		if (description)
			INFO(build->ctx, "%s\n", description);

		// Show all files which will be removed
		pakfire_filelist_dump(removees, PAKFIRE_FILE_DUMP_FULL);

		// Remove all files on the removee list
		if (flags & PAKFIRE_BUILD_CLEANUP_FILES) {
			r = pakfire_filelist_cleanup(removees, PAKFIRE_FILE_CLEANUP_TIDY);
			if (r)
				goto ERROR;

			// Remove all files from the filelist
			r = pakfire_filelist_remove_all(filelist, removees);
			if (r)
				goto ERROR;
		}

		// Report an error if any files have been found
		if (flags & PAKFIRE_BUILD_ERROR_IF_NOT_EMPTY)
			r = 1;
	}

ERROR:
	if (removees)
		pakfire_filelist_unref(removees);

	return r;
}

static int __pakfire_build_remove_static_libraries(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	const struct pakfire_build_post_process_state* state = data;
	char path[PATH_MAX];
	int r;

	// Find all static libraries
	if (pakfire_file_matches(file, "**.a")) {
		// Copy the filename
		r = pakfire_string_set(path, pakfire_file_get_abspath(file));
		if (r)
			return -1;

		// Remove the extension
		r = pakfire_path_replace_extension(path, "so");
		if (r)
			return -1;

		// Only delete if there is a shared object with the same name
		if (pakfire_path_exists(path))
			return pakfire_filelist_add(state->removees, file);
	}

	return 0;
}

static int pakfire_build_post_remove_static_libraries(
		struct pakfire_build* build, struct pakfire_filelist* filelist) {
	return pakfire_build_post_process_files(build, filelist,
		"Removing static libaries...",
		__pakfire_build_remove_static_libraries,
		PAKFIRE_BUILD_CLEANUP_FILES);
}

static int __pakfire_build_remove_libtool_archives(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	const struct pakfire_build_post_process_state* state = data;

	// Find all libtool archive files
	if (pakfire_file_matches(file, "**.la"))
		return pakfire_filelist_add(state->removees, file);

	return 0;
}

static int pakfire_build_post_remove_libtool_archives(
		struct pakfire_build* build, struct pakfire_filelist* filelist) {
	return pakfire_build_post_process_files(build, filelist,
		"Removing libtool archives...",
		__pakfire_build_remove_libtool_archives,
		PAKFIRE_BUILD_CLEANUP_FILES);
}

static int __pakfire_build_check_broken_symlinks(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	const struct pakfire_build_post_process_state* state = data;
	int r;

	// Ignore anything that isn't a symlink
	switch (pakfire_file_get_type(file)) {
		case S_IFLNK:
			if (!pakfire_file_symlink_target_exists(file)) {
				r = pakfire_filelist_add(state->removees, file);
				if (r)
					return r;
			}

			break;

		// Ignore anything that isn't a symlink
		default:
			break;
	}

	return 0;
}

static int pakfire_build_post_check_broken_symlinks(
		struct pakfire_build* build, struct pakfire_filelist* filelist) {
	return pakfire_build_post_process_files(build, filelist,
		"Broken symlinks have been found:",
		__pakfire_build_check_broken_symlinks,
		PAKFIRE_BUILD_ERROR_IF_NOT_EMPTY);
}

static int pakfire_build_fix_script_interpreter(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	// Only run this for regular files
	switch (pakfire_file_get_type(file)) {
		case S_IFREG:
			break;

		// Ignore anything that isn't a symlink
		default:
			return 0;
	}

	// The file must be executable
	if (!pakfire_file_is_executable(file))
		return 0;

	// Do not run this on ELF files
	else if (pakfire_file_matches_class(file, PAKFIRE_FILE_ELF))
		return 0;

	// Fix the interpreter
	return pakfire_file_fix_interpreter(file);
}

static int pakfire_build_check_buildroot(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* data) {
	const struct pakfire_build_post_process_state* state = data;
	int r;

	// Skip Python byte-code files
	if (pakfire_file_matches(file, "**.pyc"))
		return 0;

	else if (pakfire_file_matches(file, "**.pyo"))
		return 0;

	// Skip .packlist
	else if (pakfire_file_matches(file, "**.packlist"))
		return 0;

	// Check if the file contains the pattern
	r = pakfire_file_contains(file, state->build->buildroot, -1);
	if (r <= 0)
		return r;

	// The file contains the pattern, let's list it as broken
	r = pakfire_filelist_add(state->removees, file);
	if (r < 0)
		return r;

	return 0;
}

/*
	File Issues
*/

static int pakfire_build_run_post_build_checks(struct pakfire_build* build) {
	struct pakfire_filelist* filelist = NULL;
	int r;

	// Create a filelist of all files in the build
	r = pakfire_filelist_create(&filelist, build->pakfire);
	if (r) {
		ERROR(build->ctx, "Could not create filelist: %m\n");
		goto ERROR;
	}

	// Scan for all files in BUILDROOT
	r = pakfire_filelist_scan(filelist, build->buildroot, NULL, NULL, 0);
	if (r)
		goto ERROR;

	// If the filelist is empty, we can are done
	if (pakfire_filelist_is_empty(filelist)) {
		DEBUG(build->ctx, "Empty BUILDROOT. Skipping post build checks...\n");
		r = 0;
		goto ERROR;
	}

	// Remove any static libraries
	r = pakfire_build_post_remove_static_libraries(build, filelist);
	if (r)
		goto ERROR;

	// Remove any libtool archives
	r = pakfire_build_post_remove_libtool_archives(build, filelist);
	if (r)
		goto ERROR;

	// Check for any broken symlinks
	r = pakfire_build_post_check_broken_symlinks(build, filelist);
	if (r)
		goto ERROR;

	// Fix script interpreters
	r = pakfire_filelist_walk(filelist,
			pakfire_build_fix_script_interpreter, NULL, 0, NULL);
	if (r)
		goto ERROR;

	// Check for buildroot
	r = pakfire_build_post_process_files(
			build, filelist, "Files contain the path to the build root:",
			pakfire_build_check_buildroot, PAKFIRE_BUILD_ERROR_IF_NOT_EMPTY);
	if (r < 0)
		goto ERROR;

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

static const char* post_build_scripts[] = {
	"compress-man-pages",
	NULL,
};

static int pakfire_build_run_post_build_scripts(struct pakfire_build* build) {
	// Set default arguments for build scripts
	const char* args[] = {
		build->buildroot,
		NULL,
	};

	// Run them one by one
	for (const char** script = post_build_scripts; *script; script++) {
		int r = pakfire_build_run_script(build, *script, args, NULL, NULL, NULL, NULL);
		if (r)
			return r;
	}

	return 0;
}

static int pakfire_build_strip(struct pakfire_build* build) {
	struct pakfire_stripper* stripper = NULL;
	int r;

	// Create a new stripper
	r = pakfire_stripper_create(&stripper, build->pakfire, build->jail, build->buildroot);
	if (r < 0)
		goto ERROR;

	// Strip!
	r = pakfire_stripper_run(stripper);
	if (r < 0)
		goto ERROR;

ERROR:
	if (stripper)
		pakfire_stripper_unref(stripper);

	return r;
}

static void pakfire_build_free(struct pakfire_build* build) {
	if (build->repo) {
		pakfire_repo_clean(build->repo, PAKFIRE_REPO_CLEAN_FLAGS_DESTROY);
		pakfire_repo_unref(build->repo);
	}

	if (build->jail)
		pakfire_jail_unref(build->jail);
	if (build->env)
		pakfire_env_unref(build->env);

	if (build->cgroup) {
		// Destroy the cgroup
		pakfire_cgroup_destroy(build->cgroup, PAKFIRE_CGROUP_DESTROY_RECURSIVE);

		// Free it
		pakfire_cgroup_unref(build->cgroup);
	}

	// Remove buildroot
	if (*build->buildroot)
		pakfire_rmtree(build->buildroot, 0);

	if (build->pakfire)
		pakfire_unref(build->pakfire);
	if (build->ctx)
		pakfire_ctx_unref(build->ctx);
	free(build);
}

static int pakfire_build_parse_id(struct pakfire_build* build, const char* id) {
	int r;

	// Try parsing the Build ID
	if (id) {
		r = uuid_parse(id, build->id);
		if (r) {
			ERROR(build->ctx, "Could not parse build ID '%s'\n", id);
			return -EINVAL;
		}

	// Otherwise initialize the Build ID with something random
	} else {
		uuid_generate_random(build->id);
	}

	// Store the ID as string, too
	uuid_unparse_lower(build->id, build->_id);

	return 0;
}

/*
	Sets up a new cgroup for this build
*/
static int pakfire_build_setup_cgroup(struct pakfire_build* build) {
	struct pakfire_config* config = NULL;
	char path[PATH_MAX];
	int r;

	// Compose path
	r = pakfire_string_format(path, "pakfire/build-%s", build->_id);
	if (r) {
		ERROR(build->ctx, "Could not compose path for cgroup: %m\n");
		goto ERROR;
	}

	// Create a new cgroup
	r = pakfire_cgroup_create(&build->cgroup, build->ctx, NULL, path, 0);
	if (r < 0) {
		ERROR(build->ctx, "Could not create cgroup for build %s: %s\n",
			build->_id, strerror(-r));
		goto ERROR;
	}

	// Enable accounting to collect stats
	r = pakfire_cgroup_enable_accounting(build->cgroup);
	if (r < 0) {
		switch (-r) {
			case ENOTSUP:
				DEBUG(build->ctx, "Could not enable accounting in cgroup\n");
				break;

			default:
				goto ERROR;
		}
	}

	// Fetch config
	config = pakfire_get_config(build->pakfire);
	if (!config)
		goto ERROR;

	// Guarantee some minimum memory
	size_t memory_guaranteed = pakfire_config_get_bytes(config, "build",
		"memory_guaranteed", PAKFIRE_BUILD_MEMORY_GUARANTEED);
	if (memory_guaranteed) {
		r = pakfire_cgroup_set_guaranteed_memory(build->cgroup, memory_guaranteed);
		if (r < 0) {
			switch (-r) {
				case ENOTSUP:
					ERROR(build->ctx, "Could not apply the memory guarantee. Ignoring.\n");
					break;

				default:
					goto ERROR;
			}
		}
	}

	// Limit memory
	size_t memory_limit = pakfire_config_get_bytes(config, "build", "memory_limit", 0);
	if (memory_limit) {
		r = pakfire_cgroup_set_memory_limit(build->cgroup, memory_limit);
		if (r < 0) {
			switch (-r) {
				case ENOTSUP:
					ERROR(build->ctx, "Could not apply the memory limit. Ignoring.\n");
					break;

				default:
					goto ERROR;
			}
		}
	}

	// Set PID limit
	size_t pid_limit = pakfire_config_get_int(config, "build",
		"pid_limit", PAKFIRE_BUILD_PID_LIMIT);
	if (pid_limit) {
		r = pakfire_cgroup_set_pid_limit(build->cgroup, pid_limit);
		if (r < 0) {
			switch (-r) {
				case ENOTSUP:
					ERROR(build->ctx, "Could not apply the PID limit. Ignoring.\n");
					break;

				default:
					goto ERROR;
			}
		}
	}

ERROR:
	if (config)
		pakfire_config_unref(config);

	return r;
}

/*
	Sets up a new jail for this build
*/
static int pakfire_build_setup_jail(struct pakfire_build* build) {
	int r;

	// Create a new jail
	r = pakfire_jail_create(&build->jail, build->pakfire);
	if (r) {
		ERROR(build->ctx, "Could not create jail for build %s: %m\n", build->_id);
		return r;
	}

	// Connect the jail to our cgroup
	r = pakfire_jail_set_cgroup(build->jail, build->cgroup);
	if (r < 0) {
		switch (-r) {
			case EINVAL:
				ERROR(build->ctx, "cgroup cannot be used. Ignoring.\n");
				break;

			default:
				ERROR(build->ctx, "Could not set cgroup for jail: %m\n");
				return r;
		}
	}

	// Build everything with a slightly lower priority
	r = pakfire_jail_nice(build->jail, 5);
	if (r) {
		ERROR(build->ctx, "Could not set nice level: %m\n");
		return r;
	}

	// Done
	return 0;
}

/*
	Sets up the ccache for this build
*/
static int pakfire_build_setup_ccache(struct pakfire_build* build) {
	int r;

	// Check if we want a ccache
	if (pakfire_build_has_flag(build, PAKFIRE_BUILD_DISABLE_CCACHE)) {
		DEBUG(build->ctx, "ccache usage has been disabled for this build\n");

		// Set CCACHE_DISABLE=1 so that if ccache is installed, it will disable itself
		r = pakfire_env_set(build->env, "CCACHE_DISABLE", "1");
		if (r) {
			ERROR(build->ctx, "Could not disable ccache: %m\n");
			return r;
		}

		return 0;
	}

	// Set CCACHE_DIR
	r = pakfire_env_set(build->env, "CCACHE_DIR", CCACHE_DIR);
	if (r) {
		ERROR(build->ctx, "Could not set ccache directory: %m\n");
		return r;
	}

	// Set CCACHE_TEMPDIR
	r = pakfire_env_set(build->env, "CCACHE_TEMPDIR", "/tmp");
	if (r) {
		ERROR(build->ctx, "Could not set ccache tempdir: %m\n");
		return r;
	}

	// Set a default path
	r = pakfire_cache_path(build->pakfire, build->ccache_path, "%s", "ccache");
	if (r)
		return r;

	// Ensure the directory exist
	r = pakfire_mkdir(build->ccache_path, 0755);
	if (r < 0) {
		ERROR(build->ctx, "Could not create ccache %s: %s\n",
				build->ccache_path, strerror(-r));
		return r;
	}

	// Bind-mount the directory into the jail
	r = pakfire_jail_bind(build->jail, build->ccache_path, CCACHE_DIR,
			MS_NOSUID|MS_NOEXEC|MS_NODEV);
	if (r < 0) {
		ERROR(build->ctx, "Could not mount the ccache: %s\n", strerror(-r));
		return r;
	}

	return 0;
}

static int pakfire_build_setup_repo(struct pakfire_build* build) {
	char path[PATH_MAX] = PAKFIRE_TMP_DIR "/pakfire-build-repo.XXXXXX";
	char url[PATH_MAX];
	int r;

	// Create a new repository
	r = pakfire_repo_create(&build->repo, build->pakfire, PAKFIRE_REPO_RESULT);
	if (r) {
		ERROR(build->ctx, "Could not create repository %s: %m", PAKFIRE_REPO_RESULT);
		return r;
	}

	// Set description
	pakfire_repo_set_description(build->repo, _("Build Repository"));

	// Create a temporary directory
	const char* p = pakfire_mkdtemp(path);
	if (!p) {
		ERROR(build->ctx, "Could not create a the build repository: %m\n");
		return 1;
	}

	// Format the URL
	r = pakfire_string_format(url, "file://%s", path);
	if (r)
		return r;

	// Set the URL
	pakfire_repo_set_baseurl(build->repo, url);

	return r;
}

static int pakfire_build_set_time_start(struct pakfire_build* build) {
	int r;

	// Fetch current time
	r = clock_gettime(CLOCK_MONOTONIC, &build->time_start);
	if (r < 0)
		ERROR(build->ctx, "Could not fetch start time: %m\n");

	return r;
}

static int pakfire_build_setup_pakfire(
		struct pakfire_build* build, struct pakfire_config* config, const char* arch) {
	int flags = PAKFIRE_FLAGS_BUILD;
	int r;

	// Is this a local build?
	if (build->flags & PAKFIRE_BUILD_LOCAL)
		flags |= PAKFIRE_FLAGS_BUILD_LOCAL | PAKFIRE_USE_TMPFS;

	// Enable snapshot?
	if (build->flags & PAKFIRE_BUILD_ENABLE_SNAPSHOT)
		flags |= PAKFIRE_USE_SNAPSHOT;

	// Create a new Pakfire instance
	r = pakfire_create(&build->pakfire, build->ctx, config, NULL, arch, flags);
	if (r < 0)
		return r;

	return 0;
}

/*
	This creates a new temporary directory that we will bind-mount into the
	build environment. That will save us lots of path conversions because the
	path will always be the same, both inside and outside of the jail.
*/
static int pakfire_build_setup_buildroot(struct pakfire_build* build) {
	char path[PATH_MAX] = PAKFIRE_TMP_DIR "/pakfire-buildroot.XXXXXX";
	const char* buildroot = NULL;
	int r;

	// Create BUILDROOT
	buildroot = pakfire_mkdtemp(path);
	if (!buildroot) {
		ERROR(build->ctx, "Could not create BUILDROOT: %m\n");
		return -errno;
	}

	// Store the path
	r = pakfire_string_set(build->buildroot, buildroot);
	if (r < 0)
		return r;

	// Bind-mount into the jail
	r = pakfire_jail_bind(build->jail, buildroot, buildroot, MS_NOSUID|MS_NOEXEC|MS_NODEV);
	if (r < 0) {
		ERROR(build->ctx, "Could not mount %s into the jail: %s\n", buildroot, strerror(-r));
		return r;
	}

	return 0;
}

int pakfire_build_create(struct pakfire_build** build, struct pakfire_ctx* ctx,
		struct pakfire_config* config, const char* arch, const char* id, int flags) {
	int r;

	// Allocate build object
	struct pakfire_build* b = calloc(1, sizeof(*b));
	if (!b)
		return 1;

	// Reference the context
	b->ctx = pakfire_ctx_ref(ctx);

	// Initialize reference counter
	b->nrefs = 1;

	// Copy flags
	b->flags = flags;

	// Store start time
	r = pakfire_build_set_time_start(b);
	if (r)
		goto ERROR;

	// Parse ID
	r = pakfire_build_parse_id(b, id);
	if (r)
		goto ERROR;

	// Setup Pakfire
	r = pakfire_build_setup_pakfire(b, config, arch);
	if (r < 0)
		goto ERROR;

	// Create an environment
	r = pakfire_env_create(&b->env, b->ctx);
	if (r < 0)
		goto ERROR;

	// Setup repo
	r = pakfire_build_setup_repo(b);
	if (r)
		goto ERROR;

	// Create cgroup
	r = pakfire_build_setup_cgroup(b);
	if (r)
		goto ERROR;

	// Create jail
	r = pakfire_build_setup_jail(b);
	if (r)
		goto ERROR;

	// Create the buildroot
	r = pakfire_build_setup_buildroot(b);
	if (r < 0)
		goto ERROR;

	// Setup ccache
	r = pakfire_build_setup_ccache(b);
	if (r)
		goto ERROR;

	*build = b;
	return 0;

ERROR:
	pakfire_build_free(b);
	return r;
}

struct pakfire_build* pakfire_build_ref(struct pakfire_build* build) {
	++build->nrefs;

	return build;
}

struct pakfire_build* pakfire_build_unref(struct pakfire_build* build) {
	if (--build->nrefs > 0)
		return build;

	pakfire_build_free(build);
	return NULL;
}

int pakfire_build_set_ccache_path(
		struct pakfire_build* build, const char* path) {
	// Check if this can be called
	if (pakfire_build_has_flag(build, PAKFIRE_BUILD_DISABLE_CCACHE))
		return -EPERM;

	// Check input value
	if (!path || !*path)
		return -EINVAL;

	// Store the path
	return pakfire_string_set(build->ccache_path, path);
}

static int pakfire_build_read_makefile(struct pakfire_build* build,
		struct pakfire_parser** parser, struct pakfire_package* package) {
	char path[PATH_MAX];
	int r;

	struct pakfire_parser_error* error = NULL;

	const char* nevra = pakfire_package_get_string(package, PAKFIRE_PKG_NEVRA);
	const char* name  = pakfire_package_get_string(package, PAKFIRE_PKG_NAME);

	// Compose path to makefile
	r = pakfire_path(build->pakfire, path, "/usr/src/packages/%s/%s.nm", nevra, name);
	if (r < 0)
		return 1;

	// Read makefile
	r = pakfire_read_makefile(parser, build->pakfire, path, &error);
	if (r) {
		if (error) {
			ERROR(build->ctx, "Could not parse makefile %s: %s\n", path,
				pakfire_parser_error_get_message(error));
		} else {
			ERROR(build->ctx, "Could not parse makefile %s: %m\n", path);
		}

		goto ERROR;
	}

	// Set BUILDROOT
	r = pakfire_parser_set(*parser, NULL, "BUILDROOT", build->buildroot, 0);
	if (r < 0)
		goto ERROR;

ERROR:
	if (error)
		pakfire_parser_error_unref(error);

	return r;
}

static int pakfire_build_perform(struct pakfire_build* build,
		struct pakfire_parser* makefile) {
	int r;

	// Prepare the build
	r = pakfire_build_stage(build, makefile, "prepare");
	if (r)
		goto ERROR;

	// Perform the build
	r = pakfire_build_stage(build, makefile, "build");
	if (r)
		goto ERROR;

	// Test the build
	if (!pakfire_build_has_flag(build, PAKFIRE_BUILD_DISABLE_TESTS)) {
		r = pakfire_build_stage(build, makefile, "test");
		if (r)
			goto ERROR;
	}

	// Install everything
	r = pakfire_build_stage(build, makefile, "install");
	if (r)
		goto ERROR;

	// Run post build checks
	r = pakfire_build_run_post_build_checks(build);
	if (r) {
		ERROR(build->ctx, "Post build checks failed\n");
		goto ERROR;
	}

	// Run post build scripts
	r = pakfire_build_run_post_build_scripts(build);
	if (r)
		goto ERROR;

	// Run the stripper
	r = pakfire_build_strip(build);
	if (r < 0)
		goto ERROR;

	// Done!
	return 0;

ERROR:
	// Drop to a shell for debugging
	if (pakfire_build_has_flag(build, PAKFIRE_BUILD_INTERACTIVE))
		pakfire_build_shell(build, NULL, NULL);

	return r;
}

static int __pakfire_build_unpackaged_file(
		struct pakfire_ctx* ctx, struct pakfire_file* file, void* p) {
	char* s = pakfire_file_dump(file, PAKFIRE_FILE_DUMP_FULL);
	if (s) {
		ERROR(ctx, "%s\n", s);
		free(s);
	}

	return 0;
}

static int pakfire_build_check_unpackaged_files(struct pakfire_build* build) {
	struct pakfire_filelist* filelist = NULL;
	int r;

	// Create a new filelist
	r = pakfire_filelist_create(&filelist, build->pakfire);
	if (r)
		goto ERROR;

	// Scan for all files in BUILDROOT
	r = pakfire_filelist_scan(filelist, build->buildroot, NULL, NULL, 0);
	if (r)
		goto ERROR;

	if (!pakfire_filelist_is_empty(filelist)) {
		ERROR(build->ctx, "Unpackaged files found:\n");

		r = pakfire_filelist_walk(filelist, __pakfire_build_unpackaged_file, NULL, 0, NULL);
		if (r)
			goto ERROR;

		// Report an error
		r = 1;
	}

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

static int pakfire_build_install_package(struct pakfire_ctx* ctx,
		struct pakfire_package* pkg, void* p) {
	struct pakfire_transaction* transaction = (struct pakfire_transaction*)p;

	return pakfire_transaction_request_package(transaction,
		PAKFIRE_JOB_INSTALL, pkg, PAKFIRE_JOB_ESSENTIAL);
}

static int pakfire_build_install_test(struct pakfire_build* build) {
	struct pakfire_transaction* transaction = NULL;
	char* problems = NULL;
	int r;

	// Create a new transaction
	r = pakfire_transaction_create(&transaction, build->pakfire,
			PAKFIRE_TRANSACTION_ALLOW_UNINSTALL);
	if (r < 0)
		goto ERROR;

	// Add all packages
	r = pakfire_repo_walk_packages(build->repo, pakfire_build_install_package, transaction, 0);
	if (r < 0)
		goto ERROR;

	// Solve the request
	r = pakfire_transaction_solve(transaction, PAKFIRE_SOLVE_SHOW_SOLUTIONS, &problems);
	switch (r) {
		// All okay
		case 0:
			break;

		// Dependency Error
		case 1:
			ERROR(build->ctx, "Install test failed:\n%s\n", problems);
			break;

		// Any other errors
		default:
			ERROR(build->ctx, "Install test failed: %m\n");
			goto ERROR;
	}

ERROR:
	if (transaction)
		pakfire_transaction_unref(transaction);
	if (problems)
		free(problems);

	return r;
}

static int pakfire_build_post_check(struct pakfire_build* build) {
	int r;

	// Check for unpackaged files
	r = pakfire_build_check_unpackaged_files(build);
	if (r)
		return r;

	// Perform install test
	r = pakfire_build_install_test(build);
	if (r)
		return r;

	return 0;
}

static int pakfire_build_init(struct pakfire_build* build,
		struct pakfire_package* package, const char** packages) {
	struct pakfire_transaction* transaction = NULL;
	char* problems = NULL;
	int r;

	// Create a new transaction
	r = pakfire_transaction_create(&transaction, build->pakfire,
			PAKFIRE_TRANSACTION_ALLOW_DOWNGRADE|PAKFIRE_TRANSACTION_ALLOW_UNINSTALL);
	if (r)
		goto ERROR;

	// Request to install all essential packages
	for (const char** p = PAKFIRE_BUILD_PACKAGES; *p; p++) {
		r = pakfire_transaction_request(transaction,
			PAKFIRE_JOB_INSTALL, *p, PAKFIRE_JOB_ESSENTIAL);
		if (r)
			goto ERROR;
	}

	// Add the source package (if any)
	if (package) {
		r = pakfire_transaction_request_package(transaction,
			PAKFIRE_JOB_INSTALL, package, PAKFIRE_JOB_ESSENTIAL);
		if (r)
			goto ERROR;
	}

	// Any custom packages
	if (packages) {
		for (const char** p = packages; *p; p++) {
			r = pakfire_transaction_request(transaction,
				PAKFIRE_JOB_INSTALL, *p, PAKFIRE_JOB_ESSENTIAL);
			if (r < 0)
				goto ERROR;
		}
	}

	// Also update everything that has already been installed
	r = pakfire_transaction_request(transaction, PAKFIRE_JOB_SYNC, NULL, 0);
	if (r)
		goto ERROR;

	// Solve the transaction
	r = pakfire_transaction_solve(transaction, 0, &problems);
	if (r) {
		if (problems)
			ERROR(build->ctx, "Could not install packages:\n%s\n", problems);

		goto ERROR;
	}

	// Run the transaction
	r = pakfire_transaction_run(transaction);
	if (r)
		goto ERROR;

ERROR:
	if (transaction)
		pakfire_transaction_unref(transaction);
	if (problems)
		free(problems);

	return r;
}

static int pakfire_build_lint_archive(
		struct pakfire_ctx* ctx, struct pakfire_package* pkg, struct pakfire_archive* archive, void* data) {
	return pakfire_archive_lint(archive, NULL, NULL);
}

static int pakfire_build_lint(struct pakfire_build* build) {
	int r;

	// Lint all packages
	r = pakfire_repo_walk_archives(build->repo,
			pakfire_build_lint_archive, build, PAKFIRE_PACKAGELIST_KEEPGOING);

	if (r > 0)
		ERROR(build->ctx, "Linting failed\n");

	return r;
}

struct pakfire_build_result {
	struct pakfire_build* build;

	// Callback
	pakfire_build_result_callback callback;
	void* data;
};

static int pakfire_build_result(struct pakfire_ctx* ctx, struct pakfire_package* pkg,
		struct pakfire_archive* archive, void* data) {
	const struct pakfire_build_result* result = data;
	struct pakfire_build* build = result->build;
	int r;

	// Call the callback
	r = result->callback(ctx, build->pakfire, build, archive, result->data);
	if (r) {
		ERROR(ctx, "Result callback returned %d\n", r);
		return r;
	}

	return 0;
}

int pakfire_build_exec(struct pakfire_build* build, const char* path,
		pakfire_build_result_callback result_callback, void* data) {
	struct pakfire_package* package = NULL;
	struct pakfire_parser* makefile = NULL;
	char* problems = NULL;
	char duration[TIME_STRING_MAX];
	int r;

	// Fetch architecture
	const char* arch = pakfire_get_arch(build->pakfire);

	// Open the source package
	r = pakfire_commandline_add(build->pakfire, path, &package);
	if (r)
		goto ERROR;

	const char* nevra = pakfire_package_get_string(package, PAKFIRE_PKG_NEVRA);

	INFO(build->ctx, "Building %s...\n", nevra);

	// Dump more details about the package
	char* p = pakfire_package_dump(package, PAKFIRE_PKG_DUMP_LONG);
	if (p) {
		INFO(build->ctx, "%s\n", p);
		free(p);
	}

	// Check if this package can be build in this environment
	if (!pakfire_package_supports_build_arch(package, arch)) {
		ERROR(build->ctx, "%s does not support being built on %s\n", nevra, arch);
		r = -ENOTSUP;
		goto ERROR;
	}

	// Perform an install check to see whether we can build this at all
	r = pakfire_package_installcheck(package, &problems,
			PAKFIRE_TRANSACTION_ALLOW_UNINSTALL|PAKFIRE_TRANSACTION_ALLOW_DOWNGRADE);
	if (r) {
		ERROR(build->ctx, "Cannot build %s:\n%s\n", nevra, problems);
		goto ERROR;
	}

	// Install everything we need and the source package, too
	r = pakfire_build_init(build, package, NULL);
	if (r) {
		ERROR(build->ctx, "Could not install the source package: %m\n");
		goto ERROR;
	}

	// Open the makefile
	r = pakfire_build_read_makefile(build, &makefile, package);
	if (r)
		goto ERROR;

	// Perform the actual build
	r = pakfire_build_perform(build, makefile);
	if (r)
		goto ERROR;

	// Create the packages
	r = pakfire_build_packages(build, makefile);
	if (r) {
		ERROR(build->ctx, "Could not create packages: %m\n");
		goto ERROR;
	}

	// Perform post build checks
	r = pakfire_build_post_check(build);
	if (r)
		goto ERROR;

	// Lint the packages
	r = pakfire_build_lint(build);
	if (r)
		goto ERROR;

	// Call the result callback (if any)
	if (result_callback) {
		struct pakfire_build_result result = {
			.build    = build,
			.callback = result_callback,
			.data     = data,
		};

		r = pakfire_repo_walk_archives(build->repo, pakfire_build_result, &result, 0);
		if (r < 0)
			goto ERROR;
	}

	// Log duration
	r = pakfire_format_time(duration, pakfire_build_duration(build));
	if (r)
		goto ERROR;

	INFO(build->ctx, "Build successfully completed in %s\n", duration);

ERROR:
	if (makefile)
		pakfire_parser_unref(makefile);
	if (package)
		pakfire_package_unref(package);
	if (problems)
		free(problems);

	return r;
}

static int pakfire_build_mkimage_install_deps(struct pakfire_build* build,
		const char* type) {
	struct pakfire_transaction* transaction = NULL;
	char requires[NAME_MAX];
	char* problems = NULL;
	int r;

	// Format requires
	r = pakfire_string_format(requires, "mkimage(%s)", type);
	if (r)
		goto ERROR;

	// Create a new transaction
	r = pakfire_transaction_create(&transaction, build->pakfire, 0);
	if (r) {
		ERROR(build->ctx, "Could not create transaction: %m\n");
		goto ERROR;
	}

	// Add requires to the request
	r = pakfire_transaction_request(transaction,
			PAKFIRE_JOB_INSTALL, requires, PAKFIRE_JOB_ESSENTIAL);
	if (r) {
		ERROR(build->ctx, "Could not add '%s' to the transaction: %m\n", requires);
		goto ERROR;
	}

	// Solve the request
	r = pakfire_transaction_solve(transaction, 0, &problems);
	if (r) {
		ERROR(build->ctx, "Could not solve the request:\n%s\n", problems);
		goto ERROR;
	}

	// Run the transaction
	r = pakfire_transaction_run(transaction);
	if (r)
		goto ERROR;

ERROR:
	if (transaction)
		pakfire_transaction_unref(transaction);
	if (problems)
		free(problems);

	return r;
}

static int pakfire_build_collect_packages(struct pakfire_build* build, const char* path) {
	struct pakfire_transaction* transaction = NULL;
	char* problems = NULL;
	char* p = NULL;
	int r;

	// Create a new transaction
	r = pakfire_transaction_create(&transaction, build->pakfire, 0);
	if (r) {
		ERROR(build->ctx, "Could not create transaction: %m\n");
		goto ERROR;
	}

	// XXX
	// Install the base system
	r = pakfire_transaction_request(transaction,
			PAKFIRE_JOB_INSTALL, "ipfire-release", PAKFIRE_JOB_ESSENTIAL);
	if (r) {
		ERROR(build->ctx, "Could not install 'ipfire-release': %m\n");
		goto ERROR;
	}

	// Solve the transaction
	r = pakfire_transaction_solve(transaction, 0, &problems);
	if (r) {
		ERROR(build->ctx, "Could not solve request:\n%s\n", problems);
		goto ERROR;
	}

	// Empty transaction?
	if (!pakfire_transaction_count(transaction)) {
		ERROR(build->ctx, "The transaction is unexpectedly empty\n");
		r = 1;
		goto ERROR;
	}

	// Dump the transaction
	p = pakfire_transaction_dump(transaction, 80);
	if (!p) {
		ERROR(build->ctx, "Could not dump the transaction: %m\n");
		r = 1;
		goto ERROR;
	}

	// Log the dump
	INFO(build->ctx, "%s\n", p);

	// Download all packages
	r = pakfire_transaction_download(transaction);
	if (r) {
		ERROR(build->ctx, "Could not download the transaction: %m\n");
		goto ERROR;
	}

	// Create a repository with all packages in this transaction
	r = pakfire_transaction_compose_repo(transaction, NULL, path);
	if (r) {
		ERROR(build->ctx, "Could not create repository: %m\n");
		goto ERROR;
	}

	// XXX Should we perform installcheck here?

ERROR:
	if (transaction)
		pakfire_transaction_unref(transaction);
	if (problems)
		free(problems);
	if (p)
		free(p);

	return r;
}

int pakfire_build_mkimage(struct pakfire_build* build,
		const char* type, FILE* f) {
	FILE* t = NULL;
	char packagesdir[PATH_MAX];
	char path[PATH_MAX];
	char* p = NULL;
	int r;

	// Check inputs
	if (!type || !f)
		return -EINVAL;

	// Create a path inside the build environment
	r = pakfire_path(build->pakfire, path, "%s",
		PAKFIRE_TMP_DIR "/pakfire-image.XXXXXX");
	if (r)
		goto ERROR;

	// Allocate a temporary file
	t = pakfire_mktemp(path, 0600);
	if (!t) {
		ERROR(build->ctx, "Could not allocate a temporary file: %m\n");
		r = 1;
		goto ERROR;
	}

	// Create a path for all packages
	r = pakfire_path(build->pakfire, packagesdir, "%s",
		PAKFIRE_TMP_DIR "/pakfire-packages.XXXXXX");
	if (r)
		goto ERROR;

	p = pakfire_mkdtemp(packagesdir);
	if (!p)
		goto ERROR;

	// Collect all packages
	r = pakfire_build_collect_packages(build, packagesdir);
	if (r)
		goto ERROR;

	// Install all packages
	r = pakfire_build_init(build, NULL, NULL);
	if (r)
		goto ERROR;

	// Install all dependencies
	r = pakfire_build_mkimage_install_deps(build, type);
	if (r)
		goto ERROR;

	const char* args[] = {
		type,
		pakfire_relpath(build->pakfire, path),
		pakfire_relpath(build->pakfire, packagesdir),
		NULL,
	};

	// Run the mkimage script
	r = pakfire_build_run_script(build, "mkimage", args, NULL, NULL, NULL, NULL);
	if (r)
		goto ERROR;

	// Copy data to its destination
	r = pakfire_copy(build->ctx, t, f);
	if (r)
		goto ERROR;

ERROR:
	if (t)
		fclose(t);

	// Unlink the temporary file
	if (*path)
		unlink(path);

	// Remove all packages
	pakfire_rmtree(packagesdir, 0);

	return r;
}

int pakfire_build_clean(struct pakfire* pakfire, int flags) {
	struct pakfire_repo* local = NULL;
	int r = 0;

	struct pakfire_ctx* ctx = pakfire_ctx(pakfire);

	// Fetch local repository
	local = pakfire_get_repo(pakfire, PAKFIRE_REPO_LOCAL);
	if (!local) {
		ERROR(ctx, "Could not find repository %s: %m\n", PAKFIRE_REPO_LOCAL);
		goto ERROR;
	}

	// Destroy everything in it
	r = pakfire_repo_clean(local, PAKFIRE_REPO_CLEAN_FLAGS_DESTROY);
	if (r)
		goto ERROR;

ERROR:
	if (local)
		pakfire_repo_unref(local);
	if (ctx)
		pakfire_ctx_unref(ctx);

	return r;
}

/*
	Drops the user into a shell
*/
int pakfire_build_shell(struct pakfire_build* build, const char* argv[], const char** packages) {
	int r;

	// Initialize the build environment
	r = pakfire_build_init(build, NULL, packages);
	if (r)
		return r;

	// Export local repository if running in interactive mode
	r = pakfire_build_enable_repos(build);
	if (r)
		return r;

	// Run the command (if given)
	if (argv)
		return pakfire_jail_exec_command(build->jail, argv, build->env, 0);

	// Otherwise run the shell
	return pakfire_jail_shell(build->jail, build->env);
}
