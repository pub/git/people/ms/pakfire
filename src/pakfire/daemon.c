/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>

#include <json.h>

#include <krb5/krb5.h>

#include <systemd/sd-bus.h>
#include <systemd/sd-daemon.h>
#include <systemd/sd-event.h>

#include <pakfire/arch.h>
#include <pakfire/cgroup.h>
#include <pakfire/ctx.h>
#include <pakfire/daemon.h>
#include <pakfire/httpclient.h>
#include <pakfire/job.h>
#include <pakfire/json.h>
#include <pakfire/proctitle.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

// How often to submit stats?
#define PAKFIRE_STATS_WHEN_BUSY S_TO_US(15)
#define PAKFIRE_STATS_WHEN_IDLE S_TO_US(60)

// It would be nice store the credentials cache in memory, but on Debian,
// cURL is compiled with Heimdal which is incompatible with the KRB5 in-memory cache.
// The on-disk format is however is compatible between two implementations.
#define KRB5_CREDENTIALS_CACHE "FILE:/tmp/krb5cc_pakfire-daemon"

// The default keytab
#define KRB5_DEFAULT_KEYTAB "/etc/krb5.keytab"

#define MAX_JOBS 64

struct pakfire_daemon {
	struct pakfire_ctx* ctx;
	int nrefs;

	// HTTP Client
	struct pakfire_httpclient* client;

	// Build Service
	struct pakfire_buildservice* service;

	// URL
	char url[PATH_MAX];

	// Kerberos Keytab
	char keytab[PATH_MAX];

	// Event Loop
	sd_event* loop;

	// dbus
	sd_bus* bus;
	int inhibitfd;

	// Kerberos Authentication
	struct {
		// Context
		krb5_context ctx;

		// Credentials Cache
		krb5_ccache ccache;

		// Host Princial
		krb5_principal principal;
	} krb5;

	// Authentication Timer
	sd_event_source* auth_timer;

	// The control connection
	struct pakfire_xfer* control;

	// Connection Timer and Holdoff Time
	sd_event_source* connect_timer;
	unsigned int reconnect_holdoff;

	// Timer for submitting stats
	sd_event_source* stats_timer;

	// cgroup
	struct pakfire_cgroup* cgroup;

	// Jobs
	struct pakfire_job* jobs[MAX_JOBS];
	unsigned int running_jobs;
};

static struct pakfire_job* pakfire_daemon_find_job(
		struct pakfire_daemon* self, const char* job_id) {
	// Walk through all jobs
	for (unsigned int i = 0; i < MAX_JOBS; i++) {
		if (!self->jobs[i])
			continue;

		// Return the matching job
		if (pakfire_job_has_id(self->jobs[i], job_id))
			return pakfire_job_ref(self->jobs[i]);
	}

	return NULL;
}

/*
	Terminates all running jobs
*/
static int pakfire_daemon_terminate_jobs(struct pakfire_daemon* daemon) {
	int r;

	for (unsigned int i = 0; i < MAX_JOBS; i++) {
		// Skip any empty slots
		if (!daemon->jobs[i])
			continue;

		// Terminate the job
		r = pakfire_job_terminate(daemon->jobs[i], SIGTERM);
		if (r)
			return r;
	}

	return 0;
}

static int pakfire_daemon_terminate(sd_event_source* source,
		const struct signalfd_siginfo* si, void* data) {
	struct pakfire_daemon* daemon = data;

	DEBUG(daemon->ctx, "Received signal to terminate...\n");

	// Terminate all jobs
	pakfire_daemon_terminate_jobs(daemon);

	return sd_event_exit(sd_event_source_get_event(source), 0);
}

static int pakfire_daemon_submit_stats(sd_event_source* s, uint64_t usec, void* p) {
	struct pakfire_daemon* daemon = p;
	struct pakfire_cpustat cpustat = {};
	struct pakfire_loadavg loadavg = {};
	struct pakfire_meminfo meminfo = {};
	struct json_object* stats = NULL;
	struct json_object* data = NULL;
	int r;

	// If we have any jobs running, we will submit our stats
	// every five seconds, otherwise 30 seconds is enough.
	uint64_t next = (daemon->running_jobs > 0) ? PAKFIRE_STATS_WHEN_BUSY : PAKFIRE_STATS_WHEN_IDLE;

	// Reset the timer
	r = sd_event_source_set_time_relative(daemon->stats_timer, next);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not set the stat timer: %s\n", strerror(-r));
		return r;
	}

	// Don't do anything if we don't have a control connection
	if (!daemon->control) {
		DEBUG(daemon->ctx, "Won't send stats without a control connection\n");
		return 0;
	}

	DEBUG(daemon->ctx, "Submitting stats...\n");

	// Fetch CPU stats
	r = pakfire_cpustat(&cpustat);
	if (r) {
		ERROR(daemon->ctx, "Failed to fetch CPU stats: %s\n", strerror(-r));
		goto ERROR;
	}

	// Fetch load average
	r = pakfire_loadavg(&loadavg);
	if (r) {
		ERROR(daemon->ctx, "Failed to fetch load average: %s\n", strerror(-r));
		goto ERROR;
	}

	// Fetch meminfo
	r = pakfire_meminfo(&meminfo);
	if (r) {
		ERROR(daemon->ctx, "Failed to fetch meminfo: %s\n", strerror(-r));
		goto ERROR;
	}

	// Create new data object
	data = json_object_new_object();
	if (!data)
		goto ERROR;

	// CPU Stats: User
	r = pakfire_json_add_double(data, "cpu_user", cpustat.user);
	if (r)
		goto ERROR;

	// CPU Stats: Nice
	r = pakfire_json_add_double(data, "cpu_nice", cpustat.nice);
	if (r)
		goto ERROR;

	// CPU Stats: System
	r = pakfire_json_add_double(data, "cpu_system", cpustat.system);
	if (r)
		goto ERROR;

	// CPU Stats: Idle
	r = pakfire_json_add_double(data, "cpu_idle", cpustat.idle);
	if (r)
		goto ERROR;

	// CPU Stats: IO Wait
	r = pakfire_json_add_double(data, "cpu_iowait", cpustat.iowait);
	if (r)
		goto ERROR;

	// CPU Stats: IRQ
	r = pakfire_json_add_double(data, "cpu_irq", cpustat.irq);
	if (r)
		goto ERROR;

	// CPU Stats: Soft IRQ
	r = pakfire_json_add_double(data, "cpu_softirq", cpustat.softirq);
	if (r)
		goto ERROR;

	// CPU Stats: Steal
	r = pakfire_json_add_double(data, "cpu_steal", cpustat.steal);
	if (r)
		goto ERROR;

	// CPU Stats: Guest
	r = pakfire_json_add_double(data, "cpu_guest", cpustat.guest);
	if (r)
		goto ERROR;

	// CPU Stats: Guest Nice
	r = pakfire_json_add_double(data, "cpu_guest_nice", cpustat.guest_nice);
	if (r)
		goto ERROR;

	// Load Average: 1
	r = pakfire_json_add_double(data, "loadavg1", loadavg.load1);
	if (r)
		goto ERROR;

	// Load Average: 5
	r = pakfire_json_add_double(data, "loadavg5", loadavg.load5);
	if (r)
		goto ERROR;

	// Load Average: 15
	r = pakfire_json_add_double(data, "loadavg15", loadavg.load15);
	if (r)
		goto ERROR;

	// Memory: Total
	r = pakfire_json_add_uint64(data, "mem_total", meminfo.total);
	if (r)
		goto ERROR;

	// Memory: Available
	r = pakfire_json_add_uint64(data, "mem_available", meminfo.available);
	if (r)
		goto ERROR;

	// Memory: Used
	r = pakfire_json_add_int64(data, "mem_used", meminfo.used);
	if (r)
		goto ERROR;

	// Memory: Free
	r = pakfire_json_add_uint64(data, "mem_free", meminfo.free);
	if (r)
		goto ERROR;

	// Memory: Active
	r = pakfire_json_add_uint64(data, "mem_active", meminfo.active);
	if (r)
		goto ERROR;

	// Memory: Inactive
	r = pakfire_json_add_uint64(data, "mem_inactive", meminfo.inactive);
	if (r)
		goto ERROR;

	// Memory: Buffers
	r = pakfire_json_add_uint64(data, "mem_buffers", meminfo.buffers);
	if (r)
		goto ERROR;

	// Memory: Cached
	r = pakfire_json_add_uint64(data, "mem_cached", meminfo.cached);
	if (r)
		goto ERROR;

	// Memory: Shared
	r = pakfire_json_add_uint64(data, "mem_shared", meminfo.shared);
	if (r)
		goto ERROR;

	// Swap: Total
	r = pakfire_json_add_uint64(data, "swap_total", meminfo.swap_total);
	if (r)
		goto ERROR;

	// Swap: Used
	r = pakfire_json_add_uint64(data, "swap_used", meminfo.swap_used);
	if (r)
		goto ERROR;

	// Swap: Free
	r = pakfire_json_add_uint64(data, "swap_free", meminfo.swap_free);
	if (r)
		goto ERROR;

	// Create a new stats object
	stats = json_object_new_object();
	if (!stats) {
		r = -errno;
		goto ERROR;
	}

	// Set type
	r = pakfire_json_add_string(stats, "type", "stats");
	if (r)
		goto ERROR;

	// Set data
	r = json_object_object_add(stats, "data", data);
	if (r)
		goto ERROR;

	// Dereference data so it won't be double-freed later
	data = NULL;

	// Send the message
	r = pakfire_daemon_send_message(daemon, stats);
	if (r < 0) {
		switch (-r) {
			// Ignore if we are not connected
			case ENOTCONN:
				r = 0;
				break;

			// Raise anything else
			default:
				goto ERROR;
		}
	}

ERROR:
	if (stats)
		json_object_put(stats);
	if (data)
		json_object_put(data);

	return r;
}

/*
 * Prevents that the system can be shut down when there are jobs running...
 */
static int pakfire_daemon_inhibit_shutdown(struct pakfire_daemon* daemon) {
	sd_bus_error error = SD_BUS_ERROR_NULL;
	sd_bus_message* reply = NULL;
	int r;

	// Don't do this again if we are already holding the lock
	if (daemon->inhibitfd >= 0)
		return 0;

	// Call the Inhibit method
	r = sd_bus_call_method(
		daemon->bus,

		// Destination, Path & Interface
		"org.freedesktop.login1",
		"/org/freedesktop/login1",
		"org.freedesktop.login1.Manager",

		// Method
		"Inhibit",

		// Error
		&error,

		// Reply
		&reply,

		// Arguments
		"ssss",
		"shutdown:sleep:idle",
		"pakfire-daemon",
		"Prevent shutdown while build jobs are running",
		"block"
	);
	if (r < 0) {
		ERROR(daemon->ctx, "Failed to inhibit shutdown: %s\n", error.message);
		goto ERROR;
	}

    // Read the file descriptor from the reply
	r = sd_bus_message_read(reply, "h", &daemon->inhibitfd);
	if (r < 0) {
		ERROR(daemon->ctx, "Failed to parse response message: %s\n", strerror(-r));
		goto ERROR;
    }

	DEBUG(daemon->ctx, "Successfully inhibited shutdown\n");

ERROR:
    sd_bus_error_free(&error);
    if (reply)
	    sd_bus_message_unref(reply);

	return r;
}

static int pakfire_daemon_release_inhibit_shutdown(struct pakfire_daemon* daemon) {
	// If we don't hold the lock, we cannot release it
	if (daemon->inhibitfd >= 0) {
		close(daemon->inhibitfd);
		daemon->inhibitfd = -EBADF;

		DEBUG(daemon->ctx, "Released shutdown inhibition\n");
	}

	return 0;
}

/*
	Called when we have received a message for a specific job
*/
static int pakfire_daemon_handle_job_message(
		struct pakfire_daemon* self, const char* job_id, struct json_object* message) {
	struct pakfire_job* job = NULL;
	int r;

	// Find the job
	job = pakfire_daemon_find_job(self, job_id);
	if (!job) {
		WARN(self->ctx, "Received message for job %s which does not exist. Ignoring.\n", job_id);
		return 0;
	}

	// Dispatch the message to the job
	r = pakfire_job_handle_message(job, message);
	if (r < 0)
		goto ERROR;

ERROR:
	if (job)
		pakfire_job_unref(job);

	return r;
}

/*
	Called when a new job has been received
*/
static int pakfire_daemon_job(struct pakfire_daemon* daemon, json_object* m) {
	struct pakfire_job* job = NULL;
	struct json_object* data = NULL;
	int r;

	// Inhibit shutdown
	r = pakfire_daemon_inhibit_shutdown(daemon);
	if (r < 0)
		goto ERROR;

	// Fetch the data from the message
	if (!json_object_object_get_ex(m, "data", &data)) {
		ERROR(daemon->ctx, "Job does not have any data\n");
		r = -EINVAL;
		goto ERROR;
	}

	// Create a new job
	r = pakfire_job_create(&job, daemon->ctx, daemon, data);
	if (r) {
		ERROR(daemon->ctx, "Could not create a new job: %s\n", strerror(-r));
		goto ERROR;
	}

	// Launch the job
	r = pakfire_job_launch(job);
	if (r < 0) {
		ERROR(daemon->ctx, "Failed to launch the job: %s\n", strerror(-r));
		goto ERROR;
	}

	// Store the job
	for (unsigned int i = 0; i < MAX_JOBS; i++) {
		if (!daemon->jobs[i]) {
			daemon->jobs[i] = pakfire_job_ref(job);
			break;
		}
	}

	// Increment the number of running jobs
	daemon->running_jobs++;

ERROR:
	if (job)
		pakfire_job_unref(job);

	return r;
}

static int pakfire_daemon_recv(struct pakfire_xfer* xfer,
		const char* message, const size_t size, void* data) {
	struct pakfire_daemon* daemon = data;
	struct json_object* type = NULL;
	struct json_object* m = NULL;
	int r;

	const char* job_id = NULL;

	// Parse the JSON message
	m = pakfire_json_parse(daemon->ctx, message, size);
	if (!m) {
		r = -errno;
		goto ERROR;
	}

	// Does this message have a build job ID?
	r = pakfire_json_get_string(m, "job_id", &job_id);
	if (r < 0) {
		switch (-r) {
			// Fall through if the key did not exist
			case ENOMSG:
				break;

			// Otherwise raise the error
			default:
				goto ERROR;
		}
	}

	// If we have a job ID, this message is for a specific job
	if (job_id) {
		r = pakfire_daemon_handle_job_message(daemon, job_id, m);
		if (r < 0)
			goto ERROR;

		goto ERROR;
	}

	// Fetch the message type
	if (!json_object_object_get_ex(m, "type", &type)) {
		ERROR(daemon->ctx, "Invalid message with missing type\n");

		return -EINVAL;
	}

	// Fetch the type
	const char* t = json_object_get_string(type);

	// Handle new jobs
	if (strcmp(t, "job") == 0) {
		r = pakfire_daemon_job(daemon, m);

	} else {
		ERROR(daemon->ctx, "Unknown message. Ignoring:\n%s\n",
			json_object_to_json_string_ext(m, JSON_C_TO_STRING_SPACED | JSON_C_TO_STRING_PRETTY));

		r = 0;
	}

ERROR:
	if (m)
		json_object_put(m);

	return r;
}

/*
	Called when we are ready to stream the log.

	This function calls each running job which will only send one line
	at a time to ensure that one job will not take the entire bandwidth.
*/
int pakfire_daemon_stream_logs(struct pakfire_daemon* self) {
	unsigned int lines;
	int r;

	// Bail if we don't have a control connection
	if (!self->control)
		return 0;

	// Bail if the connection isn't ready to send
	else if (!pakfire_xfer_is_ready_to_send(self->control))
		return 0;

	do {
		// Reset lines
		lines = 0;

		// Have every job send one line
		for (unsigned int i = 0; i < MAX_JOBS; i++) {
			// Skip any empty slots
			if (!self->jobs[i])
				continue;

			// Stream logs
			r = pakfire_job_stream_logs(self->jobs[i]);
			if (r < 0) {
				switch (-r) {
					case EAGAIN:
						return 0;

					default:
						return r;
				}
			}

			// Add up lines
			lines += r;
		}
	} while (lines);

	return 0;
}

static int pakfire_daemon_send(struct pakfire_xfer* xfer, void* data) {
	struct pakfire_daemon* self = data;
	int r;

	// Stream logs
	r = pakfire_daemon_stream_logs(self);
	if (r < 0)
		return r;

	return 0;
}

/*
	This function is called whenever the connection to the build service could not
	be established or was interrupted. It will try to reconnect.
*/
static int pakfire_daemon_close(struct pakfire_xfer* xfer, int code, void* data) {
	struct pakfire_daemon* daemon = data;
	int r;

	// Remove the connection from the client
	r = pakfire_httpclient_dequeue(daemon->client, xfer);
	if (r < 0) {
		ERROR(daemon->ctx, "Failed to remove the control connection: %s\n", strerror(-r));
		return r;
	}

	// Drop the control connection
	if (daemon->control) {
		pakfire_xfer_unref(daemon->control);
		daemon->control = NULL;
	}

	INFO(daemon->ctx, "Will attempt to reconnect in %u second(s)\n",
		daemon->reconnect_holdoff / S_TO_US(1));

	// Set the reconnection timer
	r = sd_event_source_set_time_relative(daemon->connect_timer, daemon->reconnect_holdoff);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not set the reconnection timer: %s\n", strerror(-r));
		return r;
	}

	// Activate the timer
	r = sd_event_source_set_enabled(daemon->connect_timer, SD_EVENT_ONESHOT);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not activate the connect timer: %s\n", strerror(-r));
		return r;
	}

	// Double the holdoff time
	daemon->reconnect_holdoff *= 2;

	// Cap reconnection attempts to every minute
	if (daemon->reconnect_holdoff > S_TO_US(60))
		daemon->reconnect_holdoff = S_TO_US(60);

	// Disable sending stats until we are connected
	r = sd_event_source_set_enabled(daemon->stats_timer, SD_EVENT_OFF);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not disable the stats timer: %s\n", strerror(-r));
		return r;
	}

	return 0;
}

static int pakfire_daemon_xfer_create(struct pakfire_xfer** xfer,
	struct pakfire_daemon* daemon, const char* url, ...) __attribute__((format(printf, 3, 4)));

static int pakfire_daemon_xfer_create(struct pakfire_xfer** xfer,
		struct pakfire_daemon* daemon, const char* url, ...) {
	struct pakfire_xfer* x = NULL;
	va_list args;
	int r;

	va_start(args, url);

	// Create a new xfer
	r = pakfire_xfer_create(&x, daemon->ctx, url, args);
	if (r < 0)
		goto ERROR;

	// Set the base URL
	r = pakfire_xfer_set_baseurl(x, daemon->url);
	if (r < 0)
		goto ERROR;

	// Success
	*xfer = pakfire_xfer_ref(x);

ERROR:
	if (x)
		pakfire_xfer_unref(x);
	va_end(args);

	return r;
}

static int pakfire_daemon_connected(struct pakfire_xfer* xfer, void* data) {
	struct pakfire_daemon* daemon = data;
	int r;

	DEBUG(daemon->ctx, "Connected!\n");

	// Update the process title
	r = pakfire_proctitle_set("pakfire-daemon: Connected\n");
	if (r < 0)
		return r;

	// Store a reference to the control connection
	daemon->control = pakfire_xfer_ref(xfer);

	// Reset the holdoff timer
	daemon->reconnect_holdoff = S_TO_US(1);

	// Submit stats continuously
	r = sd_event_source_set_enabled(daemon->stats_timer, SD_EVENT_ON);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not activate the stat timer: %s\n", strerror(-r));
		return r;
	}

	return 0;
}

static int pakfire_daemon_connect(sd_event_source* s, uint64_t usec, void* data) {
	struct pakfire_daemon* daemon = data;
	struct pakfire_sysinfo sysinfo = {};
	struct pakfire_cpuinfo cpuinfo = {};
	struct pakfire_xfer* xfer = NULL;
	const char* arch = NULL;
	int r;

	DEBUG(daemon->ctx, "Connecting...\n");

	// Update the process title
	r = pakfire_proctitle_set("pakfire-daemon: Connecting...\n");
	if (r < 0)
		return r;

	// Fetch the distro
	const struct pakfire_distro* distro = pakfire_ctx_get_distro(daemon->ctx);

	// Fetch system information
	r = pakfire_sysinfo(&sysinfo);
	if (r < 0) {
		ERROR(daemon->ctx, "Failed to fetch system info: %s\n", strerror(-r));
		goto ERROR;
	}

	// Fetch native arch
	arch = pakfire_arch_native();
	if (!arch) {
		ERROR(daemon->ctx, "Failed to fetch native arch: %s\n", strerror(errno));
		r = -errno;
		goto ERROR;
	}

	// Fetch CPU info
	r = pakfire_cpuinfo(&cpuinfo);
	if (r) {
		ERROR(daemon->ctx, "Failed to fetch CPU info: %s\n", strerror(-r));
		goto ERROR;
	}

	// Create a new xfer
	r = pakfire_daemon_xfer_create(&xfer, daemon, "/api/v1/builders/control");
	if (r)
		goto ERROR;

	// Enable authentication
	r = pakfire_xfer_auth(xfer);
	if (r)
		goto ERROR;

	// Send our version
	r = pakfire_xfer_add_query(xfer, "version", "%s", PACKAGE_FULLVERSION);
	if (r < 0)
		goto ERROR;

	// System Vendor
	if (*sysinfo.vendor) {
		r = pakfire_xfer_add_query(xfer, "sys_vendor", "%s", sysinfo.vendor);
		if (r < 0)
			goto ERROR;
	}

	// System Model
	if (*sysinfo.name) {
		r = pakfire_xfer_add_query(xfer, "sys_name", "%s", sysinfo.name);
		if (r < 0)
			goto ERROR;
	}

	// CPU Model
	if (*cpuinfo.model) {
		r = pakfire_xfer_add_query(xfer, "cpu_model", "%s", cpuinfo.model);
		if (r < 0)
			goto ERROR;
	}

	// CPU Count
	r = pakfire_xfer_add_query(xfer, "cpu_count", "%u", cpuinfo.count);
	if (r < 0)
		goto ERROR;

	// Arch
	r = pakfire_xfer_add_query(xfer, "arch", "%s", arch);
	if (r < 0)
		goto ERROR;

	// OS
	r = pakfire_xfer_add_query(xfer, "os_name", "%s", distro->pretty_name);
	if (r < 0)
		goto ERROR;

	// Make this a WebSocket connection
	r = pakfire_xfer_socket(xfer, pakfire_daemon_connected,
		pakfire_daemon_recv, pakfire_daemon_send, pakfire_daemon_close, daemon);
	if (r)
		goto ERROR;

	// Enqueue the transfer
	r = pakfire_httpclient_enqueue(daemon->client, xfer);
	if (r)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);

	return r;
}

// Currently we are not doing anything here. We just need to block SIGCHLD.
static int pakfire_daemon_SIGCHLD(sd_event_source* s, const struct signalfd_siginfo* si, void* data) {
	return 0;
}

static int pakfire_daemon_setup_krb5(struct pakfire_daemon* daemon) {
	char hostname[HOST_NAME_MAX];
	const char* error = NULL;
	char* name = NULL;
	int r;

	// Fetch our own hostname
	r = gethostname(hostname, sizeof(hostname));
	if (r < 0) {
		ERROR(daemon->ctx, "Could not fetch hostname: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Setup a Kerberos context
	r = krb5_init_context(&daemon->krb5.ctx);
	if (r) {
		error = krb5_get_error_message(daemon->krb5.ctx, r);

		ERROR(daemon->ctx, "Could not initialize Kerberos: %s\n", error);
		goto ERROR;
	}

	// Make the principal
	r = krb5_sname_to_principal(daemon->krb5.ctx,
			hostname, "host", KRB5_NT_SRV_HST, &daemon->krb5.principal);
	if (r) {
		error = krb5_get_error_message(daemon->krb5.ctx, r);

		ERROR(daemon->ctx, "Could not generate the host principal: %s\n", error);
		goto ERROR;
	}

	// Print the principal name
	r = krb5_unparse_name(daemon->krb5.ctx, daemon->krb5.principal, &name);
	if (r) {
		error = krb5_get_error_message(daemon->krb5.ctx, r);

		ERROR(daemon->ctx, "Could not print the host principal: %s\n", error);
		goto ERROR;
	}

	DEBUG(daemon->ctx, "Authenticating as: %s\n", name);

	// Create a new credentials cache
	r = krb5_cc_resolve(daemon->krb5.ctx, KRB5_CREDENTIALS_CACHE, &daemon->krb5.ccache);
	if (r) {
		error = krb5_get_error_message(daemon->krb5.ctx, r);

		ERROR(daemon->ctx, "Could not resolve the credentials cache: %s\n", error);
		goto ERROR;
	}

	// Initialize the cache for the principal
	r = krb5_cc_initialize(daemon->krb5.ctx, daemon->krb5.ccache, daemon->krb5.principal);
	if (r) {
		error = krb5_get_error_message(daemon->krb5.ctx, r);

		ERROR(daemon->ctx, "Could not initialize the credentials cache: %s\n", error);
		goto ERROR;
	}

	// Set the credentials cache environment variable
	r = setenv("KRB5CCNAME", KRB5_CREDENTIALS_CACHE, 1);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not set KRB5CCNAME: %m\n");
		r = -errno;
		goto ERROR;
	}

ERROR:
	if (error)
		krb5_free_error_message(daemon->krb5.ctx, error);
	if (name)
		krb5_free_unparsed_name(daemon->krb5.ctx, name);

	return r;
}

static int pakfire_daemon_auth(sd_event_source* s, uint64_t usec, void* data) {
	struct pakfire_daemon* daemon = data;
	krb5_get_init_creds_opt* options = NULL;
	krb5_keytab keytab = NULL;
	krb5_creds creds = {};
	const char* error = NULL;
	char time[128];
	char* p = NULL;
	int r;

	DEBUG(daemon->ctx, "Authenticating...\n");

	// Resolve the keytab
	r = krb5_kt_resolve(daemon->krb5.ctx, daemon->keytab, &keytab);
	if (r) {
		error = krb5_get_error_message(daemon->krb5.ctx, r);

		ERROR(daemon->ctx, "Could not resolve the keytab: %s\n", error);
		goto ERROR;
	}

	// Fetch the credentials using the keytab
	r = krb5_get_init_creds_keytab(daemon->krb5.ctx, &creds, daemon->krb5.principal,
			keytab, 0, NULL, options);
	if (r) {
		error = krb5_get_error_message(daemon->krb5.ctx, r);

		ERROR(daemon->ctx, "Could not fetch credentials: %s\n", error);
		goto ERROR;
	}

	// Determine the end time
	time_t t_end = creds.times.endtime;

	// Format the expiry time
	p = ctime_r(&t_end, time);
	if (!p) {
		ERROR(daemon->ctx, "Could not format the expiry time: %m\n");
		r = -errno;
		goto ERROR;
	}

	DEBUG(daemon->ctx, "Successfully fetched credentials\n");
	DEBUG(daemon->ctx, "  Expires: %s\n", time);

	// Store the credentials in the cache
	r = krb5_cc_store_cred(daemon->krb5.ctx, daemon->krb5.ccache, &creds);
	if (r) {
		error = krb5_get_error_message(daemon->krb5.ctx, r);

		ERROR(daemon->ctx, "Could not store credentials: %s\n", error);
		goto ERROR;
	}

	// Refresh one hour before the expiry time
	uint64_t t_refresh = (t_end - 3600) * S_TO_US(1);

	// Authenticate again just before the credentials expire
	r = sd_event_source_set_time(daemon->auth_timer, t_refresh);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not reset the auth timer: %s\n", strerror(-r));
		goto ERROR;
	}

	// Turn on the connection timer
	r = sd_event_source_set_enabled(daemon->connect_timer, SD_EVENT_ONESHOT);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not enable the connection timer: %s\n", strerror(-r));
		goto ERROR;
	}

	/*
		XXX This function needs some better error handling. In case the communication
		with the Kerberos server fails, we should reschedule a call very soon (maybe
		within a minute) and once the credentials have expired, we should stop the
		(re-)connection timer.
	*/

ERROR:
	if (error)
		krb5_free_error_message(daemon->krb5.ctx, error);
	if (keytab)
		krb5_kt_close(daemon->krb5.ctx, keytab);

	return r;
}

static int pakfire_daemon_prepare_for_shutdown(
		sd_bus_message* message, void* data, sd_bus_error* error) {
	struct pakfire_daemon* daemon = data;
    int shutdown;
    int r;

    // Read the boolean value from the signal message
    r = sd_bus_message_read(message, "b", &shutdown);
    if (r < 0) {
		ERROR(daemon->ctx, "Failed to parse PrepareForShutdown signal: %s\n", strerror(-r));
		return r;
    }

	// If shutdown is true, the system is going to shut down soon.
	// If false, the shutdown has been canceled.
	if (shutdown) {
		DEBUG(daemon->ctx, "Expecting the system to shut down soon...\n");

		// XXX TODO
	} else {
		DEBUG(daemon->ctx, "Shutdown has been canceled\n");

		// XXX TODO
	}

    return 0;
}

static int pakfire_daemon_setup_bus(struct pakfire_daemon* daemon) {
	int r;

	// Connect to the system bus
	r = sd_bus_open_system(&daemon->bus);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not connect to system bus: %s\n", strerror(-r));
		return r;
	}

	// Attach the bus to the event loop
	r = sd_bus_attach_event(daemon->bus, daemon->loop, SD_EVENT_PRIORITY_NORMAL);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not attach the bus to the event loop: %s\n", strerror(-r));
		return r;
	}

	// Add a signal match
	r = sd_bus_match_signal(
		daemon->bus,
		NULL,

		// Destination, Path & Interface
		"org.freedesktop.login1",
		"/org/freedesktop/login1",
		"org.freedesktop.login1.Manager",

		// Signal & Handler
		"PrepareForShutdown",
		pakfire_daemon_prepare_for_shutdown,
		daemon
	);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not register PrepareForShutdown signal: %s\n", strerror(-r));
		return r;
	}

	return 0;
}

static int pakfire_daemon_setup_loop(struct pakfire_daemon* daemon) {
	int r;

	// Fetch a reference to the default event loop
	r = sd_event_default(&daemon->loop);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not setup event loop: %s\n", strerror(-r));
		return r;
	}

	// Enable the watchdog
	r = sd_event_set_watchdog(daemon->loop, 1);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not activate watchdog: %s\n", strerror(-r));
		return r;
	}

	// Listen for SIGTERM
	r = sd_event_add_signal(daemon->loop, NULL, SIGTERM|SD_EVENT_SIGNAL_PROCMASK,
		pakfire_daemon_terminate, daemon);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not register handling SIGTERM: %s\n", strerror(-r));
		return r;
	}

	// Listen for SIGINT
	r = sd_event_add_signal(daemon->loop, NULL, SIGINT|SD_EVENT_SIGNAL_PROCMASK,
		pakfire_daemon_terminate, daemon);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not register handling SIGINT: %s\n", strerror(-r));
		return r;
	}

	// Listen for SIGCHLD
	r = sd_event_add_signal(daemon->loop, NULL, SIGCHLD|SD_EVENT_SIGNAL_PROCMASK,
		pakfire_daemon_SIGCHLD, daemon);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not register handling SIGCHLD: %s\n", strerror(-r));
		return r;
	}

	// Setup the authentication timer
	r = sd_event_add_time_relative(daemon->loop, &daemon->auth_timer,
		CLOCK_MONOTONIC, 0, 0, pakfire_daemon_auth, daemon);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not register the authentication timer: %s\n", strerror(-r));
		return r;
	}

	// Authenticate continuously
	r = sd_event_source_set_enabled(daemon->auth_timer, SD_EVENT_ON);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not activate the auth timer: %s\n", strerror(-r));
		return r;
	}

	// Setup the reconnection timer
	r = sd_event_add_time_relative(daemon->loop, &daemon->connect_timer,
		CLOCK_MONOTONIC, 0, 0, pakfire_daemon_connect, daemon);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not register the connection timer: %s\n", strerror(-r));
		return r;
	}

	// Disable the reconnection timer until we are authenticated
	r = sd_event_source_set_enabled(daemon->connect_timer, SD_EVENT_OFF);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not disable the connection timer: %s\n", strerror(-r));
		return r;
	}

	// Setup the stats timer
	r = sd_event_add_time_relative(daemon->loop, &daemon->stats_timer,
		CLOCK_MONOTONIC, 0, 0, pakfire_daemon_submit_stats, daemon);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not register the stat timer: %s\n", strerror(-r));
		return r;
	}

	// Disable sending stats until we are connected
	r = sd_event_source_set_enabled(daemon->stats_timer, SD_EVENT_OFF);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not disable the stats timer: %s\n", strerror(-r));
		return r;
	}

	return 0;
}

static int pakfire_daemon_configure(struct pakfire_daemon* daemon) {
	struct pakfire_config* config = NULL;
	const char* keytab = NULL;
	const char* url = NULL;
	int r;

	// Fetch the configuration
	config = pakfire_ctx_get_config(daemon->ctx);
	if (!config) {
		ERROR(daemon->ctx, "Could not fetch configuration: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Fetch the URL
	url = pakfire_config_get(config, "daemon", "url", "https://pakfire.ipfire.org");

	// Store the URL
	r = pakfire_string_set(daemon->url, url);
	if (r < 0)
		goto ERROR;

	// Fetch the keytab
	keytab = pakfire_config_get(config, "daemon", "keytab", KRB5_DEFAULT_KEYTAB);

	// Store the keytab
	r = pakfire_string_set(daemon->keytab, keytab);
	if (r < 0)
		goto ERROR;

ERROR:
	if (config)
		pakfire_config_unref(config);

	return r;
}

static void pakfire_daemon_free(struct pakfire_daemon* daemon) {
	// Release shutdown inhibition
	pakfire_daemon_release_inhibit_shutdown(daemon);

	if (daemon->krb5.principal)
		krb5_free_principal(daemon->krb5.ctx, daemon->krb5.principal);
	if (daemon->krb5.ccache)
		krb5_cc_close(daemon->krb5.ctx, daemon->krb5.ccache);
	if (daemon->krb5.ctx)
		krb5_free_context(daemon->krb5.ctx);

	if (daemon->auth_timer)
		sd_event_source_unref(daemon->auth_timer);
	if (daemon->connect_timer)
		sd_event_source_unref(daemon->connect_timer);
	if (daemon->stats_timer)
		sd_event_source_unref(daemon->stats_timer);
	if (daemon->service)
		pakfire_buildservice_unref(daemon->service);
	if (daemon->client)
		pakfire_httpclient_unref(daemon->client);
	if (daemon->cgroup)
		pakfire_cgroup_unref(daemon->cgroup);
	if (daemon->control)
		pakfire_xfer_unref(daemon->control);
	if (daemon->loop)
		sd_event_unref(daemon->loop);
	if (daemon->bus)
		sd_bus_unref(daemon->bus);
	if (daemon->ctx)
		pakfire_ctx_unref(daemon->ctx);
	free(daemon);
}

int pakfire_daemon_create(struct pakfire_daemon** daemon, struct pakfire_ctx* ctx) {
	struct pakfire_daemon* d = NULL;
	int r;

	// Allocate some memory
	d = calloc(1, sizeof(*d));
	if (!d)
		return -errno;

	// Store a reference to the context
	d->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	d->nrefs = 1;

	// Initialize file descriptors
	d->inhibitfd = -EBADF;

	// Read configuration
	r = pakfire_daemon_configure(d);
	if (r < 0)
		goto ERROR;

	// Setup the event loop
	r = pakfire_daemon_setup_loop(d);
	if (r)
		goto ERROR;

	// Setup dbus
	r = pakfire_daemon_setup_bus(d);
	if (r < 0)
		goto ERROR;

	// Setup Kerberos Authentication
	r = pakfire_daemon_setup_krb5(d);
	if (r)
		goto ERROR;

	// Connect to the build service
	r = pakfire_buildservice_create(&d->service, d->ctx, d->url);
	if (r < 0)
		goto ERROR;

	// Create the cgroup
	r = pakfire_cgroup_create(&d->cgroup, d->ctx, NULL, "pakfire-daemon", 0);
	if (r < 0)
		goto ERROR;

	// Create the HTTP client
	r = pakfire_httpclient_create(&d->client, d->ctx, d->loop);
	if (r < 0)
		goto ERROR;

	// Reconnect after one second
	d->reconnect_holdoff = S_TO_US(1);

	// Return the pointer
	*daemon = d;

	return 0;

ERROR:
	if (d)
		pakfire_daemon_unref(d);

	return r;
}

struct pakfire_daemon* pakfire_daemon_ref(struct pakfire_daemon* daemon) {
	++daemon->nrefs;

	return daemon;
}

struct pakfire_daemon* pakfire_daemon_unref(struct pakfire_daemon* daemon) {
	if (--daemon->nrefs > 0)
		return daemon;

	pakfire_daemon_free(daemon);
	return NULL;
}

sd_event* pakfire_daemon_loop(struct pakfire_daemon* daemon) {
	return sd_event_ref(daemon->loop);
}

struct pakfire_buildservice* pakfire_daemon_buildservice(struct pakfire_daemon* daemon) {
	return pakfire_buildservice_ref(daemon->service);
}

struct pakfire_httpclient* pakfire_daemon_httpclient(struct pakfire_daemon* daemon) {
	return pakfire_httpclient_ref(daemon->client);
}

const char* pakfire_daemon_url(struct pakfire_daemon* daemon) {
	return daemon->url;
}

int pakfire_daemon_main(struct pakfire_daemon* daemon) {
	int r;

	// We are now ready
	sd_notify(0, "READY=1\n" "STATUS=Ready...");

	// Launch the event loop
	r = sd_event_loop(daemon->loop);
	if (r < 0) {
		ERROR(daemon->ctx, "Could not run the event loop: %s\n", strerror(-r));
		goto ERROR;
	}

	// Let systemd know that we are shutting down
	sd_notify(0, "STOPPING=1\n" "STATUS=Shutting down...");

	return 0;

ERROR:
	sd_notifyf(0, "ERRNO=%i", -r);

	return 1;
}

/*
	Called after a job has exited
*/
int pakfire_daemon_job_finished(struct pakfire_daemon* daemon, struct pakfire_job* job) {
	int r;

	DEBUG(daemon->ctx, "Removing job %p\n", job);

	for (unsigned int i = 0; i < MAX_JOBS; i++) {
		if (daemon->jobs[i] != job)
			continue;

		// Dereference the job and clear the slot
		pakfire_job_unref(daemon->jobs[i]);
		daemon->jobs[i] = NULL;

		break;
	}

	// Now we have one less job running
	daemon->running_jobs--;

	// Release the shutdown inhibition if there are no more jobs running
	if (!daemon->running_jobs) {
		r = pakfire_daemon_release_inhibit_shutdown(daemon);
		if (r < 0)
			return r;
	}

	return 0;
}

int pakfire_daemon_send_message(struct pakfire_daemon* self, struct json_object* message) {
	const char* m = NULL;
	size_t length = 0;
	int r;

	// Return an error if we are not connected
	if (!self->control)
		return -ENOTCONN;

	// Serialize to string
	m = json_object_to_json_string_length(message,
			JSON_C_TO_STRING_SPACED | JSON_C_TO_STRING_PRETTY, &length);
	if (!m) {
		ERROR(self->ctx, "Failed to serialize message: %m\n");
		return -errno;
	}

	// Send the message
	r = pakfire_xfer_send_message(self->control, m, length);
	if (r < 0) {
		switch (-r) {
			case EAGAIN:
				break;

			default:
				ERROR(self->ctx, "Failed to send message: %s\n", strerror(-r));
				return r;
		}
	}

	return 0;
}
