/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <linux/limits.h>
#include <pwd.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/file.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

#include <archive.h>
#include <archive_entry.h>
#include <solv/evr.h>
#include <solv/pool.h>
#include <solv/poolarch.h>
#include <solv/queue.h>

#include <pakfire/arch.h>
#include <pakfire/build.h>
#include <pakfire/config.h>
#include <pakfire/constants.h>
#include <pakfire/ctx.h>
#include <pakfire/db.h>
#include <pakfire/deps.h>
#include <pakfire/dist.h>
#include <pakfire/logging.h>
#include <pakfire/i18n.h>
#include <pakfire/mount.h>
#include <pakfire/os.h>
#include <pakfire/package.h>
#include <pakfire/packagelist.h>
#include <pakfire/pakfire.h>
#include <pakfire/parser.h>
#include <pakfire/path.h>
#include <pakfire/pwd.h>
#include <pakfire/repo.h>
#include <pakfire/snapshot.h>
#include <pakfire/string.h>
#include <pakfire/transaction.h>
#include <pakfire/util.h>

struct pakfire {
	struct pakfire_ctx* ctx;
	int nrefs;

	char path[PATH_MAX];
	int fd;

	char cache_path[PATH_MAX];

	struct pakfire_arches {
		char nominal[ARCH_MAX];
		const char* effective;
	} arches;

	int flags;

	// Keep some internal state
	enum pakfire_internal_flags {
		PAKFIRE_HAS_PATH        = (1 << 0),
		PAKFIRE_DESTROY_ON_FREE = (1 << 1),
		PAKFIRE_UMOUNT_PATH     = (1 << 2),
		PAKFIRE_POOL_READY      = (1 << 3),
	} internal_flags;

	// UID/GID of running user
	struct pakfire_user {
		uid_t uid;
		char name[NAME_MAX];
		char home[PATH_MAX];
		struct pakfire_subid subuids;
	} user;

	struct pakfire_group {
		gid_t gid;
		char name[NAME_MAX];
		struct pakfire_subid subgids;
	} group;

	// Pool
	Pool* pool;

	struct pakfire_config* config;

	// Distro
	struct pakfire_distro distro;

	// Snapshot
	struct pakfire_snapshot* snapshot;

	// States
	unsigned int in_free:1;

	// Disk Writer
	struct archive* writer;
};

/*
	This is a list of all features that are supported by this version of Pakfire
*/
static const char* features[] = {
	"RichDependencies",

	// Package Formats
	"PackageFormat-6",
	"PackageFormat-5",

	// Compression
	"Compress-XZ",
	"Compress-Zstandard",

	// Digests
	"Digest-BLAKE2b512",
	"Digest-BLAKE2s256",
	"Digest-SHA3-512",
	"Digest-SHA3-256",
	"Digest-SHA2-512",
	"Digest-SHA2-256",

	// Systemd
	"systemd-sysusers",
	"systemd-tmpfiles",

	// The end
	NULL,
};

int pakfire_on_root(struct pakfire* pakfire) {
	return (strcmp(pakfire->path, "/") == 0);
}

uid_t pakfire_uid(struct pakfire* pakfire) {
	return pakfire->user.uid;
}

gid_t pakfire_gid(struct pakfire* pakfire) {
	return pakfire->group.gid;
}

const struct pakfire_subid* pakfire_subuid(struct pakfire* pakfire) {
	return &pakfire->user.subuids;
}

const struct pakfire_subid* pakfire_subgid(struct pakfire* pakfire) {
	return &pakfire->group.subgids;
}

/*
	Maps any UID/GIDs to the SUBUID/SUBGIDs so that we can transparently
	copy files in and out of the jail environment.
*/
static unsigned int pakfire_map_id(struct pakfire* pakfire,
		const struct pakfire_subid* subid, const unsigned int id) {
	// Nothing to do if we are running on root
	if (pakfire_on_root(pakfire))
		return id;

	// Map the ID
	unsigned int mapped_id = subid->id + id;

	// Check if the ID is in range
	if (id > subid->length) {
		ERROR(pakfire->ctx, "Mapped ID is out of range. Setting to %u\n", subid->id);
		mapped_id = subid->id;
	}

	return mapped_id;
}

static unsigned int pakfire_unmap_id(struct pakfire* pakfire,
		const struct pakfire_subid* subid, const unsigned int id) {
	// Nothing to do if we are running on root
	if (pakfire_on_root(pakfire))
		return id;

	// Unmap the ID
	int unmapped_id = id - subid->id;

	// Check if the ID is in range
	if (unmapped_id < 0) {
		ERROR(pakfire->ctx, "Mapped ID is out of range. Setting to %u\n", subid->id);
		unmapped_id = subid->id;
	}

	return unmapped_id;
}

static void pool_log(Pool* pool, void* data, int type, const char* s) {
	struct pakfire* pakfire = (struct pakfire*)data;

	DEBUG(pakfire->ctx, "pool: %s", s);
}

static Id pakfire_handle_ns_pakfire(struct pakfire* pakfire, const char* name) {
	// Find all supported features
	for (const char** feature = features; *feature; feature++) {
		if (strcmp(*feature, name) == 0)
			return 1;
	}

	// Not supported
	return 0;
}

static Id pakfire_handle_ns_arch(struct pakfire* pakfire, const char* name) {
	const char* arch = pakfire_get_arch(pakfire);

	return strcmp(arch, name) == 0;
}

static Id pakfire_namespace_callback(Pool* pool, void* data, Id ns, Id id) {
	struct pakfire* pakfire = (struct pakfire*)data;

	const char* namespace = pool_id2str(pool, ns);
	const char* name = pakfire_dep2str(pakfire, id);

	DEBUG(pakfire->ctx, "Namespace callback called for %s(%s)\n", namespace, name);

	// Handle the pakfire() namespace
	if (strcmp(namespace, "pakfire") == 0)
		return pakfire_handle_ns_pakfire(pakfire, name);

	// Handle the arch() namespace
	else if (strcmp(namespace, "arch") == 0)
		return pakfire_handle_ns_arch(pakfire, name);

	// Not handled here
	else
		return 0;
}

static int pakfire_setup_path(struct pakfire* self, const char* path) {
	int r;

	// Template when running Pakfire in a temporary environment
	char tmppath[PATH_MAX] = PAKFIRE_TMP_DIR "/pakfire-root-XXXXXX";

	// Don't do any of this when in stub mode
	if (pakfire_has_flag(self, PAKFIRE_FLAGS_STUB))
		return 0;

	// If we don't have a path, create something temporary...
	if (!path) {
		// Create a new temporary directory
		path = pakfire_mkdtemp(tmppath);
		if (!path)
			return -errno;

		// Destroy the temporary directory afterwards
		self->internal_flags |= PAKFIRE_DESTROY_ON_FREE;

		// Mount the snapshot
		if (self->snapshot) {
			r = pakfire_snapshot_mount(self->snapshot, path);
			if (r < 0) {
				ERROR(self->ctx, "Could not mount snapshot: %s\n", strerror(-r));
				return r;
			}

		// Mount a tmpfs
		} else if (pakfire_has_flag(self, PAKFIRE_USE_TMPFS)) {
			r = mount("pakfire_root", path, "tmpfs", 0, NULL);
			if (r) {
				ERROR(self->ctx, "Could not mount tmpfs: %m\n");
				return -errno;
			}

			// Umount path later
			self->internal_flags |= PAKFIRE_UMOUNT_PATH;
		}
	}

	// Store the path
	r = pakfire_string_set(self->path, path);
	if (r < 0)
		return r;

	// Open the path
	r = open(self->path, O_DIRECTORY);
	if (r < 0) {
		ERROR(self->ctx, "Could not open %s: %m\n", self->path);
		return -errno;
	}

	// Store the file descriptor
	self->fd = r;

	return 0;
}

static int pakfire_lock_running_kernel(struct pakfire* pakfire) {
	struct utsname utsname;
	char buffer[NAME_MAX];

	// Call uname()
	int r = uname(&utsname);
	if (r) {
		ERROR(pakfire->ctx, "uname() failed: %m\n");
		return r;
	}

	DEBUG(pakfire->ctx, "Locking running kernel %s\n", utsname.release);

	r = pakfire_string_format(buffer, "kernel(%s)", utsname.release);
	if (r)
		return r;

	// Add a locking pool job
	Id id = pool_str2id(pakfire->pool, buffer, 1);
	if (id)
		queue_push2(&pakfire->pool->pooljobs, SOLVER_LOCK|SOLVER_SOLVABLE_PROVIDES, id);

	return 0;
}

static int pakfire_setup_pool(struct pakfire* pakfire) {
	int r;

	// Initialize the pool
	Pool* pool = pakfire->pool = pool_create();
	pool_setdisttype(pool, DISTTYPE_RPM);

	// Don't replace source packages with binary packages
	pool_set_flag(pool, POOL_FLAG_IMPLICITOBSOLETEUSESCOLORS, 1);

#ifdef ENABLE_DEBUG
	// Enable debug output
	pool_setdebuglevel(pool, 2);
#endif

	// Set architecture of the pool
	pool_setarchpolicy(pool, pakfire_get_effective_arch(pakfire));

	// Set path
	pool_set_rootdir(pool, pakfire->path);

	// Set debug callback
	pool_setdebugcallback(pool, pool_log, pakfire);

	// Install namespace callback
	pool_setnamespacecallback(pool, pakfire_namespace_callback, pakfire);

	// These packages can be installed multiple times simultaneously
	static const char* pakfire_multiinstall_packages[] = {
		"kernel",
		"kernel-devel",
		NULL,
	};

	for (const char** package = pakfire_multiinstall_packages; *package; package++) {
		Id id = pakfire_str2dep(pakfire, *package);
		if (!id)
			continue;

		queue_push2(&pool->pooljobs, SOLVER_SOLVABLE_PROVIDES|SOLVER_MULTIVERSION, id);
	}

	// Lock the running kernel
	if (pakfire_on_root(pakfire)) {
		r = pakfire_lock_running_kernel(pakfire);
		if (r < 0)
			return r;
	}

	return 0;
}

static int pakfire_load_installed_packages(struct pakfire* self) {
	struct pakfire_repo* repo = NULL;
	struct pakfire_db* db = NULL;
	int r;

	// Fetch the system repository
	repo = pakfire_get_repo(self, PAKFIRE_REPO_SYSTEM);

	// Open the database (or create a new one)
	r = pakfire_db_open(&db, self, PAKFIRE_DB_READWRITE);
	if (r < 0)
		goto ERROR;

	// Load database content
	r = pakfire_db_load(db, repo);
	if (r < 0)
		goto ERROR;

ERROR:
	if (repo)
		pakfire_repo_unref(repo);
	if (db)
		pakfire_db_unref(db);

	return r;
}

static int pakfire_setup_default_repos(struct pakfire* self) {
	struct pakfire_repo* commandline = NULL;
	struct pakfire_repo* dummy = NULL;
	struct pakfire_repo* system = NULL;
	int r;

	// Create a dummy repository
	r = pakfire_repo_create(&dummy, self, PAKFIRE_REPO_DUMMY);
	if (r < 0)
		goto ERROR;

	// Disable the repository
	pakfire_repo_set_enabled(dummy, 0);

	// Create the system repository
	r = pakfire_repo_create(&system, self, PAKFIRE_REPO_SYSTEM);
	if (r < 0)
		goto ERROR;

	// Set this repository as the installed one
	pool_set_installed(self->pool, pakfire_repo_get_repo(system));

	// Create the command line repo
	r = pakfire_repo_create(&commandline, self, PAKFIRE_REPO_COMMANDLINE);
	if (r < 0)
		goto ERROR;

ERROR:
	if (commandline)
		pakfire_repo_unref(commandline);
	if (dummy)
		pakfire_repo_unref(dummy);
	if (system)
		pakfire_repo_unref(system);

	return r;
}

static void pakfire_free(struct pakfire* pakfire) {
	struct pakfire_repo* repo = NULL;
	int r;

	// Avoid recursive free
	if (pakfire->in_free)
		return;

	pakfire->in_free = 1;

	// Destroy the commandline repository
	repo = pakfire_get_repo(pakfire, PAKFIRE_REPO_COMMANDLINE);
	if (repo) {
		r = pakfire_repo_clean(repo, PAKFIRE_REPO_CLEAN_FLAGS_DESTROY);
		if (r)
			ERROR(pakfire->ctx, "Could not cleanup %s repository: %m\n", PAKFIRE_REPO_COMMANDLINE);

		pakfire_repo_unref(repo);
	}

	// Write back the local repository
	repo = pakfire_get_repo(pakfire, PAKFIRE_REPO_LOCAL);
	if (repo) {
		// Write back the local repository metadata
		r = pakfire_repo_write_metadata(repo, NULL);
		if (r)
			ERROR(pakfire->ctx, "Could not write the local repository: %s. Ignoring.\n", strerror(-r));

		pakfire_repo_unref(repo);
	}

	// Close the path
	if (pakfire->fd >= 0)
		close(pakfire->fd);

	// Umount the snapshot
	if (pakfire->snapshot) {
		r = pakfire_snapshot_umount(pakfire->snapshot);
		if (r)
			ERROR(pakfire->ctx, "Could not umount the snapshot: %s\n", strerror(-r));

		pakfire_snapshot_unref(pakfire->snapshot);
	}

	// Umount path
	if (pakfire->internal_flags & PAKFIRE_UMOUNT_PATH) {
		r = umount2(pakfire->path, 0);
		if (r)
			ERROR(pakfire->ctx, "Could not umount ramdisk at %s: %m\n", pakfire->path);
	}

	// Destroy path
	if (pakfire->internal_flags & PAKFIRE_DESTROY_ON_FREE) {
		DEBUG(pakfire->ctx, "Destroying %s\n", pakfire->path);

		// Destroy the temporary directory
		r = pakfire_rmtree(pakfire->path, 0);
		if (r)
			ERROR(pakfire->ctx, "Could not destroy %s: %s\n", pakfire->path, strerror(-r));
	}

	pakfire_repo_free_all(pakfire);

	if (pakfire->pool)
		pool_free(pakfire->pool);
	if (pakfire->writer)
		archive_write_free(pakfire->writer);
	if (pakfire->config)
		pakfire_config_unref(pakfire->config);
	if (pakfire->ctx)
		pakfire_ctx_unref(pakfire->ctx);

	free(pakfire);
}

// Safety check in case this is being launched on the host system
static int pakfire_safety_checks(struct pakfire* pakfire) {
	// Nothing to do if we are not working on root
	if (!pakfire_on_root(pakfire))
		return 0;

	// We must be root in order to operate in /
	if (pakfire->user.uid) {
		ERROR(pakfire->ctx, "Must be running as root on /\n");
		return -EPERM;
	}

	return 0;
}

static int pakfire_read_one_repo_config(struct pakfire* pakfire, DIR* dir, const char* path) {
	FILE* f = NULL;
	int fd = -1;
	int r;

	// Open the file
	fd = openat(dirfd(dir), path, O_CLOEXEC);
	if (fd < 0)
		return -errno;

	// Re-open as file handle
	f = fdopen(fd, "r");
	if (!f)
		return -errno;

	// Read the configuration
	r = pakfire_config_read(pakfire->config, f);

	// Cleanup
	fclose(f);

	return r;
}

static int pakfire_read_repo_config(struct pakfire* pakfire) {
	struct dirent* entry = NULL;
	char path[PATH_MAX];
	int r;

	// Make path absolute
	r = pakfire_path(pakfire, path, "%s", PAKFIRE_CONFIG_DIR "/repos");
	if (r)
		return r;

	DEBUG(pakfire->ctx, "Reading repository configuration from %s\n", path);

	// Open path
	DIR* dir = opendir(path);
	if (!dir) {
		switch (errno) {
			case ENOENT:
				return 0;

			default:
				ERROR(pakfire->ctx, "Could not open %s: %m\n", path);
				return -errno;
		}
	}

	for (;;) {
		entry = readdir(dir);
		if (!entry)
			break;

		// Skip anything that isn't a regular file
		if (entry->d_type != DT_REG)
			continue;

		// Skip any files that don't end on .repo
		if (!pakfire_path_match("*.repo", entry->d_name))
			continue;

		// Read the configuration
		r = pakfire_read_one_repo_config(pakfire, dir, entry->d_name);
		if (r)
			goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	if (dir)
		closedir(dir);

	return r;
}

static int pakfire_setup_local_repo(struct pakfire* self) {
	struct pakfire_repo* local = NULL;
	char path[PATH_MAX];
	char url[PATH_MAX];
	int r;

	// Fetch distro ID
	const char* distro_id = pakfire_get_distro_id(self);
	if (!distro_id) {
		ERROR(self->ctx, "Could not fetch the distro ID\n");
		return -EINVAL;
	}

	// Fetch the distro version ID
	const char* version_id = pakfire_get_distro_version_id(self);
	if (!version_id) {
		ERROR(self->ctx, "Could not fetch the distro version ID\n");
		return -EINVAL;
	}

	// Make the repository path
	r = pakfire_string_format(path,
			"~/.local/share/%s/local/%s/%s", PACKAGE_NAME, distro_id, version_id);
	if (r < 0) {
		ERROR(self->ctx, "Could not compose the local repository path: %s\n", strerror(-r));
		goto ERROR;
	}

	// Expand the path
	r = pakfire_path_expand(path, path);
	if (r < 0) {
		ERROR(self->ctx, "Could not expand the local repository path: %s\n", strerror(-r));
		goto ERROR;
	}

	// Create the path
	r = pakfire_mkdir(path, 0700);
	if (r < 0) {
		ERROR(self->ctx, "Could not create the local repository at %s: %s\n",
			path, strerror(-r));
		goto ERROR;
	}

	// Create a new repository
	r = pakfire_repo_create(&local, self, PAKFIRE_REPO_LOCAL);
	if (r < 0) {
		ERROR(self->ctx, "Could not create local repository: %s\n", strerror(-r));
		goto ERROR;
	}

	// Set description
	r = pakfire_repo_set_description(local, _("Locally Built Packages"));
	if (r < 0) {
		ERROR(self->ctx, "Could not set local repository description: %s\n", strerror(-r));
		goto ERROR;
	}

	// Compose the URL
	r = pakfire_string_format(url, "file://%s", path);
	if (r < 0) {
		ERROR(self->ctx, "Could not compose the local repository URL: %s\n", strerror(-r));
		goto ERROR;
	}

	// Set the URL
	r = pakfire_repo_set_baseurl(local, url);
	if (r < 0) {
		ERROR(self->ctx, "Could not set local repository URL: %s\n", strerror(-r));
		goto ERROR;
	}

ERROR:
	if (local)
		pakfire_repo_unref(local);

	return r;
}

static int pakfire_refresh_repo(struct pakfire* pakfire, struct pakfire_repo* repo, void* data) {
	return pakfire_repo_refresh(repo, 0);
}

const char* pakfire_get_distro_name(struct pakfire* pakfire) {
	if (*pakfire->distro.name)
		return pakfire->distro.name;

	return NULL;
}

const char* pakfire_get_distro_id(struct pakfire* pakfire) {
	if (*pakfire->distro.id)
		return pakfire->distro.id;

	return NULL;
}

const char* pakfire_get_distro_vendor(struct pakfire* pakfire) {
	if (*pakfire->distro.vendor)
		return pakfire->distro.vendor;

	return NULL;
}

const char* pakfire_get_distro_version(struct pakfire* pakfire) {
	if (*pakfire->distro.version)
		return pakfire->distro.version;

	return NULL;
}

const char* pakfire_get_distro_version_id(struct pakfire* pakfire) {
	if (*pakfire->distro.version_id)
		return pakfire->distro.version_id;

	return NULL;
}

const char* pakfire_get_distro_tag(struct pakfire* pakfire) {
	int r;

	// Generate the tag
	if (!*pakfire->distro.tag) {
		r = pakfire_string_format(pakfire->distro.tag,
				"%s%s", pakfire->distro.id, pakfire->distro.version_id);
		if (r < 0) {
			errno = -r;
			return NULL;
		}
	}

	return pakfire->distro.tag;
}

static int pakfire_config_import_distro(struct pakfire* pakfire) {
	int r;

	// Nothing to do if there is no distro section
	if (!pakfire_config_has_section(pakfire->config, "distro"))
		return 0;

	// Name
	const char* name = pakfire_config_get(pakfire->config, "distro", "name", NULL);
	if (name) {
		r = pakfire_string_set(pakfire->distro.name, name);
		if (r < 0) {
			ERROR(pakfire->ctx, "Could not set distro name: %s\n", strerror(-r));
			return r;
		}
	}

	// ID
	const char* id = pakfire_config_get(pakfire->config, "distro", "id", NULL);
	if (id) {
		r = pakfire_string_set(pakfire->distro.id, id);
		if (r < 0) {
			ERROR(pakfire->ctx, "Could not set distro ID: %s\n", strerror(-r));
			return r;
		}
	}

	// Version ID
	const char* version_id = pakfire_config_get(pakfire->config, "distro", "version_id", NULL);
	if (version_id) {
		r = pakfire_string_set(pakfire->distro.version_id, version_id);
		if (r < 0) {
			ERROR(pakfire->ctx, "Could not set distro version ID: %s\n", strerror(-r));
			return r;
		}
	}

	// Codename
	const char* codename = pakfire_config_get(pakfire->config, "distro", "codename", NULL);
	if (codename) {
		r = pakfire_string_set(pakfire->distro.version_codename, codename);
		if (r < 0) {
			ERROR(pakfire->ctx, "Could not set distro codename: %s\n", strerror(-r));
			return r;
		}
	}

	// Fill in version
	if (*pakfire->distro.version_codename) {
		r = pakfire_string_format(pakfire->distro.version, "%s (%s)",
				pakfire->distro.version_id, pakfire->distro.version_codename);
		if (r < 0) {
			ERROR(pakfire->ctx, "Could not set distro version: %s\n", strerror(-r));
			return r;
		}
	} else {
		r = pakfire_string_set(pakfire->distro.version, pakfire->distro.version_id);
		if (r < 0) {
			ERROR(pakfire->ctx, "Could not set distro version: %s\n", strerror(-r));
			return r;
		}
	}

	// Fill in pretty name
	r = pakfire_string_format(pakfire->distro.pretty_name, "%s %s",
			pakfire->distro.name, pakfire->distro.version);
	if (r < 0) {
		ERROR(pakfire->ctx, "Could not set distro pretty name: %s\n", strerror(-r));
		return r;
	}

	// Vendor
	const char* vendor = pakfire_config_get(pakfire->config, "distro", "vendor", NULL);
	if (vendor) {
		r = pakfire_string_set(pakfire->distro.vendor, vendor);
		if (r < 0) {
			ERROR(pakfire->ctx, "Could not set distro vendor: %s\n", strerror(-r));
			return r;
		}
	}

	// Slogan
	const char* slogan = pakfire_config_get(pakfire->config, "distro", "slogan", NULL);
	if (slogan) {
		r = pakfire_string_set(pakfire->distro.slogan, slogan);
		if (r < 0) {
			ERROR(pakfire->ctx, "Could not set distro slogan: %s\n", strerror(-r));
			return r;
		}
	}

	return 0;
}

static int pakfire_read_os_release(struct pakfire* pakfire, const char* path) {
	char file[PATH_MAX];
	int r;

	// Compose path
	r = pakfire_path_append(file, path, "/etc/os-release");
	if (r < 0)
		return r;

	// Read the distro
	r = pakfire_distro(&pakfire->distro, file);
	if (r < 0) {
		switch (-r) {
			case ENOENT:
				return 0;

			default:
				ERROR(pakfire->ctx, "Could not read /etc/os-release: %s\n", strerror(-r));
				break;
		}
	}

	return r;
}

static int pakfire_set_cache_path(struct pakfire* pakfire) {
	const char* cache_path = pakfire_ctx_get_cache_path(pakfire->ctx);
	const char* arch = pakfire_get_effective_arch(pakfire);

	// Format the final path
	return pakfire_string_format(pakfire->cache_path, "%s/%s/%s/%s",
		cache_path, pakfire->distro.id, pakfire->distro.version_id, arch);
}

static int pakfire_setup_user(struct pakfire* pakfire) {
	struct passwd user;
	struct passwd* u = NULL;
	struct group group;
	struct group* g = NULL;
	char buffer[1024];
	int r;

	// Fetch the UID/GID we are running as
	const uid_t uid = geteuid();
	const gid_t gid = getegid();

	// Fetch all user information
	r = getpwuid_r(uid, &user, buffer, sizeof(buffer), &u);
	if (r)
		goto ERROR;

	// Store UID
	pakfire->user.uid = pakfire->user.subuids.id = uid;

	// Store username
	r = pakfire_string_set(pakfire->user.name, user.pw_name);
	if (r)
		goto ERROR;

	// Store home directory
	r = pakfire_string_set(pakfire->user.home, user.pw_dir);
	if (r)
		goto ERROR;

	// Fetch all group information
	r = getgrgid_r(gid, &group, buffer, sizeof(buffer), &g);
	if (r)
		goto ERROR;

	// Store GID
	pakfire->group.gid = pakfire->group.subgids.id = gid;

	// Store name
	r = pakfire_string_set(pakfire->group.name, group.gr_name);
	if (r)
		goto ERROR;

	/*
		Set default ranges for SUBUID/SUBGID

		For root, we set the entire range, but for unprivileged users,
		we can only map our own UID/GID. This may later be overwritten
		from /etc/sub{u,g}id.
	*/
	if (uid == 0)
		pakfire->user.subuids.length = pakfire->group.subgids.length = 0xffffffff - 1;
	else
		pakfire->user.subuids.length = pakfire->group.subgids.length = 1;

	// Read SUBUID/SUBGIDs from file
	if (!pakfire_on_root(pakfire)) {
		// Fetch SUBUIDs
		r = pakfire_getsubuid(pakfire, pakfire->user.name, &pakfire->user.subuids);
		switch (r) {
			case 0:
			case 1:
				break;

			default:
				goto ERROR;
		}

		// Fetch SUBGIDs
		r = pakfire_getsubgid(pakfire, pakfire->user.name, &pakfire->group.subgids);
		switch (r) {
			case 0:
			case 1:
				break;

			default:
				goto ERROR;
		}
	}

	// Success
	r = 0;

ERROR:
	return r;
}

int pakfire_create(struct pakfire** pakfire, struct pakfire_ctx* ctx,
		struct pakfire_config* config, const char* path, const char* arch, int flags) {
	struct pakfire* p = NULL;
	int r;

	// Always require a configuration
	if (!config)
		return -EINVAL;

	// Default to the native architecture
	if (!arch)
		arch = pakfire_arch_native();

	// Check path
	if (path) {
		// Check that we don't have a path in stub mode
		if (flags & PAKFIRE_FLAGS_STUB) {
			ERROR(ctx, "Cannot use path in stub mode\n");
			r = -EINVAL;
			goto ERROR;
		}

		// Check that we don't have path set when using snapshots
		if (flags & PAKFIRE_USE_SNAPSHOT) {
			ERROR(ctx, "Cannot use path with snapshots\n");
			r = -EINVAL;
			goto ERROR;
		}

		// Path must be absolute
		if (!pakfire_string_startswith(path, "/")) {
			ERROR(ctx, "Invalid path: %s\n", path);
			r = -EINVAL;
			goto ERROR;
		}
	}

	// Allocate a new object
	p = calloc(1, sizeof(*p));
	if (!p)
		return -errno;

	// Reference the context
	p->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	p->nrefs = 1;

	// Store the flags
	p->flags = flags;

	// Store a reference to the configuration
	p->config = pakfire_config_ref(config);

	// Initialize file descriptors
	p->fd = -EBADF;

	// Store the nominal architecture
	r = pakfire_string_set(p->arches.nominal, arch);
	if (r < 0)
		goto ERROR;

	// Determine the effective architecture
	p->arches.effective = pakfire_arch_is_supported_by_host(arch);
	if (!p->arches.effective) {
		ERROR(p->ctx, "Unsupported architecture: %s\n", arch);
		r = -ENOTSUP;
		goto ERROR;
	}

	// Setup user/group
	r = pakfire_setup_user(p);
	if (r < 0) {
		ERROR(p->ctx, "Could not parse user information: %s\n", strerror(-r));
		goto ERROR;
	}

	// Store if we have a path
	if (path)
		p->internal_flags |= PAKFIRE_HAS_PATH;

	// Import distro configuration
	r = pakfire_config_import_distro(p);
	if (r < 0) {
		ERROR(p->ctx, "Could not parse distribution from configuration: %s\n", strerror(-r));
		goto ERROR;
	}

	// Read /etc/os-release
	if (p->internal_flags & PAKFIRE_HAS_PATH) {
		r = pakfire_read_os_release(p, path);
		if (r < 0)
			goto ERROR;

		// Perform some safety checks
		r = pakfire_safety_checks(p);
		if (r < 0)
			goto ERROR;
	}

	// Set cache path
	r = pakfire_set_cache_path(p);
	if (r < 0) {
		ERROR(p->ctx, "Could not set cache path: %m\n");
		goto ERROR;
	}

	// Are we using a snapshot?
	if (p->flags & PAKFIRE_USE_SNAPSHOT) {
		// Find the most recent snapshot
		r = pakfire_snapshot_find(&p->snapshot, p);
		if (r < 0) {
			ERROR(p->ctx, "Could not find a snapshot: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Setup path
	r = pakfire_setup_path(p, path);
	if (r < 0)
		goto ERROR;

	// Setup the pool
	r = pakfire_setup_pool(p);
	if (r < 0)
		goto ERROR;

	// Create the default repositories
	r = pakfire_setup_default_repos(p);
	if (r < 0)
		goto ERROR;

	// Create repositories
	r = pakfire_repo_import(p, p->config);
	if (r < 0)
		goto ERROR;

	// Skip a couple of steps when in stub mode
	if (!pakfire_has_flag(p, PAKFIRE_FLAGS_STUB)) {
		// Setup the local repository
		if (p->flags & (PAKFIRE_FLAGS_BUILD|PAKFIRE_FLAGS_BUILD_LOCAL)) {
			r = pakfire_setup_local_repo(p);
			if (r < 0)
				goto ERROR;
		}

		// Read repository configuration
		if (p->internal_flags & PAKFIRE_HAS_PATH) {
			r = pakfire_read_repo_config(p);
			if (r < 0)
				goto ERROR;
		}
	}

	// Load installed packages
	if (!pakfire_has_flag(p, PAKFIRE_FLAGS_STUB)) {
		r = pakfire_load_installed_packages(p);
		if (r < 0)
			goto ERROR;
	}

	// Refresh repositories
	r = pakfire_repo_walk(p, pakfire_refresh_repo, NULL);
	if (r < 0)
		goto ERROR;

	DEBUG(p->ctx, "Pakfire initialized at %p\n", p);
	DEBUG(p->ctx, "  user   = %s (%u)\n", p->user.name, p->user.uid);
	DEBUG(p->ctx, "  group  = %s (%u)\n", p->group.name, p->group.gid);
	DEBUG(p->ctx, "  arch   = %s (%s)\n", pakfire_get_arch(p), pakfire_get_effective_arch(p));
	DEBUG(p->ctx, "  path   = %s\n", pakfire_get_path(p));
	if (p->user.subuids.id)
		DEBUG(p->ctx, "  subuid = %u (%zu)\n", p->user.subuids.id, p->user.subuids.length);
	if (p->group.subgids.id)
		DEBUG(p->ctx, "  subgid = %u (%zu)\n", p->group.subgids.id, p->group.subgids.length);

	// Dump distribution configuration
	DEBUG(p->ctx, "  Distribution: %s\n", p->distro.pretty_name);
	DEBUG(p->ctx, "    name       = %s\n", p->distro.name);
	DEBUG(p->ctx, "    id         = %s\n", p->distro.id);
	DEBUG(p->ctx, "    version    = %s\n", p->distro.version);
	DEBUG(p->ctx, "    version_id = %s\n", p->distro.version_id);
	if (*p->distro.version_codename)
		DEBUG(p->ctx, "    codename   = %s\n", p->distro.version_codename);
	if (*p->distro.vendor)
		DEBUG(p->ctx, "    vendor     = %s\n", p->distro.vendor);
	if (*p->distro.slogan)
		DEBUG(p->ctx, "    slogan     = %s\n", p->distro.slogan);

	// Return the pointer
	*pakfire = pakfire_ref(p);

ERROR:
	if (p)
		pakfire_unref(p);

	return r;
}

struct pakfire* pakfire_ref(struct pakfire* pakfire) {
	++pakfire->nrefs;

	return pakfire;
}

struct pakfire* pakfire_unref(struct pakfire* pakfire) {
	if (--pakfire->nrefs > 0)
		return pakfire;

	pakfire_free(pakfire);
	return NULL;
}

struct pakfire_ctx* pakfire_ctx(struct pakfire* pakfire) {
	return pakfire_ctx_ref(pakfire->ctx);
}

int pakfire_has_flag(struct pakfire* pakfire, const int flag) {
	return pakfire->flags & flag;
}

struct pakfire_config* pakfire_get_config(struct pakfire* pakfire) {
	if (!pakfire->config)
		return NULL;

	return pakfire_config_ref(pakfire->config);
}

const char* pakfire_get_path(struct pakfire* pakfire) {
	return pakfire->path;
}

int pakfire_openat(struct pakfire* pakfire, const char* path, int flags) {
	// Make paths relative
	while (*path == '/')
		path++;

	return openat(pakfire->fd, path, flags);
}

int __pakfire_path(struct pakfire* pakfire, char* path, const size_t length,
		const char* format, ...) {
	char buffer[PATH_MAX];
	va_list args;
	int r;

	// Check if path is set
	if (!*pakfire->path) {
		ERROR(pakfire->ctx, "pakfire_path() called without path being set\n");
		return -ENOTSUP;
	}

	// Format input into buffer
	va_start(args, format);
	r = __pakfire_string_vformat(buffer, sizeof(buffer), format, args);
	va_end(args);

	// Break on any errors
	if (r)
		return r;

	// Join paths together
	return __pakfire_path_append(path, length, pakfire->path, buffer);
}

const char* pakfire_relpath(struct pakfire* pakfire, const char* path) {
	return pakfire_path_relpath(pakfire->path, path);
}

int __pakfire_cache_path(struct pakfire* pakfire, char* path, size_t length,
		const char* format, ...) {
	char buffer[PATH_MAX];
	va_list args;
	int r;

	// Format input into buffer
	va_start(args, format);
	r = pakfire_string_vformat(buffer, format, args);
	va_end(args);

	// Break on any errors
	if (r < 0)
		return r;

	// Join paths together
	return __pakfire_path_append(path, length, pakfire->cache_path, buffer);
}

int pakfire_repo_walk(struct pakfire* pakfire,
		pakfire_repo_walk_callback callback, void* p) {
	struct pakfire_repo* repo = NULL;
	Repo* solv_repo = NULL;
	int i = 0;
	int r;

	Pool* pool = pakfire->pool;

	// Run func for every repository
	FOR_REPOS(i, solv_repo) {
		r = pakfire_repo_open(&repo, pakfire, solv_repo);
		if (r < 0)
			return r;

		// Run callback
		r = callback(pakfire, repo, p);
		pakfire_repo_unref(repo);

		// Raise any errors
		if (r)
			return r;
	}

	return 0;
}

static int __pakfire_repo_clean(struct pakfire* pakfire, struct pakfire_repo* repo,
		void* p) {
	int flags = *(int*)p;

	return pakfire_repo_clean(repo, flags);
}

int pakfire_clean(struct pakfire* pakfire, int flags) {
	int r;

	// Clean all repositories
	r = pakfire_repo_walk(pakfire, __pakfire_repo_clean, &flags);
	if (r)
		return r;

	// Clean all snapshots
	r = pakfire_snapshot_clean(pakfire);
	if (r < 0)
		return r;

	// Clean build environments
	r = pakfire_build_clean(pakfire, flags);
	if (r)
		return r;

	// Remove the cache
	return pakfire_rmtree(PAKFIRE_CACHE_DIR, 0);
}

const char* pakfire_get_arch(struct pakfire* pakfire) {
	return pakfire->arches.nominal;
}

const char* pakfire_get_effective_arch(struct pakfire* pakfire) {
	return pakfire->arches.effective;
}

int pakfire_version_compare(struct pakfire* pakfire, const char* evr1, const char* evr2) {
	return pool_evrcmp_str(pakfire->pool, evr1, evr2, EVRCMP_COMPARE);
}

Pool* pakfire_get_solv_pool(struct pakfire* pakfire) {
	return pakfire->pool;
}

void pakfire_pool_has_changed(struct pakfire* pakfire) {
	pakfire->internal_flags &= ~PAKFIRE_POOL_READY;
}

static int __pakfire_repo_internalize(struct pakfire* pakfire, struct pakfire_repo* repo,
		void* p) {
	int flags = *(int*)p;

	return pakfire_repo_internalize(repo, flags);
}

void pakfire_pool_internalize(struct pakfire* pakfire) {
	int flags = 0;

	// Nothing to do if the pool is ready
	if (pakfire->internal_flags & PAKFIRE_POOL_READY)
		return;

	// Internalize all repositories
	pakfire_repo_walk(pakfire, __pakfire_repo_internalize, &flags);

	// Create fileprovides
	pool_addfileprovides(pakfire->pool);

	// Create whatprovides index
	pool_createwhatprovides(pakfire->pool);

	// Mark the pool as ready
	pakfire->internal_flags |= PAKFIRE_POOL_READY;
}

struct pakfire_repolist* pakfire_get_repos(struct pakfire* pakfire) {
	struct pakfire_repo* repo = NULL;
	struct pakfire_repolist* list;

	int r = pakfire_repolist_create(&list);
	if (r)
		return NULL;

	Pool* pool = pakfire_get_solv_pool(pakfire);
	Repo* solv_repo;
	int i;

	FOR_REPOS(i, solv_repo) {
		// Skip the dummy repository
		if (strcmp(solv_repo->name, PAKFIRE_REPO_DUMMY) == 0)
			continue;

		// Create repository
		r = pakfire_repo_open(&repo, pakfire, solv_repo);
		if (r < 0)
			goto ERROR;

		// Append it to the list
		r = pakfire_repolist_append(list, repo);
		if (r) {
			pakfire_repo_unref(repo);
			goto ERROR;
		}

		pakfire_repo_unref(repo);
	}

	return list;

ERROR:
	pakfire_repolist_unref(list);
	return NULL;
}

struct pakfire_repo* pakfire_get_repo(struct pakfire* pakfire, const char* name) {
	struct pakfire_repo* repo = NULL;
	Repo* solv_repo = NULL;
	int r;
	int i;

	Pool* pool = pakfire_get_solv_pool(pakfire);
	if (!pool)
		return NULL;

	FOR_REPOS(i, solv_repo) {
		if (strcmp(solv_repo->name, name) == 0) {
			r = pakfire_repo_open(&repo, pakfire, solv_repo);
			if (r < 0)
				return NULL;

			return repo;
		}
	}

	// Nothing found
	return NULL;
}

struct pakfire_repo* pakfire_get_installed_repo(struct pakfire* pakfire) {
	struct pakfire_repo* repo = NULL;
	int r;

	if (!pakfire->pool->installed)
		return NULL;

	// Open the repository
	r = pakfire_repo_open(&repo, pakfire, pakfire->pool->installed);
	if (r < 0)
		return NULL;

	return repo;
}

/*
	Convenience function to dist() a package on the fly
*/
static int pakfire_commandline_dist(struct pakfire* pakfire, struct pakfire_repo* repo,
		const char* path, struct pakfire_package** pkg) {
	struct pakfire_archive* archive = NULL;
	int r;

	// Run dist()
	r = pakfire_dist(pakfire, path, &archive, NULL);
	if (r < 0)
		goto ERROR;

	// Add the archive to the repository
	r = pakfire_repo_import_archive(repo, archive, pkg);
	if (r < 0)
		goto ERROR;

ERROR:
	if (archive)
		pakfire_archive_unref(archive);

	return r;
}

/*
	Convenience function to add a package to the @commandline repository
*/
int pakfire_commandline_add(struct pakfire* pakfire, const char* path,
		struct pakfire_package** package) {
	struct pakfire_repo* repo = NULL;
	int r;

	// Find the commandline repository
	repo = pakfire_get_repo(pakfire, PAKFIRE_REPO_COMMANDLINE);
	if (!repo) {
		ERROR(pakfire->ctx, "Could not find the commandline repository: %m\n");
		return 1;
	}

	// Add the package
	r = pakfire_repo_add(repo, path, package);
	switch (-r) {
		case ENOMSG:
			r = pakfire_commandline_dist(pakfire, repo, path, package);
			break;

		default:
			goto ERROR;
	}

ERROR:
	if (repo)
		pakfire_repo_unref(repo);

	return r;
}

static int __pakfire_search(struct pakfire* pakfire, struct pakfire_packagelist* list,
		const Id* keys, const char* what, int flags) {
	Dataiterator di;
	Queue matches;
	int r;

	// Get the pool ready
	pakfire_pool_internalize(pakfire);

	// Initialize the result queue
	queue_init(&matches);

	// Setup the data interator
	dataiterator_init(&di, pakfire->pool, 0, 0, 0, what, flags);

	// Search through these keys and add matches to the queue
	for (const Id* key = keys; *key; key++) {
		dataiterator_set_keyname(&di, *key);
		dataiterator_set_search(&di, 0, 0);

		while (dataiterator_step(&di))
			queue_pushunique(&matches, di.solvid);
	}

	// Import matches into the package list
	r = pakfire_packagelist_import_solvables(list, pakfire, &matches);
	if (r)
		goto ERROR;

ERROR:
	dataiterator_free(&di);
	queue_free(&matches);

	return r;
}

static int pakfire_search_filelist(struct pakfire* pakfire, const char* what, int flags,
		struct pakfire_packagelist* list) {
	const Id keys[] = {
		SOLVABLE_FILELIST,
		ID_NULL,
	};

	return __pakfire_search(pakfire, list, keys, what, SEARCH_FILES|SEARCH_GLOB);
}

static int pakfire_search_dep(struct pakfire* pakfire, Id type, const char* what, int flags,
		struct pakfire_packagelist* list) {
	int r;

	// Get the pool ready
	pakfire_pool_internalize(pakfire);

	// Translate dependency to ID
	Id dep = pakfire_str2dep(pakfire, what);
	if (!dep) {
		errno = EINVAL;
		return 1;
	}

	Queue matches;
	queue_init(&matches);

	// Search for anything that matches
	pool_whatmatchesdep(pakfire->pool, type, dep, &matches, 0);

	// Add the result to the packagelist
	r = pakfire_packagelist_import_solvables(list, pakfire, &matches);
	if (r)
		goto ERROR;

ERROR:
	queue_free(&matches);

	return r;
}

int pakfire_whatprovides(struct pakfire* pakfire, const char* what, int flags,
		struct pakfire_packagelist* list) {
	int r;

	// Check for valid input
	if (!what || !list) {
		errno = EINVAL;
		return 1;
	}

	// Search for all packages that match this dependency
	r = pakfire_search_dep(pakfire, SOLVABLE_PROVIDES, what, flags, list);
	if (r)
		return r;

	// Search the filelist
	if (*what == '/') {
		r = pakfire_search_filelist(pakfire, what, flags, list);
		if (r)
			return r;
	}

	return 0;
}

static int __pakfire_whatrequires(struct pakfire_ctx* ctx, struct pakfire_package* pkg, void* data) {
	struct pakfire_packagelist* reverse = NULL;
	struct pakfire_packagelist* list = data;
	int r;

	// Fetch all elements
	r = pakfire_package_get_reverse_requires(pkg, &reverse);
	if (r < 0)
		goto ERROR;

	// Merge them into the result list
	r = pakfire_packagelist_add_list(list, reverse);
	if (r < 0)
		goto ERROR;

ERROR:
	if (reverse)
		pakfire_packagelist_unref(reverse);

	return r;
}

int pakfire_whatrequires(struct pakfire* pakfire, const char* what, int flags,
		struct pakfire_packagelist* list) {
	struct pakfire_packagelist* packages = NULL;
	int r;

	const Id keys[] = {
		SOLVABLE_NAME,
		ID_NULL,
	};

	// Create a new package list
	r = pakfire_packagelist_create(&packages, pakfire->ctx);
	if (r)
		goto ERROR;

	// Find any packages that match the name
	r = __pakfire_search(pakfire, packages, keys, what, SEARCH_STRING);
	if (r)
		goto ERROR;

	// Find everything for all packages
	r = pakfire_packagelist_walk(packages, __pakfire_whatrequires, list, 0);
	if (r)
		goto ERROR;

	// Append any simple dependencies
	r = pakfire_search_dep(pakfire, SOLVABLE_REQUIRES, what, flags, list);
	if (r)
		goto ERROR;

ERROR:
	if (packages)
		pakfire_packagelist_unref(packages);

	return r;
}

int pakfire_search(struct pakfire* pakfire, const char* what, int flags,
		struct pakfire_packagelist* list) {
	const Id keys[] = {
		SOLVABLE_NAME,
		SOLVABLE_SUMMARY,
		SOLVABLE_DESCRIPTION,
		ID_NULL
	};

	const Id keys_name_only[] = {
		SOLVABLE_NAME,
		ID_NULL
	};

	return __pakfire_search(pakfire,
		list,
		(flags & PAKFIRE_SEARCH_NAME_ONLY) ? keys_name_only : keys,
		what,
		SEARCH_SUBSTRING|SEARCH_NOCASE);
}

static const char* pakfire_user_lookup(void* data, la_int64_t uid) {
	struct pakfire* pakfire = (struct pakfire*)data;

	// Unmap the UID first
	uid = pakfire_unmap_id(pakfire, &pakfire->user.subuids, uid);

	// Fast path for "root"
	if (uid == 0)
		return "root";

	DEBUG(pakfire->ctx, "Looking up name for UID %ld\n", uid);

	// Find a matching entry in /etc/passwd
	struct passwd* entry = pakfire_getpwuid(pakfire, uid);
	if (!entry) {
		ERROR(pakfire->ctx, "Could not retrieve uname for %ld: %m\n", uid);
		return 0;
	}

	DEBUG(pakfire->ctx, "Mapping UID %ld to %s\n", uid, entry->pw_name);

	return entry->pw_name;
}

static const char* pakfire_group_lookup(void* data, la_int64_t gid) {
	struct pakfire* pakfire = (struct pakfire*)data;

	// Unmap the GID first
	gid = pakfire_unmap_id(pakfire, &pakfire->group.subgids, gid);

	// Fast path for "root"
	if (gid == 0)
		return "root";

	DEBUG(pakfire->ctx, "Looking up name for GID %ld\n", gid);

	// Find a matching entry in /etc/group
	struct group* entry = pakfire_getgrgid(pakfire, gid);
	if (!entry) {
		ERROR(pakfire->ctx, "Could not retrieve gname for %ld: %m\n", gid);
		return 0;
	}

	DEBUG(pakfire->ctx, "Mapping GID %ld to %s\n", gid, entry->gr_name);

	return entry->gr_name;
}

struct archive* pakfire_get_disk_reader(struct pakfire* pakfire) {
	struct archive* reader = NULL;
	int r;

	// Create a new reader
	reader = archive_read_disk_new();
	if (!reader) {
		ERROR(pakfire->ctx, "Could not set up reader: %m\n");
		return NULL;
	}

	// Do not read fflags
	r = archive_read_disk_set_behavior(reader, ARCHIVE_READDISK_NO_FFLAGS);
	if (r) {
		ERROR(pakfire->ctx, "Could not change behavior of reader: %s\n",
			archive_error_string(reader));
		goto ERROR;
	}

	// Install user/group lookups
	archive_read_disk_set_uname_lookup(reader, pakfire, pakfire_user_lookup, NULL);
	archive_read_disk_set_gname_lookup(reader, pakfire, pakfire_group_lookup, NULL);

	return reader;

ERROR:
	if (reader)
		archive_read_free(reader);

	return NULL;
}

static la_int64_t pakfire_uid_lookup(void* data, const char* name, la_int64_t uid) {
	struct pakfire* pakfire = (struct pakfire*)data;

	// Cannot handle empty names
	if (!*name)
		goto ERROR;

	// Fast path for "root"
	if (strcmp(name, "root") == 0)
		goto ERROR;

	DEBUG(pakfire->ctx, "Looking up UID for '%s' (%ld)\n", name, uid);

	// Find a matching entry in /etc/passwd
	struct passwd* entry = pakfire_getpwnam(pakfire, name);
	if (!entry) {
		ERROR(pakfire->ctx, "Could not retrieve UID for '%s': %m\n", name);
		goto ERROR;
	}

	DEBUG(pakfire->ctx, "Mapping %s to UID %u\n", name, entry->pw_uid);

	return pakfire_map_id(pakfire, &pakfire->user.subuids, entry->pw_uid);

ERROR:
	// Return root on error
	return pakfire_map_id(pakfire, &pakfire->user.subuids, 0);
}

static la_int64_t pakfire_gid_lookup(void* data, const char* name, la_int64_t gid) {
	struct pakfire* pakfire = (struct pakfire*)data;

	// Cannot handle empty names
	if (!*name)
		goto ERROR;

	// Fast path for "root"
	if (strcmp(name, "root") == 0)
		goto ERROR;

	DEBUG(pakfire->ctx, "Looking up GID for '%s' (%ld)\n", name, gid);

	// Find a matching entry in /etc/group
	struct group* entry = pakfire_getgrnam(pakfire, name);
	if (!entry) {
		ERROR(pakfire->ctx, "Could not retrieve GID for '%s': %m\n", name);
		goto ERROR;
	}

	DEBUG(pakfire->ctx, "Mapping %s to GID %u\n", name, entry->gr_gid);

	return pakfire_map_id(pakfire, &pakfire->group.subgids, entry->gr_gid);

ERROR:
	// Return root on error
	return pakfire_map_id(pakfire, &pakfire->group.subgids, 0);
}

struct archive* pakfire_get_disk_writer(struct pakfire* pakfire) {
	// Set flags for extracting files
	const int flags =
		ARCHIVE_EXTRACT_ACL |
		ARCHIVE_EXTRACT_OWNER |
		ARCHIVE_EXTRACT_PERM |
		ARCHIVE_EXTRACT_TIME |
		ARCHIVE_EXTRACT_UNLINK |
		ARCHIVE_EXTRACT_XATTR;

	if (!pakfire->writer) {
		// Create a new writer
		pakfire->writer = archive_write_disk_new();
		if (!pakfire->writer) {
			ERROR(pakfire->ctx, "Could not set up writer: %m\n");
			return NULL;
		}

		archive_write_disk_set_options(pakfire->writer, flags);

		// Install our own routine for user/group lookups
		archive_write_disk_set_user_lookup(pakfire->writer,
			pakfire, pakfire_uid_lookup, NULL);
		archive_write_disk_set_group_lookup(pakfire->writer,
			pakfire, pakfire_gid_lookup, NULL);
	}

	return pakfire->writer;
}

// Convenience functions to install/erase/update packages

static int pakfire_verify(struct pakfire* pakfire, int *changed) {
#if 0
	return pakfire_perform_transaction_simple(pakfire, 0, PAKFIRE_JOB_VERIFY,
		0, changed, NULL, NULL);
#endif
	#warning TODO
	return 0;
}

static int pakfire_check_files(struct pakfire* pakfire,
		struct pakfire_db* db, struct pakfire_filelist* errors) {
	struct pakfire_filelist* filelist = NULL;
	int r;

	// Fetch the filelist
	r = pakfire_db_filelist(db, &filelist);
	if (r)
		goto ERROR;

	// Verify the filelist
	r = pakfire_filelist_verify(filelist, errors);

ERROR:
	if (filelist)
		pakfire_filelist_unref(filelist);

	return r;
}

int pakfire_check(struct pakfire* pakfire, struct pakfire_filelist* errors) {
	struct pakfire_db* db = NULL;
	int r;

	// Open database in read-only mode and try to load all installed packages
	r = pakfire_db_open(&db, pakfire, PAKFIRE_DB_READWRITE);
	if (r)
		goto ERROR;

	// Perform a database integrity check
	r = pakfire_db_check(db);
	if (r)
		goto ERROR;

	// Check if all dependencies are intact
	r = pakfire_verify(pakfire, NULL);
	if (r)
		goto ERROR;

	// Check files
	r = pakfire_check_files(pakfire, db, errors);
	if (r)
		goto ERROR;

ERROR:
	if (db)
		pakfire_db_unref(db);

	return r;
}

int pakfire_update_snapshot(struct pakfire* pakfire) {
	struct pakfire_snapshot* snapshot = NULL;
	int r;

	// Make a new snapshot
	r = pakfire_snapshot_make(&snapshot, pakfire);
	if (r < 0)
		goto ERROR;

ERROR:
	if (snapshot)
		pakfire_snapshot_unref(snapshot);

	return r;
}

/*
 * Convenience function to install a couple of packages. Simple.
 */
int pakfire_install(struct pakfire* self, const char** packages) {
	struct pakfire_transaction* transaction = NULL;
	char* problems = NULL;
	int r;

	// Create a new transaction
	r = pakfire_transaction_create(&transaction, self, 0);
	if (r < 0)
		goto ERROR;

	// Install all packages
	for (const char** package = packages; *package; package++) {
		r = pakfire_transaction_request(transaction, PAKFIRE_JOB_INSTALL, *package, 0);
		if (r < 0)
			goto ERROR;
	}

	// Solve the transaction
	r = pakfire_transaction_solve(transaction, 0, &problems);
	if (r) {
		if (problems)
			ERROR(self->ctx, "Could not install packages:\n%s\n", problems);

		goto ERROR;
	}

	// Run the transaction
	r = pakfire_transaction_run(transaction);
	if (r < 0)
		goto ERROR;

ERROR:
	if (transaction)
		pakfire_transaction_unref(transaction);
	if (problems)
		free(problems);

	return r;
}
