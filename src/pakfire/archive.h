/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2014 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_ARCHIVE_H
#define PAKFIRE_ARCHIVE_H

#include <stddef.h>

#include <archive_entry.h>

struct pakfire_archive;

#include <pakfire/filelist.h>
#include <pakfire/hashes.h>
#include <pakfire/linter.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/repo.h>
#include <pakfire/scriptlet.h>

int pakfire_archive_open(struct pakfire_archive** archive, struct pakfire* pakfire, const char* path);
struct pakfire_archive* pakfire_archive_ref(struct pakfire_archive* archive);
struct pakfire_archive* pakfire_archive_unref(struct pakfire_archive* archive);

enum pakfire_archive_read_flags {
	PAKFIRE_ARCHIVE_READ_FOLLOW_SYMLINKS = (1 << 0),
};

FILE* pakfire_archive_read(struct pakfire_archive* archive, const char* filename, int flags);
int pakfire_archive_extract(struct pakfire_archive* archive,
	const char* path, const int flags);

const char* pakfire_archive_get_path(struct pakfire_archive* archive);

unsigned int pakfire_archive_get_format(struct pakfire_archive* archive);

int pakfire_archive_get_filelist(struct pakfire_archive* self, struct pakfire_filelist** filelist);

int pakfire_archive_verify(struct pakfire_archive* archive, int* status);

ssize_t pakfire_archive_get_size(struct pakfire_archive* archive);

int pakfire_archive_verify_checksum(struct pakfire_archive* archive,
	const enum pakfire_hash_type type, const unsigned char* checksum, const size_t length);

int pakfire_archive_make_package(struct pakfire_archive* archive,
	struct pakfire_repo* repo, struct pakfire_package** package);

int pakfire_archive_lint(struct pakfire_archive* archive,
	pakfire_linter_result_callback callback, void* data);

// Walk

typedef int (*pakfire_archive_walk_callback)(struct pakfire_archive* archive,
	struct archive* a, struct archive_entry* e, void* p);
typedef int (*pakfire_archive_walk_filter_callback)(struct pakfire_archive* archive,
	struct archive_entry* e, void* p);

enum pakfire_archive_walk_codes {
	PAKFIRE_WALK_OK    = 0,
	PAKFIRE_WALK_ERROR = 1,

	// After this code has been sent, we will not process any further entries
	PAKFIRE_WALK_END   = -10,

	// Request the next entry (only in filter callback)
	PAKFIRE_WALK_SKIP  = -20,

	// Like PAKFIRE_WALK_OK, but the callback will not be called again
	PAKFIRE_WALK_DONE  = -30,

	// Start again from the beginning
	PAKFIRE_WALK_AGAIN = -40,
};

int pakfire_archive_walk_payload(struct pakfire_archive* archive,
	pakfire_archive_walk_callback callback, void* data);

int pakfire_archive_unlink(struct pakfire_archive* archive);
int pakfire_archive_copy(struct pakfire_archive* archive, const char* path);
int pakfire_archive_link_or_copy(struct pakfire_archive* archive, const char* path);

struct pakfire_scriptlet* pakfire_archive_get_scriptlet(
	struct pakfire_archive* archive, const char* type);

int pakfire_archive_apply_systemd_sysusers(struct pakfire_archive* archive);

#endif /* PAKFIRE_ARCHIVE_H */
