/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_MIRRORLIST_H
#define PAKFIRE_MIRRORLIST_H

struct pakfire_mirrorlist;

#include <pakfire/ctx.h>
#include <pakfire/mirror.h>

int pakfire_mirrorlist_create(struct pakfire_mirrorlist** list, struct pakfire_ctx* ctx);

struct pakfire_mirrorlist* pakfire_mirrorlist_ref(struct pakfire_mirrorlist* list);
struct pakfire_mirrorlist* pakfire_mirrorlist_unref(struct pakfire_mirrorlist* list);

int pakfire_mirrorlist_read(struct pakfire_mirrorlist* list, const char* path);

int pakfire_mirrorlist_add_mirror(
	struct pakfire_mirrorlist* list, struct pakfire_mirror* mirror);

int pakfire_mirrorlist_empty(struct pakfire_mirrorlist* ml);

struct pakfire_mirror* pakfire_mirrorlist_get_first(struct pakfire_mirrorlist* list);
struct pakfire_mirror* pakfire_mirrorlist_get_next(
	struct pakfire_mirrorlist* list, struct pakfire_mirror* mirror);

#endif /* PAKFIRE_MIRRORLIST_H */
