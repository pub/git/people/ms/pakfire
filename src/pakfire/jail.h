/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_JAIL_H
#define PAKFIRE_JAIL_H

#include <pakfire/cgroup.h>
#include <pakfire/env.h>
#include <pakfire/pakfire.h>
#include <pakfire/pty.h>

struct pakfire_jail;

int pakfire_jail_create(struct pakfire_jail** jail, struct pakfire* pakfire);

struct pakfire_jail* pakfire_jail_ref(struct pakfire_jail* jail);
struct pakfire_jail* pakfire_jail_unref(struct pakfire_jail* jail);

// Mountpoints
int pakfire_jail_bind(struct pakfire_jail* jail,
	const char* source, const char* target, int flags);

// Resource Limits
int pakfire_jail_nice(struct pakfire_jail* jail, int nice);

// Timeout
int pakfire_jail_set_timeout(struct pakfire_jail* jail, unsigned int timeout);

typedef int (*pakfire_jail_callback)(struct pakfire_ctx* ctx, void* data);

// Standard Input
typedef int (*pakfire_jail_stdin_callback)
	(struct pakfire_ctx* ctx, struct pakfire_jail* jail, void* data, int fd);
void pakfire_jail_set_stdin_callback(struct pakfire_jail* jail,
	pakfire_jail_stdin_callback callback, void* data);

enum pakfire_jail_exec_flags {
	PAKFIRE_JAIL_INTERACTIVE      = (1 << 0),
	PAKFIRE_JAIL_HAS_NETWORKING   = (1 << 1),
	PAKFIRE_JAIL_NOENT_OK         = (1 << 2),
	PAKFIRE_JAIL_HAS_LOOP_DEVICES = (1 << 3),
};

int pakfire_jail_exec(struct pakfire_jail* jail,
	pakfire_jail_callback callback, void* data, int flags);

int pakfire_jail_exec_command(struct pakfire_jail* jail,
	const char* argv[], struct pakfire_env* env, int flags);

int pakfire_jail_exec_capture_output(struct pakfire_jail* jail,
	const char* argv[], struct pakfire_env* env, int flags, char** output, size_t* length);

// Resource limits
int pakfire_jail_set_cgroup(struct pakfire_jail* jail, struct pakfire_cgroup* cgroup);

int pakfire_jail_communicate(
	struct pakfire_jail* jail, const char* argv[], struct pakfire_env* env, int flags,
	pakfire_pty_stdin_callback stdin_callback, void* stdin_data,
	pakfire_pty_stdout_callback stdout_callback, void* stdout_data);

// Convenience functions
int pakfire_jail_run(struct pakfire* pakfire,
	const char* argv[], struct pakfire_env* env, int flags, char** output, size_t* output_length);
int pakfire_jail_run_script(struct pakfire* pakfire,
	const char* script, const size_t length, const char* argv[], struct pakfire_env* env, int flags);

int pakfire_jail_exec_script(struct pakfire_jail* jail,
	const char* script, const size_t size, const char* args[], struct pakfire_env* env,
	pakfire_pty_stdin_callback stdin_callback, void* stdin_data,
	pakfire_pty_stdout_callback stdout_callback, void* stdout_data);

int pakfire_jail_shell(struct pakfire_jail* jail, struct pakfire_env* env);
int pakfire_jail_ldconfig(struct pakfire* pakfire);
int pakfire_jail_run_systemd_tmpfiles(struct pakfire* pakfire);

#endif /* PAKFIRE_JAIL_H */
