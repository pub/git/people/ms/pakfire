/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PATH_H
#define PAKFIRE_PATH_H

#include <stddef.h>
#include <time.h>

#define pakfire_path_normalize(path) \
	__pakfire_path_normalize(path, sizeof(path))
int __pakfire_path_normalize(char* p, const size_t length);

#define pakfire_path_format(path, format, ...) \
	__pakfire_path_format(path, sizeof(path), format, __VA_ARGS__)
int __pakfire_path_format(char* buffer, const size_t length, const char* format, ...)
	__attribute__((format(printf, 3, 4)));;

#define pakfire_path_append(path, s1, s2) \
	__pakfire_path_append(path, sizeof(path), s1, s2)
int __pakfire_path_append(char* buffer, const size_t length, const char* s1, const char* s2);

#define pakfire_path_merge(path, s1, s2) \
	__pakfire_path_merge(path, sizeof(path), s1, s2)
int __pakfire_path_merge(char* buffer, const size_t length, const char* s1, const char* s2);

#define pakfire_path_basename(path, s) \
	__pakfire_path_basename(path, sizeof(path), s)
int __pakfire_path_basename(char* buffer, const size_t length, const char* s);

#define pakfire_path_dirname(path, s) \
	__pakfire_path_dirname(path, sizeof(path), s)
int __pakfire_path_dirname(char* buffer, const size_t length, const char* s);

#define pakfire_path_relative(path, root, s) \
	__pakfire_path_relative(path, sizeof(path), root, s)
int __pakfire_path_relative(char* buffer, const size_t length, const char* root, const char* s);

int pakfire_path_is_absolute(const char* s);

#define pakfire_path_absolute(path, s) \
	__pakfire_path_absolute(path, sizeof(path), s)
int __pakfire_path_absolute(char* buffer, const size_t length, const char* s);

int pakfire_path_match(const char* p, const char* s);
int pakfire_path_is_pattern(const char* path);

#define pakfire_path_replace_extension(path, extension) \
	__pakfire_path_replace_extension(path, sizeof(path), extension)
int __pakfire_path_replace_extension(char* path, const size_t length, const char* extension);

#define pakfire_path_realpath(dest, path) \
	__pakfire_path_realpath(dest, sizeof(dest), path)
int __pakfire_path_realpath(char* dest, const size_t length, const char* path);

#define pakfire_path_expand(dest, path) \
	__pakfire_path_expand(dest, sizeof(dest), path)
int __pakfire_path_expand(char* dest, const size_t length, const char* path);

int pakfire_path_exists(const char* path);

time_t pakfire_path_age(const char* path);

#endif /* PAKFIRE_PATH_H */
