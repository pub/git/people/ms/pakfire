/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_LINTER_H
#define PAKFIRE_LINTER_H

#include <pakfire/archive.h>
#include <pakfire/ctx.h>
#include <pakfire/file.h>
#include <pakfire/package.h>

enum pakfire_linter_priority {
	PAKFIRE_LINTER_INFO,
	PAKFIRE_LINTER_WARNING,
	PAKFIRE_LINTER_ERROR,
};

typedef int (*pakfire_linter_result_callback)(struct pakfire_ctx* ctx,
	struct pakfire_archive* archive, struct pakfire_package* package,
	struct pakfire_file* file, int priority, const char* message, void* data);

struct pakfire_linter;

int pakfire_linter_create(struct pakfire_linter** linter,
	struct pakfire* pakfire, struct pakfire_archive* archive);

struct pakfire_linter* pakfire_linter_ref(struct pakfire_linter* linter);
struct pakfire_linter* pakfire_linter_unref(struct pakfire_linter* linter);

void pakfire_linter_set_result_callback(struct pakfire_linter* linter,
	pakfire_linter_result_callback callback, void* data);

int pakfire_linter_lint(struct pakfire_linter* linter);

int pakfire_linter_result(struct pakfire_linter* linter, struct pakfire_file* file,
	int priority, const char* format, ...) __attribute__((format(printf, 4, 5)));

#define pakfire_linter_info(linter, format, ...) \
	pakfire_linter_result(linter, NULL, PAKFIRE_LINTER_INFO, format, ## __VA_ARGS__)
#define pakfire_linter_warning(linter, format, ...) \
	pakfire_linter_result(linter, NULL, PAKFIRE_LINTER_WARNING, format, ## __VA_ARGS__)
#define pakfire_linter_error(linter, format, ...) \
	pakfire_linter_result(linter, NULL, PAKFIRE_LINTER_ERROR, format, ## __VA_ARGS__)

#endif /* PAKFIRE_LINTER_H */
