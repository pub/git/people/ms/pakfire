/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>

#include <openssl/crypto.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/sha.h>

#include <pakfire/ctx.h>
#include <pakfire/hasher.h>
#include <pakfire/hashes.h>

struct pakfire_hasher {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Selected hashes
	enum pakfire_hash_type types;

	// EVP Contexts
	EVP_MD_CTX* sha3_512;
	EVP_MD_CTX* sha3_256;
	EVP_MD_CTX* blake2b512;
	EVP_MD_CTX* blake2s256;
	EVP_MD_CTX* sha2_512;
	EVP_MD_CTX* sha2_256;

	// Computed Hashes
	struct pakfire_hashes hashes;
};

static void pakfire_hasher_free(struct pakfire_hasher* self) {
	if (self->sha3_512)
		EVP_MD_CTX_free(self->sha3_512);
	if (self->sha3_256)
		EVP_MD_CTX_free(self->sha3_256);
	if (self->blake2b512)
		EVP_MD_CTX_free(self->blake2b512);
	if (self->blake2s256)
		EVP_MD_CTX_free(self->blake2s256);
	if (self->sha2_512)
		EVP_MD_CTX_free(self->sha2_512);
	if (self->sha2_256)
		EVP_MD_CTX_free(self->sha2_256);
	if (self->ctx)
		pakfire_ctx_unref(self->ctx);
	free(self);
}

static EVP_MD_CTX* pakfire_hasher_setup_hash(struct pakfire_hasher* self, const EVP_MD* md) {
	EVP_MD_CTX* evp_ctx = NULL;
	int r;

	// Setup a new context
	evp_ctx = EVP_MD_CTX_new();
	if (!evp_ctx) {
		ERROR(self->ctx, "Could not initialize OpenSSL context: %s\n",
			ERR_error_string(ERR_get_error(), NULL));
		goto ERROR;
	}

	// Setup digest
	r = EVP_DigestInit_ex(evp_ctx, md, NULL);
	if (r != 1) {
		ERROR(self->ctx, "Could not setup digest: %s\n",
			ERR_error_string(ERR_get_error(), NULL));
		goto ERROR;
	}

	return evp_ctx;

ERROR:
	if (evp_ctx)
		EVP_MD_CTX_free(evp_ctx);

	return NULL;
}

static int pakfire_hasher_setup_hashes(struct pakfire_hasher* self) {
	// Initialize context for SHA-3-512
	if (self->types & PAKFIRE_HASH_SHA3_512) {
		self->sha3_512 = pakfire_hasher_setup_hash(self, EVP_sha3_512());
		if (!self->sha3_512)
			return -ENOTSUP;
	}

	// Initialize context for SHA-3-256
	if (self->types & PAKFIRE_HASH_SHA3_256) {
		self->sha3_256 = pakfire_hasher_setup_hash(self, EVP_sha3_256());
		if (!self->sha3_256)
			return -ENOTSUP;
	}

	// Initialize context for BLAKE2B512
	if (self->types & PAKFIRE_HASH_BLAKE2B512) {
		self->blake2b512 = pakfire_hasher_setup_hash(self, EVP_blake2b512());
		if (!self->blake2b512)
			return -ENOTSUP;
	}

	// Initialize context for BLAKE2S256
	if (self->types & PAKFIRE_HASH_BLAKE2S256) {
		self->blake2s256 = pakfire_hasher_setup_hash(self, EVP_blake2s256());
		if (!self->blake2s256)
			return -ENOTSUP;
	}

	// Initialize context for SHA-2-512
	if (self->types & PAKFIRE_HASH_SHA2_512) {
		self->sha2_512 = pakfire_hasher_setup_hash(self, EVP_sha512());
		if (!self->sha2_512)
			return -ENOTSUP;
	}

	// Initialize context for SHA-2-256
	if (self->types & PAKFIRE_HASH_SHA2_256) {
		self->sha2_256 = pakfire_hasher_setup_hash(self, EVP_sha256());
		if (!self->sha2_256)
			return -ENOTSUP;
	}

	return 0;
}

int pakfire_hasher_create(struct pakfire_hasher** hasher,
		struct pakfire_ctx* ctx, enum pakfire_hash_type types) {
	struct pakfire_hasher* self = NULL;
	int r;

	// Something must have been selected
	if (!types)
		return -EINVAL;

	// Allocate some memory
	self = calloc(1, sizeof(*self));
	if (!self)
		return -errno;

	// Store a reference to the context
	self->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	self->nrefs = 1;

	// Store hash types
	self->types = self->hashes.types = types;

	// Setup all hashes
	r = pakfire_hasher_setup_hashes(self);
	if (r < 0)
		goto ERROR;

	// Return the pointer
	*hasher = self;

	return 0;

ERROR:
	pakfire_hasher_free(self);

	return r;
};

struct pakfire_hasher* pakfire_hasher_ref(struct pakfire_hasher* self) {
	self->nrefs++;

	return self;
}

struct pakfire_hasher* pakfire_hasher_unref(struct pakfire_hasher* self) {
	if (--self->nrefs > 0)
		return self;

	pakfire_hasher_free(self);
	return NULL;
}

static int __pakfire_hasher_update(struct pakfire_hasher* self, EVP_MD_CTX* evp_ctx,
		const char* buffer, const size_t length) {
	int r;

	// Nothing to do if digest not initialized
	if (!evp_ctx)
		return 0;

	// Update digest
	r = EVP_DigestUpdate(evp_ctx, buffer, length);
	if (r != 1) {
		ERROR(self->ctx, "EVP_Digest_Update() failed: %s\n",
				ERR_error_string(ERR_get_error(), NULL));

		return -ENOTSUP;
	}

	return 0;
}

int pakfire_hasher_update(struct pakfire_hasher* self, const char* buffer, const size_t length) {
	int r;

	// SHA-3-512
	r = __pakfire_hasher_update(self, self->sha3_512, buffer, length);
	if (r < 0)
		return r;

	// SHA-3-256
	r = __pakfire_hasher_update(self, self->sha3_256, buffer, length);
	if (r < 0)
		return r;

	// BLAKE2B512
	r = __pakfire_hasher_update(self, self->blake2b512, buffer, length);
	if (r < 0)
		return r;

	// BLAKE2S256
	r = __pakfire_hasher_update(self, self->blake2s256, buffer, length);
	if (r < 0)
		return r;

	// SHA-2-512
	r = __pakfire_hasher_update(self, self->sha2_512, buffer, length);
	if (r < 0)
		return r;

	// SHA-2-256
	r = __pakfire_hasher_update(self, self->sha2_256, buffer, length);
	if (r < 0)
		return r;

	return 0;
}

static int __pakfire_hasher_finalize(struct pakfire_hasher* self,
		EVP_MD_CTX* evp_ctx, unsigned char* digest) {
	int r;

	// Nothing to do if digest not initialized
	if (!evp_ctx)
		return 0;

	// Finalize digest
	r = EVP_DigestFinal_ex(evp_ctx, digest, NULL);
	if (r != 1) {
		ERROR(self->ctx, "EVP_DigestFinal_ex() failed: %s\n",
				ERR_error_string(ERR_get_error(), NULL));

		return -ENOTSUP;
	}

	return 0;
}

int pakfire_hasher_finalize(struct pakfire_hasher* self, struct pakfire_hashes* computed_hashes) {
	int r;

	// Finalize SHA-3-512
	r = __pakfire_hasher_finalize(self, self->sha3_512, self->hashes.sha3_512);
	if (r < 0)
		return r;

	// Finalize SHA-3-256
	r = __pakfire_hasher_finalize(self, self->sha3_256, self->hashes.sha3_256);
	if (r < 0)
		return r;

	// Finalize BLAKE2b512
	r = __pakfire_hasher_finalize(self, self->blake2b512, self->hashes.blake2b512);
	if (r < 0)
		return r;

	// Finalize BLAKE2s256
	r = __pakfire_hasher_finalize(self, self->blake2s256, self->hashes.blake2s256);
	if (r < 0)
		return r;

	// Finalize SHA-2-512
	r = __pakfire_hasher_finalize(self, self->sha2_512, self->hashes.sha2_512);
	if (r < 0)
		return r;

	// Finalize SHA-2-256
	r = __pakfire_hasher_finalize(self, self->sha2_256, self->hashes.sha2_256);
	if (r < 0)
		return r;

	// Optionally return the computed hashes
	if (computed_hashes) {
		r = pakfire_hashes_import(computed_hashes, &self->hashes);
		if (r < 0)
			return r;
	}

	return 0;
}

/*
 * Convenience function to hash a buffer
 */
int pakfire_hash_buffer(struct pakfire_ctx* ctx, const char* buffer, const size_t length,
		const enum pakfire_hash_type types, struct pakfire_hashes* hashes) {
	struct pakfire_hasher* hasher = NULL;
	int r;

	// Create a new hasher
	r = pakfire_hasher_create(&hasher, ctx, types);
	if (r < 0)
		goto ERROR;

	// To make the static analyzer happy
	assert(hasher);

	// Feed the buffer into the hash functions
	r = pakfire_hasher_update(hasher, buffer, length);
	if (r < 0)
		goto ERROR;

	// Finalize the hash functions
	r = pakfire_hasher_finalize(hasher, hashes);
	if (r < 0)
		goto ERROR;

ERROR:
	if (hasher)
		pakfire_hasher_unref(hasher);

	return r;
}

/*
	Convenience function to hash a file
*/
int pakfire_hash_file(struct pakfire_ctx* ctx,
		FILE* f, const enum pakfire_hash_type types, struct pakfire_hashes* hashes) {
	struct pakfire_hasher* hasher = NULL;
	char buffer[65536];
	size_t bytes_read;
	int r;

	// Create a new hasher
	r = pakfire_hasher_create(&hasher, ctx, types);
	if (r < 0)
		goto ERROR;

	// To make the static analyzer happy
	assert(hasher);

	// Read the file into the hash functions
	while (!feof(f)) {
		bytes_read = fread(buffer, 1, sizeof(buffer), f);

		// Raise any reading errors
		if (ferror(f)) {
			r = -errno;
			goto ERROR;
		}

		// Feed the buffer into the hash functions
		r = pakfire_hasher_update(hasher, buffer, bytes_read);
		if (r < 0)
			goto ERROR;
	}

	// Finalize the hash functions
	r = pakfire_hasher_finalize(hasher, hashes);
	if (r < 0)
		goto ERROR;

ERROR:
	if (hasher)
		pakfire_hasher_unref(hasher);

	return r;
}

int pakfire_hash_path(struct pakfire_ctx* ctx,
		const char* path, const enum pakfire_hash_type types, struct pakfire_hashes* hashes) {
	FILE* f = NULL;
	int r;

	// Open the file
	f = fopen(path, "r");
	if (!f)
		return -errno;

	// Perform the hashing
	r = pakfire_hash_file(ctx, f, types, hashes);

	// Close the file handle
	if (f)
		fclose(f);

	return r;
}
