/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_SCRIPTLET_H
#define PAKFIRE_SCRIPTLET_H

#include <pakfire/ctx.h>
#include <pakfire/pakfire.h>

extern const char* pakfire_scriptlet_types[13];

struct pakfire_scriptlet;

int pakfire_scriptlet_create(struct pakfire_scriptlet** scriptlet,
	struct pakfire_ctx* ctx, const char* type, const char* data, size_t size);
struct pakfire_scriptlet* pakfire_scriptlet_ref(struct pakfire_scriptlet* scriptlet);
struct pakfire_scriptlet* pakfire_scriptlet_unref(struct pakfire_scriptlet* scriptlet);

const char* pakfire_scriptlet_get_type(struct pakfire_scriptlet* scriptlet);
const char* pakfire_scriptlet_get_data(struct pakfire_scriptlet* scriptlet, size_t* size);

int pakfire_scriptlet_execute(struct pakfire_scriptlet* scriptlet, struct pakfire* pakfire);

#endif /* PAKFIRE_SCRIPTLET_H */
