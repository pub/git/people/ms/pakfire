/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fts.h>
#include <dirent.h>
#include <fcntl.h>
#include <linux/limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/file.h>
#include <sys/mount.h>

#include <pakfire/logging.h>
#include <pakfire/pakfire.h>
#include <pakfire/path.h>
#include <pakfire/snapshot.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define PAKFIRE_SNAPSHOT_TIMESTAMP_FORMAT "%Y-%m-%d-%H%M%S"

// The time after which a snapshot will be refreshed
#define PAKFIRE_SNAPSHOT_MAX_AGE 86400 // 24 hrs

struct pakfire_snapshot {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Path
	char path[PATH_MAX];
	int fd;

	// File Systems
	char overlayfs[PATH_MAX];
	char tmpfs[PATH_MAX];

	// Overlayfs Directories
	char upperdir[PATH_MAX];
	char workdir[PATH_MAX];

	// State
	enum pakfire_snapshot_state {
		PAKFIRE_SNAPSHOT_INIT = 0,
		PAKFIRE_SNAPSHOT_MOUNTED,
		PAKFIRE_SNAPSHOT_UMOUNTED,
		PAKFIRE_SNAPSHOT_DESTROYED,
	} state;
};

static void pakfire_snapshot_free(struct pakfire_snapshot* self) {
	// Ensure this is umounted
	pakfire_snapshot_umount(self);

	if (self->fd >= 0)
		close(self->fd);

	if (self->ctx)
		pakfire_ctx_unref(self->ctx);
	free(self);
}

int pakfire_snapshot_create(
		struct pakfire_snapshot** snapshot, struct pakfire_ctx* ctx, const char* path) {
	struct pakfire_snapshot* self = NULL;
	int r;

	// Allocate a new snapshot
	self = calloc(1, sizeof(*self));
	if (!self)
		return -errno;

	// Store a reference to the context
	self->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	self->nrefs = 1;

	// Store the path
	r = pakfire_string_set(self->path, path);
	if (r < 0)
		goto ERROR;

	// Open the directory
	self->fd = open(path, O_DIRECTORY|O_CLOEXEC);
	if (self->fd < 0) {
		r = -errno;
		goto ERROR;
	}

	// Lock the snapshot
	r = flock(self->fd, LOCK_SH);
	if (r < 0) {
		ERROR(self->ctx, "Could not lock %s: %m\n", self->path);
		r = -errno;
		goto ERROR;
	}

	// Return the snapshot
	*snapshot = self;
	return 0;

ERROR:
	pakfire_snapshot_free(self);

	return r;
}

struct pakfire_snapshot* pakfire_snapshot_ref(struct pakfire_snapshot* self) {
	++self->nrefs;

	return self;
}

struct pakfire_snapshot* pakfire_snapshot_unref(struct pakfire_snapshot* self) {
	if (--self->nrefs > 0)
		return self;

	pakfire_snapshot_free(self);
	return NULL;
}

static int pakfire_snapshot_parse_timestamp(const char* name, time_t* t) {
	struct tm brokentime = {};

	// Parse the string
	char* p = strptime(name, PAKFIRE_SNAPSHOT_TIMESTAMP_FORMAT, &brokentime);
	if (!p)
		return -errno;

	// If p is not pointing to the end of the string,
	// which means that strptime could not parse the entire string.
	else if (*p)
		return -EINVAL;

	// Convert time into time_t
	*t = mktime(&brokentime);
	if (*t < 0)
		return -errno;

	return 0;
}

static int pakfire_snapshot_filter(const struct dirent* dirent) {
	time_t t = -1;
	time_t now;
	int r;

	// Only consider directories or symlinks
	switch (dirent->d_type) {
		case DT_DIR:
		case DT_LNK:
			break;

		// Ignore the rest
		default:
			return 0;
	}

	// Skip any hidden directories
	if (*dirent->d_name == '.')
		return 0;

	// Parse the timestamp
	r = pakfire_snapshot_parse_timestamp(dirent->d_name, &t);
	if (r < 0)
		return r;

	// Fetch the current time
	now = time(NULL);
	if (now < 0)
		return -errno;

	// Ignore snapshots that are too old
	if (now - t >= PAKFIRE_SNAPSHOT_MAX_AGE)
		return 0;

	// Select the rest
	return 1;
}

/*
	Finds and returns the latest snapshot
*/
int pakfire_snapshot_find(struct pakfire_snapshot** snapshot, struct pakfire* pakfire) {
	struct dirent** paths = NULL;
	int num_paths = 0;
	char path[PATH_MAX];
	int fd = -EBADF;
	int r;

	struct pakfire_ctx* ctx = pakfire_ctx(pakfire);

	// Make the path
	r = pakfire_cache_path(pakfire, path, "%s", "snapshots");
	if (r < 0)
		return r;

	// Open the path
	fd = open(path, O_DIRECTORY|O_CLOEXEC);
	if (fd < 0) {
		switch (errno) {
			// If the directory does not exist, we will make a new snapshot
			case ENOENT:
				r = pakfire_snapshot_make(snapshot, pakfire);
				if (r < 0)
					goto ERROR;

				// Anyways, we are done
				goto ERROR;

			default:
				r = -errno;
				goto ERROR;
		}
	}

	// Find all snapshots in the directory
	num_paths = scandirat(fd, ".", &paths, pakfire_snapshot_filter, versionsort);
	if (num_paths < 0) {
		r = num_paths;
		goto ERROR;

	// Create a new snapshot if none was found
	} else if (num_paths == 0) {
		r = pakfire_snapshot_make(snapshot, pakfire);
		if (r < 0)
			goto ERROR;

		// Anyways, we are done
		goto ERROR;
	}

	// List what we have found
	for (int i = 0; i < num_paths; i++) {
		DEBUG(ctx, "Found snapshot: %s\n", paths[i]->d_name);
	}

	// Build the path to the snapshot
	r = pakfire_path_append(path, path, paths[num_paths - 1]->d_name);
	if (r < 0)
		goto ERROR;

	DEBUG(ctx, "Selecting snapshot %s\n", path);

	// Open the snapshot
	r = pakfire_snapshot_create(snapshot, ctx, path);
	if (r < 0)
		goto ERROR;

ERROR:
	if (paths) {
		for (int i = 0; i < num_paths; i++)
			free(paths[i]);
		free(paths);
	}
	if (fd >= 0)
		close(fd);
	if (ctx)
		pakfire_ctx_unref(ctx);

	return r;
}

static int pakfire_snapshot_mount_tmpfs(struct pakfire_snapshot* self) {
	char* path = NULL;
	int r;

	// Make path
	r = pakfire_string_set(self->tmpfs, PAKFIRE_TMP_DIR "/pakfire-tmpfs.XXXXXX");
	if (r < 0)
		return r;

	// Create a temporary directory
	path = pakfire_mkdtemp(self->tmpfs);
	if (!path)
		return -errno;

	// Perform mount
	r = mount("pakfire_tmpfs", self->tmpfs, "tmpfs", 0, NULL);
	if (r < 0)
		return -errno;

	// Make the upper directory
	r = pakfire_path_append(self->upperdir, self->tmpfs, "upper");
	if (r < 0)
		return r;

	// Create the upper directory
	r = pakfire_mkdir(self->upperdir, 0755);
	if (r < 0)
		return r;

	// Make the work directory
	r = pakfire_path_append(self->workdir, self->tmpfs, "work");
	if (r < 0)
		return r;

	// Create the work directory
	r = pakfire_mkdir(self->workdir, 0755);
	if (r < 0)
		return r;

	return 0;
}

int pakfire_snapshot_mount(struct pakfire_snapshot* self, const char* path) {
	int mountfd = -EBADF;
	int fsfd = -EBADF;
	int r;

	DEBUG(self->ctx, "Mounting snapshot %s to %s\n", self->path, path);

	// Mount the tmpfs
	r = pakfire_snapshot_mount_tmpfs(self);
	if (r < 0)
		goto ERROR;

	// Create a new overlayfs
	fsfd = fsopen("overlay", FSOPEN_CLOEXEC);
	if (fsfd < 0) {
		r = -errno;
		goto ERROR;
	}

	// Set the lower directory
	r = fsconfig(fsfd, FSCONFIG_SET_STRING, "lowerdir", self->path, 0);
	if (r < 0)
		goto ERROR;

	// Set the upper directory
	r = fsconfig(fsfd, FSCONFIG_SET_STRING, "upperdir", self->upperdir, 0);
	if (r < 0)
		goto ERROR;

	// Set some work directory
	r = fsconfig(fsfd, FSCONFIG_SET_STRING, "workdir", self->workdir, 0);
	if (r < 0)
		goto ERROR;

	// Create a new filesystem
	r = fsconfig(fsfd, FSCONFIG_CMD_CREATE, NULL, NULL, 0);
	if (r < 0)
		goto ERROR;

	// Perform mount
	mountfd = fsmount(fsfd, FSMOUNT_CLOEXEC, 0);
	if (mountfd < 0) {
		r = -errno;
		goto ERROR;
	}

	// Store the path
	r = pakfire_string_set(self->overlayfs, path);
	if (r < 0)
		goto ERROR;

	// Move the snapshot to the right place
	r = move_mount(mountfd, "", AT_FDCWD, path, MOVE_MOUNT_F_EMPTY_PATH);
	if (r < 0)
		goto ERROR;

	// Mark as mounted
	self->state = PAKFIRE_SNAPSHOT_MOUNTED;

ERROR:
	if (mountfd >= 0)
		close(mountfd);
	if (fsfd >= 0)
		close(fsfd);

	return r;
}

int pakfire_snapshot_umount(struct pakfire_snapshot* self) {
	int r;

	switch (self->state) {
		case PAKFIRE_SNAPSHOT_MOUNTED:
			// Umount the overlayfs
			if (*self->overlayfs) {
				r = umount(self->overlayfs);
				if (r < 0)
					return r;
			}

			// Umount the tmpfs
			if (*self->tmpfs) {
				r = umount(self->tmpfs);
				if (r < 0)
					return r;

				r = pakfire_rmtree(self->tmpfs, 0);
				if (r < 0)
					return r;
			}

			// We are now umounted
			self->state = PAKFIRE_SNAPSHOT_UMOUNTED;
			break;

		default:
			break;
	}

	return 0;
}

static int pakfire_snapshot_destroy(struct pakfire_snapshot* self) {
	int r;

	// Check if the snapshot is mounted
	switch (self->state) {
		case PAKFIRE_SNAPSHOT_MOUNTED:
			DEBUG(self->ctx, "Snapshot is mounted\n");
			return -EBUSY;

		default:
			break;
	}

	/*
		Try to get an exclusive lock on the directory.

		If this fails, another process is using this snapshot.
	*/
	r = flock(self->fd, LOCK_EX|LOCK_NB);
	if (r < 0) {
		switch (errno) {
			case EWOULDBLOCK:
				DEBUG(self->ctx, "Snapshot %s is used elsewhere\n", self->path);
				return -EBUSY;

			default:
				DEBUG(self->ctx,
					"Could not acquire an exclusive lock on %s: %m\n", self->path);
				break;
		}
	}

	DEBUG(self->ctx, "Destroying snapshot %s\n", self->path);

	// Remove all files
	r = pakfire_rmtree(self->path, 0);
	if (r < 0) {
		ERROR(self->ctx, "Could not destroy snapshot %s: %s\n",
			self->path, strerror(-r));
		return r;
	}

	// Mark as destroyed
	self->state = PAKFIRE_SNAPSHOT_DESTROYED;

	return 0;
}

static int pakfire_snapshot_install_packages(struct pakfire* pakfire, const char** packages) {
	struct pakfire_transaction* transaction = NULL;
	int r;

	// Create a new transaction
	r = pakfire_transaction_create(&transaction, pakfire, 0);
	if (r)
		goto ERROR;

	// Install all build dependencies
	for (const char** p = packages; *p; p++) {
		r = pakfire_transaction_request(transaction, PAKFIRE_JOB_INSTALL, *p, PAKFIRE_JOB_ESSENTIAL);
		if (r < 0)
			goto ERROR;
	}

	// Solve the transaction
	r = pakfire_transaction_solve(transaction, 0, NULL);
	if (r)
		goto ERROR;

	// Run the transaction
	r = pakfire_transaction_run(transaction);
	if (r < 0)
		goto ERROR;

ERROR:
	if (transaction)
		pakfire_transaction_unref(transaction);

	return r;
}

/*
	Creates a new snapshot
*/
int pakfire_snapshot_make(struct pakfire_snapshot** snapshot, struct pakfire* pakfire) {
	struct pakfire_config* config = NULL;
	struct pakfire* p = NULL;
	char snapshot_path[PATH_MAX];
	char tmp[PATH_MAX];
	const char* arch = NULL;
	const char* path = NULL;
	char time[1024];
	int flags = PAKFIRE_FLAGS_BUILD|PAKFIRE_FLAGS_BUILD_LOCAL;
	int r;

	const char* packages[] = {
		"build-essential",
		NULL,
	};

	struct pakfire_ctx* ctx = pakfire_ctx(pakfire);

	// Fetch the configuration
	config = pakfire_get_config(pakfire);

	// Fetch the architecture
	arch = pakfire_get_arch(pakfire);

	// Store the current time
	r = pakfire_strftime_now(time, PAKFIRE_SNAPSHOT_TIMESTAMP_FORMAT);
	if (r < 0)
		goto ERROR;

	// Make the final snapshot path
	r = pakfire_cache_path(pakfire, snapshot_path, "snapshots/%s", time);
	if (r < 0)
		goto ERROR;

	// Make a new temporary path
	r = pakfire_cache_path(pakfire, tmp, "%s", "snapshots/.XXXXXXX");
	if (r < 0)
		goto ERROR;

	// Create the temporary directory
	path = pakfire_mkdtemp(tmp);
	if (!path) {
		r = -errno;
		goto ERROR;
	}

	// Create a new pakfire instance
	r = pakfire_create(&p, ctx, config, tmp, arch, flags);
	if (r < 0) {
		ERROR(ctx, "Could not clone pakfire: %s\n", strerror(-r));
		goto ERROR;
	}

	// Install packages
	r = pakfire_snapshot_install_packages(p, packages);
	if (r < 0)
		goto ERROR;

	// Close the pakfire instance
	pakfire_unref(p);
	p = NULL;

	// Move the snapshot to its final place
	r = rename(tmp, snapshot_path);
	if (r < 0) {
		ERROR(ctx, "Could not move the snapshot to %s: %m\n", snapshot_path);
		r = -errno;
		goto ERROR;
	}

	// Open the snapshot
	r = pakfire_snapshot_create(snapshot, ctx, snapshot_path);
	if (r < 0)
		goto ERROR;

	// Cleanup any older snapshots (and ignore if something goes wrong)
	pakfire_snapshot_clean(pakfire);

ERROR:
	if (p)
		pakfire_unref(p);
	if (config)
		pakfire_config_unref(config);
	if (ctx)
		pakfire_ctx_unref(ctx);

	// Cleanup the temporary directory
	if (r)
		pakfire_rmtree(tmp, 0);

	return r;
}

/*
	Cleans up any unused snapshots
*/
int pakfire_snapshot_clean(struct pakfire* pakfire) {
	struct pakfire_snapshot* snapshot = NULL;
	char path[PATH_MAX];
	FTS* f = NULL;
	int r;

	struct pakfire_ctx* ctx = pakfire_ctx(pakfire);

	DEBUG(ctx, "Cleaning up snapshots...\n");

	// Make the path
	r = pakfire_cache_path(pakfire, path, "%s", "snapshots");
	if (r < 0)
		return r;

	char* paths[] = {
		path, NULL,
	};

	// Open the directory
	f = fts_open(paths, FTS_PHYSICAL|FTS_NOCHDIR|FTS_NOSTAT, NULL);

	for (;;) {
		FTSENT* node = fts_read(f);
		if (!node)
			break;

		// Skip the main directory
		if (node->fts_level == 0) {
			continue;

		// Skip any subdirectories
		} else if (node->fts_level > 1) {
			fts_set(f, node, FTS_SKIP);
			continue;
		}

		// We only care about directories
		switch (node->fts_info) {
			case FTS_D:
				break;

			default:
				continue;
		}

		// Open the snapshot
		r = pakfire_snapshot_create(&snapshot, ctx, node->fts_path);
		if (r < 0)
			goto ERROR;

		// Destroy the snapshot
		r = pakfire_snapshot_destroy(snapshot);
		if (r < 0) {
			switch (-r) {
				// Ignore if the snapshot is locked and move on
				case EBUSY:
					break;

				default:
					goto ERROR;
			}
		}

		// Free the snapshot
		pakfire_snapshot_unref(snapshot);
		snapshot = NULL;
	}

ERROR:
	if (snapshot)
		pakfire_snapshot_unref(snapshot);
	if (ctx)
		pakfire_ctx_unref(ctx);
	if (f)
		fts_close(f);

	return r;
}
