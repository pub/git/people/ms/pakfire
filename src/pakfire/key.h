/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_KEY_H
#define PAKFIRE_KEY_H

#include <time.h>

struct pakfire_key;

#include <pakfire/ctx.h>

typedef enum pakfire_key_algos {
	PAKFIRE_KEY_ALGO_NULL = 0,
	PAKFIRE_KEY_ALGO_ED25519,
} pakfire_key_algo_t;

typedef enum pakfire_key_export_mode {
	PAKFIRE_KEY_EXPORT_MODE_PUBLIC = 0,
	PAKFIRE_KEY_EXPORT_MODE_PRIVATE,
} pakfire_key_export_mode_t;

typedef unsigned char pakfire_key_id[8];

struct pakfire_key* pakfire_key_ref(struct pakfire_key* key);
struct pakfire_key* pakfire_key_unref(struct pakfire_key* key);

// Access key properties
pakfire_key_id* pakfire_key_get_id(struct pakfire_key* key);
const char* pakfire_key_get_algo(struct pakfire_key* key);
const char* pakfire_key_get_comment(struct pakfire_key* key);

int pakfire_key_generate(struct pakfire_key** key, struct pakfire_ctx* ctx,
	const pakfire_key_algo_t algo, const char* comment);
int pakfire_key_export(struct pakfire_key* key, FILE* f, const pakfire_key_export_mode_t mode);
int pakfire_key_export_string(struct pakfire_key* self, char** buffer, size_t* length);
int pakfire_key_import(struct pakfire_key** key, struct pakfire_ctx* ctx, FILE* f);

// Sign
int pakfire_key_sign(struct pakfire_key* key, FILE* s, FILE* f, const char* comment);
int pakfire_key_sign_string(struct pakfire_key* key,
	FILE* s, const void* data, const size_t length, const char* comment);
int pakfire_key_verify(struct pakfire_key* key,
	FILE* f, const void* data, const size_t length);

int pakfire_key_import_from_string(struct pakfire_key** key,
	struct pakfire_ctx* ctx, const char* data, const size_t length);

#endif /* PAKFIRE_KEY_H */
