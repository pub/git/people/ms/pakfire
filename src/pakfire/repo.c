/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <fts.h>
#include <linux/limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <solv/repo.h>
#include <solv/repo_solv.h>
#include <solv/repo_write.h>

#include <json.h>

#include <pakfire/archive.h>
#include <pakfire/config.h>
#include <pakfire/constants.h>
#include <pakfire/ctx.h>
#include <pakfire/hashes.h>
#include <pakfire/i18n.h>
#include <pakfire/json.h>
#include <pakfire/logging.h>
#include <pakfire/package.h>
#include <pakfire/pakfire.h>
#include <pakfire/path.h>
#include <pakfire/progress.h>
#include <pakfire/repo.h>
#include <pakfire/string.h>
#include <pakfire/util.h>
#include <pakfire/xfer.h>
#include <pakfire/xfopen.h>

#define METADATA_CHECKSUMS (PAKFIRE_HASH_SHA3_512|PAKFIRE_HASH_BLAKE2B512)

// Refresh mirror lists once every 6 hours
#define REFRESH_AGE_MIRRORLIST			6 * 3600

// Refresh repository metadata once every 2 hours
#define REFRESH_AGE_METADATA			2 * 3600

#define MAX_DESCRIPTION 4096

struct pakfire_repomd {
	// Version
	int64_t version;

	// Revision
	int64_t revision;

	// Package Database
	struct {
		// Path
		char path[PATH_MAX];

		// Size
		size_t size;

		// Hashes
		struct pakfire_hashes hashes;
	} packages;
};

struct pakfire_repo_appdata {
	// Reference Counter
	int nrefs;

	// SOLV Repo
	Repo* repo;

	// Metadata
	struct pakfire_repomd repomd;

	// Description
	char description[MAX_DESCRIPTION];

	// Base URL
	char baseurl[PATH_MAX];
	char expanded_baseurl[PATH_MAX];

	// Refresh Interval
	time_t refresh;

	// Key
	struct pakfire_key* key;

	// Mirrorlist
	struct pakfire_mirrorlist* mirrorlist;

	// Mirrorlist URL
	char mirrorlist_url[PATH_MAX];

	// Filesystem Layout
	enum pakfire_repo_filesystem_layout {
		PAKFIRE_REPO_UUID = (1 << 0),
		PAKFIRE_REPO_FLAT = (1 << 1),
		PAKFIRE_REPO_VIRT = (1 << 2),
	} fs_layout;

	// Markers
	int ready:1;
};

struct pakfire_repo {
	struct pakfire_ctx* ctx;
	struct pakfire* pakfire;
	int nrefs;

	Repo* repo;
	struct pakfire_repo_appdata* appdata;

	struct pakfire_key* key;
};

static const char* pakfire_repo_get_expanded_baseurl(struct pakfire_repo* repo);

int pakfire_repo_is_internal(struct pakfire_repo* repo) {
	const char* name = pakfire_repo_get_name(repo);
	if (!name)
		return 0;

	return (*name == '@');
}

static int pakfire_repo_is_commandline(struct pakfire_repo* repo) {
	return pakfire_repo_name_equals(repo, PAKFIRE_REPO_COMMANDLINE);
}

int pakfire_repo_is_local(struct pakfire_repo* repo) {
	// The commandline repository is semi-local
	if (pakfire_repo_is_commandline(repo))
		return 1;

	return pakfire_string_startswith(repo->appdata->baseurl, "file://");
}

/*
	LIBSOLV knows a subpriority which is a good way to indicate that
	packages from a local repository should be preferred without
	preferring an older version of a package over a new one.

	We set this automatically to reduce bandwidth usage.
 */
static void pakfire_repo_update_subpriority(struct pakfire_repo* repo) {
	// Prefer local repositories
	if (pakfire_repo_is_local(repo))
		repo->repo->subpriority = 1;

	// Packages in the commandline repository would have either been local
	// or downloaded in order to read them, so they should be preferred, too.
	else if (pakfire_repo_is_commandline(repo))
		repo->repo->subpriority = 1;

	// Otherwise reset
	else
		repo->repo->subpriority = 0;
}

int pakfire_repo_name_equals(struct pakfire_repo* repo, const char* name) {
	const char* n = pakfire_repo_get_name(repo);
	if (!n)
		return 0;

	return strcmp(n, name) == 0;
}

char* pakfire_repo_url_replace(struct pakfire_repo* repo, const char* url) {
	if (!url)
		return NULL;

	const struct replacement {
		const char* pattern;
		const char* replacement;
	} replacements[] = {
		{ "%{name}",    pakfire_repo_get_name(repo) },
		{ "%{arch}",    pakfire_get_effective_arch(repo->pakfire) },
		{ "%{distro}",  pakfire_get_distro_id(repo->pakfire) },
		{ "%{version}", pakfire_get_distro_version_id(repo->pakfire) },
		{ NULL },
	};

	char* buffer = strdup(url);
	if (!buffer)
		return NULL;

	for (const struct replacement* repl = replacements; repl->pattern; repl++) {
		// Skip if there is no replacement
		if (!repl->replacement)
			continue;

		char* r = pakfire_string_replace(
			buffer, repl->pattern, repl->replacement);
		free(buffer);

		// Break on any errors
		if (!r)
			return NULL;

		// Free the old buffer and continue working with the new data
		buffer = r;
	}

#ifdef ENABLE_DEBUG
	if (strcmp(url, buffer) != 0) {
		DEBUG(repo->ctx, "Repository URL updated:");
		DEBUG(repo->ctx, "  From: %s\n", url);
		DEBUG(repo->ctx, "  To  : %s\n", buffer);
	}
#endif

	return buffer;
}

int __pakfire_repo_path(struct pakfire_repo* repo,
		char* path, const size_t length, const char* format, ...) {
	char buffer[PATH_MAX];
	va_list args;
	int r;

	// Format the path
	va_start(args, format);
	r = __pakfire_string_vformat(buffer, sizeof(buffer), format, args);
	va_end(args);
	if (r < 0)
		return r;

	// For any internal repositories we are done
	if (pakfire_repo_is_internal(repo))
		return __pakfire_string_set(path, length, buffer);

	// For local repositories return the local path
	if (pakfire_repo_is_local(repo))
		return __pakfire_string_format(path, length,
			"%s/%s", pakfire_repo_get_path(repo), buffer);

	const char* name = pakfire_repo_get_name(repo);

	// Otherwise return the cached path
	return __pakfire_cache_path(repo->pakfire, path, length, "%s/%s", name, buffer);
}

static int pakfire_repo_xfer_create(struct pakfire_xfer** xfer, struct pakfire_repo* repo,
	const char* url, ...) __attribute__((format(printf, 3, 4)));

/*
	This is a convenience function to create a xfer with the
	settings of this repository.
*/
static int pakfire_repo_xfer_create(
		struct pakfire_xfer** xfer, struct pakfire_repo* repo, const char* url, ...) {
	struct pakfire_mirrorlist* mirrorlist = NULL;
	struct pakfire_xfer* x = NULL;
	const char* baseurl = NULL;
	va_list args;
	int r;

	// Create a new transfer
	va_start(args, url);
	r = pakfire_xfer_create(&x, repo->ctx, url, args);
	va_end(args);

	// Return on error
	if (r < 0)
		goto ERROR;

	// Set the baseurl
	baseurl = pakfire_repo_get_expanded_baseurl(repo);
	if (baseurl) {
		r = pakfire_xfer_set_baseurl(x, baseurl);
		if (r < 0)
			goto ERROR;
	}

	// Set the mirrorlist
	mirrorlist = pakfire_repo_get_mirrorlist(repo);
	if (mirrorlist) {
		r = pakfire_xfer_set_mirrorlist(x, mirrorlist);
		if (r < 0)
			goto ERROR;
	}

	// Success
	*xfer = pakfire_xfer_ref(x);

ERROR:
	if (mirrorlist)
		pakfire_mirrorlist_unref(mirrorlist);
	if (x)
		pakfire_xfer_unref(x);

	return r;
}

static int pakfire_repo_import_key(struct pakfire_repo* self, const char* data) {
	struct pakfire_key* key = NULL;
	int r;

	// Free any formerly imported keys (this should not happen)
	if (self->key) {
		pakfire_key_unref(self->key);
		self->key = NULL;
	}

	// Import the key
	r = pakfire_key_import_from_string(&key, self->ctx, data, strlen(data));
	if (r < 0) {
		ERROR(self->ctx, "Could not import key for repository '%s': %s\n",
			pakfire_repo_get_name(self), strerror(-r));
		goto ERROR;
	}

	// If the key could be successfully imported, we will store it in the appdata
	self->appdata->key = pakfire_key_ref(key);

ERROR:
	if (key)
		pakfire_key_unref(key);

	return r;
}

static int __pakfire_repo_import(struct pakfire_config* config, const char* section, void* data) {
	struct pakfire_repo* self = NULL;
	struct pakfire* pakfire = data;
	int r;

	// Ignore if the section does not start with "repo:"
	if (!pakfire_string_startswith(section, "repo:"))
		return 0;

	// Extract the name
	const char* name = section + strlen("repo:");

	// Skip if the name is empty
	if (!*name)
		return 0;

	// Create a new repository
	r = pakfire_repo_create(&self, pakfire, name);
	if (r < 0)
		goto ERROR;

	// Make the static analyzer happy
	assert(self);

	// Enabled
	pakfire_repo_set_enabled(self, pakfire_config_get_bool(config, section, "enabled", 1));

	// Priority
	int priority = pakfire_config_get_int(config, section, "priority", 0);
	if (priority)
		pakfire_repo_set_priority(self, priority);

	// Description
	const char* description = pakfire_config_get(config, section, "description", NULL);
	if (description) {
		r = pakfire_repo_set_description(self, description);
		if (r < 0) {
			ERROR(self->ctx, "Could not set description: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// baseurl
	const char* baseurl = pakfire_config_get(config, section, "baseurl", NULL);
	if (baseurl) {
		r = pakfire_repo_set_baseurl(self, baseurl);
		if (r) {
			ERROR(self->ctx, "Could not set base URL: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Refresh Interval
	const char* refresh = pakfire_config_get(config, section, "refresh", NULL);
	if (refresh) {
		self->appdata->refresh = r = pakfire_string_parse_interval(refresh);
		if (r < 0) {
			ERROR(self->ctx, "Could not set refresh interval: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// mirrors
	const char* mirrors = pakfire_config_get(config, section, "mirrors", NULL);
	if (mirrors) {
		r = pakfire_repo_set_mirrorlist_url(self, mirrors);
		if (r < 0) {
			ERROR(self->ctx, "Could not set the mirrorlist URL: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Key
	const char* key = pakfire_config_get(config, section, "key", NULL);
	if (key) {
		r = pakfire_repo_import_key(self, key);
		if (r) {
			ERROR(self->ctx, "Could not set the key: %s\n", strerror(-r));
			goto ERROR;
		}
	}

ERROR:
	if (self)
		pakfire_repo_unref(self);

	return r;
}

int pakfire_repo_import(struct pakfire* pakfire, struct pakfire_config* config) {
	int r;

	// Import all repositories from the configuration
	r = pakfire_config_walk_sections(config, __pakfire_repo_import, pakfire);
	if (r)
		return r;

	return 0;
}

static int pakfire_repo_to_packagelist(
		struct pakfire_repo* self, struct pakfire_packagelist** list) {
	struct pakfire_packagelist* l = NULL;
	struct pakfire_package* pkg = NULL;
	Solvable* s = NULL;
	Id id;
	int i;
	int r;

	// Create a new packagelist
	r = pakfire_packagelist_create(&l, self->ctx);
	if (r < 0)
		goto ERROR;

	// Ensure the repo is internalised
	pakfire_repo_internalize(self, 0);

	FOR_REPO_SOLVABLES(self->repo, i, s) {
		// Convert the solvable into an ID
		id = pool_solvable2id(self->repo->pool, s);

		// Create a new package
		r = pakfire_package_create_from_solvable(&pkg, self->pakfire, self, id);
		if (r < 0)
			goto ERROR;

		// Add the package to the list
		r = pakfire_packagelist_add(l, pkg);
		if (r < 0)
			goto ERROR;

		pakfire_package_unref(pkg);
		pkg = NULL;
	}

	// Return the list
	*list = pakfire_packagelist_ref(l);

ERROR:
	if (pkg)
		pakfire_package_unref(pkg);
	if (l)
		pakfire_packagelist_unref(l);

	return r;
}

Id pakfire_repo_add_solvable(struct pakfire_repo* repo) {
	Id id = repo_add_solvable(repo->repo);

	// Mark this repository as changed
	pakfire_repo_has_changed(repo);

	return id;
}

int __pakfire_repo_make_path(struct pakfire_repo* self, char* path, size_t length,
		struct pakfire_archive* archive, struct pakfire_package* pkg) {
	const char* uuid = NULL;

	// Fetch NEVRA
	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);
	if (!nevra)
		return -EINVAL;

	// Fetch the filename this package should have
	const char* filename = pakfire_package_get_filename(pkg);
	if (!filename)
		return -ENODATA;

	// Compose the destination path
	switch (self->appdata->fs_layout) {
		case PAKFIRE_REPO_FLAT:
			return __pakfire_string_set(path, length, filename);

		case PAKFIRE_REPO_UUID:
			// Fetch the UUID
			uuid = pakfire_package_get_string(pkg, PAKFIRE_PKG_UUID);
			if (!uuid) {
				ERROR(self->ctx, "%s does not have a UUID\n", nevra);
				return -ENODATA;
			}

			return __pakfire_string_format(path, length, "%s/%s", uuid, filename);

		// In virtual mode, we don't actually import the file
		case PAKFIRE_REPO_VIRT:
			return __pakfire_string_set(path, length, pakfire_archive_get_path(archive));
	}

	return -EINVAL;
}

int pakfire_repo_import_archive(struct pakfire_repo* self,
		struct pakfire_archive* archive, struct pakfire_package** package) {
	struct pakfire_package* pkg = NULL;
	char path[PATH_MAX];
	int r;

	// This only works on internal or local repositories
	if (!pakfire_repo_is_internal(self) && !pakfire_repo_is_local(self))
		return -ENOTSUP;

	// Add the metadata
	r = pakfire_archive_make_package(archive, self, &pkg);
	if (r < 0)
		goto ERROR;

	// Unless this is a virtual repository, copy the archive
	switch (self->appdata->fs_layout) {
		case PAKFIRE_REPO_VIRT:
			break;

		default:
			// Make the path
			r = pakfire_repo_path(self, path, "%s",
					pakfire_package_get_string(pkg, PAKFIRE_PKG_PATH));
			if (r < 0)
				goto ERROR;

			// Copy (or link) the archive
			r = pakfire_archive_link_or_copy(archive, path);
			if (r < 0)
				goto ERROR;
	}

	// Return the package if requested
	if (package)
		*package = pakfire_package_ref(pkg);

	goto CLEANUP;

ERROR:
	// Destroy the package if the import wasn't successful
	if (pkg)
		pakfire_package_destroy(pkg);

CLEANUP:
	if (pkg)
		pakfire_package_unref(pkg);

	return r;
}

struct pakfire_mirrorlist* pakfire_repo_get_mirrorlist(struct pakfire_repo* self) {
	if (self->appdata->mirrorlist)
		return pakfire_mirrorlist_ref(self->appdata->mirrorlist);

	return NULL;
}

struct pakfire_repo_scan_ctx {
	// Progress
	struct pakfire_progress* progress;

	// Flags
	enum pakfire_repo_scan_flags {
		PAKFIRE_REPO_SCAN_SKIP_EXISTING = (1 << 0),
	} flags;

	// Counter
	unsigned int num_archives;

	// Packages
	struct pakfire_packagelist* packages;
};

typedef int (*pakfire_repo_scan_callback)(
	struct pakfire_repo* repo, struct pakfire_repo_scan_ctx* scan_ctx, FTSENT* entry);

static int __pakfire_repo_scan(struct pakfire_repo* repo,
		struct pakfire_repo_scan_ctx* scan_ctx, pakfire_repo_scan_callback callback) {
	FTSENT* entry = NULL;
	FTS* fts = NULL;
	int r = 0;

	// Fetch the repository path
	const char* path = pakfire_repo_get_path(repo);
	if (!path)
		return -EINVAL;

	char* paths[] = {
		(char*)path, NULL,
	 };

	// Open the path
	fts = fts_open(paths, FTS_PHYSICAL|FTS_NOCHDIR|FTS_NOSTAT, NULL);
	if (!fts) {
		ERROR(repo->ctx, "Could not open %s: %m\n", path);
		r = -errno;
		goto ERROR;
	}

	// Scan for everything
	for (;;) {
		entry = fts_read(fts);
		if (!entry)
			break;

		// We only care about actual files
		if (!(entry->fts_info & FTS_F))
			continue;

		// Skip anything that doesn't have the correct file extension
		if (!pakfire_path_match("**.pfm", entry->fts_path))
			continue;

		// Call the callback
		r = callback(repo, scan_ctx, entry);
		if (r)
			goto ERROR;
	}

ERROR:
	if (fts)
		fts_close(fts);

	return r;
}

static int __pakfire_repo_scan_count(struct pakfire_repo* repo,
		struct pakfire_repo_scan_ctx* scan_ctx, FTSENT* entry) {
	++scan_ctx->num_archives;

	return 0;
}

static int __pakfire_repo_scan_archive(struct pakfire_repo* repo,
		struct pakfire_repo_scan_ctx* scan_ctx, FTSENT* entry) {
	struct pakfire_archive* archive = NULL;
	struct pakfire_package* package = NULL;
	int r = 0;

	// Skip this package if it already exists
	if (scan_ctx->flags & PAKFIRE_REPO_SCAN_SKIP_EXISTING) {
		if (pakfire_packagelist_has_path(scan_ctx->packages, entry->fts_path)) {
			goto DONE;
		}
	}

	// Open archive
	r = pakfire_archive_open(&archive, repo->pakfire, entry->fts_path);
	if (r < 0)
		goto ERROR;

	// Import package into the repository
	r = pakfire_archive_make_package(archive, repo, &package);
	if (r < 0)
		goto ERROR;

DONE:
	// Increment progress bar
	pakfire_progress_increment(scan_ctx->progress, 1);

ERROR:
	if (package)
		pakfire_package_unref(package);
	if (archive)
		pakfire_archive_unref(archive);

	return r;
}

int pakfire_repo_scan(struct pakfire_repo* repo, int flags) {
	int r;

	// Scan context
	struct pakfire_repo_scan_ctx scan_ctx = {
		.flags = flags,
	};

	// Fetch name
	const char* name = pakfire_repo_get_name(repo);

	// Fetch path
	const char* path = pakfire_repo_get_path(repo);
	if (!path)
		return -EINVAL;

	// Create progress indicator
	r = pakfire_progress_create(&scan_ctx.progress, repo->ctx,
			PAKFIRE_PROGRESS_SHOW_COUNTER|PAKFIRE_PROGRESS_SHOW_ELAPSED_TIME, NULL);
	if (r)
		goto ERROR;

	// Add title to progress
	r = pakfire_progress_set_title(scan_ctx.progress, _("Scanning %s"), name);
	if (r)
		goto ERROR;

	// Load all existing packages
	if (scan_ctx.flags & PAKFIRE_REPO_SCAN_SKIP_EXISTING) {
		r = pakfire_repo_to_packagelist(repo, &scan_ctx.packages);
		if (r < 0)
			goto ERROR;
	}

	// Start progress (so that we will let the user know something is happening if
	// scanning for files takes a little bit longer...)
	r = pakfire_progress_start(scan_ctx.progress, 0);
	if (r < 0)
		goto ERROR;

	// Count how many files we have
	r = __pakfire_repo_scan(repo, &scan_ctx, __pakfire_repo_scan_count);
	if (r < 0)
		goto ERROR;

	// Update the progressbar
	pakfire_progress_set_max_value(scan_ctx.progress, scan_ctx.num_archives);

	// Scan all packages
	r = __pakfire_repo_scan(repo, &scan_ctx, __pakfire_repo_scan_archive);
	if (r < 0)
		goto ERROR;

	// Mark repository data as changed
	pakfire_repo_has_changed(repo);

	// Finish the progress
	r = pakfire_progress_finish(scan_ctx.progress);
	if (r < 0)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	if (scan_ctx.packages)
		pakfire_packagelist_unref(scan_ctx.packages);
	if (scan_ctx.progress)
		pakfire_progress_unref(scan_ctx.progress);

	return r;
}

static int pakfire_repo_sync_remove(struct pakfire_ctx* ctx, struct pakfire_package* pkg, void* data) {
	int r;

	// Check if the package is available
	r = pakfire_package_is_available(pkg);
	switch (r) {
		// The package is not available
		case 0:
			// Destroy the package
			return pakfire_package_destroy(pkg);

		// The package is available
		case 1:
			return 0;

		// Error
		default:
			return r;
	}

	return 0;
}

static int pakfire_repo_sync(struct pakfire_repo* self) {
	int r;

	// Log action
	DEBUG(self->ctx, "Syncing %s...\n", pakfire_repo_get_name(self));

	// Remove all packages that have disappeared
	r = pakfire_repo_walk_packages(self, pakfire_repo_sync_remove, NULL, 0);
	if (r < 0)
		return r;

	// We need to find all new packages
	r = pakfire_repo_scan(self, PAKFIRE_REPO_SCAN_SKIP_EXISTING);
	if (r < 0)
		return r;

	return 0;
}

static int pakfire_repo_download_database(
		struct pakfire_repo* repo, const char* filename, const char* path) {
	struct pakfire_xfer* xfer = NULL;
	char title[NAME_MAX];
	int r;

	// Don't perform any downloads for local repositories
	if (pakfire_repo_is_local(repo))
		return 0;

	// Do nothing if the file already exists
	if (pakfire_path_exists(path)) {
		DEBUG(repo->ctx, "Database %s already present. Skipping download\n", filename);
		return 0;
	}

	// Make title
	r = pakfire_string_format(title, _("Package Database: %s"), pakfire_repo_get_name(repo));
	if (r < 0)
		goto ERROR;

	// Create a new transfers
	r = pakfire_repo_xfer_create(&xfer, repo, "%s", filename);
	if (r < 0)
		goto ERROR;

	// Set title
	r = pakfire_xfer_set_title(xfer, title);
	if (r < 0)
		goto ERROR;

	// Set path
	r = pakfire_xfer_set_output_path(xfer, path);
	if (r < 0)
		goto ERROR;

	// Set digests
	r = pakfire_xfer_verify_hashes(xfer, &repo->appdata->repomd.packages.hashes);
	if (r < 0)
		goto ERROR;

	// Run the xfer
	r = pakfire_xfer_run(xfer, 0);
	if (r < 0)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);

	return r;
}

static int pakfire_repo_read_database(struct pakfire_repo* self) {
	struct pakfire_repomd* repomd = &self->appdata->repomd;
	char path[PATH_MAX];
	FILE* f = NULL;
	int r;

	// Make sure the path is set
	if (!*repomd->packages.path) {
		// If we are opening a local repository without a database,
		// we will perform a full scan instead.
		if (pakfire_repo_is_local(self))
			return pakfire_repo_scan(self, 0);

		ERROR(self->ctx, "Package database path is not set. Aborting.\n");
		r = -EINVAL;
		goto ERROR;
	}

	// Make the total path
	r = pakfire_repo_path(self, path, "%s", repomd->packages.path);
	if (r < 0)
		goto ERROR;

	// Download the database
	r = pakfire_repo_download_database(self, repomd->packages.path, path);
	if (r < 0) {
		ERROR(self->ctx, "Failed to download package database %s: %s\n",
			repomd->packages.path, strerror(-r));
		goto ERROR;
	}

	DEBUG(self->ctx, "Read package database from %s...\n", path);

	// Open the database file
	f = fopen(path, "r");
	if (!f) {
		ERROR(self->ctx, "Could not open package database at %s: %m\n", path);
		r = -errno;
		goto ERROR;
	}

	// Automatically detect compression
	f = pakfire_xfopen(f, "r");
	if (!f) {
		r = -errno;
		goto ERROR;
	}

	// Drop any previous data
	r = pakfire_repo_clear(self);
	if (r < 0)
		goto ERROR;

	// Read database
	r = pakfire_repo_read_solv(self, f, 0);
	if (r < 0)
		goto ERROR;

	// Perform a sync for all local repositories
	if (pakfire_repo_is_local(self)) {
		r = pakfire_repo_sync(self);
		if (r < 0)
			goto ERROR;
	}

ERROR:
	if (f)
		fclose(f);

	return r;
}

static void pakfire_repo_reset_metadata(struct pakfire_repo* self) {
	memset(&self->appdata->repomd, 0, sizeof(self->appdata->repomd));
}

static int pakfire_repo_parse_repomd(struct pakfire_repo* self,
		struct pakfire_repomd* repomd, struct json_object* root) {
	struct json_object* files = NULL;
	struct json_object* file = NULL;
	struct json_object* chksums = NULL;
	const char* type = NULL;
	const char* filename = NULL;
	ssize_t size = 0;
	struct pakfire_hashes hashes = {};
	int r;

	// Parse version
	r = pakfire_json_get_int64(root, "version", &repomd->version);
	if (r < 0)
		goto ERROR;

	// Check if we support this version
	switch (repomd->version) {
		case 1:
			break;

		default:
			ERROR(self->ctx, "Unsupported version of repository metadata: %ld\n", repomd->version);
			r = -ENOTSUP;
			goto ERROR;
	}

	DEBUG(self->ctx, "Parsing repository metadata in version %ld\n", repomd->version);

	// Parse revision
	r = pakfire_json_get_int64(root, "revision", &repomd->revision);
	if (r < 0)
		goto ERROR;

	// Fetch files
	r = pakfire_json_get_array(root, "files", &files);
	if (r < 0)
		goto ERROR;

	// Iterate over all files
	for (size_t i = 0; i < json_object_array_length(files); i++) {
		file = json_object_array_get_idx(files, i);

		// Read type
		r = pakfire_json_get_string(file, "type", &type);
		if (r < 0)
			goto ERROR;

		// Read filename
		r = pakfire_json_get_string(file, "filename", &filename);
		if (r < 0)
			goto ERROR;

		// Read size
		r = pakfire_json_get_int64(file, "size", &size);
		if (r < 0)
			goto ERROR;

		// Fetch checksums
		r = pakfire_json_get_object(file, "chksums", &chksums);
		if (r < 0)
			goto ERROR;

		// Parse checksums
		json_object_object_foreach(chksums, hash_name, hexdigest) {
			enum pakfire_hash_type hash_type = pakfire_hash_by_name(hash_name);

			// Import the hexdigest
			r = pakfire_hashes_set_hex(&hashes, hash_type, json_object_get_string(hexdigest));
			if (r < 0)
				goto ERROR;
		}

		// We found the packages database
		if (pakfire_string_equals(type, "packages")) {
			// Store the filename
			r = pakfire_string_set(repomd->packages.path, filename);
			if (r < 0)
				goto ERROR;

			// Store the filelist
			repomd->packages.size = size;

			// Store the hashes
			r = pakfire_hashes_import(&repomd->packages.hashes, &hashes);
			if (r < 0)
				goto ERROR;

			// Since we currently only know this one file, we can stop once we found it
			break;
		}

		// Reset the hashes
		pakfire_hashes_reset(&hashes);
	}

	return 0;

ERROR:
	// If the entire metadata could not be parsed, we reset everything
	pakfire_repo_reset_metadata(self);

	return r;
}

static int pakfire_repo_read_metadata(struct pakfire_repo* repo, const char* path) {
	struct json_object* json = NULL;
	int r;

	DEBUG(repo->ctx, "Reading repository metadata from %s...\n", path);

	// Parse the existing metadata
	json = pakfire_json_parse_from_file(repo->ctx, path);
	if (!json) {
		switch (errno) {
			case ENOENT:
				// Log some message
				DEBUG(repo->ctx, "Repository metadata is not available\n");

				// Otherwise accept this and hope we will be able to download some data
				return 0;

			default:
				ERROR(repo->ctx, "Could not parse metadata from %s: %m\n", path);
				r = -errno;
				goto ERROR;
		}
	}

	// Parse the JSON object
	r = pakfire_repo_parse_repomd(repo, &repo->appdata->repomd, json);
	if (r < 0) {
		switch (-r) {
			// Ignore if the file on disk was malformed or is not supported
			case EBADMSG:
			case ENOTSUP:
				r = 0;
				goto ERROR;

			default:
				ERROR(repo->ctx, "Could not parse repository metadata for %s: %s\n",
					pakfire_repo_get_name(repo), strerror(-r));
				goto ERROR;
		}
	}

ERROR:
	// Free the parsed JSON object
	if (json)
		json_object_put(json);

	return r;
}

static int pakfire_repo_download_mirrorlist(struct pakfire_repo* self,
		const char* path, const int force) {
	struct pakfire_xfer* xfer = NULL;
	char* url = NULL;
	time_t age = -1;
	int r;

	// Do nothing if running in offline mode
	if (pakfire_ctx_has_flag(self->ctx, PAKFIRE_CTX_OFFLINE))
		return 0;

	// Check if this needs to be refreshed
	if (!force) {
		age = pakfire_path_age(path);

		if (age > 0 && age < REFRESH_AGE_MIRRORLIST) {
			DEBUG(self->ctx, "Skip refreshing mirrorlist which is %lds old\n", age);
			return 0;
		}
	}

	// Replace any variables in the URL
	url = pakfire_repo_url_replace(self, self->appdata->mirrorlist_url);
	if (!url) {
		ERROR(self->ctx, "Could not expand the mirrorlist URL: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Create a new transfer
	r = pakfire_repo_xfer_create(&xfer, self, "%s", url);
	if (r < 0)
		goto ERROR;

	// Set the output path
	r = pakfire_xfer_set_output_path(xfer, path);
	if (r < 0)
		goto ERROR;

	// Perform the transfer
	r = pakfire_xfer_run(xfer, PAKFIRE_XFER_NO_PROGRESS);
	if (r < 0)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (url)
		free(url);

	return r;
}

static int pakfire_repo_refresh_mirrorlist(struct pakfire_repo* self, const int force) {
	struct pakfire_mirrorlist* mirrorlist = NULL;
	char path[PATH_MAX];
	int r;

	// Local repositories don't need a mirrorlist
	if (pakfire_repo_is_local(self))
		return 0;

	// This repository does not have a mirrorlist
	if (!*self->appdata->mirrorlist_url)
		return 0;

	// Fetch repo name
	const char* name = pakfire_repo_get_name(self);

	// Make path to mirrorlist
	r = pakfire_cache_path(self->pakfire, path, "repodata/%s/mirrorlist", name);
	if (r < 0)
		goto ERROR;

	// Download the mirrorlist
	r = pakfire_repo_download_mirrorlist(self, path, force);
	if (r < 0) {
		ERROR(self->ctx, "Could not download the mirrorlist: %s\n", strerror(-r));
		goto ERROR;
	}

	DEBUG(self->ctx, "Reading mirrorlist for %s from %s\n", name, path);

	// Allocate a new mirrorlist
	r = pakfire_mirrorlist_create(&mirrorlist, self->ctx);
	if (r < 0)
		goto ERROR;

	// Read the mirrorlist
	r = pakfire_mirrorlist_read(mirrorlist, path);
	if (r < 0)
		goto ERROR;

	// Store the mirrorlist
	self->appdata->mirrorlist = pakfire_mirrorlist_ref(mirrorlist);

ERROR:
	if (mirrorlist)
		pakfire_mirrorlist_unref(mirrorlist);

	return r;
}

static int pakfire_repo_verify_metadata(
		struct pakfire_repo* self, const struct pakfire_buffer* repomd) {
	struct pakfire_buffer signature = {};
	struct pakfire_xfer* xfer = NULL;
	FILE* f = NULL;
	int r;

	// Create a new xfer
	r = pakfire_repo_xfer_create(&xfer, self, "repomd.json.sig");
	if (r < 0)
		goto ERROR;

	// Write the signature to a buffer
	r = pakfire_xfer_set_output_buffer(xfer, &signature.data, &signature.length);
	if (r < 0)
		goto ERROR;

	// Perform the transfer
	r = pakfire_xfer_run(xfer, PAKFIRE_XFER_NO_PROGRESS);
	if (r < 0)
		goto ERROR;

	// Fail if we could not fetch any data
	if (!signature.data) {
		ERROR(self->ctx, "Could not fetch repository signature\n");
		r = -ENODATA;
		goto ERROR;
	}

	// Map the signature in a file handle
	f = fmemopen(signature.data, signature.length, "r");
	if (!f) {
		r = -errno;
		goto ERROR;
	}

	// Verify the downloaded metadata
	r = pakfire_key_verify(self->appdata->key, f, repomd->data, repomd->length);
	if (r < 0)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (f)
		fclose(f);
	if (signature.data)
		free(signature.data);

	return r;
}

static int pakfire_repo_download_metadata(struct pakfire_repo* repo, const char* path, int force) {
	struct pakfire_repomd repomd = {};
	struct pakfire_buffer buffer = {};
	struct pakfire_xfer* xfer = NULL;
	struct json_object* json = NULL;
	int r;

	// Local repositories don't need to download metadata
	if (pakfire_repo_is_local(repo))
		return 0;

	// Do nothing if running in offline mode
	if (pakfire_ctx_has_flag(repo->ctx, PAKFIRE_CTX_OFFLINE))
		return 0;

	// If metadata is empty, we always refresh
	if (!repo->appdata->repomd.version)
		force = 1;

	// Fetch refresh interval
	time_t refresh = repo->appdata->refresh;

	// Use a default value if unset
	if (refresh < 0)
		refresh = REFRESH_AGE_METADATA;

	// If the refresh interval is set to zero, we will always force a refresh
	if (refresh == 0)
		force = 1;

	// Check if this needs to be refreshed
	if (!force) {
		time_t age = pakfire_path_age(path);

		if (age > 0 && age < refresh) {
			DEBUG(repo->ctx, "Skip refreshing metadata which is %lds old\n", age);
			return 0;
		}
	}

	// Create a new transfer
	r = pakfire_repo_xfer_create(&xfer, repo, "repomd.json");
	if (r < 0)
		goto ERROR;

	// Set the output path
	r = pakfire_xfer_set_output_buffer(xfer, &buffer.data, &buffer.length);
	if (r < 0)
		goto ERROR;

	// Perform the transfer
	r = pakfire_xfer_run(xfer, PAKFIRE_XFER_NO_PROGRESS);
	if (r < 0)
		goto ERROR;

	// Verify the downloaded data
	if (repo->appdata->key) {
		r = pakfire_repo_verify_metadata(repo, &buffer);
		if (r < 0)
			goto ERROR;
	}

	// Parse JSON from buffer
	json = pakfire_json_parse(repo->ctx, buffer.data, buffer.length);
	if (!json) {
		r = -EBADMSG;
		goto ERROR;
	}

	// Parse the metadata
	r = pakfire_repo_parse_repomd(repo, &repomd, json);
	if (r < 0)
		goto ERROR;

	// Check if the metadata is more recent than what we had
	if (repo->appdata->repomd.revision > repomd.revision) {
		WARN(repo->ctx, "Downloaded metadata is older than what we have. Ignoring.\n");

	// If we have downloaded more recent data, we want to import it
	} else if (repo->appdata->repomd.revision < repomd.revision) {
		// Copy the object
		memcpy(&repo->appdata->repomd, &repomd, sizeof(repo->appdata->repomd));

		// Ensure the parent directory exists
		r = pakfire_mkparentdir(path, 0755);
		if (r < 0)
			goto ERROR;

		// Write the metadata to disk for next time
		r = pakfire_json_write(json, path);
		if (r < 0) {
			ERROR(repo->ctx, "Failed to store repository metadata in %s: %s\n",
				path, strerror(-r));
			goto ERROR;
		}
	}

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (json)
		json_object_put(json);
	if (buffer.data)
		free(buffer.data);

	return r;
}

static void pakfire_repo_free_appdata(struct pakfire_repo_appdata* appdata) {
	// Don't free if something is still holding references
	if (--appdata->nrefs > 0)
		return;

	// If there are no further references, automatically destroy the entire repository
	repo_free(appdata->repo, 0);

	if (appdata->mirrorlist)
		pakfire_mirrorlist_unref(appdata->mirrorlist);
	if (appdata->key)
		pakfire_key_unref(appdata->key);
	free(appdata);
}

static void pakfire_repo_free(struct pakfire_repo* self) {
	if (self->appdata)
		pakfire_repo_free_appdata(self->appdata);
	if (self->pakfire)
		pakfire_unref(self->pakfire);
	if (self->ctx)
		pakfire_ctx_unref(self->ctx);
	free(self);
}

void pakfire_repo_free_all(struct pakfire* pakfire) {
	Repo* repo = NULL;
	int i;

	// Fetch the pool
	Pool* pool = pakfire_get_solv_pool(pakfire);
	if (!pool)
		return;

	FOR_REPOS(i, repo) {
		if (repo->appdata)
			pakfire_repo_free_appdata(repo->appdata);
	}
}

static int pakfire_repo_setup_appdata(struct pakfire_repo* self) {
	struct pakfire_repo_appdata* appdata = NULL;

	// Allocate appdata
	appdata = calloc(1, sizeof(*appdata));
	if (!appdata)
		return -errno;

	// Initialize the reference counter
	appdata->nrefs = 2;

	// Reference the SOLV repository
	appdata->repo = self->repo;

	// Refresh Interval: Set to invalid
	appdata->refresh = -1;

	// The local build repository will have a flat layout
	if (pakfire_repo_name_equals(self, PAKFIRE_REPO_LOCAL))
		appdata->fs_layout = PAKFIRE_REPO_FLAT;

	// Make any other internal repositories virtual
	else if (pakfire_repo_is_internal(self))
		appdata->fs_layout = PAKFIRE_REPO_VIRT;

	// All other repositories follow the UUID format
	else
		appdata->fs_layout = PAKFIRE_REPO_UUID;

	// Store the pointer
	self->appdata = self->repo->appdata = appdata;

	return 0;
}

int pakfire_repo_create(struct pakfire_repo** repo,
		struct pakfire* pakfire, const char* name) {
	struct pakfire_repo* self = NULL;
	int r;

	// Allocate some memory
	self = calloc(1, sizeof(*self));
	if (!self)
		return -errno;

	// Store a reference to the context
	self->ctx = pakfire_ctx(pakfire);

	// Store a reference to Pakfire
	self->pakfire = pakfire_ref(pakfire);

	// Initialize the reference counter
	self->nrefs = 1;

	// Fetch the pool
	Pool* pool = pakfire_get_solv_pool(pakfire);

	// Allocate a libsolv repository
	self->repo = repo_create(pool, name);
	if (!self->repo) {
		ERROR(self->ctx, "Could not allocate repo: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Allocate repository appdata
	r = pakfire_repo_setup_appdata(self);
	if (r < 0) {
		ERROR(self->ctx, "Could not setup repository appdata for %s: %s\n",
			name, strerror(-r));
		goto ERROR;
	}

	// Make the static analyzer happy
	assert(self->appdata);

	// Update the subpriority
	pakfire_repo_update_subpriority(self);

	// Setup/clear repository data
	r = pakfire_repo_clear(self);
	if (r < 0)
		goto ERROR;

	// Return the pointer
	*repo = self;
	return 0;

ERROR:
	if (self->appdata)
		pakfire_repo_free_appdata(self->appdata);
	pakfire_repo_free(self);

	return r;
}

int pakfire_repo_open(struct pakfire_repo** repo, struct pakfire* pakfire, Repo* _repo) {
	struct pakfire_repo* self = NULL;

	// Allocate some memory
	self = calloc(1, sizeof(*self));
	if (!self)
		return -errno;

	// Store a reference to the context
	self->ctx = pakfire_ctx(pakfire);

	// Store a reference to Pakfire
	self->pakfire = pakfire_ref(pakfire);

	// Initialize the reference counter
	self->nrefs = 1;

	// Store a reference to the repository
	self->repo = _repo;

	// Initialize appdata
	self->appdata = self->repo->appdata;
	self->appdata->nrefs++;

	// Return the pointer
	*repo = self;

	return 0;
}

struct pakfire_repo* pakfire_repo_ref(struct pakfire_repo* repo) {
	repo->nrefs++;

	return repo;
}

struct pakfire_repo* pakfire_repo_unref(struct pakfire_repo* repo) {
	if (--repo->nrefs > 0)
		return repo;

	pakfire_repo_free(repo);
	return NULL;
}

int pakfire_repo_clear(struct pakfire_repo* self) {
	repo_empty(self->repo, 0);

	return 0;
}

Repo* pakfire_repo_get_repo(struct pakfire_repo* repo) {
	return repo->repo;
}

Repodata* pakfire_repo_get_repodata(struct pakfire_repo* repo) {
	return repo_last_repodata(repo->repo);
}

int pakfire_repo_identical(struct pakfire_repo* repo1, struct pakfire_repo* repo2) {
	return (repo1->repo == repo2->repo);
}

int pakfire_repo_cmp(struct pakfire_repo* repo1, struct pakfire_repo* repo2) {
	Repo* r1 = repo1->repo;
	Repo* r2 = repo2->repo;

	if (r1->priority > r2->priority)
		return 1;

	else if (r1->priority < r2->priority)
		return -1;

	return strcmp(r1->name, r2->name);
}

int pakfire_repo_count(struct pakfire_repo* repo) {
	Pool* pool = pakfire_get_solv_pool(repo->pakfire);
	int cnt = 0;

	for (int i = 2; i < pool->nsolvables; i++) {
		Solvable* s = pool->solvables + i;
		if (s->repo && s->repo == repo->repo)
			cnt++;
	}

	return cnt;
}

void pakfire_repo_has_changed(struct pakfire_repo* repo) {
	repo->appdata->ready = 0;

	// Mark pool as changed, too
	pakfire_pool_has_changed(repo->pakfire);
}

int pakfire_repo_internalize(struct pakfire_repo* repo, int flags) {
	if (repo->appdata->ready)
		return 0;

	// Internalize all repository data
	repo_internalize(repo->repo);

	// Mark this repository as ready
	repo->appdata->ready = 1;

	return 0;
}

const char* pakfire_repo_get_name(struct pakfire_repo* repo) {
	return repo->repo->name;
}

const char* pakfire_repo_get_description(struct pakfire_repo* repo) {
	return repo->appdata->description;
}

int pakfire_repo_set_description(struct pakfire_repo* repo, const char* description) {
	return pakfire_string_set(repo->appdata->description, description);
}

int pakfire_repo_get_enabled(struct pakfire_repo* repo) {
	return !repo->repo->disabled;
}

void pakfire_repo_set_enabled(struct pakfire_repo* repo, int enabled) {
	repo->repo->disabled = !enabled;

	pakfire_repo_has_changed(repo);
}

int pakfire_repo_get_priority(struct pakfire_repo* repo) {
	return repo->repo->priority;
}

void pakfire_repo_set_priority(struct pakfire_repo* repo, int priority) {
	repo->repo->priority = priority;
}

const char* pakfire_repo_get_baseurl(struct pakfire_repo* repo) {
	return repo->appdata->baseurl;
}

static const char* pakfire_repo_get_expanded_baseurl(struct pakfire_repo* repo) {
	char* url = NULL;
	int r;

	// Return NULL if baseurl isn't set
	if (!*repo->appdata->baseurl)
		return NULL;

	if (!*repo->appdata->expanded_baseurl) {
		url = pakfire_repo_url_replace(repo, repo->appdata->baseurl);
		if (!url)
			goto ERROR;

		// Store the result
		r = pakfire_string_set(repo->appdata->expanded_baseurl, url);
		if (r)
			goto ERROR;
	}

ERROR:
	if (url)
		free(url);

	return (*repo->appdata->expanded_baseurl) ? repo->appdata->expanded_baseurl : NULL;
}

int pakfire_repo_set_baseurl(struct pakfire_repo* repo, const char* baseurl) {
	int r;

	// Store the URL
	r = pakfire_string_set(repo->appdata->baseurl, baseurl);
	if (r)
		return r;

	// Invalidate the expanded URL
	*repo->appdata->expanded_baseurl = '\0';

	// Update sub-priority
	pakfire_repo_update_subpriority(repo);

	return 0;
}

const char* pakfire_repo_get_path(struct pakfire_repo* repo) {
	const char* baseurl = NULL;

	if (!pakfire_repo_is_local(repo))
		return NULL;

	// Get the base URL
	baseurl = pakfire_repo_get_expanded_baseurl(repo);
	if (!baseurl)
		return NULL;

	// The URL must start with file://
	if (!pakfire_string_startswith(baseurl, "file://")) {
		errno = -EINVAL;
		return NULL;
	}

	return baseurl + strlen("file://");
}

struct pakfire_key* pakfire_repo_get_key(struct pakfire_repo* self) {
	if (self->appdata->key)
		return pakfire_key_ref(self->appdata->key);

	return NULL;
}

const char* pakfire_repo_get_mirrorlist_url(struct pakfire_repo* self) {
	if (*self->appdata->mirrorlist_url)
		return self->appdata->mirrorlist_url;

	return NULL;
}

int pakfire_repo_set_mirrorlist_url(struct pakfire_repo* repo, const char* url) {
	return pakfire_string_set(repo->appdata->mirrorlist_url, url);
}

int pakfire_repo_write_config(struct pakfire_repo* repo, FILE* f) {
	struct pakfire_config* config = NULL;
	struct pakfire_key* key = NULL;
	char* section = NULL;
	char* buffer = NULL;
	char refresh[1024];
	size_t length = 0;
	int r;

	// Do nothing for the installed repository
	if (pakfire_repo_is_installed_repo(repo))
		return 0;

	// Create section name
	r = asprintf(&section, "repo:%s", pakfire_repo_get_name(repo));
	if (r < 0)
		goto ERROR;

	// Create a new configuration
	r = pakfire_config_create(&config);
	if (r < 0)
		goto ERROR;

	// Description
	const char* description = pakfire_repo_get_description(repo);
	if (description) {
		r = pakfire_config_set(config, section, "description", description);
		if (r < 0) {
			ERROR(repo->ctx, "Could not set description: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Disabled?
	if (!pakfire_repo_get_enabled(repo)) {
		r = pakfire_config_set(config, section, "enabled", "0");
		if (r < 0) {
			ERROR(repo->ctx, "Could not set enabled: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Base URL
	const char* baseurl = pakfire_repo_get_baseurl(repo);
	if (baseurl) {
		r = pakfire_config_set(config, section, "baseurl", baseurl);
		if (r < 0) {
			ERROR(repo->ctx, "Could not set base URL: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Refresh Interval
	if (repo->appdata->refresh >= 0) {
		r = pakfire_string_format_interval(refresh, repo->appdata->refresh);
		if (r < 0)
			goto ERROR;

		// Set the refresh interval
		r = pakfire_config_set(config, section, "refresh", refresh);
		if (r < 0) {
			ERROR(repo->ctx, "Could not set refresh interval: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Mirror List
	const char* mirrorlist = pakfire_repo_get_mirrorlist_url(repo);
	if (mirrorlist) {
		r = pakfire_config_set(config, section, "mirrors", mirrorlist);
		if (r < 0) {
			ERROR(repo->ctx, "Could not set mirrors: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Key
	key = pakfire_repo_get_key(repo);
	if (key) {
		r = pakfire_key_export_string(key, &buffer, &length);
		if (r < 0) {
			ERROR(repo->ctx, "Could not export the key: %s\n", strerror(-r));
			goto ERROR;
		}

		r = pakfire_config_set_format(config, section, "key", "%.*s", (int)length, buffer);
		if (r < 0) {
			ERROR(repo->ctx, "Could not set key: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Priority
	unsigned int priority = pakfire_repo_get_priority(repo);
	if (priority) {
		r = pakfire_config_set_format(config, section, "priority", "%u", priority);
		if (r < 0) {
			ERROR(repo->ctx, "Could not set priority: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Write the configuration
	r = pakfire_config_dump(config, f);

ERROR:
	if (key)
		pakfire_key_unref(key);
	if (config)
		pakfire_config_unref(config);
	if (section)
		free(section);
	if (buffer)
		free(buffer);

	return r;
}

int pakfire_repo_is_installed_repo(struct pakfire_repo* self) {
	return (self->repo == self->repo->pool->installed);
}

int pakfire_repo_download_package(struct pakfire_xfer** xfer,
		struct pakfire_repo* repo, struct pakfire_package* pkg) {
	enum pakfire_hash_type hash = PAKFIRE_HASH_UNDEFINED;
	const unsigned char* checksum = NULL;
	size_t checksum_length = 0;
	struct pakfire_hashes hashes = {};
	struct pakfire_xfer* x = NULL;
	const char* cache_path = NULL;
	const char* nevra = NULL;
	const char* url = NULL;
	int r = 1;

	// Fetch nevra
	nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);
	if (!nevra)
		goto ERROR;

	// Where do we store the package?
	cache_path = pakfire_package_get_string(pkg, PAKFIRE_PKG_CACHE_PATH);
	if (!cache_path) {
		ERROR(repo->ctx, "Could not retrieve package cache path for %s: %m\n", nevra);
		goto ERROR;
	}

	// Where do we find the package on the server?
	url = pakfire_package_get_string(pkg, PAKFIRE_PKG_PATH);
	if (!url) {
		ERROR(repo->ctx, "Could not retrieve package path for %s: %m\n", nevra);
		goto ERROR;
	}

	// Retrieve package digest
	r = pakfire_package_get_checksum(pkg, &hash, &checksum, &checksum_length);
	if (r < 0) {
		ERROR(repo->ctx, "Failed to fetch checksum: %s\n", strerror(-r));
		goto ERROR;
	}

	// Checksum must be set
	if (!checksum) {
		ERROR(repo->ctx, "Package %s has no checksum\n", nevra);
		goto ERROR;
	}

	// Store the check in hashes
	r = pakfire_hashes_set(&hashes, hash, checksum, checksum_length);
	if (r < 0)
		goto ERROR;

	// Create a new transfer
	r = pakfire_repo_xfer_create(&x, repo, "%s", url);
	if (r)
		goto ERROR;

	// Set title
	r = pakfire_xfer_set_title(x, nevra);
	if (r)
		goto ERROR;

	// Fetch downloadsize
	size_t downloadsize = pakfire_package_get_num(pkg, PAKFIRE_PKG_DOWNLOADSIZE, 0);

	// Set size
	if (downloadsize > 0) {
		r = pakfire_xfer_set_size(x, downloadsize);
		if (r < 0)
			goto ERROR;
	}

	// Set output path
	r = pakfire_xfer_set_output_path(x, cache_path);
	if (r < 0)
		goto ERROR;

	// Set digest
	r = pakfire_xfer_verify_hashes(x, &hashes);
	if (r < 0)
		goto ERROR;

	// Success
	*xfer = x;

	return 0;

ERROR:
	if (x)
		pakfire_xfer_unref(x);

	return r;
}

static int pakfire_repo_download(struct pakfire_repo* repo, const char* url,
		struct pakfire_package** package) {
	struct pakfire_xfer* xfer = NULL;
	FILE* f = NULL;
	int r;

	char path[PATH_MAX] = PAKFIRE_TMP_DIR "/pakfire-download.XXXXXX";

	// Allocate a temporary file name
	f = pakfire_mktemp(path, 0);
	if (!f) {
		r = -errno;
		goto ERROR;
	}

	// Create a new transfer
	r = pakfire_repo_xfer_create(&xfer, repo, "%s", url);
	if (r)
		goto ERROR;

	// Set destination
	r = pakfire_xfer_set_output(xfer, f);
	if (r)
		goto ERROR;

	// Reset f since its control has been passed to xfer
	f = NULL;

	// Run the download
	r = pakfire_xfer_run(xfer, 0);
	if (r)
		goto ERROR;

	// Try to add it to this repository
	r = pakfire_repo_add(repo, path, package);
	if (r)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (f)
		fclose(f);

	return r;
}

int pakfire_repo_add(struct pakfire_repo* repo, const char* path,
		struct pakfire_package** package) {
	struct pakfire_archive* archive = NULL;
	int r;

	// This operation is only supported for the command line repository
	if (!pakfire_repo_is_commandline(repo))
		return -ENOTSUP;

	// Download the package if we got given a URL
	if (pakfire_string_is_url(path)) {
		r = pakfire_repo_download(repo, path, package);
		if (r)
			ERROR(repo->ctx, "Could not download %s\n", path);

		return r;
	}

	// Try to open the archive
	r = pakfire_archive_open(&archive, repo->pakfire, path);
	if (r < 0)
		goto ERROR;

	// Add it to this repository
	r = pakfire_repo_import_archive(repo, archive, package);
	if (r < 0)
		goto ERROR;

ERROR:
	if (archive)
		pakfire_archive_unref(archive);

	return r;
}

int pakfire_repo_read_solv(struct pakfire_repo* repo, FILE *f, int flags) {
	int r;

	// Import SOLV data
	r = repo_add_solv(repo->repo, f, flags);

	switch (r) {
		// Everything OK
		case 0:
			break;

		// Not SOLV format
		case 1:
		// Unsupported version
		case 2:
			return -ENOTSUP;

		// End of file
		case 3:
			return -EIO;

		// Corrupted
		case 4:
		case 5:
		case 6:
		default:
			return -EBADMSG;
	}

	pakfire_repo_has_changed(repo);

	return 0;
}

int pakfire_repo_write_solv(struct pakfire_repo* repo, FILE *f, int flags) {
	Repodata* meta = NULL;
	Queue addedfileprovides;
	int r;

	pakfire_pool_internalize(repo->pakfire);

	// Initialize file provides
	queue_init(&addedfileprovides);

	// Create another repodata section for meta information
	meta = repo_add_repodata(repo->repo, 0);
	if (!meta) {
		ERROR(repo->ctx, "Could not create meta repodata: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Set tool version
	repodata_set_str(meta, SOLVID_META, REPOSITORY_TOOLVERSION, LIBSOLV_TOOLVERSION);

	// Do not propagate this
	repodata_unset(meta, SOLVID_META, REPOSITORY_EXTERNAL);

	// Fetch file provides from pool
	pool_addfileprovides_queue(repo->repo->pool, &addedfileprovides, 0);
	if (addedfileprovides.count)
		repodata_set_idarray(meta, SOLVID_META,
			REPOSITORY_ADDEDFILEPROVIDES, &addedfileprovides);
	else
		repodata_unset(meta, SOLVID_META, REPOSITORY_ADDEDFILEPROVIDES);

	// Free some memory
	pool_freeidhashes(repo->repo->pool);

	// Internalize the repository data
	repodata_internalize(meta);

	// Export repository data
	r = repo_write(repo->repo, f);
	if (r)
		goto ERROR;

	// Flush buffers
	r = fflush(f);
	if (r) {
		ERROR(repo->ctx, "Could not flush after writing repository: %m\n");
		goto ERROR;
	}

ERROR:
	if (meta)
		repodata_free(meta);
	queue_free(&addedfileprovides);

	return r;
}

static int pakfire_repo_delete_all_packages(
		struct pakfire_repo* repo, const char* prefix) {
	struct pakfire_package* pkg = NULL;
	Solvable* s = NULL;
	int i = 0;
	int r;

	Pool* pool = repo->repo->pool;

	FOR_REPO_SOLVABLES(repo->repo, i, s) {
		Id id = pool_solvable2id(pool, s);

		// Allocate package
		r = pakfire_package_create_from_solvable(&pkg, repo->pakfire, repo, id);
		if (r)
			return 1;

		const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);
		const char* path = pakfire_package_get_string(pkg, PAKFIRE_PKG_PATH);

		// If prefix is set, skip anything that doesn't match
		if (prefix && !pakfire_string_startswith(path, prefix))
			goto NEXT;

		DEBUG(repo->ctx, "Removing %s at %s\n", nevra, path);

		// Delete the file
		r = unlink(path);
		if (r) {
			ERROR(repo->ctx, "Could not unlink %s at %s: %m\n", nevra, path);
		}

NEXT:
		pakfire_package_unref(pkg);
	}

	return 0;
}

int pakfire_repo_clean(struct pakfire_repo* repo, int flags) {
	char cache_path[PATH_MAX];
	int r;

	// Delete all temporary files from the command line repository
	if (pakfire_repo_name_equals(repo, PAKFIRE_REPO_COMMANDLINE)) {
		r = pakfire_repo_delete_all_packages(repo, PAKFIRE_TMP_DIR);
		if (r)
			return r;
	}

	// Drop all meta-data from memory
	repo_empty(repo->repo, 0);

	// End here for all "internal" repositories
	if (pakfire_repo_is_internal(repo))
		return 0;

	// Do not destroy files in local repositories
	if (pakfire_repo_is_local(repo)) {
		if (flags & PAKFIRE_REPO_CLEAN_FLAGS_DESTROY) {
			// Find path to repository
			const char* path = pakfire_repo_get_path(repo);
			if (!path)
				return 1;

			// Destroy it
			r = pakfire_rmtree(path, 0);
			if (r)
				return r;
		}

		return 0;
	}

	// Destroy all files in the cache directory
	r = pakfire_cache_path(repo->pakfire, cache_path,
		"repodata/%s", pakfire_repo_get_name(repo));
	if (r)
		return r;

	return pakfire_rmtree(cache_path, 0);
}

int pakfire_repo_refresh(struct pakfire_repo* repo, int force) {
	char path[PATH_MAX];
	int r;

	const char* name = pakfire_repo_get_name(repo);

	// Skip refreshing any "internal" repositories
	if (pakfire_repo_is_internal(repo))
		return 0;

	// Do nothing if this repository is not enabled
	int enabled = pakfire_repo_get_enabled(repo);
	if (!enabled) {
		DEBUG(repo->ctx, "Skip refreshing repository '%s'\n", name);
		return 0;
	}

	// If we are forced-refreshing a local repository, we simply scan the entire thing again
	if (force && pakfire_repo_is_local(repo))
		return pakfire_repo_scan(repo, 0);

	// Refresh mirrorlist
	r = pakfire_repo_refresh_mirrorlist(repo, force);
	if (r) {
		ERROR(repo->ctx, "Could not refresh mirrorlist, but will continue anyways...\n");
	}

	// Make the metadata path
	r = pakfire_repo_path(repo, path, "%s", "repomd.json");
	if (r)
		return r;

	// Parse metadata from disk
	r = pakfire_repo_read_metadata(repo, path);
	if (r < 0)
		return r;

	// Potentially download new metadata
	r = pakfire_repo_download_metadata(repo, path, force);
	if (r < 0)
		return r;

	// Read the database
	r = pakfire_repo_read_database(repo);
	if (r < 0)
		return r;

	return 0;
}

static int pakfire_repo_cleanup_metadata(struct pakfire_repo* self) {
	char path[PATH_MAX];
	int r;

	// Make sure we are running only on local repositories
	if (!pakfire_repo_is_local(self))
		return -ENOTSUP;

	// Make the path
	r = pakfire_repo_path(self, path, "%s", "repodata");
	if (r < 0)
		return r;

	// Remove it all
	return pakfire_rmtree(path, 0);
}

static int pakfire_repo_metadata_add_file(struct pakfire_repo* self,
		struct json_object* repomd, const char* type, const char* path) {
	enum pakfire_hash_type hash = PAKFIRE_HASH_UNDEFINED;
	struct pakfire_hashes checksums = {};
	struct json_object* files = NULL;
	struct json_object* file = NULL;
	struct json_object* chksums = NULL;
	char filename[PATH_MAX];
	char* hexdigest = NULL;
	struct stat st = {};
	int r;

	// Stat the file
	r = stat(path, &st);
	if (r < 0) {
		ERROR(self->ctx, "Could not stat %s: %m\n", path);
		r = -errno;
		goto ERROR;
	}

	// Make the filename
	r = pakfire_path_relative(filename, pakfire_repo_get_path(self), path);
	if (r < 0)
		goto ERROR;

	// Compute the database checksums
	r = pakfire_hash_path(self->ctx, path, METADATA_CHECKSUMS, &checksums);
	if (r < 0) {
		ERROR(self->ctx, "Failed to compute file hashes: %s\n", strerror(-r));
		goto ERROR;
	}

	// Fetch the files array
	files = json_object_object_get(repomd, "files");
	if (!files) {
		ERROR(self->ctx, "Repository metadata does not seem to have a files array\n");
		return -ENOENT;
	}

	// Create a new object
	file = pakfire_json_new_object();
	if (!file) {
		ERROR(self->ctx, "Failed to create a new file object: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Add the type
	r = pakfire_json_add_string(file, "type", type);
	if (r < 0)
		goto ERROR;

	// Add the filename
	r = pakfire_json_add_string(file, "filename", filename);
	if (r < 0)
		goto ERROR;

	// Add the filesize
	r = pakfire_json_add_uint64(file, "size", st.st_size);
	if (r < 0)
		goto ERROR;

	// Add checksums
	r = pakfire_json_add_object(file, "chksums", &chksums);
	if (r < 0)
		goto ERROR;

	// Add all checksums
	PAKFIRE_HASHES_FOREACH(hash) {
		r = pakfire_hashes_get_hex(&checksums, hash, &hexdigest);
		if (r < 0)
			goto ERROR;

		if (hexdigest) {
			r = pakfire_json_add_string(chksums, pakfire_hash_name(hash), hexdigest);
			if (r < 0)
				goto ERROR;

			free(hexdigest);
			hexdigest = NULL;
		}
	}

	// Append it to the files array
	r = json_object_array_add(files, file);
	if (r < 0) {
		ERROR(self->ctx, "Failed to add the file to the files array\n");
		goto ERROR;
	}

	return 0;

ERROR:
	if (file)
		json_object_put(file);
	if (hexdigest)
		free(hexdigest);

	return r;
}

static int pakfire_repo_write_database(struct pakfire_repo* self, struct json_object* repomd) {
	char filename[PATH_MAX];
	char path[PATH_MAX];
	char tmp[PATH_MAX];
	int r;

	// Create path for a temporary database export file
	r = pakfire_repo_path(self, tmp, "%s", "repodata/.pakfire-solv.XXXXXX");
	if (r < 0)
		return r;

	// Create a temporary file to write to
	FILE* f = pakfire_mktemp(tmp, 0644);
	if (!f) {
		ERROR(self->ctx, "Could not open temporary file for writing: %m\n");
		return 1;
	}

	// Initialize the output being compressed
	f = pakfire_zstdfopen(f, "w19");
	if (!f) {
		ERROR(self->ctx, "Could not initialize compression: %m\n");
		return 1;
	}

	// Write the SOLV database to the temporary file
	r = pakfire_repo_write_solv(self, f, 0);
	if (r) {
		ERROR(self->ctx, "Could not write SOLV data: %m\n");
		goto ERROR;
	}

	// Close file
	fclose(f);
	f = NULL;

	// Create a filename for the database file
	r = pakfire_strftime_now(filename, "%Y-%m-%d-%H%M.%s.solv.zst");
	if (r) {
		ERROR(self->ctx, "Could not format database filename: %m\n");
		goto ERROR;
	}

	// Make final database path
	r = pakfire_repo_path(self, path, "repodata/%s", filename);
	if (r < 0) {
		ERROR(self->ctx, "Could not join database path: %m\n");
		goto ERROR;
	}

	// Link database to its destination
	r = link(tmp, path);
	if (r < 0) {
		ERROR(self->ctx, "Could not link database %s: %m\n", path);
		r = -errno;
		goto ERROR;
	}

	// Add the database to the filelist
	r = pakfire_repo_metadata_add_file(self, repomd, "packages", path);
	if (r < 0)
		goto ERROR;

ERROR:
	if (f)
		fclose(f);

	// Remove temporary file
	unlink(tmp);

	return r;
}

static int pakfire_repo_make_metadata(struct pakfire_repo* self, struct json_object* md) {
	int r;

	// Set the version
	r = pakfire_json_add_int64(md, "version", 1);
	if (r < 0)
		return r;

	time_t revision = time(NULL);

	// Set revision
	r = pakfire_json_add_int64(md, "revision", revision);
	if (r < 0)
		return r;

	// Add a new files array
	r = pakfire_json_add_array(md, "files", NULL);
	if (r < 0)
		return r;

	return 0;
}

int pakfire_repo_write_metadata(struct pakfire_repo* self, struct pakfire_key* key) {
	struct json_object* repomd = NULL;
	char repomd_path[PATH_MAX];
	char sigpath[PATH_MAX];
	FILE* f = NULL;
	FILE* s = NULL;
	int r = 1;

	// This can only be called for local repositories
	if (!pakfire_repo_is_local(self))
		return -ENOTSUP;

	// Cleanup any previous metadata
	r = pakfire_repo_cleanup_metadata(self);
	if (r < 0)
		goto ERROR;

	// Make path to repomd.json
	r = pakfire_repo_path(self, repomd_path, "%s", "repomd.json");
	if (r < 0)
		goto ERROR;

	// Make the signature path
	r = pakfire_string_format(sigpath, "%s.sig", repomd_path);
	if (r < 0)
		goto ERROR;

	// Compose JSON object
	repomd = json_object_new_object();
	if (!repomd) {
		ERROR(self->ctx, "Could not allocate a new JSON object: %m\n");
		r = 1;
		goto ERROR;
	}

	// Fill the metadata object
	r = pakfire_repo_make_metadata(self, repomd);
	if (r < 0)
		goto ERROR;

	// Write the database
	r = pakfire_repo_write_database(self, repomd);
	if (r < 0)
		goto ERROR;

	// Open repomd.json for writing
	f = fopen(repomd_path, "w+");
	if (!f) {
		ERROR(self->ctx, "Could not open %s for writing: %m\n", repomd_path);
		r = -errno;
		goto ERROR;
	}

	// Write out repomd.json
	r = json_object_to_fd(fileno(f), repomd, 0);
	if (r) {
		ERROR(self->ctx, "Could not write repomd.json: %m\n");
		goto ERROR;
	}

	// Open the signature path for writing
	s = fopen(sigpath, "w");
	if (!s) {
		ERROR(self->ctx, "Could not open %s for writing: %m\n", sigpath);
		r = -errno;
		goto ERROR;
	}

	// Sign the metadata
	if (key) {
		r = pakfire_key_sign(key, s, f, NULL);
		if (r < 0) {
			ERROR(self->ctx, "Could not sign the repository: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Success
	r = 0;

ERROR:
	if (f)
		fclose(f);
	if (s)
		fclose(s);
	json_object_put(repomd);

	return r;
}

int pakfire_repo_compose(struct pakfire* pakfire, const char* path,
		struct pakfire_key* key, const char** files) {
	struct pakfire_archive* archive = NULL;
	struct pakfire_repo* repo = NULL;
	char realpath[PATH_MAX];
	char baseurl[PATH_MAX];
	int r;

	// path cannot be NULL
	if (!path) {
		errno = EINVAL;
		return 1;
	}

	// Fetch context
	struct pakfire_ctx* ctx = pakfire_ctx(pakfire);

	// XXX Check if the key is a secret key

	size_t num_files = 0;

	// Count files
	if (files) {
		for (const char** file = files; *file; file++)
			num_files++;
	}

	// Create the destination if it doesn't exist, yet
	pakfire_mkdir(path, 0755);

	// Make path absolute
	r = pakfire_path_realpath(realpath, path);
	if (r)
		goto ERROR;

	// Prefix path with file:// to form baseurl
	r = pakfire_string_format(baseurl, "file://%s", realpath);
	if (r)
		goto ERROR;

	// Create a new temporary repository at path
	r = pakfire_repo_create(&repo, pakfire, "tmp");
	if (r) {
		ERROR(ctx, "Could not create a temporary repository: %m\n");
		goto ERROR;
	}

	// Set baseurl to path
	r = pakfire_repo_set_baseurl(repo, baseurl);
	if (r) {
		ERROR(ctx, "Could not set baseurl %s: %m\n", baseurl);
		goto ERROR;
	}

	// Find everything that is already part of the repository
	r = pakfire_repo_scan(repo, 0);
	if (r) {
		ERROR(ctx, "Could not refresh repository: %m\n");
		goto ERROR;
	}

	DEBUG(ctx, "Adding %zu file(s) to the repository\n", num_files);

	// Add more files
	if (files) {
		for (const char** file = files; *file; file++) {
			DEBUG(ctx, "Adding %s to repository...\n", *file);

			// Open source archive
			r = pakfire_archive_open(&archive, pakfire, *file);
			if (r) {
				ERROR(ctx, "Could not open %s: %m\n", *file);
				goto OUT;
			}

			// Import the archive
			r = pakfire_repo_import_archive(repo, archive, NULL);
			if (r < 0) {
				ERROR(ctx, "Could not import %s: %s\n", *file, strerror(-r));
				goto OUT;
			}

OUT:
			if (archive) {
				pakfire_archive_unref(archive);
				archive = NULL;
			}

			if (r)
				goto ERROR;
		}
	}

	// Write metadata to disk
	r = pakfire_repo_write_metadata(repo, key);
	if (r)
		goto ERROR;

	// Success
	r = 0;

ERROR:
	if (repo) {
		pakfire_repo_clear(repo);
		pakfire_repo_unref(repo);
	}
	if (ctx)
		pakfire_ctx_unref(ctx);

	return r;
}

int pakfire_repo_walk_packages(struct pakfire_repo* self,
		int (*callback)(struct pakfire_ctx* ctx, struct pakfire_package* pkg, void* data), void* data, int flags) {
	struct pakfire_packagelist* list = NULL;
	int r;

	// Import all packages
	r = pakfire_repo_to_packagelist(self, &list);
	if (r < 0)
		goto ERROR;

	// Iterate over all packages
	r = pakfire_packagelist_walk(list, callback, data, flags);
	if (r < 0)
		goto ERROR;

ERROR:
	if (list)
		pakfire_packagelist_unref(list);

	return r;
}

struct pakfire_repo_walk_archives_state {
	pakfire_repo_walk_archives_callback callback;
	void* data;
};

static int __pakfire_repo_walk_archives(
		struct pakfire_ctx* ctx, struct pakfire_package* pkg, void* data) {
	const struct pakfire_repo_walk_archives_state* state = data;
	struct pakfire_archive* archive = NULL;
	int r;

	// Check if we actually have a callback
	if (!state->callback)
		return -EINVAL;

	// Fetch the archive
	r = pakfire_package_get_archive(pkg, &archive);
	if (r < 0)
		return r;

	// Call the callback
	r = state->callback(ctx, pkg, archive, state->data);

	// Cleanup
	pakfire_archive_unref(archive);

	return r;
}

int pakfire_repo_walk_archives(struct pakfire_repo* self,
		pakfire_repo_walk_archives_callback callback, void* data, int flags) {
	struct pakfire_packagelist* list = NULL;
	int r;

	struct pakfire_repo_walk_archives_state state = {
		.callback = callback,
		.data     = data,
	};

	// This only works on local repositories
	if (!pakfire_repo_is_local(self))
		return -ENOTSUP;

	// Import all packages
	r = pakfire_repo_to_packagelist(self, &list);
	if (r < 0)
		goto ERROR;

	// Iterate over all packages
	r = pakfire_packagelist_walk(list, __pakfire_repo_walk_archives, &state, flags);
	if (r < 0)
		goto ERROR;

ERROR:
	if (list)
		pakfire_packagelist_unref(list);

	return r;
}
