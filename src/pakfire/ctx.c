/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>

#include <curl/curl.h>
#include <magic.h>

#include <pakfire/config.h>
#include <pakfire/ctx.h>
#include <pakfire/logging.h>
#include <pakfire/os.h>
#include <pakfire/path.h>
#include <pakfire/string.h>

struct pakfire_ctx {
	// Reference counter
	int nrefs;

	// Flags
	int flags;

	// Config
	struct pakfire_config* config;

	// Distro
	struct pakfire_distro distro;

	// Logging
	struct pakfire_ctx_log {
		int level;

		// Callback
		pakfire_log_callback callback;
		void* data;
	} log;

	// Paths
	struct pakfire_ctx_paths {
		char cache[PATH_MAX];
	} paths;

	// Confirm
	struct pakfire_ctx_confirm {
		pakfire_confirm_callback callback;
		void* data;
	} confirm;

	// Progress
	struct pakfire_ctx_progress {
		pakfire_progress_callback callback;
		void* data;
	} progress;

	// Pick Solution
	struct pakfire_ctx_pick_solution {
		pakfire_pick_solution_callback callback;
		void* data;
	} pick_solution;

	// cURL share handle
	CURLSH* share;

	// Magic Context
	magic_t magic;
};

static int parse_log_level(const char* level) {
	char* end = NULL;

	int l = strtol(level, &end, 10);

	if (*end == '\0' || isspace(*end))
		return l;

	if (strcmp(level, "error") == 0)
		return LOG_ERR;

	if (strcmp(level, "warning") == 0)
		return LOG_WARNING;

	if (strcmp(level, "info") == 0)
		return LOG_INFO;

	if (strcmp(level, "debug") == 0)
		return LOG_DEBUG;

	return 0;
}

static int pakfire_ctx_setup_logging(struct pakfire_ctx* ctx) {
	const char* env = NULL;

	// Set the default level to INFO
	int level = LOG_INFO;

	// Try setting the log level from the environment
	env = secure_getenv("PAKFIRE_LOG");
	if (env)
		level = parse_log_level(env);

	// Store the log level
	pakfire_ctx_set_log_level(ctx, level);

	// Log to syslog by default
	pakfire_ctx_set_log_callback(ctx, pakfire_log_syslog, NULL);

	return 0;
}

static int pakfire_ctx_default_confirm_callback(struct pakfire_ctx* ctx,
		struct pakfire* pakfire, void* data, const char* message, const char* question) {
	// Just log the message
	INFO(ctx, "%s\n", message);

	return 0;
}

static int pakfire_ctx_load_config(struct pakfire_ctx* ctx, const char* path) {
	FILE* f = NULL;
	int r;

	// Load some default configuration if not path was provided
	if (!path)
		path = PAKFIRE_CONFIG_DIR "/pakfire.conf";

	// Open the configuration file
	f = fopen(path, "r");
	if (!f) {
		switch (errno) {
			// Ignore if the file does not exist
			case ENOENT:
				return 0;

			default:
				return -errno;
		}
	}

	// Read the configuration file
	r = pakfire_config_read(ctx->config, f);

	// Cleanup
	fclose(f);

	return r;
}

static void pakfire_ctx_free(struct pakfire_ctx* ctx) {
	// Release cURL Share Handle
	if (ctx->share)
		curl_share_cleanup(ctx->share);

	// Release Magic Context
	if (ctx->magic)
		magic_close(ctx->magic);

	if (ctx->config)
		pakfire_config_unref(ctx->config);
	free(ctx);
}

int pakfire_ctx_create(struct pakfire_ctx** ctx, const char* path) {
	struct pakfire_ctx* c = NULL;
	int r;

	// Allocate the context
	c = calloc(1, sizeof(*c));
	if (!c)
		return -errno;

	// Initialize the reference counter
	c->nrefs = 1;

	// Initialize the configuration
	r = pakfire_config_create(&c->config);
	if (r)
		goto ERROR;

	// Load configuration
	r = pakfire_ctx_load_config(c, path);
	if (r)
		goto ERROR;

	// Setup logging
	r = pakfire_ctx_setup_logging(c);
	if (r)
		goto ERROR;

	// Setup the default cache path
	r = pakfire_ctx_set_cache_path(c, PAKFIRE_CACHE_DIR);
	if (r)
		goto ERROR;

	// Load the distribution information
	r = pakfire_distro(&c->distro, NULL);
	if (r)
		goto ERROR;

	// Set the default confirm callback
	pakfire_ctx_set_confirm_callback(c, pakfire_ctx_default_confirm_callback, NULL);

	// Return the pointer
	*ctx = c;

	return 0;

ERROR:
	if (c)
		pakfire_ctx_free(c);

	return r;
}

struct pakfire_ctx* pakfire_ctx_ref(struct pakfire_ctx* ctx) {
	ctx->nrefs++;

	return ctx;
}

struct pakfire_ctx* pakfire_ctx_unref(struct pakfire_ctx* ctx) {
	if (--ctx->nrefs > 0)
		return ctx;

	pakfire_ctx_free(ctx);
	return NULL;
}

// Flags

int pakfire_ctx_has_flag(struct pakfire_ctx* ctx, int flag) {
	return ctx->flags & flag;
}

int pakfire_ctx_set_flag(struct pakfire_ctx* ctx, int flag) {
	ctx->flags |= flag;

	return 0;
}

// Config

struct pakfire_config* pakfire_ctx_get_config(struct pakfire_ctx* ctx) {
	if (!ctx->config)
		return NULL;

	return pakfire_config_ref(ctx->config);
}

// Distro

const struct pakfire_distro* pakfire_ctx_get_distro(struct pakfire_ctx* ctx) {
	return &ctx->distro;
}

// Logging

int pakfire_ctx_get_log_level(struct pakfire_ctx* ctx) {
	return ctx->log.level;
}

void pakfire_ctx_set_log_level(struct pakfire_ctx* ctx, int level) {
	ctx->log.level = level;
}

void pakfire_ctx_set_log_callback(struct pakfire_ctx* ctx,
		pakfire_log_callback callback, void* data) {
	ctx->log.callback = callback;
	ctx->log.data     = data;
}

void pakfire_ctx_log(struct pakfire_ctx* ctx, int level, const char* file, int line,
		const char* fn, const char* format, ...) {
	va_list args;

	// Return if the callback hasn't been set
	if (!ctx->log.callback)
		return;

	va_start(args, format);
	ctx->log.callback(ctx->log.data, level, file, line, fn, format, args);
	va_end(args);
}

// Paths

const char* pakfire_ctx_get_cache_path(struct pakfire_ctx* ctx) {
	return ctx->paths.cache;
}

int pakfire_ctx_set_cache_path(struct pakfire_ctx* ctx, const char* path) {
	return pakfire_path_expand(ctx->paths.cache, path);
}

// Confirm

void pakfire_ctx_set_confirm_callback(struct pakfire_ctx* ctx,
		pakfire_confirm_callback callback, void* data) {
	ctx->confirm.callback = callback;
	ctx->confirm.data     = data;
}

int pakfire_ctx_confirm(struct pakfire_ctx* ctx, struct pakfire* pakfire,
		const char* message, const char* question) {
	// Run callback
	if (!ctx->confirm.callback)
		return 0;

	return ctx->confirm.callback(ctx, pakfire, ctx->confirm.data, message, question);
}

// Progress

void pakfire_ctx_set_progress_callback(struct pakfire_ctx* ctx,
		pakfire_progress_callback callback, void* data) {
	ctx->progress.callback = callback;
	ctx->progress.data     = data;
}

int pakfire_ctx_setup_progress(struct pakfire_ctx* ctx, struct pakfire_progress* progress) {
	if (!ctx->progress.callback)
		return 0;

	return ctx->progress.callback(ctx, ctx->progress.data, progress);
}

// Pick Solution

void pakfire_ctx_set_pick_solution_callback(struct pakfire_ctx* ctx,
		pakfire_pick_solution_callback callback, void* data) {
	ctx->pick_solution.callback = callback;
	ctx->pick_solution.data     = data;

}

int pakfire_ctx_pick_solution(struct pakfire_ctx* ctx, struct pakfire* pakfire,
		struct pakfire_transaction* transaction) {
	if (!ctx->pick_solution.callback)
		return 1;

	return ctx->pick_solution.callback(ctx, pakfire, ctx->pick_solution.data, transaction);
}

// cURL

CURLSH* pakfire_ctx_curl_share(struct pakfire_ctx* ctx) {
	int r;

	// Setup a new handle
	if (!ctx->share) {
		ctx->share = curl_share_init();
		if (!ctx->share) {
			ERROR(ctx, "Could not setup cURL share handle\n");
			goto ERROR;
		}

		// Share connections between handles
		r = curl_share_setopt(ctx->share, CURLSHOPT_SHARE, CURL_LOCK_DATA_CONNECT);
		if (r) {
			ERROR(ctx, "Could not configure the share handle: %s\n", curl_share_strerror(r));
			goto ERROR;
		}
	}

	return ctx->share;

ERROR:
	if (ctx->share) {
		curl_share_cleanup(ctx->share);
		ctx->share = NULL;
	}

	return NULL;
}

// Magic

magic_t pakfire_ctx_magic(struct pakfire_ctx* ctx) {
	int r;

	// Initialize the context if not already done
	if (!ctx->magic) {
		// Allocate a new context
		ctx->magic = magic_open(MAGIC_MIME_TYPE | MAGIC_ERROR | MAGIC_NO_CHECK_TOKENS);
		if (!ctx->magic) {
			ERROR(ctx, "Could not allocate magic context: %m\n");
			return NULL;
		}

		// Load the database
		r = magic_load(ctx->magic, NULL);
		if (r) {
			ERROR(ctx, "Could not open the magic database: %s\n", magic_error(ctx->magic));
			goto ERROR;
		}
	}

	return ctx->magic;

ERROR:
	if (ctx->magic)
		magic_close(ctx->magic);

	// Reset the pointer
	ctx->magic = NULL;

	return NULL;
}
