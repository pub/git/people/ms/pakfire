/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pakfire/parse.h>
#include <pakfire/string.h>

int pakfire_parse_file(const char* path, pakfire_parse_line parse, void* data) {
	FILE* f = NULL;
	char* line = NULL;
	size_t length = 0;
	int r = 0;

	ssize_t bytes_read = 0;

	// Open the file
	f = fopen(path, "r");
	if (!f)
		return -errno;

	// Walk through the file line by line
	for (;;) {
		bytes_read = getline(&line, &length, f);
		if (bytes_read < 0)
			break;

		// Remove the trailing newline
		if (line[length - 1] == '\n')
			line[--length] = '\0';

		// Call the parse callback function
		r = parse(line, length, data);
		if (r)
			goto ERROR;
	}

ERROR:
	if (line)
		free(line);
	if (f)
		fclose(f);

	return r;
}

int pakfire_parse_split_line(char* line, size_t length, char** key, char** value, char delim) {
	char* k = line;
	char* v = NULL;
	char* p = NULL;

	// Strip any trailing whitespace
	pakfire_string_strip(line);

	if (!*line)
		return 0;

	// Find the delimiter
	p = strchr(line, delim);
	if (!p)
		return -EINVAL;

	// The value starts somewhere after the delimiter
	v = p + 1;

	// Replace the delimiter by NULL
	*p = '\0';

	// Strip the key
	pakfire_string_strip(k);

	// The key is empty
	if (!*k)
		return -EINVAL;

	// Strip the value
	pakfire_string_strip(v);

	// Return the pointers
	*key   = k;
	*value = v;

	return 0;
}
