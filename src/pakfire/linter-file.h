/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_LINTER_FILE_H
#define PAKFIRE_LINTER_FILE_H

struct pakfire_linter_file;

#include <pakfire/ctx.h>
#include <pakfire/file.h>
#include <pakfire/linter.h>

int pakfire_linter_file_create(struct pakfire_linter_file** lfile,
	struct pakfire_ctx* ctx, struct pakfire_linter* linter, struct pakfire_file* file, int fd);

struct pakfire_linter_file* pakfire_linter_file_ref(struct pakfire_linter_file* lfile);
struct pakfire_linter_file* pakfire_linter_file_unref(struct pakfire_linter_file* lfile);

int pakfire_linter_file_lint(struct pakfire_linter_file* lfile);

#endif /* PAKFIRE_LINTER_FILE_H */
