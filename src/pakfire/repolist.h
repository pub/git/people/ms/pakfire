/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_REPOLIST_H
#define PAKFIRE_REPOLIST_H

#include <pakfire/repo.h>

struct pakfire_repolist;

int pakfire_repolist_create(struct pakfire_repolist** list);

struct pakfire_repolist* pakfire_repolist_ref(struct pakfire_repolist* list);
struct pakfire_repolist* pakfire_repolist_unref(struct pakfire_repolist* list);

void pakfire_repolist_clear(struct pakfire_repolist* list);

size_t pakfire_repolist_size(struct pakfire_repolist* list);
int pakfire_repolist_empty(struct pakfire_repolist* list);

struct pakfire_repo* pakfire_repolist_get(struct pakfire_repolist* list, size_t index);
int pakfire_repolist_append(struct pakfire_repolist* list, struct pakfire_repo* repo);

#endif /* PAKFIRE_REPOLIST_H */
