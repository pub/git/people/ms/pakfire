/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2013 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_REPO_H
#define PAKFIRE_REPO_H

#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include <solv/repo.h>

struct pakfire_repo;

#include <pakfire/archive.h>
#include <pakfire/config.h>
#include <pakfire/httpclient.h>
#include <pakfire/key.h>
#include <pakfire/package.h>
#include <pakfire/packagelist.h>
#include <pakfire/pakfire.h>
#include <pakfire/xfer.h>

int pakfire_repo_create(struct pakfire_repo** repo, struct pakfire* pakfire, const char* name);
int pakfire_repo_open(struct pakfire_repo** repo, struct pakfire* pakfire, Repo* _repo);

struct pakfire_repo* pakfire_repo_ref(struct pakfire_repo* repo);
struct pakfire_repo* pakfire_repo_unref(struct pakfire_repo* repo);

int pakfire_repo_clear(struct pakfire_repo* repo);

int pakfire_repo_identical(struct pakfire_repo* repo1, struct pakfire_repo* repo2);
int pakfire_repo_cmp(struct pakfire_repo* repo1, struct pakfire_repo* repo2);
int pakfire_repo_count(struct pakfire_repo* repo);

const char* pakfire_repo_get_name(struct pakfire_repo* repo);

const char* pakfire_repo_get_description(struct pakfire_repo* repo);
int pakfire_repo_set_description(struct pakfire_repo* repo, const char* description);

int pakfire_repo_get_enabled(struct pakfire_repo* repo);
void pakfire_repo_set_enabled(struct pakfire_repo* repo, int enabled);

int pakfire_repo_get_priority(struct pakfire_repo* repo);
void pakfire_repo_set_priority(struct pakfire_repo* repo, int priority);

const char* pakfire_repo_get_baseurl(struct pakfire_repo* repo);
int pakfire_repo_set_baseurl(struct pakfire_repo* repo, const char* baseurl);

struct pakfire_key* pakfire_repo_get_key(struct pakfire_repo* repo);

const char* pakfire_repo_get_mirrorlist_url(struct pakfire_repo* repo);
int pakfire_repo_set_mirrorlist_url(struct pakfire_repo* repo, const char* url);

struct pakfire_mirrorlist* pakfire_repo_get_mirrors(struct pakfire_repo* repo);

#define pakfire_repo_path(repo, path, format, ...) \
	__pakfire_repo_path(repo, path, sizeof(path), format, __VA_ARGS__)
int __pakfire_repo_path(struct pakfire_repo* repo,
	char* path, const size_t length, const char* format, ...)
	__attribute__((format(printf, 4, 5)));

int pakfire_repo_write_config(struct pakfire_repo* repo, FILE* f);

int pakfire_repo_is_installed_repo(struct pakfire_repo* repo);

int pakfire_repo_read_solv(struct pakfire_repo* repo, FILE *f, int flags);
int pakfire_repo_write_solv(struct pakfire_repo* repo, FILE *f, int flags);

// Cache

enum pakfire_repo_clean_flags {
	PAKFIRE_REPO_CLEAN_FLAGS_NONE     = 0,
	PAKFIRE_REPO_CLEAN_FLAGS_DESTROY  = (1 << 0),
};

int pakfire_repo_clean(struct pakfire_repo* repo, int flags);

// Scan

int pakfire_repo_scan(struct pakfire_repo* repo, int flags);

// Refresh

int pakfire_repo_refresh(struct pakfire_repo* repo, int force);

// Compose

int pakfire_repo_write_metadata(struct pakfire_repo* repo, struct pakfire_key* key);

int pakfire_repo_compose(struct pakfire* pakfire, const char* path,
	struct pakfire_key* key, const char** files);

#define PAKFIRE_REPO_COMMANDLINE		"@commandline"
#define PAKFIRE_REPO_DUMMY				"@dummy"
#define PAKFIRE_REPO_LOCAL              "local"
#define PAKFIRE_REPO_RESULT             "@build"
#define PAKFIRE_REPO_SYSTEM				"@system"

int pakfire_repo_name_equals(struct pakfire_repo* repo, const char* name);
int pakfire_repo_is_internal(struct pakfire_repo* repo);
int pakfire_repo_is_local(struct pakfire_repo* repo);

char* pakfire_repo_url_replace(struct pakfire_repo* repo, const char* url);

int pakfire_repo_import(struct pakfire* pakfire, struct pakfire_config* config);
const char* pakfire_repo_get_path(struct pakfire_repo* repo);

void pakfire_repo_has_changed(struct pakfire_repo* repo);
int pakfire_repo_internalize(struct pakfire_repo* repo, int flags);
Id pakfire_repo_add_solvable(struct pakfire_repo* repo);

#define pakfire_repo_make_path(repo, path, archive, pkg) \
	__pakfire_repo_make_path(repo, path, sizeof(path), archive, pkg)
int __pakfire_repo_make_path(struct pakfire_repo* self,
	char* path, size_t length, struct pakfire_archive* archive, struct pakfire_package* pkg);

int pakfire_repo_import_archive(struct pakfire_repo* self,
	struct pakfire_archive* archive, struct pakfire_package** package);

int pakfire_repo_download_package(struct pakfire_xfer** xfer,
	struct pakfire_repo* repo, struct pakfire_package* pkg);

int pakfire_repo_add(struct pakfire_repo* repo, const char* path,
	struct pakfire_package** package);

void pakfire_repo_free_all(struct pakfire* pakfire);

Repo* pakfire_repo_get_repo(struct pakfire_repo* repo);
Repodata* pakfire_repo_get_repodata(struct pakfire_repo* repo);

struct pakfire_mirrorlist* pakfire_repo_get_mirrorlist(struct pakfire_repo* repo);

int pakfire_repo_walk_packages(struct pakfire_repo* self,
	int (*callback)(struct pakfire_ctx* ctx, struct pakfire_package* pkg, void* data), void* data, int flags);

typedef int (*pakfire_repo_walk_archives_callback)
	(struct pakfire_ctx* ctx, struct pakfire_package* pkg, struct pakfire_archive* archive, void* data);

int pakfire_repo_walk_archives(struct pakfire_repo* self,
	pakfire_repo_walk_archives_callback callback, void* data, int flags);

#endif /* PAKFIRE_REPO_H */
