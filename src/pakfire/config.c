/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>

#include <pakfire/config.h>
#include <pakfire/string.h>
#include <pakfire/util.h>

#define KEY_MAX_LENGTH		256
#define VALUE_MAX_LENGTH	4096
#define SECTION_MAX_LENGTH	256
#define LINE_MAX_LENGTH		1024

struct pakfire_config_section {
	STAILQ_ENTRY(pakfire_config_section) nodes;

	// Section name
	char name[SECTION_MAX_LENGTH];

	// Entries
	STAILQ_HEAD(entries, pakfire_config_entry) entries;
};

struct pakfire_config_entry {
	STAILQ_ENTRY(pakfire_config_entry) nodes;

	// Key & Value
	char key[KEY_MAX_LENGTH];
	char value[VALUE_MAX_LENGTH];
};

struct pakfire_config {
	int nrefs;

	STAILQ_HEAD(sections, pakfire_config_section) sections;
};

int pakfire_config_create(struct pakfire_config** config) {
	struct pakfire_config* c = calloc(1, sizeof(*c));
	if (!c)
		return 1;

	// Initialize reference counter
	c->nrefs = 1;

	// Initialise entries
	STAILQ_INIT(&c->sections);

	*config = c;
	return 0;
}

static void pakfire_config_free_entry(struct pakfire_config_entry* entry) {
	free(entry);
}

static void pakfire_config_free_section(struct pakfire_config_section* section) {
	struct pakfire_config_entry* entry = NULL;

	// Free all entries
	while (!STAILQ_EMPTY(&section->entries)) {
		entry = STAILQ_FIRST(&section->entries);
		STAILQ_REMOVE_HEAD(&section->entries, nodes);

		pakfire_config_free_entry(entry);
	}

	free(section);
}

static void pakfire_config_free(struct pakfire_config* config) {
	struct pakfire_config_section* section = NULL;

	// Free all sections
	while (!STAILQ_EMPTY(&config->sections)) {
		section = STAILQ_FIRST(&config->sections);
		STAILQ_REMOVE_HEAD(&config->sections, nodes);

		pakfire_config_free_section(section);
	}

	free(config);
}

struct pakfire_config* pakfire_config_ref(struct pakfire_config* config) {
	++config->nrefs;

	return config;
}

struct pakfire_config* pakfire_config_unref(struct pakfire_config* config) {
	if (--config->nrefs > 0)
		return config;

	pakfire_config_free(config);
	return NULL;
}

static struct pakfire_config_section* pakfire_config_create_section(
		struct pakfire_config* config, const char* name) {
	struct pakfire_config_section* section = NULL;
	int r;

	// Allocate a new section
	section = calloc(1, sizeof(*section));
	if (!section)
		goto ERROR;

	// Initialize entries
	STAILQ_INIT(&section->entries);

	// Store name
	r = pakfire_string_set(section->name, name);
	if (r)
		goto ERROR;

	// Append it to the list of sections
	STAILQ_INSERT_TAIL(&config->sections, section, nodes);

	// Return the section
	return section;

ERROR:
	if (section)
		pakfire_config_free_section(section);

	return NULL;
}

static struct pakfire_config_section* pakfire_config_get_section(
		struct pakfire_config* config, const char* name) {
	struct pakfire_config_section* section = NULL;

	// Find an existing section
	STAILQ_FOREACH(section, &config->sections, nodes) {
		if (strcmp(section->name, name) == 0)
			return section;
	}

	return NULL;
}

static int pakfire_config_entry_is_multiline(const struct pakfire_config_entry* entry) {
	const char* p = strchr(entry->value, '\n');

	if (p)
		return 1;

	return 0;
}

static struct pakfire_config_entry* pakfire_config_create_entry(
		struct pakfire_config* config, const char* name, const char* key) {
	struct pakfire_config_section* section = NULL;
	struct pakfire_config_entry* entry = NULL;
	int r;

	// Allocate a new entry
	entry = calloc(1, sizeof(*entry));
	if (!entry)
		goto ERROR;

	// Store key
	r = pakfire_string_set(entry->key, key);
	if (r)
		goto ERROR;

	// Fetch the section
	section = pakfire_config_get_section(config, name);
	if (!section) {
		section = pakfire_config_create_section(config, name);
		if (!section)
			goto ERROR;
	}

	// Append it to the list of entries
	STAILQ_INSERT_TAIL(&section->entries, entry, nodes);

	return entry;

ERROR:
	if (entry)
		pakfire_config_free_entry(entry);

	return NULL;
}

static struct pakfire_config_entry* pakfire_config_find(struct pakfire_config* config,
		const char* name, const char* key) {
	struct pakfire_config_section* section = NULL;
	struct pakfire_config_entry* entry = NULL;

	// Check for valid input
	if (!key) {
		errno = EINVAL;
		return NULL;
	}

	// Treat NULL as an empty section
	if (!name)
		name = "";

	// Fetch the section
	section = pakfire_config_get_section(config, name);
	if (!section)
		return NULL;

	STAILQ_FOREACH(entry, &section->entries, nodes) {
		if (strcmp(entry->key, key) == 0)
			return entry;
	}

	// No match
	return NULL;
}

int pakfire_config_set(struct pakfire_config* config,
		const char* section, const char* key, const char* value) {
	// Check if this entry exists
	struct pakfire_config_entry* entry = pakfire_config_find(config, section, key);

	// Create a new entry if it doesn't
	if (!entry) {
		entry = pakfire_config_create_entry(config, section, key);
		if (!entry)
			return 1;
	}

	// Store the value
	return pakfire_string_set(entry->value, value);
}

int pakfire_config_set_format(struct pakfire_config* config,
		const char* section, const char* key, const char* format, ...) {
	char* buffer = NULL;
	va_list args;
	int r;

	va_start(args, format);
	r = vasprintf(&buffer, format, args);
	va_end(args);

	// Break on any errors
	if (r < 0)
		return r;

	// Set the value
	r = pakfire_config_set(config, section, key, buffer);

	// Cleanup
	if (buffer)
		free(buffer);

	return r;
}

static int pakfire_config_append(struct pakfire_config* config,
		const char* section, const char* key, const char* value2) {
	char* buffer = NULL;
	int r;

	// Fetch the current value
	const char* value1 = pakfire_config_get(config, section, key, NULL);

	// Short cut if value1 is empty
	if (!value1 || !*value1)
		return pakfire_config_set(config, section, key, value2);

	// Join both values together
	r = asprintf(&buffer, "%s\n%s", value1, value2);
	if (r < 0)
		return 1;

	// Set the new value
	r = pakfire_config_set(config, section, key, buffer);

	if (buffer)
		free(buffer);

	return r;
}

const char* pakfire_config_get(struct pakfire_config* config,
		const char* section, const char* key, const char* _default) {
	struct pakfire_config_entry* entry = pakfire_config_find(config, section, key);

	// Return the value if set
	if (entry && *entry->value)
		return entry->value;

	// Otherwise return the default value
	return _default;
}

long int pakfire_config_get_int(struct pakfire_config* config,
		const char* section, const char* key, long int _default) {
	struct pakfire_config_entry* entry = pakfire_config_find(config, section, key);

	// Return the value if set
	if (entry && *entry->value)
		return strtol(entry->value, NULL, 10);

	// Otherwise return the default value
	return _default;
}

int pakfire_config_get_bool(struct pakfire_config* config,
		const char* section, const char* key, int _default) {
	struct pakfire_config_entry* entry = pakfire_config_find(config, section, key);

	// Return the value if set
	if (entry && *entry->value) {
		static const struct boolean {
			const char* string;
			int ret;
		} booleans[] = {
			{ "true",  1 },
			{ "yes",   1 },
			{ "on",    1 },
			{ "1",     1 },
			{ "false", 0 },
			{ "no",    0 },
			{ "off",   0 },
			{ "0",     0 },
			{ NULL,    0 },
		};

		for (const struct boolean* boolean = booleans; boolean->string; boolean++) {
			if (strcmp(entry->value, boolean->string) == 0)
				return boolean->ret;
		}
	}

	// Otherwise return the default value
	return _default;
}

size_t pakfire_config_get_bytes(struct pakfire_config* config,
		const char* section, const char* key, const size_t _default) {
	struct pakfire_config_entry* entry = pakfire_config_find(config, section, key);

	if (entry && *entry->value)
		return pakfire_string_parse_bytes(entry->value);

	// Otherwise return the default
	return _default;
}

int pakfire_config_walk_sections(struct pakfire_config* config,
		pakfire_config_walk_callback callback, void* p) {
	struct pakfire_config_section* default_section = NULL;
	struct pakfire_config_section* section = NULL;
	int r = 0;

	// Try to fetch the default section
	default_section = pakfire_config_get_section(config, "");

	// Run the default section first
	if (default_section) {
		r = callback(config, default_section->name, p);
		if (r)
			goto ERROR;
	}

	// Then iterate over all other sections
	STAILQ_FOREACH(section, &config->sections, nodes) {
		// Skip the default section
		if (section == default_section)
			continue;

		r = callback(config, section->name, p);
		if (r)
			goto ERROR;
	}

ERROR:
	return r;
}

int pakfire_config_has_section(struct pakfire_config* config, const char* name) {
	struct pakfire_config_section* section = NULL;

	// Fetch the section
	section = pakfire_config_get_section(config, name);
	if (section)
		return 1;

	return 0;
}

int pakfire_config_read(struct pakfire_config* config, FILE* f) {
	char section[SECTION_MAX_LENGTH] = "";
	char key[KEY_MAX_LENGTH] = "";
	int r = -EINVAL;

	char* line = NULL;
	size_t l = 0;

	while (!feof(f)) {
		ssize_t length = getline(&line, &l, f);
		if (length < 0)
			break;

		// Break if there was an error
		if (ferror(f))
			break;

		// Terminate line after comment sign
		char* comment = strchr(line, '#');
		if (comment)
			*comment = '\0';

		// Remove trailing space
		pakfire_string_rstrip(line);

		// Skip empty lines
		if (*line == '\0')
			continue;

		// Update length after rstrip()
		length = strlen(line);

		// Is this a new section?
		if (line[0] == '[' && line[length - 1] == ']') {
			line[length - 1] = '\0';

			// Clear the previous key
			*key = '\0';

			r = pakfire_string_set(section, line + 1);
			if (r)
				goto ERROR;

			continue;
		}

		// Check if this is a multiline value
		if (isspace(*line)) {
			// We cannot assign this value anywhere
			if (!*key) {
				r = -EINVAL;
				goto ERROR;
			}

			// Strip any leading whitespace
			pakfire_string_lstrip(line);

			// Append the value to the existing one
			r = pakfire_config_append(config, section, key, line);
			if (r)
				goto ERROR;

			// Skip to the next line
			continue;
		}

		// Handle any assignments

		// Find the separator
		char* value = strchr(line, '=');
		if (!value)
			continue;

		// Split the string
		*value++ = '\0';

		// Remove trailing space
		pakfire_string_strip(line);
		pakfire_string_strip(value);

		// Update the key
		pakfire_string_set(key, line);

		// Store the value
		r = pakfire_config_set(config, section, key, value);
		if (r)
			goto ERROR;
	}

ERROR:
	if (line)
		free(line);

	return r;
}

int pakfire_config_read_path(struct pakfire_config* config, const char* path) {
	FILE* f = NULL;
	int r;

	// Open the file
	f = fopen(path, "r");
	if (!f)
		return -errno;

	// Read the configuration
	r = pakfire_config_read(config, f);

	// Close the file
	fclose(f);

	return r;
}

int pakfire_config_parse(struct pakfire_config* config, const char* s, ssize_t length) {
	FILE* f = NULL;
	int r;

	// Check inputs
	if (!s)
		return -EINVAL;

	// Automatically determine the length
	if (length < 0)
		length = strlen(s);

	// Don't try to parse empty input
	if (!length)
		return 0;

	// Open a memory stream
	f = fmemopen((char*)s, length, "r");
	if (!f)
		return -errno;

	// Read the buffer
	r = pakfire_config_read(config, f);

	// Close the buffer
	fclose(f);

	return r;
}

static int pakfire_config_dump_multiline_entry(struct pakfire_config_entry* entry, FILE* f) {
	char* value = NULL;
	char* p = NULL;
	int r;

	// Write the key line
	r = fprintf(f, "%s =\n", entry->key);
	if (r <= 0)
		goto ERROR;

	// Copy the value
	value = strdup(entry->value);
	if (!value) {
		r = 1;
		goto ERROR;
	}

	// Write it line by line
	char* line = strtok_r(value, "\n", &p);
	while (line) {
		r = fprintf(f, "  %s\n", line);
		if (r <= 0)
			goto ERROR;

		line = strtok_r(NULL, "\n", &p);
	}

	// Success
	r = 0;

ERROR:
	if (value)
		free(value);

	return r;
}

static int pakfire_config_dump_entry(struct pakfire_config_entry* entry, FILE* f) {
	int r;

	// Handle multiline entries separately
	if (pakfire_config_entry_is_multiline(entry))
		return pakfire_config_dump_multiline_entry(entry, f);

	// Write the entry
	r = fprintf(f, "%s = %s\n", entry->key, entry->value);
	if (r <= 0)
		return r;

	return 0;
}

static int pakfire_config_dump_section(struct pakfire_config* config,
		const char* name, void* p) {
	struct pakfire_config_section* section = NULL;
	struct pakfire_config_entry* entry = NULL;
	int r = 0;

	FILE* f = (FILE*)p;

	// Fetch the section
	section = pakfire_config_get_section(config, name);
	if (!section)
		return -errno;

	// Print the section name
	if (*name) {
		r = fprintf(f, "\n[%s]\n", name);
		if (r <= 0)
			goto ERROR;
	}

	// Iterate over all entries
	STAILQ_FOREACH(entry, &section->entries, nodes) {
		r = pakfire_config_dump_entry(entry, f);
		if (r)
			goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	return r;
}

int pakfire_config_dump(struct pakfire_config* config, FILE* f) {
	return pakfire_config_walk_sections(config, pakfire_config_dump_section, f);
}
