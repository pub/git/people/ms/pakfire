/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_PARSE_H
#define PAKFIRE_PARSE_H

/*
	A callback that is called for each line that there is to parse.
*/
typedef int (*pakfire_parse_line)(char* line, size_t length, void* data);

// Parses the given file
int pakfire_parse_file(const char* path, pakfire_parse_line parse, void* data);

// Splits a line into two parts separated by the given delimiter
int pakfire_parse_split_line(char* line, size_t length, char** key, char** value, char delim);

#endif /* PAKFIRE_PARSE_H */
