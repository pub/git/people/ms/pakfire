/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2017 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_LOGGING_H
#define PAKFIRE_LOGGING_H

#include <stdarg.h>
#include <syslog.h>

typedef void (*pakfire_log_callback)(void* data, int priority, const char* file,
	int line, const char* fn, const char* format, va_list args);

#include <pakfire/ctx.h>
#include <pakfire/pakfire.h>

void pakfire_log_stderr(void* data, int priority, const char* file,
	int line, const char* fn, const char* format, va_list args)
	__attribute__((format(printf, 6, 0)));
void pakfire_log_syslog(void* data, int priority, const char* file,
	int line, const char* fn, const char* format, va_list args)
	__attribute__((format(printf, 6, 0)));

#define pakfire_ctx_log_condition(ctx, level, arg...) \
	do { \
		if (pakfire_ctx_get_log_level(ctx) >= level) \
			pakfire_ctx_log(ctx, level, __FILE__, __LINE__, __FUNCTION__, ## arg); \
	} while (0)

// This function does absolutely nothing
static inline void __attribute__((always_inline, format(printf, 2, 3)))
	pakfire_ctx_log_null(struct pakfire_ctx* ctx, const char *format, ...) {}

#define INFO(ctx, arg...) pakfire_ctx_log_condition(ctx, LOG_INFO, ## arg)
#define WARN(ctx, arg...) pakfire_ctx_log_condition(ctx, LOG_WARNING, ## arg)
#define ERROR(ctx, arg...) pakfire_ctx_log_condition(ctx, LOG_ERR, ## arg)

#ifdef ENABLE_DEBUG
#	define DEBUG(ctx, arg...) pakfire_ctx_log_condition(ctx, LOG_DEBUG, ## arg)
#else
#	define DEBUG pakfire_ctx_log_null
#endif

#endif /* PAKFIRE_LOGGING_H */
