/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_CGROUP_H
#define PAKFIRE_CGROUP_H

#include <pakfire/ctx.h>

enum pakfire_cgroup_controllers {
	PAKFIRE_CGROUP_CONTROLLER_CPU     = (1 << 0),
	PAKFIRE_CGROUP_CONTROLLER_MEMORY  = (1 << 1),
	PAKFIRE_CGROUP_CONTROLLER_PIDS    = (1 << 2),
	PAKFIRE_CGROUP_CONTROLLER_IO      = (1 << 3),
};

struct pakfire_cgroup;

struct pakfire_cgroup_stats {
	// CPU
	struct pakfire_cgroup_cpu_stats {
		unsigned long system_usec;
		unsigned long usage_usec;
		unsigned long user_usec;

		unsigned long nr_periods;
		unsigned long nr_throttled;
		unsigned long throttled_usec;
		unsigned long nr_bursts;
		unsigned long burst_usec;
	} cpu;

	// Memory
	struct pakfire_cgroup_memory_stats {
		// Amount of memory used in anonymous mappings
		unsigned long anon;

		// Amount of memory used to cache filesystem data
		unsigned long file;

		// Amount of total kernel memory
		unsigned long kernel;

		// Amount of memory allocated to kernel stacks
		unsigned long kernel_stack;

		// Amount of memory allocated for page tables
		unsigned long pagetables;

		// Amount of memory allocated for secondary page tables
		unsigned long sec_pagetables;

		// Amount of memory used for storing per-cpu kernel data structures
		unsigned long percpu;

		// Amount of memory used in network transmission buffers
		unsigned long sock;

		// Amount of memory used for vmap backed memory
		unsigned long vmalloc;

		// Amount of cached filesystem data that is swap-backed,
		// such as tmpfs, shm segments, shared anonymous mmap()s
		unsigned long shmem;

		// Amount of memory consumed by the zswap compression backend
		unsigned long zswap;

		// Amount of application memory swapped out to zswap
		unsigned long zswapped;

		// Amount of cached filesystem data mapped with mmap()
		unsigned long file_mapped;

		// Amount of cached filesystem data that was modified but
		// not yet written back to disk
		unsigned long file_dirty;

		// Amount of cached filesystem data that was modified and
		// is currently being written back to disk
		unsigned long file_writeback;

		// Amount of swap cached in memory. The swapcache is accounted
		// against both memory and swap usage.
		unsigned long swapcached;

		// Amount of memory used in anonymous mappings backed by
		// transparent hugepages
		unsigned long anon_thp;

		// Amount of cached filesystem data backed by transparent hugepages
		unsigned long file_thp;

		// Amount of shm, tmpfs, shared anonymous mmap()s backed by
		// transparent hugepages
		unsigned long shmem_thp;

		// Amount of memory, swap-backed and filesystem-backed,
		// on the internal memory management lists used by the
		// page reclaim algorithm.
		unsigned long inactive_anon;
		unsigned long active_anon;
		unsigned long inactive_file;
		unsigned long active_file;
		unsigned long unevictable;

		// Part of "slab" that might be reclaimed, such as dentries and inodes
		unsigned long slab_reclaimable;

		// Part of "slab" that cannot be reclaimed on memory pressure
		unsigned long slab_unreclaimable;

		// Amount of memory used for storing in-kernel data structures
		unsigned long slab;

		// Number of refaults of previously evicted anonymous pages
		unsigned long workingset_refault_anon;

		// Number of refaults of previously evicted file pages
		unsigned long workingset_refault_file;

		// Number of refaulted anonymous pages that were immediately activated
		unsigned long workingset_activate_anon;

		// Number of refaulted file pages that were immediately activated
		unsigned long workingset_activate_file;

		// Number of restored anonymous pages which have been detected as
		// an active workingset before they got reclaimed.
		unsigned long workingset_restore_anon;

		// Number of restored file pages which have been detected as an
		// active workingset before they got reclaimed.
		unsigned long workingset_restore_file;

		// Number of times a shadow node has been reclaimed
		unsigned long workingset_nodereclaim;

		// Total number of page faults incurred
		unsigned long pgfault;

		// Number of major page faults incurred
		unsigned long pgmajfault;

		// Amount of scanned pages (in an active LRU list)
		unsigned long pgrefill;

		// Amount of scanned pages (in an inactive LRU list)
		unsigned long pgscan;

		// Amount of reclaimed pages
		unsigned long pgsteal;

		// Amount of scanned pages by kswapd
		unsigned long pgscan_kswapd;

		// Amount of scanned pages (direct)
		unsigned long pgscan_direct;

		// Amount of reclaimed pages by kswapd
		unsigned long pgsteal_kswapd;

		// Amount of reclaimed pages (direct)
		unsigned long pgsteal_direct;

		// Amount of pages moved to the active LRU list
		unsigned long pgactivate;

		// Amount of pages moved to the inactive LRU list
		unsigned long pgdeactivate;

		// Amount of pages postponed to be freed under memory pressure
		unsigned long pglazyfree;

		// Amount of reclaimed lazyfree pages
		unsigned long pglazyfreed;

		// Number of transparent hugepages which were allocated to satisfy a page fault
		unsigned long thp_fault_alloc;

		// Number of transparent hugepages which were allocated to allow
		// collapsing an existing range of pages.
		unsigned long thp_collapse_alloc;
	} memory;
};

int pakfire_cgroup_create(struct pakfire_cgroup** cgroup,
	struct pakfire_ctx* ctx, struct pakfire_cgroup* parent, const char* name, int flags);

struct pakfire_cgroup* pakfire_cgroup_ref(struct pakfire_cgroup* cgroup);
struct pakfire_cgroup* pakfire_cgroup_unref(struct pakfire_cgroup* cgroup);

int pakfire_cgroup_is_valid(struct pakfire_cgroup* self);

int pakfire_cgroup_child(struct pakfire_cgroup** child,
	struct pakfire_cgroup* cgroup, const char* path, int flags);

enum pakfire_cgroup_destroy_flags {
	PAKFIRE_CGROUP_DESTROY_RECURSIVE = (1 << 0),
};

int pakfire_cgroup_destroy(struct pakfire_cgroup* cgroup, int flags);

int pakfire_cgroup_fd(struct pakfire_cgroup* cgroup);

// Killall
int pakfire_cgroup_killall(struct pakfire_cgroup* cgroup);

// Accounting
int pakfire_cgroup_enable_accounting(struct pakfire_cgroup* cgroup);

// Memory
int pakfire_cgroup_set_guaranteed_memory(struct pakfire_cgroup* cgroup, size_t mem);
int pakfire_cgroup_set_memory_limit(struct pakfire_cgroup* cgroup, size_t mem);

// PIDs
int pakfire_cgroup_set_pid_limit(struct pakfire_cgroup* cgroup, size_t limit);

// Stats
int pakfire_cgroup_stat(struct pakfire_cgroup* cgroup,
	struct pakfire_cgroup_stats* stats);
int pakfire_cgroup_stat_dump(struct pakfire_cgroup* cgroup,
	const struct pakfire_cgroup_stats* stats);

#endif /* PAKFIRE_CGROUP_H */
