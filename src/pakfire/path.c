/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2019 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <wordexp.h>

#include <pakfire/path.h>
#include <pakfire/string.h>

struct pakfire_path {
	int is_absolute;

	unsigned int num_segments;
	char** segments;
};

static void pakfire_path_free_segments(struct pakfire_path* path) {
	for (unsigned int i = 0; i < path->num_segments; i++)
		free(path->segments[i]);
	free(path->segments);

	// Reset the data structure
	path->segments = NULL;
	path->num_segments = 0;
	path->is_absolute = 0;
}

static void pakfire_path_free(struct pakfire_path* path) {
	if (path->segments)
		pakfire_path_free_segments(path);

	free(path);
}

static int pakfire_path_insert_segment(struct pakfire_path* path, unsigned int idx, const char* segment) {
	char* s = NULL;

	// Index must be within range
	if (idx >= path->num_segments + 1)
		return -ERANGE;

	// Make space
	path->segments = reallocarray(path->segments, path->num_segments + 2, sizeof(*path->segments));

	// Fail if we could not make space
	if (!path->segments)
		return -errno;

	// Copy the segment to the heap
	s = strdup(segment);
	if (!s)
		return -errno;

	unsigned int i = path->num_segments;

	do {
		if (i > idx)
			path->segments[i] = path->segments[i - 1];

		else if (i == idx)
			path->segments[i] = s;
	} while (i-- > idx);

	// Increment the number of segments we have
	path->num_segments++;

	return 0;
}

static int pakfire_path_append_segment(struct pakfire_path* path, const char* segment) {
	return pakfire_path_insert_segment(path, path->num_segments, segment);
}

static int pakfire_path_remove_segment(struct pakfire_path* path, unsigned int idx) {
	if (idx >= path->num_segments)
		return -EINVAL;

	// Free the value
	if (path->segments[idx])
		free(path->segments[idx]);

	// Move the other segments
	for (unsigned int i = idx + 1; i < path->num_segments; i++)
		path->segments[i - 1] = path->segments[i];

	// Decrement the number of segments
	path->num_segments--;

	return 0;
}

static int pakfire_path_import_segments(struct pakfire_path* path, const char* s) {
	char buffer[PATH_MAX];
	char* p = NULL;
	int r;

	// Is the path absolute?
	if (*s == '/') {
		// If we are joining strings and the new string is absolute,
		// we throw away all segments.
		pakfire_path_free_segments(path);

		path->is_absolute = 1;
	}

	// Copy path into buffer
	r = pakfire_string_set(buffer, s);
	if (r < 0)
		return r;

	const char* segment = strtok_r(buffer, "/", &p);

	// Append all segments
	while (segment) {
		r = pakfire_path_append_segment(path, segment);
		if (r < 0)
			break;

		segment = strtok_r(NULL, "/", &p);
	}

	return r;
}

static int pakfire_path_parse(struct pakfire_path** path, const char* s) {
	struct pakfire_path* p = NULL;
	int r;

	// Check input
	if (!s)
		return -EINVAL;

	// Allocate object
	p = calloc(1, sizeof(*p));
	if (!p)
		return -errno;

	// Import the path
	r = pakfire_path_import_segments(p, s);
	if (r < 0)
		goto ERROR;

	// Success
	*path = p;

	return 0;

ERROR:
	if (p)
		pakfire_path_free(p);

	return r;
}

static size_t pakfire_path_length(struct pakfire_path* path) {
	size_t length = path->num_segments;

	for (unsigned int i = 0; i < path->num_segments; i++)
		length += strlen(path->segments[i]);

	return length;
}

static int pakfire_path_to_string(struct pakfire_path* path, char* buffer, const size_t length) {
	char* p = buffer;
	char* s = NULL;

	const size_t required_length = pakfire_path_length(path) + 1;

	// Check if we have enough space
	if (length < required_length)
		return -ENOBUFS;

	// Return / for empty paths
	if (!path->num_segments) {
		if (path->is_absolute)
			return __pakfire_string_set(buffer, length, "/");
		else
			return __pakfire_string_set(buffer, length, "");
	}

	for (unsigned int i = 0; i < path->num_segments; i++) {
		s = path->segments[i];

		// Add separator
		if (i > 0 || path->is_absolute)
			*p++ = '/';

		// Add the segment
		while (*s)
			*p++ = *s++;
	}

	// Terminate the string
	*p = '\0';

	return 0;
}

static int pakfire_path_do_normalize(struct pakfire_path* path) {
	int r;

	// Make the analyzer happy
	if (!path)
		return -EINVAL;

	for (unsigned int i = 0; i < path->num_segments; i++) {
		// Simply remove single dots
		if (strcmp(path->segments[i], ".") == 0) {
			r = pakfire_path_remove_segment(path, i);
			if (r)
				return r;

			// Go back one step
			i--;

		// Remove double dots
		} else if (strcmp(path->segments[i], "..") == 0) {
			r = pakfire_path_remove_segment(path, i);
			if (r)
				return r;

			// Remove the previous element if there is one
			if (i > 0) {
				r = pakfire_path_remove_segment(path, i - 1);
				if (r)
					return r;

				i--;
			}

			// Go back one step
			i--;
		}
	}

	return 0;
}

int __pakfire_path_normalize(char* p, const size_t length) {
	struct pakfire_path* path = NULL;
	int r;

	// Parse the path
	r = pakfire_path_parse(&path, p);
	if (r)
		goto ERROR;

	// Normalize the path
	r = pakfire_path_do_normalize(path);
	if (r)
		goto ERROR;

	// Write back the path
	r = pakfire_path_to_string(path, p, length);
	if (r)
		goto ERROR;

ERROR:
	if (path)
		pakfire_path_free(path);

	return r;
}

int __pakfire_path_format(char* buffer, const size_t length, const char* format, ...) {
	va_list args;
	int r;

	va_start(args, format);
	r = __pakfire_string_vformat(buffer, length, format, args);
	va_end(args);

	// Return any errors
	if (r < 0)
		return r;

	// Normalize the path
	return __pakfire_path_normalize(buffer, length);
}

int __pakfire_path_append(char* buffer, const size_t length, const char* s1, const char* s2) {
	struct pakfire_path* path = NULL;
	int r;

	// Check inputs
	if (!buffer || !length || !s1 || !s2)
		return -EINVAL;

	// Parse the path
	r = pakfire_path_parse(&path, s1);
	if (r)
		goto ERROR;

	// Skip any leading slashes
	while (*s2 == '/')
		s2++;

	// Add the second part
	r = pakfire_path_import_segments(path, s2);
	if (r)
		goto ERROR;

	// Normalize the path
	r = pakfire_path_do_normalize(path);
	if (r)
		goto ERROR;

	// Write back the path
	r = pakfire_path_to_string(path, buffer, length);
	if (r)
		goto ERROR;

ERROR:
	if (path)
		pakfire_path_free(path);

	return r;
}

int __pakfire_path_merge(char* buffer, const size_t length, const char* s1, const char* s2) {
	struct pakfire_path* path = NULL;
	int r;

	// Check inputs
	if (!buffer || !length || !s1 || !s2)
		return -EINVAL;

	// Parse the path
	r = pakfire_path_parse(&path, s1);
	if (r)
		goto ERROR;

	// Normalize the path
	r = pakfire_path_do_normalize(path);
	if (r)
		goto ERROR;

	// Drop the last segment
	r = pakfire_path_remove_segment(path, path->num_segments - 1);
	if (r)
		goto ERROR;

	// Add the second part
	r = pakfire_path_import_segments(path, s2);
	if (r)
		goto ERROR;

	// Normalize the path
	r = pakfire_path_do_normalize(path);
	if (r)
		goto ERROR;

	// Write back the path
	r = pakfire_path_to_string(path, buffer, length);
	if (r)
		goto ERROR;

ERROR:
	if (path)
		pakfire_path_free(path);

	return r;
}

int __pakfire_path_basename(char* buffer, const size_t length, const char* s) {
	struct pakfire_path* path = NULL;
	int r;

	// Check inputs
	if (!buffer || !length)
		return -EINVAL;

	// Parse the path
	r = pakfire_path_parse(&path, s);
	if (r < 0)
		goto ERROR;

	// Make the static analyzer happy
	assert(path);

	// Drop everything but the last segment
	while (path->num_segments > 1) {
		r = pakfire_path_remove_segment(path, 0);
		if (r)
			goto ERROR;
	}

	// The result is never absolute
	path->is_absolute = 0;

	// Write back the path
	r = pakfire_path_to_string(path, buffer, length);
	if (r)
		goto ERROR;

ERROR:
	if (path)
		pakfire_path_free(path);

	return r;
}

int __pakfire_path_dirname(char* buffer, const size_t length, const char* s) {
	struct pakfire_path* path = NULL;
	int r;

	// Check inputs
	if (!buffer || !length)
		return -EINVAL;

	// Parse the path
	r = pakfire_path_parse(&path, s);
	if (r)
		goto ERROR;

	// Make the static analyzer happy
	assert(path);

	// Drop the last segment
	r = pakfire_path_remove_segment(path, path->num_segments - 1);
	if (r)
		goto ERROR;

	// Write back the path
	r = pakfire_path_to_string(path, buffer, length);
	if (r)
		goto ERROR;

ERROR:
	if (path)
		pakfire_path_free(path);

	return r;
}

int __pakfire_path_relative(char* buffer, const size_t length, const char* __root, const char* s) {
	struct pakfire_path* root = NULL;
	struct pakfire_path* path = NULL;
	int r;

	// Parse the root
	r = pakfire_path_parse(&root, __root);
	if (r)
		goto ERROR;

	// Parse the path
	r = pakfire_path_parse(&path, s);
	if (r)
		goto ERROR;

	// Make the static analyzer happy
	assert(root && path);

	// Both paths must be absolute
	if (!root->is_absolute || !path->is_absolute) {
		r = -EINVAL;
		goto ERROR;
	}

	// The result is no longer absolute
	path->is_absolute = 0;

	// Walk through all segments of the root
	while (root->num_segments && path->num_segments) {
		// If the first segments match, we drop them both
		if (strcmp(root->segments[0], path->segments[0]) == 0) {
			r = pakfire_path_remove_segment(root, 0);
			if (r)
				goto ERROR;

			r = pakfire_path_remove_segment(path, 0);
			if (r)
				goto ERROR;

		// Otherwise we have reached the end
		} else {
			r = pakfire_path_insert_segment(path, 0, "..");
			if (r)
				goto ERROR;

			break;
		}
	}

	// Write back the path
	r = pakfire_path_to_string(path, buffer, length);
	if (r)
		goto ERROR;

ERROR:
	if (path)
		pakfire_path_free(path);
	if (root)
		pakfire_path_free(root);

	return r;
}

int pakfire_path_is_absolute(const char* s) {
	struct pakfire_path* path = NULL;
	int r;

	// Parse the path
	r = pakfire_path_parse(&path, s);
	if (r < 0)
		return r;

	// Make the static analyzer happy
	assert(path);

	// Is this absolute?
	r = path->is_absolute;

	pakfire_path_free(path);

	return r;
}

int __pakfire_path_absolute(char* buffer, const size_t length, const char* s) {
	struct pakfire_path* path = NULL;
	int r;

	// Parse the path
	r = pakfire_path_parse(&path, s);
	if (r < 0)
		return r;

	// Make the static analyzer happy
	assert(path);

	// Make it absolute
	path->is_absolute = 1;

	// Write back the path
	r = pakfire_path_to_string(path, buffer, length);

	pakfire_path_free(path);

	return r;
}

/*
	This function will handle any stars in the pattern matching

	stars will be set to non-zero if we encountered a double star
*/
static int __pakfire_path_match_star(const char* p, const char* s) {
	unsigned int stars = 0;
	int r;

	// Count how many stars we found
	while (p && *p == '*') {
		// Consume the star
		p++;

		// Increment the counter
		stars++;
	}

	// We do not allow more than two stars
	if (stars > 2) {
		errno = EINVAL;
		return -1;
	}

	// Consume the string...
	for (; *s; s++) {
		// Found slash!
		if (*s == '/' && stars == 1)
			return pakfire_path_match(p, s);

		// Otherwise read as many characters as possible
		r = pakfire_path_match(p, s);
		if (r)
			return r;
	}

	// The pattern has not entirely been consumed
	if (p && *p)
		return 0;

	// If we reached the end of the string, * has consumed everything
	return 1;
}

/*
	This is our custom implementation of fnmatch()
	which supports ** and stops at slashes.
*/
int pakfire_path_match(const char* p, const char* s) {
	// Empty pattern matches nothing
	if (!p || !*p)
		return 0;

	// Consume the pattern and string...
	for (; *p; p++, s++) {
		switch (*p) {
			// Match any character
			case '?':
				// No match if we reached the end
				if (!*s)
					return 0;

				continue;

			// Match multiple characters
			case '*':
				return __pakfire_path_match_star(p, s);

			// All other characters
			default:
				// Character matches
				if (*s == *p)
					continue;

				// No match
				return 0;
		}
	}

	// There are unmatched characters left
	if (*s)
		return 0;

	// We reached the end of the string and all characters matched
	return 1;
}

// Returns true if the path contains patterns (i.e. ? and *)
int pakfire_path_is_pattern(const char* path) {
	if (!path)
		return -EINVAL;

	for (const char* p = path; *p; p++) {
		switch (*p) {
			case '*':
			case '?':
				return 1;

			default:
				break;
		}
	}

	return 0;
}

static int pakfire_path_strip_extension(char* path) {
	if (!path)
		return -EINVAL;

	// Find the extension
	char* ext = strrchr(path, '.');

	// If . could not be found, we return an error.
	if (!ext)
		return 1;

	// Otherwise, we will terminate the string
	*ext = '\0';
	return 0;
}

int __pakfire_path_replace_extension(char* path, const size_t length, const char* extension) {
	char buffer[PATH_MAX];
	int r;

	// Copy path to buffer
	r = pakfire_string_set(buffer, path);
	if (r < 0)
		return r;

	// Strip any old extension
	r = pakfire_path_strip_extension(buffer);
	if (r)
		return r;

	// Compose the new string
	return __pakfire_string_format(path, length, "%s.%s", buffer, extension);
}

int __pakfire_path_realpath(char* dest, const size_t length, const char* path) {
	char buffer[PATH_MAX];

	// Resolve path to its absolute path and store it in buffer
	char* p = realpath(path, buffer);
	if (!p)
		return 1;

	return __pakfire_string_set(dest, length, buffer);
}

int __pakfire_path_expand(char* dest, const size_t length, const char* path) {
	wordexp_t result = {};
	int r;

	// Expand the path
	r = wordexp(path, &result, 0);
	if (r) {
		r = -EINVAL;
		goto ERROR;
	}

	// There should only be one result
	if (result.we_wordc != 1) {
		r = -EINVAL;
		goto ERROR;
	}

	// Store the result
	r = __pakfire_string_set(dest, length, result.we_wordv[0]);
	if (r < 0)
		goto ERROR;

ERROR:
	wordfree(&result);

	return r;
}

int pakfire_path_exists(const char* path) {
	return !access(path, F_OK);
}

time_t pakfire_path_age(const char* path) {
	struct stat st;

	int r = stat(path, &st);
	if (r < 0)
		return -errno;

	// Get current timestamp
	time_t now = time(NULL);

	// Return the difference since the file has been created and now
	return now - st.st_ctime;
}
