/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <linux/capability.h>
#include <linux/sched.h>
#include <sched.h>
#include <signal.h>
#include <stdlib.h>
#include <syscall.h>
#include <sys/capability.h>
#include <sys/eventfd.h>
#include <sys/mount.h>
#include <sys/personality.h>
#include <sys/prctl.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>

// libnl3
#include <net/if.h>
#include <netlink/route/link.h>

// libseccomp
#include <seccomp.h>

// libuuid
#include <uuid.h>

// systemd
#include <systemd/sd-event.h>

#include <pakfire/arch.h>
#include <pakfire/cgroup.h>
#include <pakfire/env.h>
#include <pakfire/jail.h>
#include <pakfire/log_stream.h>
#include <pakfire/logging.h>
#include <pakfire/mount.h>
#include <pakfire/pakfire.h>
#include <pakfire/path.h>
#include <pakfire/pty.h>
#include <pakfire/pwd.h>
#include <pakfire/string.h>
#include <pakfire/syscalls.h>
#include <pakfire/util.h>

#define BUFFER_SIZE      1024 * 64
#define MAX_MOUNTPOINTS  8

// The default environment that will be set for every command
static const struct environ {
	const char* key;
	const char* val;
} ENV[] = {
	{ "HOME", "/root" },
	{ "LANG", "C.utf-8" },
	{ "PATH", "/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin", },
	{ "PS1",  "pakfire-jail \\w> " },
	{ "TERM", "vt100" },

	// Tell everything that it is running inside a Pakfire container
	{ "container", "pakfire" },
	{ NULL, NULL },
};

struct pakfire_jail_mountpoint {
	char source[PATH_MAX];
	char target[PATH_MAX];
	int flags;
};

struct pakfire_jail {
	struct pakfire_ctx* ctx;
	struct pakfire* pakfire;
	int nrefs;

	// A unique ID for each jail
	uuid_t uuid;
	char __uuid[UUID_STR_LEN];

	// Resource Limits
	int nice;

	// Timeout
	uint64_t timeout;

	// CGroup
	struct pakfire_cgroup* cgroup;

	// Environment
	struct pakfire_env* env;

	// Mountpoints
	struct pakfire_jail_mountpoint mountpoints[MAX_MOUNTPOINTS];
	unsigned int num_mountpoints;
};

struct pakfire_jail_exec {
	struct pakfire_jail* jail;

	sd_event* loop;
	int flags;

	// Exit Code
	int exit;

	// PID (of the child)
	pid_t pid;
	int pidfd;

	// FD to notify the client that the parent has finished initialization
	int completed_fd;

	// PTY
	struct pakfire_pty* pty;

	// Log pipes
	struct pakfire_jail_pipes {
		// Logging
		struct pakfire_log_stream* INFO;
		struct pakfire_log_stream* WARN;
		struct pakfire_log_stream* ERROR;
#ifdef ENABLE_DEBUG
		struct pakfire_log_stream* DEBUG;
#endif /* ENABLE_DEBUG */
	} log;

	// Timeout
	sd_event_source* timeout;

	struct pakfire_cgroup* cgroup;
	struct pakfire_cgroup_stats cgroup_stats;

	// The function to be called once the jail has been set up
	pakfire_jail_callback callback;
	void* data;
};

static int pivot_root(const char* new_root, const char* old_root) {
	return syscall(SYS_pivot_root, new_root, old_root);
}

static int pakfire_jail_exec_has_flag(
		const struct pakfire_jail_exec* ctx, const enum pakfire_jail_exec_flags flag) {
	return ctx->flags & flag;
}

static void pakfire_jail_free(struct pakfire_jail* jail) {
	if (jail->cgroup)
		pakfire_cgroup_unref(jail->cgroup);
	if (jail->pakfire)
		pakfire_unref(jail->pakfire);
	if (jail->env)
		pakfire_env_unref(jail->env);
	if (jail->ctx)
		pakfire_ctx_unref(jail->ctx);
	free(jail);
}

static const char* pakfire_jail_uuid(struct pakfire_jail* jail) {
	if (!*jail->__uuid)
		uuid_unparse_lower(jail->uuid, jail->__uuid);

	return jail->__uuid;
}

int pakfire_jail_create(struct pakfire_jail** jail, struct pakfire* pakfire) {
	int r;

	const char* arch = pakfire_get_effective_arch(pakfire);

	// Allocate a new jail
	struct pakfire_jail* j = calloc(1, sizeof(*j));
	if (!j)
		return 1;

	// Reference context
	j->ctx = pakfire_ctx(pakfire);

	// Reference Pakfire
	j->pakfire = pakfire_ref(pakfire);

	// Initialize reference counter
	j->nrefs = 1;

	// Generate a random UUID
	uuid_generate_random(j->uuid);

	// Create environment
	r = pakfire_env_create(&j->env, j->ctx);
	if (r < 0)
		goto ERROR;

	// Set default environment
	for (const struct environ* e = ENV; e->key; e++) {
		r = pakfire_env_set(j->env, e->key, "%s", e->val);
		if (r < 0)
			goto ERROR;
	}

	// Enable all CPU features that CPU has to offer
	if (!pakfire_arch_is_supported_by_host(arch)) {
		r = pakfire_env_set(j->env, "QEMU_CPU", "max");
		if (r < 0)
			goto ERROR;
	}

	// Set container UUID
	r = pakfire_env_set(j->env, "container_uuid", "%s", pakfire_jail_uuid(j));
	if (r < 0)
		goto ERROR;

	// Disable systemctl to talk to systemd
	if (!pakfire_on_root(j->pakfire)) {
		r = pakfire_env_set(j->env, "SYSTEMD_OFFLINE", "1");
		if (r < 0)
			goto ERROR;
	}

	// Log action
	DEBUG(j->ctx, "Created new jail %s\n", pakfire_jail_uuid(j));

	// Done
	*jail = j;
	return 0;

ERROR:
	pakfire_jail_free(j);

	return r;
}

struct pakfire_jail* pakfire_jail_ref(struct pakfire_jail* jail) {
	++jail->nrefs;

	return jail;
}

struct pakfire_jail* pakfire_jail_unref(struct pakfire_jail* jail) {
	if (--jail->nrefs > 0)
		return jail;

	pakfire_jail_free(jail);
	return NULL;
}

// Resource Limits

int pakfire_jail_nice(struct pakfire_jail* jail, int nice) {
	// Check if nice level is in range
	if (nice < -19 || nice > 20) {
		errno = EINVAL;
		return 1;
	}

	// Store nice level
	jail->nice = nice;

	return 0;
}

int pakfire_jail_set_cgroup(struct pakfire_jail* jail, struct pakfire_cgroup* cgroup) {
	// Free any previous cgroup
	if (jail->cgroup) {
		pakfire_cgroup_unref(jail->cgroup);
		jail->cgroup = NULL;
	}

	// Set any new cgroup
	if (cgroup) {
		DEBUG(jail->ctx, "Setting cgroup %p\n", cgroup);

		// Don't accept the cgroup if it is in an invalid state
		if (!pakfire_cgroup_is_valid(cgroup))
			return -EINVAL;

		jail->cgroup = pakfire_cgroup_ref(cgroup);
	}

	// Done
	return 0;
}

// Timeout

int pakfire_jail_set_timeout(
		struct pakfire_jail* jail, unsigned int timeout) {
	// Store value
	jail->timeout = timeout;

	if (timeout > 0)
		DEBUG(jail->ctx, "Timeout set to %u second(s)\n", timeout);
	else
		DEBUG(jail->ctx, "Timeout disabled\n");

	return 0;
}

/*
	This function replaces any logging in the child process.

	All log messages will be sent to the parent process through their respective pipes.
*/

static void pakfire_jail_log_redirect(void* data, int priority, const char* file,
	int line, const char* fn, const char* format, va_list args) __attribute__((format(printf, 6, 0)));

static void pakfire_jail_log_redirect(void* data, int priority, const char* file,
		int line, const char* fn, const char* format, va_list args) {
	struct pakfire_jail_exec* ctx = data;

	switch (priority) {
		case LOG_INFO:
			pakfire_log_stream_vprintf(ctx->log.INFO, format, args);
			break;

		case LOG_WARNING:
			pakfire_log_stream_vprintf(ctx->log.WARN, format, args);
			break;

		case LOG_ERR:
			pakfire_log_stream_vprintf(ctx->log.ERROR, format, args);
			break;

#ifdef ENABLE_DEBUG
		case LOG_DEBUG:
			pakfire_log_stream_vprintf(ctx->log.DEBUG, format, args);
			break;
#endif /* ENABLE_DEBUG */

		// Ignore any messages of an unknown priority
		default:
			return;
	}
}

/*
	Passes any log messages on to the context logger
*/
static int pakfire_jail_INFO(struct pakfire_log_stream* stream, const char* line, size_t length, void* data) {
	struct pakfire_jail* jail = data;

	INFO(jail->ctx, "%.*s", (int)length, line);
	return 0;
}

static int pakfire_jail_WARN(struct pakfire_log_stream* stream, const char* line, size_t length, void* data) {
	struct pakfire_jail* jail = data;

	ERROR(jail->ctx, "%.*s", (int)length, line);
	return 0;
}

static int pakfire_jail_ERROR(struct pakfire_log_stream* stream, const char* line, size_t length, void* data) {
	struct pakfire_jail* jail = data;

	ERROR(jail->ctx, "%.*s", (int)length, line);
	return 0;
}

#ifdef ENABLE_DEBUG
static int pakfire_jail_DEBUG(struct pakfire_log_stream* stream, const char* line, size_t length, void* data) {
	struct pakfire_jail* jail = data;

	DEBUG(jail->ctx, "%.*s", (int)length, line);
	return 0;
}
#endif /* ENABLE_DEBUG */

// Capabilities

// Logs all capabilities of the current process
static int pakfire_jail_show_capabilities(struct pakfire_jail* jail) {
	cap_t caps = NULL;
	char* name = NULL;
	cap_flag_value_t value_e;
	cap_flag_value_t value_i;
	cap_flag_value_t value_p;
	int r;

	// Fetch PID
	pid_t pid = getpid();

	// Fetch all capabilities
	caps = cap_get_proc();
	if (!caps) {
		ERROR(jail->ctx, "Could not fetch capabilities: %m\n");
		r = 1;
		goto ERROR;
	}

	DEBUG(jail->ctx, "Capabilities of PID %d:\n", pid);

	// Iterate over all capabilities
	for (unsigned int cap = 0; cap_valid(cap); cap++) {
		name = cap_to_name(cap);

		// Fetch effective value
		r = cap_get_flag(caps, cap, CAP_EFFECTIVE, &value_e);
		if (r)
			goto ERROR;

		// Fetch inheritable value
		r = cap_get_flag(caps, cap, CAP_INHERITABLE, &value_i);
		if (r)
			goto ERROR;

		// Fetch permitted value
		r = cap_get_flag(caps, cap, CAP_PERMITTED, &value_p);
		if (r)
			goto ERROR;

		DEBUG(jail->ctx,
			"  %-24s : %c%c%c\n",
			name,
			(value_e == CAP_SET) ? 'e' : '-',
			(value_i == CAP_SET) ? 'i' : '-',
			(value_p == CAP_SET) ? 'p' : '-'
		);

		// Free name
		cap_free(name);
		name = NULL;
	}

	// Success
	r = 0;

ERROR:
	if (name)
		cap_free(name);
	if (caps)
		cap_free(caps);

	return r;
}

static int pakfire_jail_set_capabilities(struct pakfire_jail* jail) {
	cap_t caps = NULL;
	char* name = NULL;
	int r;

	// Fetch capabilities
	caps = cap_get_proc();
	if (!caps) {
		ERROR(jail->ctx, "Could not read capabilities: %m\n");
		r = 1;
		goto ERROR;
	}

	// Walk through all capabilities
	for (cap_value_t cap = 0; cap_valid(cap); cap++) {
		cap_value_t _caps[] = { cap };

		// Fetch the name of the capability
		name = cap_to_name(cap);

		r = cap_set_flag(caps, CAP_EFFECTIVE, 1, _caps, CAP_SET);
		if (r) {
			ERROR(jail->ctx, "Could not set %s: %m\n", name);
			goto ERROR;
		}

		r = cap_set_flag(caps, CAP_INHERITABLE, 1, _caps, CAP_SET);
		if (r) {
			ERROR(jail->ctx, "Could not set %s: %m\n", name);
			goto ERROR;
		}

		r = cap_set_flag(caps, CAP_PERMITTED, 1, _caps, CAP_SET);
		if (r) {
			ERROR(jail->ctx, "Could not set %s: %m\n", name);
			goto ERROR;
		}

		// Free name
		cap_free(name);
		name = NULL;
	}

	// Restore all capabilities
	r = cap_set_proc(caps);
	if (r) {
		ERROR(jail->ctx, "Restoring capabilities failed: %m\n");
		goto ERROR;
	}

	// Add all capabilities to the ambient set
	for (unsigned int cap = 0; cap_valid(cap); cap++) {
		name = cap_to_name(cap);

		// Raise the capability
		r = prctl(PR_CAP_AMBIENT, PR_CAP_AMBIENT_RAISE, cap, 0, 0);
		if (r) {
			ERROR(jail->ctx, "Could not set ambient capability %s: %m\n", name);
			goto ERROR;
		}

		// Free name
		cap_free(name);
		name = NULL;
	}

	// Success
	r = 0;

ERROR:
	if (name)
		cap_free(name);
	if (caps)
		cap_free(caps);

	return r;
}

// Syscall Filter

static int pakfire_jail_limit_syscalls(struct pakfire_jail* jail) {
	const int syscalls[] = {
		// The kernel's keyring isn't namespaced
		SCMP_SYS(keyctl),
		SCMP_SYS(add_key),
		SCMP_SYS(request_key),

		// Disable userfaultfd
		SCMP_SYS(userfaultfd),

		// Disable perf which could leak a lot of information about the host
		SCMP_SYS(perf_event_open),

		0,
	};
	int r = 1;

	DEBUG(jail->ctx, "Applying syscall filter...\n");

	// Setup a syscall filter which allows everything by default
	scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_ALLOW);
	if (!ctx) {
		ERROR(jail->ctx, "Could not setup seccomp filter: %m\n");
		goto ERROR;
	}

	// All all syscalls
	for (const int* syscall = syscalls; *syscall; syscall++) {
		r = seccomp_rule_add(ctx, SCMP_ACT_ERRNO(EPERM), *syscall, 0);
		if (r) {
			ERROR(jail->ctx, "Could not configure syscall %d: %m\n", *syscall);
			goto ERROR;
		}
	}

	// Load syscall filter into the kernel
	r = seccomp_load(ctx);
	if (r) {
		ERROR(jail->ctx, "Could not load syscall filter into the kernel: %m\n");
		goto ERROR;
	}

ERROR:
	if (ctx)
		seccomp_release(ctx);

	return r;
}

// Mountpoints

int pakfire_jail_bind(struct pakfire_jail* jail,
		const char* source, const char* target, int flags) {
	struct pakfire_jail_mountpoint* mp = NULL;
	int r;

	// Check if there is any space left
	if (jail->num_mountpoints >= MAX_MOUNTPOINTS) {
		errno = ENOSPC;
		return 1;
	}

	// Check for valid inputs
	if (!source || !target) {
		errno = EINVAL;
		return 1;
	}

	// Select the next free slot
	mp = &jail->mountpoints[jail->num_mountpoints];

	// Copy source
	r = pakfire_string_set(mp->source, source);
	if (r) {
		ERROR(jail->ctx, "Could not copy source: %m\n");
		return r;
	}

	// Copy target
	r = pakfire_string_set(mp->target, target);
	if (r) {
		ERROR(jail->ctx, "Could not copy target: %m\n");
		return r;
	}

	// Copy flags
	mp->flags = flags;

	// Increment counter
	jail->num_mountpoints++;

	return 0;
}

static int pakfire_jail_mount_networking(struct pakfire_jail* jail) {
	int r;

	const char* paths[] = {
		"/etc/hosts",
		"/etc/resolv.conf",
		NULL,
	};

	// Bind-mount all paths read-only
	for (const char** path = paths; *path; path++) {
		r = pakfire_bind(jail->ctx, jail->pakfire, *path, NULL, MS_RDONLY);
		if (r) {
			switch (errno) {
				// Ignore if we don't have permission
				case EPERM:
					continue;

				default:
					break;
			}
			return r;
		}
	}

	return 0;
}

/*
	Mounts everything that we require in the new namespace
*/
static int pakfire_jail_mount(struct pakfire_jail* jail, struct pakfire_jail_exec* ctx) {
	struct pakfire_jail_mountpoint* mp = NULL;
	int flags = 0;
	int r;

	// Enable loop devices
	if (pakfire_jail_exec_has_flag(ctx, PAKFIRE_JAIL_HAS_LOOP_DEVICES))
		flags |= PAKFIRE_MOUNT_LOOP_DEVICES;

	// Mount all default stuff
	r = pakfire_mount_all(jail->ctx, jail->pakfire, PAKFIRE_MNTNS_OUTER, flags);
	if (r)
		return r;

	// Populate /dev
	r = pakfire_populate_dev(jail->ctx, jail->pakfire, flags);
	if (r)
		return r;

	// Mount the interpreter (if needed)
	r = pakfire_mount_interpreter(jail->ctx, jail->pakfire);
	if (r)
		return r;

	// Mount networking stuff
	if (pakfire_jail_exec_has_flag(ctx, PAKFIRE_JAIL_HAS_NETWORKING)) {
		r = pakfire_jail_mount_networking(jail);
		if (r)
			return r;
	}

	// Mount all custom stuff
	for (unsigned int i = 0; i < jail->num_mountpoints; i++) {
		// Fetch mountpoint
		mp = &jail->mountpoints[i];

		// Mount it
		r = pakfire_bind(jail->ctx, jail->pakfire, mp->source, mp->target, mp->flags);
		if (r)
			return r;
	}

	return 0;
}

// Networking

static int pakfire_jail_setup_loopback(struct pakfire_jail* jail) {
	struct nl_sock* nl = NULL;
	struct nl_cache* cache = NULL;
	struct rtnl_link* link = NULL;
	struct rtnl_link* change = NULL;
	int r;

	DEBUG(jail->ctx, "Setting up loopback...\n");

	// Allocate a netlink socket
	nl = nl_socket_alloc();
	if (!nl) {
		ERROR(jail->ctx, "Could not allocate a netlink socket: %m\n");
		r = 1;
		goto ERROR;
	}

	// Connect the socket
	r = nl_connect(nl, NETLINK_ROUTE);
	if (r) {
		ERROR(jail->ctx, "Could not connect netlink socket: %s\n", nl_geterror(r));
		goto ERROR;
	}

	// Allocate the netlink cache
	r = rtnl_link_alloc_cache(nl, AF_UNSPEC, &cache);
	if (r < 0) {
		ERROR(jail->ctx, "Unable to allocate netlink cache: %s\n", nl_geterror(r));
		goto ERROR;
	}

	// Fetch loopback interface
	link = rtnl_link_get_by_name(cache, "lo");
	if (!link) {
		ERROR(jail->ctx, "Could not find lo interface. Ignoring.\n");
		r = 0;
		goto ERROR;
	}

	// Allocate a new link
	change = rtnl_link_alloc();
	if (!change) {
		ERROR(jail->ctx, "Could not allocate change link\n");
		r = 1;
		goto ERROR;
	}

	// Set the link to UP
	rtnl_link_set_flags(change, IFF_UP);

	// Apply any changes
	r = rtnl_link_change(nl, link, change, 0);
	if (r) {
		ERROR(jail->ctx, "Unable to activate loopback: %s\n", nl_geterror(r));
		goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	if (nl)
		nl_socket_free(nl);

	return r;
}

// UID/GID Mapping

static int pakfire_jail_setup_uid_mapping(struct pakfire_jail* jail, pid_t pid) {
	char path[PATH_MAX];
	int r;

	// Make path
	r = pakfire_string_format(path, "/proc/%d/uid_map", pid);
	if (r)
		return r;

	// Fetch UID
	const uid_t uid = pakfire_uid(jail->pakfire);

	// Fetch SUBUID
	const struct pakfire_subid* subuid = pakfire_subuid(jail->pakfire);
	if (!subuid)
		return 1;

	/* When running as root, we will map the entire range.

	   When running as a non-privileged user, we will map the root user inside the jail
	   to the user's UID outside of the jail, and we will map the rest starting from one.
	*/

	// Running as root
	if (uid == 0) {
		r = pakfire_file_write(jail->pakfire, path, 0, 0, 0,
			"0 %u %zu\n", subuid->id, subuid->length);
	} else {
		r = pakfire_file_write(jail->pakfire, path, 0, 0, 0,
			"0 %u 1\n1 %u %zu\n", uid, subuid->id, subuid->length);
	}

	if (r) {
		ERROR(jail->ctx, "Could not map UIDs: %m\n");
		return r;
	}

	return r;
}

static int pakfire_jail_setup_gid_mapping(struct pakfire_jail* jail, pid_t pid) {
	char path[PATH_MAX];
	int r;

	// Fetch GID
	const gid_t gid = pakfire_gid(jail->pakfire);

	// Fetch SUBGID
	const struct pakfire_subid* subgid = pakfire_subgid(jail->pakfire);
	if (!subgid)
		return 1;

	// Make path
	r = pakfire_string_format(path, "/proc/%d/gid_map", pid);
	if (r)
		return r;

	// Running as root
	if (gid == 0) {
		r = pakfire_file_write(jail->pakfire, path, 0, 0, 0,
			"0 %u %zu\n", subgid->id, subgid->length);
	} else {
		r = pakfire_file_write(jail->pakfire, path, 0, 0, 0,
			"0 %u 1\n1 %u %zu\n", gid, subgid->id, subgid->length);
	}

	if (r) {
		ERROR(jail->ctx, "Could not map GIDs: %m\n");
		return r;
	}

	return r;
}

static int pakfire_jail_setgroups(struct pakfire_jail* jail, pid_t pid) {
	char path[PATH_MAX];
	int r;

	// Make path
	r = pakfire_string_format(path, "/proc/%d/setgroups", pid);
	if (r)
		return r;

	r = pakfire_file_write(jail->pakfire, path, 0, 0, 0, "deny\n");
	if (r) {
		ERROR(jail->ctx, "Could not set setgroups to deny: %s\n", strerror(errno));
		r = -errno;
	}

	return r;
}

static int pakfire_jail_send_signal(struct pakfire_jail* jail, int fd) {
	const uint64_t val = 1;
	int r = 0;

	DEBUG(jail->ctx, "Sending signal...\n");

	// Write to the file descriptor
	r = eventfd_write(fd, val);
	if (r < 0) {
		ERROR(jail->ctx, "Could not send signal: %s\n", strerror(errno));
		r = -errno;
	}

	// Close the file descriptor
	close(fd);

	return r;
}

static int pakfire_jail_wait_for_signal(struct pakfire_jail* jail, int fd) {
	uint64_t val = 0;
	int r = 0;

	DEBUG(jail->ctx, "Waiting for signal...\n");

	r = eventfd_read(fd, &val);
	if (r < 0) {
		ERROR(jail->ctx, "Error waiting for signal: %s\n", strerror(errno));
		r = -errno;
	}

	// Close the file descriptor
	close(fd);

	return r;
}

// This is only to block the signal
static int pakfire_jail_SIGCHLD(
		sd_event_source* source, const struct signalfd_siginfo* si, void* data) {
	return 0;
}

static int pakfire_jail_SIGTERM(
		sd_event_source* source, const struct signalfd_siginfo* si, void* data) {
	struct pakfire_jail_exec* ctx = data;
	struct pakfire_jail* self = ctx->jail;
	int r;

	// Log action
	DEBUG(self->ctx, "Received SIGTERM\n");

	// Fail if we don't have a PID file descriptor
	if (ctx->pidfd < 0)
		return -ENOTSUP;

	// Send a signal to the child process
	r = pidfd_send_signal(ctx->pidfd, SIGKILL, NULL, 0);
	if (r < 0) {
		ERROR(self->ctx, "Could not terminate jail: %m\n");
		return -errno;
	}

	return 0;
}

/*
	Called when the timer has expired.
*/
static int pakfire_jail_timer(sd_event_source* s, uint64_t usec, void* data) {
	struct pakfire_jail_exec* ctx = data;
	struct pakfire_jail* jail = ctx->jail;
	int r;

	DEBUG(jail->ctx, "Timer has fired...\n");

	// Send SIGKILL to the process
	r = pidfd_send_signal(ctx->pidfd, SIGKILL, NULL, 0);
	if (r)
		ERROR(jail->ctx, "Could not send SIGKILL to the process: %m\n");

	return r;
}

/*
	Called when the child process has exited.
*/
static int pakfire_jail_exited(sd_event_source* source, const siginfo_t* si, void* data) {
	struct pakfire_jail_exec* ctx = data;
	struct pakfire_jail* jail = ctx->jail;

	switch (si->si_code) {
		case CLD_EXITED:
			DEBUG(jail->ctx, "Process has exited with status code %d\n", si->si_status);

			// Store the exit code
			ctx->exit = si->si_status;
			break;

		case CLD_KILLED:
			ERROR(jail->ctx, "Process has been killed by signal %d\n", si->si_signo);

			// Store the exit code
			ctx->exit = 139;
			break;

		case CLD_DUMPED:
			ERROR(jail->ctx, "The child process terminated abnormally with status code %d\n",
				si->si_status);

			// Store the exit code
			ctx->exit = 128 + si->si_status;
			break;
	}

	// Drain the PTY
	pakfire_pty_drain(ctx->pty);

	// Terminate the event loop
	return sd_event_exit(ctx->loop, 0);
}

struct pakfire_jail_command {
	struct pakfire_jail* jail;
	const char** argv;
	struct pakfire_env* env;
};

static int pakfire_jail_launch_command(struct pakfire_ctx* ctx, void* data) {
	struct pakfire_jail_command* command = data;
	struct pakfire_env* env = NULL;
	char** envp = NULL;
	int r;

	// Check if argv is valid
	if (!command->argv || !command->argv[0])
		return -EINVAL;

	// Create a new environment
	r = pakfire_env_create(&env, ctx);
	if (r < 0)
		goto ERROR;

	// Merge the basic environment
	r = pakfire_env_merge(env, command->jail->env);
	if (r < 0)
		goto ERROR;

	// Merge the custom environment
	if (command->env) {
		r = pakfire_env_merge(env, command->env);
		if (r < 0)
			goto ERROR;
	}

	DEBUG(ctx, "Launching command:\n");

	// Log argv
	for (unsigned int i = 0; command->argv[i]; i++)
		DEBUG(ctx, "  argv[%u] = %s\n", i, command->argv[i]);

	// Fetch the environment
	envp = pakfire_env_get_envp(env);

	// exec() command
	r = execvpe(command->argv[0], (char**)command->argv, envp);
	if (r < 0) {
		// Translate errno into regular exit code
		switch (errno) {
			case ENOENT:
#if 0
				// Ignore if the command doesn't exist
				if (ctx->flags & PAKFIRE_JAIL_NOENT_OK)
					r = 0;
				else
					r = 127;

				break;
#endif

			default:
				r = 1;
		}

		ERROR(ctx, "Could not execve(%s): %m\n", command->argv[0]);
	}

ERROR:
	if (env)
		pakfire_env_unref(env);

	// We should not get here
	return r;
}

/*
	Performs the initialisation that needs to happen in the parent part
*/
static int pakfire_jail_parent(struct pakfire_jail* jail, struct pakfire_jail_exec* ctx) {
	int r;

	// Register the PID file descriptor
	r = sd_event_add_child_pidfd(ctx->loop, NULL, ctx->pidfd, WEXITED, pakfire_jail_exited, ctx);
	if (r < 0) {
		DEBUG(jail->ctx, "Could not register the child process with the event loop: %s\n", strerror(-r));
		return r;
	}

	// Setup logging
	r = pakfire_log_stream_in_parent(ctx->log.INFO, ctx->loop);
	if (r)
		return r;

	r = pakfire_log_stream_in_parent(ctx->log.ERROR, ctx->loop);
	if (r)
		return r;

#ifdef ENABLE_DEBUG
	r = pakfire_log_stream_in_parent(ctx->log.DEBUG, ctx->loop);
	if (r)
		return r;
#endif /* ENABLE_DEBUG */

	// Setup UID mapping
	r = pakfire_jail_setup_uid_mapping(jail, ctx->pid);
	if (r)
		return r;

	// Write "deny" to /proc/PID/setgroups
	r = pakfire_jail_setgroups(jail, ctx->pid);
	if (r)
		return r;

	// Setup GID mapping
	r = pakfire_jail_setup_gid_mapping(jail, ctx->pid);
	if (r)
		return r;

	// Parent has finished initialisation
	DEBUG(jail->ctx, "Parent has finished initialization\n");

	// Send signal to client
	r = pakfire_jail_send_signal(jail, ctx->completed_fd);
	if (r)
		return r;

	return 0;
}

static int pakfire_jail_switch_root(struct pakfire_jail* jail, const char* root) {
	int r;

	// Change to the new root
	r = chdir(root);
	if (r) {
		ERROR(jail->ctx, "chdir(%s) failed: %m\n", root);
		return r;
	}

	// Switch Root!
	r = pivot_root(".", ".");
	if (r) {
		ERROR(jail->ctx, "Failed changing into the new root directory %s: %m\n", root);
		return r;
	}

	// Umount the old root
	r = umount2(".", MNT_DETACH);
	if (r) {
		ERROR(jail->ctx, "Could not umount the old root filesystem: %m\n");
		return r;
	}

	return 0;
}

static int pakfire_jail_child(struct pakfire_jail* jail, struct pakfire_jail_exec* ctx) {
	int r;

	// Redirect any logging to our log pipe
	pakfire_ctx_set_log_callback(jail->ctx, pakfire_jail_log_redirect, ctx);

	// Tell the context that we are running inside the jail now
	pakfire_ctx_set_flag(jail->ctx, PAKFIRE_CTX_IN_JAIL);

	// Fetch my own PID
	pid_t pid = getpid();

	DEBUG(jail->ctx, "Launched child process in jail with PID %d\n", pid);

	// Die with parent
	r = prctl(PR_SET_PDEATHSIG, SIGKILL, 0, 0, 0);
	if (r) {
		ERROR(jail->ctx, "Could not configure to die with parent: %m\n");
		return 126;
	}

	// Make this process dumpable
	r = prctl (PR_SET_DUMPABLE, 1, 0, 0, 0);
	if (r) {
		ERROR(jail->ctx, "Could not make the process dumpable: %m\n");
		return 126;
	}

	// Don't drop any capabilities on setuid()
	r = prctl(PR_SET_KEEPCAPS, 1);
	if (r) {
		ERROR(jail->ctx, "Could not set PR_SET_KEEPCAPS: %m\n");
		return 126;
	}

	// Wait for the parent to finish initialization
	r = pakfire_jail_wait_for_signal(jail, ctx->completed_fd);
	if (r)
		return r;

	// Fetch UID/GID
	uid_t uid = getuid();
	gid_t gid = getgid();

	// Fetch EUID/EGID
	uid_t euid = geteuid();
	gid_t egid = getegid();

	DEBUG(jail->ctx, "  UID: %u (effective %u)\n", uid, euid);
	DEBUG(jail->ctx, "  GID: %u (effective %u)\n", gid, egid);

	// Log all mountpoints
	pakfire_mount_list(jail->ctx);

	// Fail if we are not PID 1
	if (pid != 1) {
		ERROR(jail->ctx, "Child process is not PID 1\n");
		return 126;
	}

	// Fail if we are not running as root
	if (uid || gid || euid || egid) {
		ERROR(jail->ctx, "Child process is not running as root\n");
		return 126;
	}

	// Mount all default stuff
	r = pakfire_mount_all(jail->ctx, jail->pakfire, PAKFIRE_MNTNS_INNER, 0);
	if (r)
		return 126;

	const char* root = pakfire_get_path(jail->pakfire);
	const char* arch = pakfire_get_effective_arch(jail->pakfire);

	// Change mount propagation to slave to receive anything from the parent namespace
	r = pakfire_mount_change_propagation(jail->ctx, "/", MS_SLAVE);
	if (r)
		return r;

	// Make root a mountpoint in the new mount namespace
	r = pakfire_mount_make_mounpoint(jail->ctx, root);
	if (r)
		return r;

	// Change mount propagation to private
	r = pakfire_mount_change_propagation(jail->ctx, root, MS_PRIVATE);
	if (r)
		return r;

	// Change root (unless root is /)
	if (!pakfire_on_root(jail->pakfire)) {
		// Mount everything
		r = pakfire_jail_mount(jail, ctx);
		if (r)
			return r;

		// chroot()
		r = pakfire_jail_switch_root(jail, root);
		if (r)
			return r;
	}

	// Set personality
	unsigned long persona = pakfire_arch_personality(arch);
	if (persona) {
		r = personality(persona);
		if (r < 0) {
			ERROR(jail->ctx, "Could not set personality (%x)\n", (unsigned int)persona);
			return 1;
		}
	}

	// Setup networking
	if (!pakfire_jail_exec_has_flag(ctx, PAKFIRE_JAIL_HAS_NETWORKING)) {
		r = pakfire_jail_setup_loopback(jail);
		if (r)
			return 1;
	}

	// Set nice level
	if (jail->nice) {
		DEBUG(jail->ctx, "Setting nice level to %d\n", jail->nice);

		r = setpriority(PRIO_PROCESS, pid, jail->nice);
		if (r) {
			ERROR(jail->ctx, "Could not set nice level: %m\n");
			return 1;
		}
	}

	// Reset umask to 022 - this always succeeds
	umask(S_IWGRP|S_IWOTH);

	// Create a new session
	r = setsid();
	if (r < 0) {
		ERROR(jail->ctx, "Could not create a new session: %s\n", strerror(errno));
		return r;
	}

	// Open a new PTY
	r = pakfire_pty_open(ctx->pty);
	if (r < 0) {
		ERROR(jail->ctx, "Could not open a new PTY: %s\n", strerror(-r));
		return r;
	}

	// Setup logging
	r = pakfire_log_stream_in_child(ctx->log.INFO);
	if (r)
		return r;

	r = pakfire_log_stream_in_child(ctx->log.WARN);
	if (r)
		return r;

	r = pakfire_log_stream_in_child(ctx->log.ERROR);
	if (r)
		return r;

#ifdef ENABLE_DEBUG
	r = pakfire_log_stream_in_child(ctx->log.DEBUG);
	if (r)
		return r;
#endif /* ENABLE_DEBUG */

	// Reset open file limit (http://0pointer.net/blog/file-descriptor-limits.html)
	r = pakfire_rlimit_reset_nofile(jail->ctx);
	if (r)
		return r;

	// Set capabilities
	r = pakfire_jail_set_capabilities(jail);
	if (r)
		return r;

	// Show capabilities
	r = pakfire_jail_show_capabilities(jail);
	if (r)
		return r;

	// Filter syscalls
	r = pakfire_jail_limit_syscalls(jail);
	if (r)
		return r;

	DEBUG(jail->ctx, "Child process initialization done\n");

	// Check if we have a callback
	if (!ctx->callback) {
		ERROR(jail->ctx, "No callback set\n");
		return 126;
	}

	// Call the callback
	return ctx->callback(jail->ctx, ctx->data);
}

// Run a command in the jail
static int __pakfire_jail_exec(struct pakfire_jail* jail,
		pakfire_jail_callback callback, void* data, int flags,
		pakfire_pty_stdin_callback stdin_callback, void* stdin_data,
		pakfire_pty_stdout_callback stdout_callback, void* stdout_data,
		char** output, size_t* output_length) {
	int pty_flags = 0;
	int r;

	// We cannot have both, an output callback and buffer
	if (stdout_callback && output)
		return -EINVAL;

	// Initialize context for this call
	struct pakfire_jail_exec ctx = {
		.jail  = jail,
		.flags = flags,
		.pidfd = -1,

		// Callback & Data
		.callback = callback,
		.data     = data,

		// Exit Code
		.exit  = -1,
	};

	DEBUG(jail->ctx, "Executing jail...\n");

	// Create a new event loop
	r = sd_event_new(&ctx.loop);
	if (r < 0) {
		ERROR(jail->ctx, "Could not create a new event loop: %s\n", strerror(-r));
		goto ERROR;
	}

	// Listen for SIGCHLD
	r = sd_event_add_signal(ctx.loop, NULL, SIGCHLD|SD_EVENT_SIGNAL_PROCMASK,
			pakfire_jail_SIGCHLD, NULL);
	if (r < 0) {
		ERROR(jail->ctx, "Failed to register SIGCHLD: %s\n", strerror(-r));
		goto ERROR;
	}

	// Listen for SIGTERM
	r = sd_event_add_signal(ctx.loop, NULL, SIGTERM|SD_EVENT_SIGNAL_PROCMASK,
			pakfire_jail_SIGTERM, &ctx);
	if (r < 0) {
		ERROR(jail->ctx, "Failed to register SIGTERM: %s\n", strerror(-r));
		goto ERROR;
	}

	// Are we running in interactive mode?
	if (pakfire_jail_exec_has_flag(&ctx, PAKFIRE_JAIL_INTERACTIVE)) {
		// Make the PTY interactive
		pty_flags |= PAKFIRE_PTY_INTERACTIVE;

		// Enable networking
		ctx.flags |= PAKFIRE_JAIL_HAS_NETWORKING;

		// We cannot capture the output in interactive mode
		if (output) {
			ERROR(jail->ctx, "Cannot capture output in interactive mode\n");
			r = -ENOTSUP;
			goto ERROR;
		}

	// Do nothing if we have a callback set
	} else if (stdout_callback) {
		// Nothing

	// Capture Output?
	} else if (output) {
		pty_flags |= PAKFIRE_PTY_CAPTURE_OUTPUT;

	// Otherwise we dump everything to the console
	} else {
		pty_flags |= PAKFIRE_PTY_CONNECT_STDOUT|PAKFIRE_PTY_CONNECT_STDERR;
	}

	/*
		Setup a file descriptor which can be used to notify the client that the parent
		has completed configuration.
	*/
	ctx.completed_fd = r = eventfd(0, EFD_CLOEXEC);
	if (ctx.completed_fd < 0) {
		ERROR(jail->ctx, "eventfd() failed: %m\n");
		goto ERROR;
	}

	// Setup the PTY
	r = pakfire_pty_create(&ctx.pty, jail->ctx, ctx.loop, pty_flags);
	if (r)
		goto ERROR;

	// Configure the callbacks
	if (stdin_callback)
		pakfire_pty_set_stdin_callback(ctx.pty, stdin_callback, stdin_data);
	if (stdout_callback)
		pakfire_pty_set_stdout_callback(ctx.pty, stdout_callback, stdout_data);

	// Setup pipes for logging
	// INFO
	r = pakfire_log_stream_create(&ctx.log.INFO, jail->ctx, pakfire_jail_INFO, jail);
	if (r)
		goto ERROR;

	// WARN
	r = pakfire_log_stream_create(&ctx.log.WARN, jail->ctx, pakfire_jail_WARN, jail);
	if (r)
		goto ERROR;

	// ERROR
	r = pakfire_log_stream_create(&ctx.log.ERROR, jail->ctx, pakfire_jail_ERROR, jail);
	if (r)
		goto ERROR;

#ifdef ENABLE_DEBUG
	// DEBUG
	r = pakfire_log_stream_create(&ctx.log.DEBUG, jail->ctx, pakfire_jail_DEBUG, jail);
	if (r)
		goto ERROR;
#endif /* ENABLE_DEBUG */

	// Setup the timer
	if (jail->timeout) {
		r = sd_event_add_time_relative(ctx.loop, &ctx.timeout, CLOCK_MONOTONIC,
				jail->timeout * 1000000, 0, pakfire_jail_timer, &ctx);
		if (r < 0) {
			ERROR(jail->ctx, "Could not setup timer: %s\n", strerror(-r));
			goto ERROR;
		}
	}

	// Configure child process
	struct clone_args args = {
		.flags =
			CLONE_NEWCGROUP |
			CLONE_NEWIPC |
			CLONE_NEWNS |
			CLONE_NEWPID |
			CLONE_NEWTIME |
			CLONE_NEWUSER |
			CLONE_NEWUTS |
			CLONE_PIDFD,
		.exit_signal = SIGCHLD,
		.pidfd = (long long unsigned int)&ctx.pidfd,
	};

	// Launch the process in a cgroup that is a leaf of the configured cgroup
	if (jail->cgroup) {
		args.flags |= CLONE_INTO_CGROUP;

		// Fetch our UUID
		const char* uuid = pakfire_jail_uuid(jail);

		// Create a temporary cgroup
		r = pakfire_cgroup_child(&ctx.cgroup, jail->cgroup, uuid, 0);
		if (r < 0) {
			ERROR(jail->ctx, "Could not create cgroup for jail: %s\n", strerror(-r));
			goto ERROR;
		}

		// Clone into this cgroup
		args.cgroup = r = pakfire_cgroup_fd(ctx.cgroup);
		if (r < 0) {
			ERROR(jail->ctx, "Could not determine the file descriptor of the cgroup\n");
			goto ERROR;
		}
	}

	// Setup networking
	if (!pakfire_jail_exec_has_flag(&ctx, PAKFIRE_JAIL_HAS_NETWORKING)) {
		args.flags |= CLONE_NEWNET;
	}

	// Fork this process
	ctx.pid = clone3(&args, sizeof(args));
	if (ctx.pid < 0) {
		ERROR(jail->ctx, "Could not clone: %m\n");
		r = -errno;
		goto ERROR;

	// Child process
	} else if (ctx.pid == 0) {
		r = pakfire_jail_child(jail, &ctx);
		_exit(r);
	}

	// Parent process
	r = pakfire_jail_parent(jail, &ctx);
	if (r)
		goto ERROR;

	DEBUG(jail->ctx, "Entering main loop...\n");

	// Main Loop
	r = sd_event_loop(ctx.loop);
	if (r < 0) {
		ERROR(jail->ctx, "Could not run the event loop: %s\n", strerror(-r));
		goto ERROR;
	}

	// Return the output
	if (output)
		*output = pakfire_pty_output(ctx.pty, output_length);

	// Return the exit code
	r = ctx.exit;

ERROR:
	// Destroy the temporary cgroup (if any)
	if (ctx.cgroup) {
		// Read cgroup stats
		pakfire_cgroup_stat(ctx.cgroup, &ctx.cgroup_stats);
		pakfire_cgroup_stat_dump(ctx.cgroup, &ctx.cgroup_stats);
		pakfire_cgroup_destroy(ctx.cgroup, PAKFIRE_CGROUP_DESTROY_RECURSIVE);
		pakfire_cgroup_unref(ctx.cgroup);
	}

	// Close any file descriptors
	if (ctx.pidfd >= 0)
		close(ctx.pidfd);

	// PTY
	if (ctx.pty)
		pakfire_pty_unref(ctx.pty);

	// Logging
	if (ctx.log.INFO)
		pakfire_log_stream_unref(ctx.log.INFO);
	if (ctx.log.WARN)
		pakfire_log_stream_unref(ctx.log.WARN);
	if (ctx.log.ERROR)
		pakfire_log_stream_unref(ctx.log.ERROR);
#ifdef ENABLE_DEBUG
	if (ctx.log.DEBUG)
		pakfire_log_stream_unref(ctx.log.DEBUG);
#endif /* ENABLE_DEBUG */

	if (ctx.timeout)
		sd_event_source_unref(ctx.timeout);
	if (ctx.loop)
		sd_event_unref(ctx.loop);

	return r;
}

int pakfire_jail_exec(struct pakfire_jail* jail,
		pakfire_jail_callback callback, void* data, int flags) {
	return __pakfire_jail_exec(jail, callback, data, flags, NULL, NULL, NULL, NULL, NULL, NULL);
}

int pakfire_jail_exec_command(struct pakfire_jail* jail,
		const char* argv[], struct pakfire_env* env, int flags) {
	struct pakfire_jail_command command = {
		.jail = jail,
		.argv = argv,
		.env  = env,
	};

	return __pakfire_jail_exec(jail, pakfire_jail_launch_command, &command, flags,
		NULL, NULL, NULL, NULL, NULL, NULL);
}

int pakfire_jail_exec_capture_output(struct pakfire_jail* jail,
		const char* argv[], struct pakfire_env* env, int flags, char** output, size_t* length) {
	struct pakfire_jail_command command = {
		.jail = jail,
		.argv = argv,
		.env  = env,
	};

	return __pakfire_jail_exec(jail, pakfire_jail_launch_command, &command, flags,
		NULL, NULL, NULL, NULL, output, length);
}

int pakfire_jail_communicate(
		struct pakfire_jail* jail, const char* argv[], struct pakfire_env* env, int flags,
		pakfire_pty_stdin_callback stdin_callback, void* stdin_data,
		pakfire_pty_stdout_callback stdout_callback, void* stdout_data) {
	struct pakfire_jail_command command = {
		.jail = jail,
		.argv = argv,
		.env  = env,
	};

	return __pakfire_jail_exec(jail, pakfire_jail_launch_command, &command, flags,
		stdin_callback, stdin_data, stdout_callback, stdout_data, NULL, NULL);
}

int pakfire_jail_exec_script(struct pakfire_jail* jail,
		const char* script, const size_t size, const char* args[], struct pakfire_env* env,
		pakfire_pty_stdin_callback stdin_callback, void* stdin_data,
		pakfire_pty_stdout_callback stdout_callback, void* stdout_data) {
	char path[PATH_MAX];
	const char** argv = NULL;
	FILE* f = NULL;
	int r;

	const char* root = pakfire_get_path(jail->pakfire);

	// Write the scriptlet to disk
	r = pakfire_path_append(path, root, PAKFIRE_TMP_DIR "/pakfire-script.XXXXXX");
	if (r)
		goto ERROR;

	// Create a temporary file
	f = pakfire_mktemp(path, 0700);
	if (!f) {
		ERROR(jail->ctx, "Could not create temporary file: %m\n");
		goto ERROR;
	}

	DEBUG(jail->ctx, "Writing script to %s:\n%.*s\n", path, (int)size, script);

	// Write data
	r = fwrite(script, 1, size, f);
	if (r < 0) {
		ERROR(jail->ctx, "Could not write script to file %s: %m\n", path);
		goto ERROR;
	}

	// Close file
	r = fclose(f);
	if (r) {
		ERROR(jail->ctx, "Could not close script file %s: %m\n", path);
		goto ERROR;
	}

	f = NULL;

	// Count how many arguments were passed
	unsigned int argc = 1;
	if (args) {
		for (const char** arg = args; *arg; arg++)
			argc++;
	}

	argv = calloc(argc + 1, sizeof(*argv));
	if (!argv) {
		ERROR(jail->ctx, "Could not allocate argv: %m\n");
		goto ERROR;
	}

	// Set command
	argv[0] = (root) ? pakfire_path_relpath(root, path) : path;

	// Copy args
	for (unsigned int i = 1; i < argc; i++)
		argv[i] = args[i-1];

	// Run the script
	r = pakfire_jail_communicate(jail, argv, env, 0,
		stdin_callback, stdin_data, stdout_callback, stdout_data);

ERROR:
	if (argv)
		free(argv);
	if (f)
		fclose(f);

	// Remove script from disk
	if (*path)
		unlink(path);

	return r;
}

/*
	A convenience function that creates a new jail, runs the given command and destroys
	the jail again.
*/
int pakfire_jail_run(struct pakfire* pakfire, const char* argv[], struct pakfire_env* env,
		int flags, char** output, size_t* output_length) {
	struct pakfire_jail* jail = NULL;
	int r;

	// Create a new jail
	r = pakfire_jail_create(&jail, pakfire);
	if (r)
		goto ERROR;

	// Execute the command
	r = pakfire_jail_exec_capture_output(jail, argv, env, flags, output, output_length);

ERROR:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

int pakfire_jail_run_script(struct pakfire* pakfire, const char* script, const size_t length,
		const char* argv[], struct pakfire_env* env, int flags) {
	struct pakfire_jail* jail = NULL;
	int r;

	// Create a new jail
	r = pakfire_jail_create(&jail, pakfire);
	if (r)
		goto ERROR;

	// Execute the command
	r = pakfire_jail_exec_script(jail, script, length, argv, env, NULL, NULL, NULL, NULL);

ERROR:
	if (jail)
		pakfire_jail_unref(jail);

	return r;
}

int pakfire_jail_shell(struct pakfire_jail* jail, struct pakfire_env* e) {
	struct pakfire_env* env = NULL;
	int r;

	const char* argv[] = {
		"/bin/bash", "--login", NULL,
	};

	// Create a new environment
	r = pakfire_env_create(&env, jail->ctx);
	if (r < 0)
		goto ERROR;

	// Copy TERM
	char* TERM = secure_getenv("TERM");
	if (TERM) {
		r = pakfire_env_set(env, "TERM", "%s", TERM);
		if (r < 0)
			goto ERROR;
	}

	// Copy LANG
	char* LANG = secure_getenv("LANG");
	if (LANG) {
		r = pakfire_env_set(env, "LANG", "%s", LANG);
		if (r < 0)
			goto ERROR;
	}

	// Merge the custom environment
	if (e) {
		r = pakfire_env_merge(env, e);
		if (r < 0)
			goto ERROR;
	}

	// Execute /bin/bash
	r = pakfire_jail_exec_command(jail, argv, env, PAKFIRE_JAIL_INTERACTIVE);
	if (r < 0)
		goto ERROR;

	// Ignore any return codes from the shell
	r = 0;

ERROR:
	if (env)
		pakfire_env_unref(env);

	return r;
}

static int pakfire_jail_run_if_possible(struct pakfire* pakfire, const char** argv) {
	char path[PATH_MAX];
	int r;

	// Fetch the context
	struct pakfire_ctx* ctx = pakfire_ctx(pakfire);

	r = pakfire_path(pakfire, path, "%s", *argv);
	if (r)
		goto ERROR;

	// Check if the file is executable
	r = access(path, X_OK);
	if (r) {
		DEBUG(ctx, "%s is not executable. Skipping...\n", *argv);
		goto ERROR;
	}

	r = pakfire_jail_run(pakfire, argv, NULL, 0, NULL, NULL);

ERROR:
	if (ctx)
		pakfire_ctx_unref(ctx);

	return r;
}

int pakfire_jail_ldconfig(struct pakfire* pakfire) {
	const char* argv[] = {
		"/sbin/ldconfig",
		NULL,
	};

	return pakfire_jail_run_if_possible(pakfire, argv);
}

int pakfire_jail_run_systemd_tmpfiles(struct pakfire* pakfire) {
	const char* argv[] = {
		"/usr/bin/systemd-tmpfiles",
		"--create",
		NULL,
	};

	return pakfire_jail_run_if_possible(pakfire, argv);
}
