/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <linux/sched.h>
#include <unistd.h>

#include <json.h>

#include <uuid/uuid.h>

#include <systemd/sd-event.h>

#include <pakfire/build.h>
#include <pakfire/config.h>
#include <pakfire/constants.h>
#include <pakfire/ctx.h>
#include <pakfire/daemon.h>
#include <pakfire/job.h>
#include <pakfire/json.h>
#include <pakfire/log_file.h>
#include <pakfire/log_buffer.h>
#include <pakfire/log_stream.h>
#include <pakfire/logging.h>
#include <pakfire/proctitle.h>
#include <pakfire/string.h>
#include <pakfire/syscalls.h>
#include <pakfire/util.h>

struct pakfire_job {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Daemon
	struct pakfire_daemon* daemon;

	// Build Service
	struct pakfire_buildservice* service;

	// Event Loop
	sd_event* loop;

	// The ID of this job (should not be longer than a UUID)
	char id[UUID_STR_LEN];

	char name[NAME_MAX];
	char arch[ARCH_MAX];

	// Flags
	enum {
		PAKFIRE_JOB_TEST       = (1 << 0),
		PAKFIRE_JOB_CCACHE     = (1 << 1),
		PAKFIRE_JOB_STREAM_LOG = (1 << 2),
	} flags;

	// Package URL
	char pkg[PATH_MAX];

	// ccache path
	char ccache_path[PATH_MAX];

	// Configuration
	struct pakfire_config* config;

	// PID File Descriptor
	int pidfd;

	// Logging
	struct {
		// File
		struct pakfire_log_file* file;

		// Buffer
		struct pakfire_log_buffer* buffer;

		// Streams
		struct pakfire_log_stream* stdout;
		struct pakfire_log_stream* stderr;

		// Stream Event
		sd_event_source* stream;
	} log;

	// Track the current state
	enum {
		PAKFIRE_JOB_STATE_INIT = 0,
		PAKFIRE_JOB_STATE_LAUNCHED,
		PAKFIRE_JOB_STATE_EXITED,
		PAKFIRE_JOB_STATE_KILLED,
	} state;

	// Uploads
	char** uploads;
};

static int pakfire_job_has_flag(struct pakfire_job* self, int flag) {
	return (self->flags & flag);
}

static int pakfire_parse_job(struct pakfire_job* job, json_object* data) {
	json_object* ccache = NULL;
	json_object* o = NULL;
	int r;

	// Fetch the Job ID
	if (!json_object_object_get_ex(data, "id", &o)) {
		ERROR(job->ctx, "Job does not have an ID\n");
		return -EINVAL;
	}

	// Store the job ID
	r = pakfire_string_set(job->id, json_object_get_string(o));
	if (r < 0)
		return r;

	// Check if we have a valid UUID
	if (!pakfire_uuid_is_valid(job->id)) {
		ERROR(job->ctx, "The job ID is not a valid UUID\n");
		return -EINVAL;
	}

	// Fetch the name
	if (!json_object_object_get_ex(data, "name", &o)) {
		ERROR(job->ctx, "Job does not have a name\n");

		return -EINVAL;
	}

	// Store the name
	r = pakfire_string_set(job->name, json_object_get_string(o));
	if (r) {
		ERROR(job->ctx, "Could not store name: %m\n");

		return r;
	}

	// Fetch the arch
	if (!json_object_object_get_ex(data, "arch", &o)) {
		ERROR(job->ctx, "Job does not have an architecture\n");

		return -EINVAL;
	}

	// Store the arch
	r = pakfire_string_set(job->arch, json_object_get_string(o));
	if (r) {
		ERROR(job->ctx, "Could not store arch: %m\n");

		return r;
	}

	// Fetch ccache settings
	if (json_object_object_get_ex(data, "ccache", &ccache)) {
		if (json_object_object_get_ex(ccache, "enabled", &o)) {
			if (json_object_get_boolean(o)) {
				job->flags |= PAKFIRE_JOB_CCACHE;
			}
		}

		// Path
		if (json_object_object_get_ex(ccache, "path", &o)) {
			r = pakfire_string_set(job->ccache_path, json_object_get_string(o));
			if (r) {
				ERROR(job->ctx, "Could not store the ccache path: %m\n");

				return r;
			}
		}
	}

	// Check if this is a test job
	if (json_object_object_get_ex(data, "test", &o)) {
		if (json_object_get_boolean(o)) {
			job->flags |= PAKFIRE_JOB_TEST;
		}
	}

	// Fetch the configuration
	if (!json_object_object_get_ex(data, "config", &o)) {
		ERROR(job->ctx, "Job does not have a configuration\n");

		return -EINVAL;
	}

	// Parse the configuration
	r = pakfire_config_parse(job->config, json_object_get_string(o), -1);
	if (r < 0) {
		ERROR(job->ctx, "Could not parse the configuration: %s\n", strerror(-r));

		return r;
	}

	// Fetch the package URL
	if (!json_object_object_get_ex(data, "pkg", &o)) {
		ERROR(job->ctx, "Job does not have a package URL\n");

		return -EINVAL;
	}

	// Store the package URL
	r = pakfire_string_set(job->pkg, json_object_get_string(o));
	if (r) {
		ERROR(job->ctx, "Could not store the package URL: %m\n");

		return r;
	}

	DEBUG(job->ctx, "Job parsing completed\n");

	INFO(job->ctx, "Received a new job:\n");
	INFO(job->ctx, "  ID   : %s\n", job->id);
	INFO(job->ctx, "  Name : %s\n", job->name);
	INFO(job->ctx, "  Arch : %s\n", job->arch);

	return 0;
}

static void pakfire_job_free(struct pakfire_job* job) {
	// Logging
	if (job->log.buffer)
		pakfire_log_buffer_unref(job->log.buffer);
	if (job->log.stdout)
		pakfire_log_stream_unref(job->log.stdout);
	if (job->log.stderr)
		pakfire_log_stream_unref(job->log.stderr);
	if (job->log.stream)
		sd_event_source_unref(job->log.stream);

	if (job->service)
		pakfire_buildservice_unref(job->service);
	if (job->uploads)
		pakfire_strings_free(job->uploads);
	if (job->config)
		pakfire_config_unref(job->config);
	if (job->loop)
		sd_event_unref(job->loop);
	if (job->daemon)
		pakfire_daemon_unref(job->daemon);
	if (job->ctx)
		pakfire_ctx_unref(job->ctx);
	free(job);
}

static int pakfire_job_xfer_create(struct pakfire_xfer** xfer,
	struct pakfire_job* job, const char* url, ...) __attribute__((format(printf, 3, 4)));

static int pakfire_job_xfer_create(struct pakfire_xfer** xfer,
		struct pakfire_job* job, const char* url, ...) {
	struct pakfire_xfer* x = NULL;
	va_list args;
	int r;

	va_start(args, url);

	// Create a new xfer
	r = pakfire_xfer_create(&x, job->ctx, url, args);
	if (r < 0)
		goto ERROR;

	// Set the base URL
	r = pakfire_xfer_set_baseurl(x, pakfire_daemon_url(job->daemon));
	if (r < 0)
		goto ERROR;

	// Success
	*xfer = pakfire_xfer_ref(x);

ERROR:
	if (x)
		pakfire_xfer_unref(x);
	va_end(args);

	return r;
}

/*
	Called when the job has finished with status as the error code.
*/
static int pakfire_job_finished(struct pakfire_job* job, int status) {
	struct pakfire_xfer* xfer = NULL;
	const char* filename = NULL;
	const char* path = NULL;
	char* logfile = NULL;
	int r;

	DEBUG(job->ctx, "Job %s has finished with status %d\n", job->id, status);

	// Fetch the filename of the log file
	filename = pakfire_log_file_filename(job->log.file);
	if (!filename) {
		ERROR(job->ctx, "Log file has no filename\n");
		r = -EINVAL;
		goto ERROR;
	}

	// Fetch the path of the log file
	path = pakfire_log_file_path(job->log.file);
	if (!path) {
		ERROR(job->ctx, "Log file has no path\n");
		r = -EINVAL;
		goto ERROR;
	}

	// Upload the log file
	r = pakfire_buildservice_upload(job->service, path, filename, &logfile);
	if (r < 0) {
		ERROR(job->ctx, "Could not upload the log file: %s\n", strerror(-r));
		goto ERROR;
	}

	// Create a new xfer
	r = pakfire_job_xfer_create(&xfer, job, "/api/v1/jobs/%s/finished", job->id);
	if (r < 0)
		goto ERROR;

	// Enable authentication
	r = pakfire_xfer_auth(xfer);
	if (r < 0)
		goto ERROR;

	// Has the job been successful?
	r = pakfire_xfer_add_param(xfer, "success", "%s", (status == 0) ? "true" : "false");
	if (r < 0)
		goto ERROR;

	// Logfile
	if (logfile) {
		r = pakfire_xfer_add_param(xfer, "logfile", "%s", logfile);
		if (r)
			goto ERROR;
	}

	// Packages
	if (job->uploads) {
		for (char** upload = job->uploads; *upload; upload++) {
			r = pakfire_xfer_add_param(xfer, "package", "%s", *upload);
			if (r < 0)
				goto ERROR;
		}
	}

	// Send the request
	r = pakfire_xfer_run_api_request(xfer, NULL);
	if (r < 0)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);

	return r;
}

static int pakfire_job_crashed(struct pakfire_job* job, const siginfo_t* si) {
	struct pakfire_xfer* xfer = NULL;
	int r;

	struct pakfire_job_log {
		char* data;
		size_t length;
	} log = {};

	DEBUG(job->ctx, "Sending crash report...\n");

	// Dump the log
	r = pakfire_log_buffer_dump(job->log.buffer, &log.data, &log.length);
	if (r < 0)
		goto ERROR;

	// Create a new transfer
	r = pakfire_job_xfer_create(&xfer, job, "/api/v1/jobs/%s/crashed", job->id);
	if (r < 0)
		goto ERROR;

	// Enable authentication
	r = pakfire_xfer_auth(xfer);
	if (r)
		goto ERROR;

	// Add the exit code
	r = pakfire_xfer_add_param(xfer, "signo", "%d", si->si_status);
	if (r < 0)
		goto ERROR;

	// If we have a log, let's send the log, too
	// XXX THIS IS NOT QUITE IDEAL BECAUSE THE LOG COULD CONTAIN NULL-CHARACTERS
	// WE SHOULD IMPLEMENT THIS WITH CURL'S MIME CALLBACK FUNCTIONS
	if (log.data) {
		r = pakfire_xfer_add_param(xfer, "log", "%.*s", (int)log.length, log.data);
		if (r < 0)
			goto ERROR;
	}

	// Send the request
	r = pakfire_xfer_run_api_request(xfer, NULL);
	if (r < 0)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (log.data)
		free(log.data);

	return r;
}

static int pakfire_job_result(struct pakfire_ctx* ctx, struct pakfire* pakfire,
		struct pakfire_build* build, struct pakfire_archive* archive, void* data) {
	struct pakfire_job* job = data;
	struct pakfire_package* pkg = NULL;
	char* uuid = NULL;
	int r;

	// Fetch package metadata
	r = pakfire_archive_make_package(archive, NULL, &pkg);
	if (r < 0)
		goto ERROR;

	// Fetch NEVRA
	const char* nevra = pakfire_package_get_string(pkg, PAKFIRE_PKG_NEVRA);
	if (!nevra) {
		r = -EINVAL;
		goto ERROR;
	}

	// Fetch filename
	const char* filename = pakfire_package_get_filename(pkg);
	if (!filename) {
		r = -EINVAL;
		goto ERROR;
	}

	// Fetch path
	const char* path = pakfire_archive_get_path(archive);
	if (!path) {
		r = -EINVAL;
		goto ERROR;
	}

	// Upload the file
	r = pakfire_buildservice_upload(job->service, path, filename, &uuid);
	if (r < 0) {
		ERROR(job->ctx, "Could not upload %s: %s\n", nevra, strerror(-r));
		goto ERROR;
	}

	// Store the ID of the upload
	r = pakfire_strings_append(&job->uploads, uuid);
	if (r < 0)
		goto ERROR;

ERROR:
	if (pkg)
		pakfire_package_unref(pkg);
	if (uuid)
		free(uuid);

	return r;
}

/*
	This method is triggered by SIGCHLD whenever the job exits
*/
static int pakfire_job_exited(sd_event_source* s, const siginfo_t* si, void* data) {
	struct pakfire_job* job = data;
	int r;

	switch (si->si_code) {
		case CLD_EXITED:
			DEBUG(job->ctx, "Job %s has exited with status code %d\n",
				job->id, si->si_status);

			// Update state
			job->state = PAKFIRE_JOB_STATE_EXITED;
			break;

		case CLD_KILLED:
			ERROR(job->ctx, "Job %s has been killed by signal %d\n",
				job->id, si->si_signo);

			// Update state
			job->state = PAKFIRE_JOB_STATE_KILLED;

			// Send some information about this
			r = pakfire_job_crashed(job, si);
			if (r < 0)
				ERROR(job->ctx, "Could not send crash report: %s\n", strerror(-r));

			break;
	}

	// Let the daemon know this is finished
	return pakfire_daemon_job_finished(job->daemon, job);
}

static int pakfire_job_send_log(struct pakfire_job* job, int priority, const char* line, size_t length) {
	int r;

	// Enqueue the line into the buffer
	r = pakfire_log_buffer_enqueue(job->log.buffer, priority, line, length);
	if (r < 0)
		return r;

	// Ask the daemon to send it
	return pakfire_daemon_stream_logs(job->daemon);
}

static int pakfire_job_stdout(struct pakfire_log_stream* stream,
		const char* line, size_t length, void* data) {
	struct pakfire_job* job = data;

	// Send the output to the build service
	return pakfire_job_send_log(job, LOG_INFO, line, length);
}

static int pakfire_job_stderr(struct pakfire_log_stream* stream,
		const char* line, size_t length, void* data) {
	struct pakfire_job* job = data;

	// Send the output to the build service
	return pakfire_job_send_log(job, LOG_ERR, line, length);
}

/*
	Called to initialize the parent process.
*/
static int pakfire_job_parent(struct pakfire_job* job) {
	int r;

	// Register the PID file descriptor
	r = sd_event_add_child_pidfd(job->loop, NULL, job->pidfd, WEXITED, pakfire_job_exited, job);
	if (r < 0) {
		DEBUG(job->ctx, "Could not register the job with the event loop: %s\n", strerror(-r));

		return r;
	}

	// Read the standard output
	r = pakfire_log_stream_in_parent(job->log.stdout, job->loop);
	if (r < 0)
		return r;

	// Read the standard error
	r = pakfire_log_stream_in_parent(job->log.stderr, job->loop);
	if (r < 0)
		return r;

	return 0;
}

static void pakfire_job_log(void* data, int priority, const char* file,
	int line, const char* fn, const char* format, va_list args)
	__attribute__((format(printf, 6, 0)));

static void pakfire_job_log(void* data, int priority, const char* file,
		int line, const char* fn, const char* format, va_list args) {
	struct pakfire_job* job = data;
	char* buffer = NULL;

	// Format the line
	const ssize_t length = vasprintf(&buffer, format, args);

	// Fail if we could not format the log line
	if (unlikely(length < 0))
		return;

	// We only forward INFO, WARNING & ERROR
	switch (priority) {
		case LOG_INFO:
			pakfire_log_stream_write(job->log.stdout, buffer, length);
			break;

		case LOG_WARNING:
		case LOG_ERR:
			pakfire_log_stream_write(job->log.stderr, buffer, length);
			break;

		default:
			break;
	}

	// Send everything but debug messages to the log file
	if (likely(job->log.file))
		pakfire_log_file_write(job->log.file, priority, buffer, length);

	// Pass everything to the upstream logger
	pakfire_ctx_log_condition(job->ctx, priority, "%s", buffer);
}

static int pakfire_job_child(struct pakfire_job* job) {
	struct pakfire_ctx* ctx = NULL;
	struct pakfire_build* build = NULL;
	int build_flags = 0;
	int status;
	int r;

	// Fetch our PID
	pid_t pid = getpid();

	DEBUG(job->ctx, "Launched job child as PID %d\n", pid);

	// Setup the standard output log stream
	r = pakfire_log_stream_in_child(job->log.stdout);
	if (r < 0)
		goto ERROR;

	// Setup the standard error log stream
	r = pakfire_log_stream_in_child(job->log.stderr);
	if (r < 0)
		goto ERROR;

	// Set the process title
	r = pakfire_proctitle_set("Job: %s (%s)", job->name, job->id);
	if (r < 0) {
		ERROR(job->ctx, "Failed to set process name: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Replace the context with a new one
	r = pakfire_ctx_create(&ctx, NULL);
	if (r < 0) {
		ERROR(ctx, "Could not create a new context: %s\n", strerror(-r));
		goto ERROR;
	}

	// Restore the log level
	pakfire_ctx_set_log_level(ctx, pakfire_ctx_get_log_level(job->ctx));

	// Setup logging
	pakfire_ctx_set_log_callback(ctx, pakfire_job_log, job);

	// Open a new log file
	r = pakfire_log_file_create(&job->log.file, ctx, NULL, job->id, PAKFIRE_LOG_FILE_COMPRESS);
	if (r < 0) {
		ERROR(ctx, "Could not open log file: %s\n", strerror(-r));
		goto ERROR;
	}

	// Disable the ccache
	if (!(job->flags & PAKFIRE_JOB_CCACHE))
		build_flags |= PAKFIRE_BUILD_DISABLE_CCACHE;

	// Create a new build environment
	r = pakfire_build_create(&build, ctx, job->config, job->arch, job->id, build_flags);
	if (r) {
		ERROR(ctx, "Could not setup the build environment: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Set the ccache path (if set)
	if (job->flags & PAKFIRE_JOB_CCACHE) {
		if (*job->ccache_path) {
			// XXX THIS NEEDS TO BE PREFIXED WITH THE BASE PATH
			r = pakfire_build_set_ccache_path(build, job->ccache_path);
			if (r) {
				ERROR(ctx, "Could not set ccache path: %m\n");
				r = -errno;
				goto ERROR;
			}
		}
	}

	// Run the build
	status = pakfire_build_exec(build, job->pkg, pakfire_job_result, job);

	// Close the log file
	r = pakfire_log_file_close(job->log.file);
	if (r < 0) {
		ERROR(ctx, "Could not close the log file: %s\n", strerror(-r));
		goto ERROR;
	}

	// Signal that the job has finished
	r = pakfire_job_finished(job, status);
	if (r < 0)
		goto ERROR;

ERROR:
	if (job->log.file)
		pakfire_log_file_unref(job->log.file);
	if (build)
		pakfire_build_unref(build);
	if (ctx)
		pakfire_ctx_unref(ctx);

	return r;
}

/*
	Launches the job
*/
int pakfire_job_launch(struct pakfire_job* job) {
	int pid;
	int r;

	DEBUG(job->ctx, "Launching job %s\n", job->id);

	// Update state
	job->state = PAKFIRE_JOB_STATE_LAUNCHED;

	// Setup standard output
	r = pakfire_log_stream_create(&job->log.stdout, job->ctx, pakfire_job_stdout, job);
	if (r < 0) {
		ERROR(job->ctx, "Could not setup standard output: %s\n", strerror(-r));
		return -errno;
	}

	// Setup standard error
	r = pakfire_log_stream_create(&job->log.stderr, job->ctx, pakfire_job_stderr, job);
	if (r < 0) {
		ERROR(job->ctx, "Could not setup standard error: %s\n", strerror(-r));
		return -errno;
	}

	// Configure child process
	struct clone_args args = {
		.flags = CLONE_PIDFD,
		.exit_signal = SIGCHLD,
		.pidfd = (long long unsigned int)&job->pidfd,
	};

	// Fork this process
	pid = clone3(&args, sizeof(args));
	if (pid < 0) {
		ERROR(job->ctx, "Could not clone: %m\n");

		return -errno;

	// Child process
	} else if (pid == 0) {
		r = pakfire_job_child(job);
		_exit(r);
	}

	return pakfire_job_parent(job);
}

static int pakfire_job_send_log_line(struct pakfire_job* job,
		const struct timeval* timestamp, int priority, const char* line, size_t length) {
	struct json_object* message = NULL;
	struct json_object* data = NULL;
	char buffer[64];
	int r;

	// Create a new JSON object
	data = json_object_new_object();
	if (!data) {
		ERROR(job->ctx, "Could not create a new JSON object: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Format timestamp
	r = pakfire_timeval_to_iso8601(buffer, timestamp);
	if (r < 0)
		goto ERROR;

	// Add the timestamp
	r = pakfire_json_add_string(data, "timestamp", buffer);
	if (r < 0)
		goto ERROR;

	// Add the priority
	r = pakfire_json_add_uint64(data, "priority", priority);
	if (r)
		goto ERROR;

	// Add the line
	r = pakfire_json_add_stringn(data, "line", line, length);
	if (r)
		goto ERROR;

	// Create a new stats object
	message = json_object_new_object();
	if (!message) {
		r = -errno;
		goto ERROR;
	}

	// Add the job ID
	r = pakfire_json_add_string(message, "job_id", job->id);
	if (r < 0)
		goto ERROR;

	// Set type
	r = pakfire_json_add_string(message, "type", "log");
	if (r)
		goto ERROR;

	// Set data
	r = json_object_object_add(message, "data", json_object_get(data));
	if (r)
		goto ERROR;

	// Send the message
	r = pakfire_daemon_send_message(job->daemon, message);
	if (r < 0) {
		ERROR(job->ctx, "Could not send log message: %s\n", strerror(-r));
		goto ERROR;
	}

ERROR:
	if (message)
		json_object_put(message);
	if (data)
		json_object_put(data);

	return r;
}

int pakfire_job_stream_logs(struct pakfire_job* self) {
	struct timeval timestamp = {};
	char* line = NULL;
	size_t length = 0;
	int priority;
	int r;

	// Don't do this if not enabled
	if (!pakfire_job_has_flag(self, PAKFIRE_JOB_STREAM_LOG))
		return 0;

	// Try to dequeue a line from the log buffer
	r = pakfire_log_buffer_dequeue(self->log.buffer, &timestamp, &priority, &line, &length);
	if (r < 0) {
		ERROR(self->ctx, "Could not dequeue from the log buffer: %s\n", strerror(-r));
		return r;
	}

	// Done if there is no data
	if (!line)
		return 0;

	// If we have received a line let's send it
	r = pakfire_job_send_log_line(self, &timestamp, priority, line, length);

	// Cleanup
	free(line);

	// Raise any errors
	if (r < 0)
		return r;

	// Otherwise return the number of lines sent
	return 1;
}

int pakfire_job_create(struct pakfire_job** job, struct pakfire_ctx* ctx,
		struct pakfire_daemon* daemon, json_object* data) {
	struct pakfire_job* j = NULL;
	int r;

	// Allocate a new object
	j = calloc(1, sizeof(*j));
	if (!j)
		return -errno;

	// Reference the context
	j->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	j->nrefs = 1;

	// Store a reference to the daemon
	j->daemon = pakfire_daemon_ref(daemon);

	// Fetch a reference to the event loop
	j->loop = pakfire_daemon_loop(daemon);
	if (!j->loop) {
		ERROR(j->ctx, "Could not fetch the event loop: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Fetch a reference to the build service
	j->service = pakfire_daemon_buildservice(daemon);

	// Initialize the PID file descriptor
	j->pidfd = -EBADF;

	// Initialize the configuration
	r = pakfire_config_create(&j->config);
	if (r < 0)
		goto ERROR;

	// Allocate the log buffer
	r = pakfire_log_buffer_create(&j->log.buffer, j->ctx, 1024);
	if (r < 0)
		goto ERROR;

	// Parse the job
	r = pakfire_parse_job(j, data);
	if (r)
		goto ERROR;

	// Return the reference
	*job = j;

	return 0;

ERROR:
	pakfire_job_free(j);

	return r;
}

struct pakfire_job* pakfire_job_ref(struct pakfire_job* job) {
	++job->nrefs;

	return job;
}

struct pakfire_job* pakfire_job_unref(struct pakfire_job* job) {
	if (--job->nrefs > 0)
		return job;

	pakfire_job_free(job);
	return NULL;
}

int pakfire_job_has_id(struct pakfire_job* job, const char* id) {
	return pakfire_string_equals(job->id, id);
}

/*
	Terminates the job (if it is still running)
*/
int pakfire_job_terminate(struct pakfire_job* job, int signal) {
	int r;

	// Fail if we don't have a PID file descriptor
	if (job->pidfd < 0)
		return -ENOTSUP;

	// Send a signal to the child process
	r = pidfd_send_signal(job->pidfd, signal, NULL, 0);
	if (r) {
		ERROR(job->ctx, "Could not terminate job: %m\n");
		return r;
	}

	return 0;
}

int pakfire_job_handle_message(struct pakfire_job* self, struct json_object* message) {
	const char* command = NULL;
	int r;

	DEBUG(self->ctx, "Message received for job %s\n", self->id);

	// Fetch the command
	r = pakfire_json_get_string(message, "command", &command);
	if (r < 0) {
		ERROR(self->ctx, "Malformed job message does not contain a command\n");
		return r;
	}

	// Launch log stream
	if (pakfire_string_equals(command, "launch-log-stream"))
		self->flags |= PAKFIRE_JOB_STREAM_LOG;

	// Terminate log stream
	else if (pakfire_string_equals(command, "terminate-log-stream"))
		self->flags &= ~PAKFIRE_JOB_STREAM_LOG;

	// Abort job
	if (pakfire_string_equals(command, "abort"))
		return pakfire_job_terminate(self, SIGTERM);

	// Log an error if we have received an unknown command
	ERROR(self->ctx, "Unhandled command for job %s: %s\n", self->id, command);

	return 0;
}
