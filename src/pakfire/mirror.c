/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>

#include <pakfire/ctx.h>
#include <pakfire/mirror.h>
#include <pakfire/string.h>

// Retry a mirror up to N times before marking it as broken
#define MIRROR_RETRIES			3

struct pakfire_mirror {
	struct pakfire_ctx* ctx;
	int nrefs;

	char url[PATH_MAX];

	unsigned int retries_left;

	enum pakfire_mirror_flags {
		PAKFIRE_MIRROR_ENABLED = (1 << 0),
	} flags;
};

static void pakfire_mirror_free(struct pakfire_mirror* mirror) {
	if (mirror->ctx)
		pakfire_ctx_unref(mirror->ctx);

	free(mirror);
}

int pakfire_mirror_create(struct pakfire_mirror** mirror,
		struct pakfire_ctx* ctx, const char* url) {
	struct pakfire_mirror* m = NULL;
	int r;

	// Allocate the mirror
	m = calloc(1, sizeof(*m));
	if (!m)
		return -errno;

	// Store a reference to the context
	m->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	m->nrefs = 1;

	// Set default flags
	m->flags = PAKFIRE_MIRROR_ENABLED;

	// Store the URL
	r = pakfire_string_set(m->url, url);
	if (r)
		goto ERROR;

	// If the URL does not end with a /, let's add it
	if (!pakfire_string_endswith(m->url, "/")) {
		r = pakfire_string_append(m->url, "/");
		if (r < 0)
			goto ERROR;
	}

	// Set the default retries
	m->retries_left = MIRROR_RETRIES;

	// Return the pointer
	*mirror = pakfire_mirror_ref(m);

ERROR:
	if (m)
		pakfire_mirror_unref(m);

	return r;
}

struct pakfire_mirror* pakfire_mirror_ref(struct pakfire_mirror* mirror) {
	++mirror->nrefs;

	return mirror;
}

struct pakfire_mirror* pakfire_mirror_unref(struct pakfire_mirror* mirror) {
	if (--mirror->nrefs > 0)
		return mirror;

	pakfire_mirror_free(mirror);
	return NULL;
}

const char* pakfire_mirror_get_url(struct pakfire_mirror* mirror) {
	return mirror->url;
}

// Enabled?

int pakfire_mirror_is_enabled(struct pakfire_mirror* mirror) {
	return (mirror->flags & PAKFIRE_MIRROR_ENABLED);
}

/*
	This function is called when a transfer has failed and will automatically
	disable the mirror.
*/
int pakfire_mirror_xfer_failed(struct pakfire_mirror* self, pakfire_xfer_error_code_t code) {
	switch (code) {
		// We should never be called with this
		case PAKFIRE_XFER_OK:
			return -EINVAL;

		/*
			The mirror seems to have responded with something
			like 502 or 503, so there is no point in tryin again.
		*/
		case PAKFIRE_XFER_TRANSPORT_ERROR:
			break;

		/*
			For all other errors, we will just count down
		*/
		default:
			if (self->retries_left-- > 0)
				return 0;
			break;
	}

	// Disable the mirror
	self->flags &= ~PAKFIRE_MIRROR_ENABLED;

	// Log action
	WARN(self->ctx, "Mirror %s has been disabled\n", self->url);

	return 0;
}
