/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>

#include <pakfire/ctx.h>
#include <pakfire/elf.h>
#include <pakfire/file.h>
#include <pakfire/hex.h>
#include <pakfire/logging.h>
#include <pakfire/path.h>
#include <pakfire/string.h>

// libelf
#include <gelf.h>
#include <elfutils/libdwelf.h>

struct pakfire_elf {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Path
	char path[PATH_MAX];

	// File Descriptor
	int fd;

	// ELF Object
	Elf* elf;

	// ELF Header
	GElf_Ehdr ehdr;

	// Number of Program Headers
	size_t phnum;

	// Strings
	size_t shstrndx;

	// Interpreter
	const char* interpreter;

	// SONAME
	const char* soname;

	// GNU Build ID
	char* build_id;

	// GNU Debuglink
	const char* debuglink;
};

static int pakfire_elf_init_libelf(struct pakfire_ctx* ctx) {
	// Initialize libelf
	if (elf_version(EV_CURRENT) == EV_NONE) {
		ERROR(ctx, "Could not initialize libelf: %s\n", elf_errmsg(-1));
		return -EINVAL;
	}

	return 0;
}

static int pakfire_elf_open_elf(struct pakfire_elf* self) {
	GElf_Ehdr* ehdr = NULL;
	int r;

	// Make sure libelf is initialized
	r = pakfire_elf_init_libelf(self->ctx);
	if (r < 0)
		return r;

	// Open the ELF file
	self->elf = elf_begin(self->fd, ELF_C_READ, NULL);
	if (!self->elf) {
		ERROR(self->ctx, "Could not open ELF file: %m\n");
		return -errno;
	}

	// Is this actually an ELF file?
	switch (elf_kind(self->elf)) {
		case ELF_K_ELF:
			break;

		// Fail for everything else
		default:
			return -ENOTSUP;
	}

	// Fetch the ELF header
	ehdr = gelf_getehdr(self->elf, &self->ehdr);
	if (!ehdr) {
		ERROR(self->ctx, "Could not fetch the ELF header: %s\n", elf_errmsg(-1));
		return -EINVAL;
	}

	// Fetch the total numbers of program headers
	r = elf_getphdrnum(self->elf, &self->phnum);
	if (r) {
		ERROR(self->ctx,
			"Could not fetch number of program headers: %s\n", elf_errmsg(-1));
		return -EINVAL;
	}

	// Find the strings
	r = elf_getshdrstrndx(self->elf, &self->shstrndx);
	if (r < 0) {
		ERROR(self->ctx, "elf_getshdrstrndx() failed: %s\n", elf_errmsg(-1));
		return -EINVAL;
	}

	return 0;
}

static void pakfire_elf_free(struct pakfire_elf* self) {
	if (self->build_id)
		free(self->build_id);
	if (self->elf)
		elf_end(self->elf);
	if (self->fd >= 0)
		close(self->fd);
	if (self->ctx)
		pakfire_ctx_unref(self->ctx);
	free(self);
}

int pakfire_elf_open(struct pakfire_elf** elf,
		struct pakfire_ctx* ctx, const char* path, int fd) {
	struct pakfire_elf* self = NULL;
	int r;

	// Require a valid file descriptor
	if (fd < 0)
		return -EBADF;

	// Allocate a new object
	self = calloc(1, sizeof(*self));
	if (!self)
		return -errno;

	// Store a reference to the context
	self->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	self->nrefs = 1;

	// Store the path
	r = pakfire_string_set(self->path, path);
	if (r < 0)
		goto ERROR;

	// Store the file descriptor
	self->fd = dup(fd);
	if (self->fd < 0) {
		ERROR(self->ctx, "Could not duplicate file descriptor: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Open the ELF file
	r = pakfire_elf_open_elf(self);
	if (r < 0)
		goto ERROR;

	// Return the pointer
	*elf = self;

	return 0;

ERROR:
	pakfire_elf_free(self);

	return r;
}

int pakfire_elf_open_file(struct pakfire_elf** elf,
		struct pakfire_ctx* ctx, struct pakfire_file* file) {
	const char* path = NULL;
	int fd = -EBADF;
	int r;

	// Fetch the path
	path = pakfire_file_get_path(file);

	// Open the file
	fd = pakfire_file_open(file, 0);
	if (fd < 0)
		return -errno;

	// Open the ELF object
	r = pakfire_elf_open(elf, ctx, path, fd);
	if (r < 0)
		goto ERROR;

ERROR:
	if (fd >= 0)
		close(fd);

	return r;
}

struct pakfire_elf* pakfire_elf_ref(struct pakfire_elf* self) {
	self->nrefs++;

	return self;
}

struct pakfire_elf* pakfire_elf_unref(struct pakfire_elf* self) {
	if (--self->nrefs > 0)
		return self;

	pakfire_elf_free(self);
	return NULL;
}

const char* pakfire_elf_path(struct pakfire_elf* self) {
	return self->path;
}

int pakfire_elf_type(struct pakfire_elf* self) {
	return self->ehdr.e_type;
}

int pakfire_elf_machine(struct pakfire_elf* self) {
	return self->ehdr.e_machine;
}

int pakfire_elf_is_elf64(struct pakfire_elf* self) {
	return self->ehdr.e_ident[EI_CLASS] = ELFCLASS64;
}

int pakfire_elf_endianess(struct pakfire_elf* self) {
	return self->ehdr.e_ident[EI_DATA];
}

const char* pakfire_elf_build_id(struct pakfire_elf* self) {
	const void* buffer = NULL;
	ssize_t length;

	if (!self->build_id) {
		// Extract the GNU Build ID
		length = dwelf_elf_gnu_build_id(self->elf, &buffer);
		if (length < 0) {
			ERROR(self->ctx, "Could not read the GNU Build ID from %s: %s\n",
				self->path, elf_errmsg(-1));
			return NULL;
		}

		// Return NULL if there is no Build ID
		if (!length)
			return NULL;

		// Convert the Build ID to hex
		self->build_id = __pakfire_hexlify(buffer, length);
		if (!self->build_id) {
			ERROR(self->ctx, "Could not convert the Build ID into hex format: %m\n");
			return NULL;
		}

		DEBUG(self->ctx, "%s has Build ID %s\n", self->path, self->build_id);
	}

	return self->build_id;
}

const char* pakfire_elf_debuglink(struct pakfire_elf* self) {
	GElf_Word crc32;

	// Fetch the debug link
	if (!self->debuglink) {
		self->debuglink = dwelf_elf_gnu_debuglink(self->elf, &crc32);
		if (!self->debuglink)
			return NULL;

		DEBUG(self->ctx, "%s has debug link pointing at %s\n",
			self->path, self->debuglink);
	}

	return self->debuglink;
}

typedef int (*pakfire_elf_foreach_program_header_callback)
	(struct pakfire_elf* self, const GElf_Phdr* phdr, void* data);

static int pakfire_elf_foreach_program_header(struct pakfire_elf* self,
		pakfire_elf_foreach_program_header_callback callback, void* data) {
	GElf_Phdr phdr = {};
	int r = 0;

	// Walk through all program headers
	for (unsigned int i = 0; i < self->phnum; i++) {
		if (!gelf_getphdr(self->elf, i, &phdr)) {
			ERROR(self->ctx, "Could not parse program header: %s\n", elf_errmsg(-1));
			return -ENOTSUP;
		}

		// Call the callback
		r = callback(self, &phdr, data);
		if (r)
			break;
	}

	return r;
}

static int pakfire_elf_get_section(struct pakfire_elf* self,
		const Elf64_Word type, const char* name, Elf_Scn** section, GElf_Shdr* header, Elf_Data** data) {
	const char* sname = NULL;
	Elf_Scn* s = NULL;
	GElf_Shdr shdr;

	// Walk through all sections
	for (;;) {
		s = elf_nextscn(self->elf, s);
		if (!s)
			break;

		// Fetch the section header
		gelf_getshdr(s, &shdr);

		// Return any matching sections
		if (shdr.sh_type == type) {
			// If we have a name, check it too
			if (name) {
				sname = elf_strptr(self->elf, self->shstrndx, shdr.sh_name);
				if (!sname)
					continue;

				// Skip if the name does not match
				if (!pakfire_string_equals(name, sname))
					continue;
			}

			// Return a pointer to the section
			*section = s;

			// Send header if requested
			if (header)
				gelf_getshdr(s, header);

			// Send data if requested
			if (data)
				*data = elf_getdata(s, NULL);

			return 0;
		}
	}

	// No section found
	return 1;
}

typedef int (*pakfire_elf_foreach_section_callback)(struct pakfire_elf* self,
	Elf_Scn* section, const GElf_Shdr* shdr, void* data);

static int pakfire_elf_foreach_section(struct pakfire_elf* self,
		const Elf64_Word type, pakfire_elf_foreach_section_callback callback, void* data) {
	Elf_Scn* section = NULL;
	GElf_Shdr shdr = {};
	int r;

	// Walk through all sections
	for (;;) {
		section = elf_nextscn(self->elf, section);
		if (!section)
			break;

		// Fetch the section header
		if (!gelf_getshdr(section, &shdr)) {
			ERROR(self->ctx, "%s: Could not fetch the ELF section header: %s\n",
				self->path, elf_errmsg(-1));
			return -EINVAL;
		}

		// Skip sections that don't match
		if (type && shdr.sh_type != type)
			continue;

		// Call the callback
		r = callback(self, section, &shdr, data);
		if (r)
			return r;
	}

	return 0;
}

typedef int (*pakfire_elf_dyn_walk_callback)
	(struct pakfire_elf* self, const GElf_Shdr* shdr, const GElf_Dyn* dyn, void* data);

static int pakfire_elf_dyn_walk(struct pakfire_elf* self,
		pakfire_elf_dyn_walk_callback callback, void* data) {
	Elf_Scn* dynamic = NULL;
	GElf_Shdr shdr;
	Elf_Data* elf_data = NULL;
	GElf_Dyn dyn;
	int r;

	// Find the dynamic linking information
	r = pakfire_elf_get_section(self, SHT_DYNAMIC, NULL, &dynamic, &shdr, &elf_data);
	if (r) {
		DEBUG(self->ctx, "%s does not have a dynamic section\n", self->path);
		return r;
	}

	// Walk through all entries...
	for (unsigned int i = 0; ; i++)  {
		// Fetch the next entry
		if (!gelf_getdyn(elf_data, i, &dyn))
			break;

		// Call the callback
		r = callback(self, &shdr, &dyn, data);
		if (r)
			return r;
	}

	return 0;
}

static int pakfire_elf_fetch_interpreter(
		struct pakfire_elf* self, const GElf_Phdr* phdr, void* data) {
	Elf_Data* chunk = NULL;

	switch (phdr->p_type) {
		case PT_INTERP:
			chunk = elf_getdata_rawchunk(self->elf, phdr->p_offset, phdr->p_filesz, ELF_T_BYTE);
			if (!chunk || !chunk->d_buf) {
				ERROR(self->ctx, "Failed to fetch the interpreter\n");
				return -EINVAL;
			}

			// Store the interpreter
			self->interpreter = (const char*)chunk->d_buf;
			break;

		default:
			break;
	}

	return 0;
}

const char* pakfire_elf_interpreter(struct pakfire_elf* self) {
	int r;

	// Fetch the interpreter if not available
	if (!self->interpreter) {
		r = pakfire_elf_foreach_program_header(self, pakfire_elf_fetch_interpreter, NULL);
		if (r < 0)
			return NULL;
	}

	return self->interpreter;
}

static int pakfire_elf_fetch_soname(struct pakfire_elf* self,
		const GElf_Shdr* shdr, const GElf_Dyn* dyn, void* data) {
	switch (dyn->d_tag) {
		case DT_SONAME:
			self->soname = elf_strptr(self->elf, shdr->sh_link, dyn->d_un.d_val);

			return 1;
	}

	return 0;
}

const char* pakfire_elf_soname(struct pakfire_elf* self) {
	int r;

	// Fetch the SONAME if not available
	if (!self->soname) {
		r = pakfire_elf_dyn_walk(self, pakfire_elf_fetch_soname, NULL);
		if (r < 0)
			return NULL;
	}

	return self->soname;
}

int pakfire_elf_is_pie(struct pakfire_elf* self) {
	switch (pakfire_elf_type(self)) {
		// Shared Object files are good
		case ET_DYN:
			return 1;

		// Everything else is bad
		default:
			break;
	}

	return 0;
}

int pakfire_elf_has_ssp(struct pakfire_elf* self) {
	GElf_Sym symbol = {};
	int r;

	// .dynsym
	Elf_Scn* dynsym = NULL;
	GElf_Shdr symhdr = {};
	Elf_Data* symdata = NULL;

	// .dynstr
	Elf_Scn* dynstr = NULL;
	GElf_Shdr strhdr = {};
	Elf_Data* strdata = NULL;

	// Fetch .dynsym
	r = pakfire_elf_get_section(self, SHT_DYNSYM, NULL, &dynsym, &symhdr, &symdata);
	if (r) {
		DEBUG(self->ctx, "%s has no .dynsym section\n", self->path);
		return 0;
	}

	// Fetch .dynstr
	r = pakfire_elf_get_section(self, SHT_STRTAB, ".dynstr", &dynstr, &strhdr, &strdata);
	if (r) {
		DEBUG(self->ctx, "%s has no .dynstr section\n", self->path);
		return 0;
	}

	// Count any global functions
	size_t counter = 0;

	// Walk through all symbols
	for (unsigned int i = 0; i < symhdr.sh_size / symhdr.sh_entsize; i++) {
		gelf_getsym(symdata, i, &symbol);

		// Fetch the symbol name
		const char* name = elf_strptr(self->elf, elf_ndxscn(dynstr), symbol.st_name);

		// Skip empty section names
		if (!name || !*name)
			continue;

		// Exit if there is a symbol called "__stack_chk_fail"
		if (pakfire_string_startswith(name, "__stack_chk_fail"))
			return 1;

		// Or if the symbol is called __stack_chk_guard
		else if (pakfire_string_startswith(name, "__stack_chk_guard"))
			return 1;

		// Or if the symbol is called __intel_security_cookie
		else if (pakfire_string_startswith(name, "__intel_security_cookie"))
			return 1;

		// Count any global functions
		if ((ELF64_ST_BIND(symbol.st_info) == STB_GLOBAL) &&
				(ELF64_ST_TYPE(symbol.st_info) == STT_FUNC))
			counter++;
	}

	// If we have something that does not have an exported function, we just assume yes
	if (!counter) {
		DEBUG(self->ctx, "%s has no functions. Skipping SSP check.\n", self->path);
		return 1;
	}

	return 0;
}

static int pakfire_elf_check_execstack(
		struct pakfire_elf* self, const GElf_Phdr* phdr, void* data) {
	switch (phdr->p_type) {
		case PT_GNU_STACK:
			DEBUG(self->ctx,
				"%s: GNU_STACK flags: %c%c%c\n",
				self->path,
				(phdr->p_flags & PF_R) ? 'R' : '-',
				(phdr->p_flags & PF_W) ? 'W' : '-',
				(phdr->p_flags & PF_X) ? 'X' : '-'
			);

			// The stack cannot be writable and executable
			if ((phdr->p_flags & PF_W) && (phdr->p_flags & PF_X))
				return 1;

		default:
			break;
	}

	return 0;
}

int pakfire_elf_has_execstack(struct pakfire_elf* self) {
	return pakfire_elf_foreach_program_header(self, pakfire_elf_check_execstack, NULL);
}

static int pakfire_elf_has_bind_now(struct pakfire_elf* self,
		const GElf_Shdr* shdr, const GElf_Dyn* dyn, void* data) {
	switch (dyn->d_tag) {
		case DT_BIND_NOW:
			return 1;
			break;

		case DT_FLAGS:
			if (dyn->d_un.d_val & DF_BIND_NOW)
				return 1;
			break;

		case DT_FLAGS_1:
			if (dyn->d_un.d_val & DF_1_NOW)
				return 1;
			break;

		default:
			break;
	}

	return 0;
}

int pakfire_elf_is_fully_relro(struct pakfire_elf* self) {
	return pakfire_elf_dyn_walk(self, pakfire_elf_has_bind_now, NULL);
}

int pakfire_elf_is_partially_relro(struct pakfire_elf* self) {
	GElf_Phdr phdr;

	// Walk through all program headers
	for (unsigned int i = 0;; i++) {
		if (!gelf_getphdr(self->elf, i, &phdr))
			break;

		switch (phdr.p_type) {
			case PT_GNU_RELRO:
				return 1;

			default:
				break;
		}
	}

	return 0;
}

static uint32_t read_4_bytes(const int endianess, const char* data) {
	uint32_t bytes = 0;

	switch (endianess) {
		// Litte Endian
		case ELFDATA2LSB:
			bytes = data[0] |
				((uint32_t)data[1] << 8) |
				((uint32_t)data[2] << 16) |
				((uint32_t)data[3] << 24);
			break;

		// Big endian
		case ELFDATA2MSB:
			bytes = data[3] |
				((uint32_t)data[2] << 8) |
				((uint32_t)data[1] << 16) |
				((uint32_t)data[0] << 24);
			break;
	}

	return bytes;
}

static int pakfire_elf_check_cf_protection_aarch64(struct pakfire_elf* self,
		const int endianess, const uint32_t type, const char* payload) {
	int flags = 0;

	switch (type) {
		case GNU_PROPERTY_AARCH64_FEATURE_1_AND:
			break;

		// Ignore the rest
		default:
			return 0;
	}

	uint32_t property = read_4_bytes(endianess, payload);

	// Check for BTI
	if (!(property & GNU_PROPERTY_AARCH64_FEATURE_1_BTI))
		flags |= PAKFIRE_ELF_MISSING_BTI;

	// Check for PAC
	if (!(property & GNU_PROPERTY_AARCH64_FEATURE_1_PAC))
		flags |= PAKFIRE_ELF_MISSING_PAC;

	return flags;
}

static int pakfire_elf_check_cf_protection_riscv64(struct pakfire_elf* self,
		const int endianess, const uint32_t type, const char* payload) {
	// There is nothing to do here
	return 0;
}

static int pakfire_elf_check_cf_protection_x86_64(struct pakfire_elf* self,
		const int endianess, const uint32_t type, const char* payload) {
	int flags = 0;

	switch (type) {
		case GNU_PROPERTY_X86_FEATURE_1_AND:
			break;

		// Ignore the rest
		default:
			return 0;
	}

	uint32_t property = read_4_bytes(endianess, payload);

	// Check for IBT
	if (!(property & GNU_PROPERTY_X86_FEATURE_1_IBT))
		flags |= PAKFIRE_ELF_MISSING_IBT;

	// Check for Shadow Stack
	if (!(property & GNU_PROPERTY_X86_FEATURE_1_SHSTK))
		flags |= PAKFIRE_ELF_MISSING_SHSTK;

	return flags;
}

static int pakfire_elf_check_cf_protection(struct pakfire_elf* self,
		Elf_Scn* section, const GElf_Shdr* shdr, void* data) {
	GElf_Nhdr nhdr = {};
	size_t offset = 0;
	size_t offset_name = 0;
	size_t offset_data = 0;
	int r;

	// Fetch data
	Elf_Data* d = elf_getdata(section, NULL);

	// Fetch the .note header
	offset = gelf_getnote(d, offset, &nhdr, &offset_name, &offset_data);
	if (!offset) {
		ERROR(self->ctx, "%s: Could not read note section: %s\n",
			self->path, elf_errmsg(-1));
		return -errno;
	}

	// Skip any other note headers
	switch (nhdr.n_type) {
		case NT_GNU_PROPERTY_TYPE_0:
			break;

		// We are not interested in this here
		default:
			return 0;
	}

#if 0
	// Check if the name starts with "GNU"
	if (nhdr.n_namesz == sizeof(ELF_NOTE_GNU) {
		if (strcmp(data->d_buf + offset_name, ELF_NOTE_GNU) == 0)
			break;
	}
#endif

	size_t length = nhdr.n_descsz;

	// XXX Check if this is aligned to 8 bytes or 4 bytes on 32 bit

	// Fetch the endianess
	const int endianess = pakfire_elf_endianess(self);

	const char* payload = (const char*)d->d_buf + offset_data;

	while (length) {
		// Read the type and size of the .note section
		const uint32_t type = read_4_bytes(endianess, payload);
		const uint32_t size = read_4_bytes(endianess, payload + 4);

		// Skip header
		length -= sizeof(type) + sizeof(size);
		payload += sizeof(type) + sizeof(size);

		if (length < size) {
			ERROR(self->ctx, "GNU Property note has an incorrect format\n");
			return -EINVAL;
		}

		switch (pakfire_elf_machine(self)) {
			case EM_AARCH64:
				r = pakfire_elf_check_cf_protection_aarch64(self, endianess, type, payload);
				if (r < 0)
					return r;
				break;

			case EM_RISCV:
				r = pakfire_elf_check_cf_protection_riscv64(self, endianess, type, payload);
				if (r < 0)
					return r;
				break;

			case EM_X86_64:
				r = pakfire_elf_check_cf_protection_x86_64(self, endianess, type, payload);
				if (r < 0)
					return r;
				break;

			default:
				ERROR(self->ctx, "Unsupported ELF type (%d)\n", pakfire_elf_machine(self));
				return -ENOTSUP;
		}

		// Skip data part
		length  -= (size + 7) & ~7;
		payload += (size + 7) & ~7;
	}

	return 0;
}

int pakfire_elf_has_cf_protection(struct pakfire_elf* self) {
	return pakfire_elf_foreach_section(self, SHT_NOTE, pakfire_elf_check_cf_protection, NULL);
}

static int pakfire_elf_check_runpath(struct pakfire_elf* self,
		const GElf_Shdr* shdr, const GElf_Dyn* dyn, void* data) {
	const char* runpath = NULL;
	const char* value = NULL;
	char buffer[PATH_MAX];
	char* p = NULL;
	int r;

	// The result
	char*** runpaths = data;

	switch (dyn->d_tag) {
		case DT_RUNPATH:
		case DT_RPATH:
			// Fetch the value
			value = elf_strptr(self->elf, shdr->sh_link, dyn->d_un.d_val);
			if (!value)
				return 1;

			DEBUG(self->ctx, "%s has a RUNPATH: %s\n", self->path, value);

			// Copy the value into a buffer we can modify
			r = pakfire_string_set(buffer, value);
			if (r < 0)
				goto ERROR;

			// Split the value by :
			runpath = strtok_r(buffer, ":", &p);

			// Iterate over all elements
			while (runpath) {
				r = pakfire_strings_append(runpaths, runpath);
				if (r < 0)
					goto ERROR;

				// Move on to the next RUNPATH
				runpath = strtok_r(NULL, ":", &p);
			}

		default:
			break;
	}

	return 0;

ERROR:
	if (*runpaths) {
		pakfire_strings_free(*runpaths);
		*runpaths = NULL;
	}

	return r;
}

int pakfire_elf_has_runpaths(struct pakfire_elf* self, char*** runpaths) {
	return pakfire_elf_dyn_walk(self, pakfire_elf_check_runpath, runpaths);
}

static int __pakfire_elf_is_stripped(struct pakfire_elf* self,
		Elf_Scn* section, const Elf64_Shdr* shdr, void* data) {
	// Fetch the section name
	const char* name = elf_strptr(self->elf, self->shstrndx, shdr->sh_name);
	if (!name)
		return -EINVAL;

	switch (shdr->sh_type) {
		// We should not have .symtab
		case SHT_SYMTAB:
			return 1;

		// We should not have .symstr
		case SHT_STRTAB:
			if (pakfire_string_equals(name, ".symstr"))
				return 1;

			break;

		// Sometimes we need to check by name...
		default:
			// There should be nothing here starting with .debug_* or .zdebug_*
			if (pakfire_string_startswith(name, ".debug_"))
				return 1;

			else if (pakfire_string_startswith(name, ".zdebug_*"))
				return 1;

			// GDB symbol lookup
			else if (pakfire_string_equals(name, ".gdb_index"))
				return 1;

			break;
	}

	return 0;
}

int pakfire_elf_is_stripped(struct pakfire_elf* self) {
	int r;

	switch (pakfire_elf_type(self)) {
		// Do not check Relocatable Objects
		case ET_REL:
			return 0;

		// Check everything else
		default:
			break;
	}

	// Run through all sections
	r = pakfire_elf_foreach_section(self, SHT_NULL, __pakfire_elf_is_stripped, NULL);
	if (r < 0)
		return r;

	// If the callback found something, the binary is not stripped
	else if (r == 1)
		return 0;

	// Otherwise we assume the binary being stripped
	return 1;
}

static int pakfire_elf_find_provides(struct pakfire_elf* self,
		Elf_Scn* section, const GElf_Shdr* shdr, void* data) {
	GElf_Verdaux verdaux = {};
	GElf_Verdef verdef = {};
	const char* version = NULL;
	const char* base = NULL;
	char*** provides = data;
	int r;

	// Fetch the section data
	Elf_Data* d = elf_getdata(section, NULL);

	size_t offset = 0;

	while (offset < shdr->sh_size) {
		if (!gelf_getverdef(d, offset, &verdef))
			break;

		size_t aux_offset = offset + verdef.vd_aux;

		for (int i = 0; i < verdef.vd_cnt; i++) {
			if (!gelf_getverdaux(d, aux_offset, &verdaux))
				break;

			// Fetch the version string
			version = elf_strptr(self->elf, shdr->sh_link, verdaux.vda_name);

			// Skip empty versions
			if (!version || !*version)
				continue;

			// Is this the base?
			if (verdef.vd_flags & VER_FLG_BASE) {
				base = version;
				break;
			}

			// Add it to the list
			r = pakfire_strings_appendf(provides, "%s(%s)%s",
					base, version, pakfire_elf_is_elf64(self) ? "(64bit)" : "");
			if (r < 0)
				return r;

			if (!verdaux.vda_next)
				break;

			aux_offset += verdaux.vda_next;
		}

		if (!verdef.vd_next)
			break;

		offset += verdef.vd_next;
	}

	return 0;
}

int pakfire_elf_provides(struct pakfire_elf* self, char*** provides) {
	int r;

	// Fetch the soname
	const char* soname = pakfire_elf_soname(self);
	if (soname) {
		r = pakfire_strings_appendf(provides,
				"%s()%s", soname, pakfire_elf_is_elf64(self) ? "(64bit)" : "");
		if (r < 0)
			return r;
	}

	return pakfire_elf_foreach_section(self, SHT_GNU_verdef, pakfire_elf_find_provides, provides);
}

static int pakfire_elf_find_requires(struct pakfire_elf* self,
		Elf_Scn* section, const GElf_Shdr* shdr, void* data) {
	GElf_Vernaux vernaux = {};
	GElf_Verneed verneed = {};
	const char* version = NULL;
	const char* name = NULL;
	char*** requires = data;
	GElf_Dyn dyn = {};
	size_t aux_offset;
	size_t offset;
	int r;

	// Fetch the section data
	Elf_Data* d = elf_getdata(section, NULL);

	switch (shdr->sh_type) {
		case SHT_DYNAMIC:
			for (size_t i = 0; i < shdr->sh_size / shdr->sh_entsize; i++) {
				// Fetch the symbol
				if (!gelf_getdyn(d, i, &dyn))
					continue;

				switch (dyn.d_tag) {
					case DT_NEEDED:
						name = elf_strptr(self->elf, shdr->sh_link, dyn.d_un.d_val);

						// Skip empty names (this should not happen)
						if (!name || !*name)
							continue;

						DEBUG(self->ctx, "%s depends on %s\n", self->path, name);

						// Add it to the list
						r = pakfire_strings_appendf(requires, "%s()%s",
								name, pakfire_elf_is_elf64(self) ? "(64bit)" : "");
						if (r < 0)
							return r;
						break;
				}
	        }
	        break;

		case SHT_GNU_verneed:
			offset = 0;

			while (offset < shdr->sh_size) {
				if (!gelf_getverneed(d, offset, &verneed))
					break;

				// Fetch the library name
				name = elf_strptr(self->elf, shdr->sh_link, verneed.vn_file);
				if (!name || !*name)
					continue;

				aux_offset = offset + verneed.vn_aux;

				for (int i = 0; i < verneed.vn_cnt; i++) {
					if (!gelf_getvernaux(d, aux_offset, &vernaux))
						break;

					// Fetch the version string
					version = elf_strptr(self->elf, shdr->sh_link, vernaux.vna_name);

					// Skip empty versions
					if (!version || !*version)
						continue;

					// Add it to the list
					r = pakfire_strings_appendf(requires, "%s(%s)%s",
							name, version, pakfire_elf_is_elf64(self) ? "(64bit)" : "");
					if (r < 0)
						return r;

					if (!vernaux.vna_next)
						break;

					aux_offset += vernaux.vna_next;
				}

				if (!verneed.vn_next)
					break;

				offset += verneed.vn_next;
			}
			break;

		default:
			break;
	}

	return 0;
}

int pakfire_elf_requires(struct pakfire_elf* self, char*** requires) {
	return pakfire_elf_foreach_section(self, SHT_NULL, pakfire_elf_find_requires, requires);
}

/*
	libdw does not seem to export the error codes in their header files,
	although there is a function to retrieve them...
*/
#ifndef DWARF_E_NO_DWARF
#define DWARF_E_NO_DWARF 6
#endif

int pakfire_elf_foreach_source_file(struct pakfire_elf* self,
		pakfire_elf_foreach_source_file_callback callback, void* data) {
	const char* filename = NULL;
	char basename[PATH_MAX];
	char path[PATH_MAX];
	Dwarf* dwarf = NULL;
	Dwarf_Files* files = NULL;
	Dwarf_Die* die = NULL;
	Dwarf_Off offset = 0;
	Dwarf_Off next_offset;
	size_t cu_header_length;
	Dwarf_Die die_mem;
	size_t count;
	int r;

	// Read DWARF information
	dwarf = dwarf_begin(self->fd, DWARF_C_READ);
	if (!dwarf) {
		switch (dwarf_errno()) {
			// If we don't have any DWARF information there is nothing to do
			case DWARF_E_NO_DWARF:
				r = 0;
				goto ERROR;

			default:
				ERROR(self->ctx, "Could not initialize DWARF context: %s\n", dwarf_errmsg(-1));
				r = -errno;
				goto ERROR;
		}
	}

	for (;;) {
		// Fetch the next compilation unit
		r = dwarf_nextcu(dwarf, offset, &next_offset, &cu_header_length, NULL, NULL, NULL);
		if (r < 0)
			goto ERROR;

		// Fetch the Debug Information Entry
		die = dwarf_offdie(dwarf, offset + cu_header_length, &die_mem);
		if (!die)
			break;

		// Fetch the source files
		r = dwarf_getsrcfiles(die, &files, &count);
		if (r < 0)
			goto NEXT;

		// Iterate over all files...
		for (unsigned int i = 0; i < count; i++) {
			// Fetch the filename
			filename = dwarf_filesrc(files, i, NULL, NULL);

			// Copy to the stack
			r = pakfire_string_set(path, filename);
			if (r < 0)
				goto ERROR;

			// Normalize the path
			r = pakfire_path_normalize(path);
			if (r < 0)
				goto ERROR;

			// Determine the basename
			r = pakfire_path_basename(basename, filename);
			if (r < 0)
				goto ERROR;

			// Ignore things like <artificial> or <built-in>
			if (pakfire_string_startswith(basename, "<")
					&& pakfire_string_endswith(basename, ">"))
				continue;

			// Call the callback
			r = callback(self->ctx, self, path, data);
			if (r)
				goto ERROR;
		}

NEXT:
		offset = next_offset;
	}

ERROR:
	if (dwarf)
		dwarf_end(dwarf);

	return r;
}
