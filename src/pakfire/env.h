/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_ENV_H
#define PAKFIRE_ENV_H

struct pakfire_env;

#include <pakfire/ctx.h>

int pakfire_env_create(struct pakfire_env** env, struct pakfire_ctx* ctx);

struct pakfire_env* pakfire_env_ref(struct pakfire_env* env);
struct pakfire_env* pakfire_env_unref(struct pakfire_env* env);

char** pakfire_env_get_envp(struct pakfire_env* env);

const char* pakfire_env_get(struct pakfire_env* env, const char* key);
int pakfire_env_set(struct pakfire_env* env, const char* key, const char* format, ...)
	__attribute__((format(printf, 3, 4)));
int pakfire_env_append(struct pakfire_env* env, const char* key, const char* format, ...)
	__attribute__((format(printf, 3, 4)));

int pakfire_env_import(struct pakfire_env* env, const char** e);

int pakfire_env_merge(struct pakfire_env* env1, struct pakfire_env* env2);

#endif /* PAKFIRE_ENV_H */
