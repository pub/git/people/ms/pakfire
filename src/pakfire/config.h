/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef PAKFIRE_CONFIG_H
#define PAKFIRE_CONFIG_H

#include <stdio.h>

struct pakfire_config;

int pakfire_config_create(struct pakfire_config** config);
struct pakfire_config* pakfire_config_ref(struct pakfire_config* config);
struct pakfire_config* pakfire_config_unref(struct pakfire_config* config);

int pakfire_config_set(struct pakfire_config* config,
	const char* section, const char* key, const char* value);
int pakfire_config_set_format(struct pakfire_config* config,
	const char* section, const char* key, const char* format, ...)
	__attribute__((format(printf, 4, 5)));

const char* pakfire_config_get(struct pakfire_config* config,
	const char* section, const char* key, const char* _default);
long int pakfire_config_get_int(struct pakfire_config* config,
	const char* section, const char* key, long int _default);
int pakfire_config_get_bool(struct pakfire_config* config,
	const char* section, const char* key, int _default);
size_t pakfire_config_get_bytes(struct pakfire_config* config,
	const char* section, const char* key, const size_t _default);

// Walk
typedef int (*pakfire_config_walk_callback)
	(struct pakfire_config* config, const char* name, void* p);
int pakfire_config_walk_sections(struct pakfire_config* config,
	pakfire_config_walk_callback callback, void* p);

int pakfire_config_has_section(struct pakfire_config* config, const char* section);

int pakfire_config_read(struct pakfire_config* config, FILE* f);
int pakfire_config_read_path(struct pakfire_config* config, const char* path);
int pakfire_config_parse(struct pakfire_config* config, const char* s, ssize_t length);

int pakfire_config_dump(struct pakfire_config* config, FILE* f);

#endif /* PAKFIRE_CONFIG_H */
