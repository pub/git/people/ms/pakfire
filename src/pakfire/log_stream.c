/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2024 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <fcntl.h>

#include <systemd/sd-event.h>

#include <pakfire/ctx.h>
#include <pakfire/log_stream.h>

struct pakfire_log_stream {
	struct pakfire_ctx* ctx;
	int nrefs;

	// Pipe
	int pipe[2];

	// Buffer
	char buffer[64 * 1024];
	size_t buffered;

	// Event Source
	sd_event_source* event;

	// Callback
	pakfire_log_stream_callback callback;
	void* data;
};

static void pakfire_log_stream_free(struct pakfire_log_stream* stream) {
	if (stream->event)
		stream->event = sd_event_source_unref(stream->event);

	// Close the pipe
	pakfire_log_stream_close(stream);

	if (stream->ctx)
		pakfire_ctx_unref(stream->ctx);
	free(stream);
}

int pakfire_log_stream_create(struct pakfire_log_stream** stream, struct pakfire_ctx* ctx,
		pakfire_log_stream_callback callback, void* data) {
	struct pakfire_log_stream* s = NULL;
	int r;

	// Allocate a new object
	s = calloc(1, sizeof(*s));
	if (!s)
		return -errno;

	// Initialize the reference counter
	s->nrefs = 1;

	// Store a reference to the context
	s->ctx = pakfire_ctx_ref(ctx);

	// Store the callback & data
	s->callback = callback;
	s->data = data;

	// Create a new pipe
	r = pipe2(s->pipe, O_CLOEXEC);
	if (r < 0) {
		ERROR(s->ctx, "Could not create a pipe: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Return the pointer
	*stream = s;

	return 0;

ERROR:
	if (s)
		pakfire_log_stream_free(s);

	return r;
}

struct pakfire_log_stream* pakfire_log_stream_ref(struct pakfire_log_stream* stream) {
	++stream->nrefs;

	return stream;
}

struct pakfire_log_stream* pakfire_log_stream_unref(struct pakfire_log_stream* stream) {
	if (--stream->nrefs > 0)
		return stream;

	pakfire_log_stream_free(stream);
	return NULL;
}

static int pakfire_log_stream_fill_buffer(struct pakfire_log_stream* stream, int fd) {
	ssize_t bytes_read;

	if (stream->buffered >= sizeof(stream->buffer))
		return -EAGAIN;

	// Read into the buffer
	bytes_read = read(fd, stream->buffer + stream->buffered, sizeof(stream->buffer) - stream->buffered);
	if (bytes_read < 0)
		return bytes_read;

	stream->buffered += bytes_read;

	return 0;
}

static int pakfire_log_stream_drain_buffer(struct pakfire_log_stream* stream) {
	const char* eol = NULL;
	size_t length;
	int r;

	// Log a message if we don't have a callback
	if (!stream->callback) {
		ERROR(stream->ctx, "Log stream has no callback set\n");
		return -EINVAL;
	}

	for (;;) {
		// Find the next line
		eol = memchr(stream->buffer, '\n', stream->buffered);
		if (!eol) {
			if (stream->buffered < sizeof(stream->buffer))
				return 0;
		}

		// Determine the length of the line
		if (eol)
			length = eol - stream->buffer + 1;
		else
			length = stream->buffered;

		// Call the callback
		r = stream->callback(stream, stream->buffer, length, stream->data);
		if (r)
			return r;

		memmove(stream->buffer, stream->buffer + length, stream->buffered - length);
		stream->buffered -= length;
	}
}

static int __pakfire_log_stream(sd_event_source* s, int fd, uint32_t events, void* data) {
	struct pakfire_log_stream* stream = data;
	int r = -EINVAL;

	if (events & EPOLLIN) {
		// Fill the buffer
		r = pakfire_log_stream_fill_buffer(stream, fd);
		if (r < 0)
			return r;

		// Drain the buffer
		r = pakfire_log_stream_drain_buffer(stream);
		if (r < 0)
			return r;
	}

	// Handle if the child process has closed the file descriptor
	if (events & EPOLLHUP) {
		if (stream->event)
			stream->event = sd_event_source_unref(stream->event);

		if (stream->pipe[0] >= 0) {
			close(stream->pipe[0]);
			stream->pipe[0] = -EBADF;
		}
	}

	return r;
}

/*
	To be called from the parent process...
*/
int pakfire_log_stream_in_parent(struct pakfire_log_stream* stream, sd_event* loop) {
	int r;

	// Close the write end of the pipe
	if (stream->pipe[1] >= 0) {
		close(stream->pipe[1]);
		stream->pipe[1] = -EBADF;
	}

	// Register the file descriptor with the event loop
	r = sd_event_add_io(loop, &stream->event, stream->pipe[0],
			EPOLLIN|EPOLLHUP|EPOLLET, __pakfire_log_stream, stream);
	if (r < 0) {
		ERROR(stream->ctx, "Could not register fd %d: %s\n", stream->pipe[0], strerror(-r));
		return r;
	}

	return 0;
}

/*
	To be called from the child process...
*/
int pakfire_log_stream_in_child(struct pakfire_log_stream* stream) {
	// Close the read end of the pipe
	if (stream->pipe[0] >= 0) {
		close(stream->pipe[0]);
		stream->pipe[0] = -EBADF;
	}

	return 0;
}

int pakfire_log_stream_printf(struct pakfire_log_stream* stream, const char* format, ...) {
	va_list args;
	int r;

	va_start(args, format);
	r = pakfire_log_stream_vprintf(stream, format, args);
	va_end(args);

	return r;
}

int pakfire_log_stream_vprintf(struct pakfire_log_stream* stream, const char* format, va_list args) {
	char* buffer = NULL;
	ssize_t length;
	int r;

	// Format the buffer
	length = vasprintf(&buffer, format, args);
	if (length < 0)
		return -errno;

	// Write the buffer to the stream
	r = pakfire_log_stream_write(stream, buffer, length);
	free(buffer);

	return r;
}

int pakfire_log_stream_write(struct pakfire_log_stream* stream, const char* buffer, size_t length) {
	// Fail if the pipe isn't open
	if (stream->pipe[1] < 0)
		return -EPIPE;

	// Send the message into the pipe
	return write(stream->pipe[1], buffer, length);
}

int pakfire_log_stream_close(struct pakfire_log_stream* stream) {
	if (stream->pipe[0] >= 0) {
		close(stream->pipe[0]);
		stream->pipe[0] = -EBADF;
	}

	if (stream->pipe[1] >= 0) {
		close(stream->pipe[1]);
		stream->pipe[1] = -EBADF;
	}

	return 0;
}
