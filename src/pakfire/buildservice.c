/*#############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2021 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <limits.h>
#include <stdlib.h>

#include <json.h>

#include <pakfire/buildservice.h>
#include <pakfire/config.h>
#include <pakfire/ctx.h>
#include <pakfire/hasher.h>
#include <pakfire/hashes.h>
#include <pakfire/logging.h>
#include <pakfire/os.h>
#include <pakfire/path.h>
#include <pakfire/string.h>
#include <pakfire/xfer.h>

#include <krb5/krb5.h>

#define DEFAULT_KEYTAB "/etc/krb5.keytab"

struct pakfire_buildservice {
	struct pakfire_ctx* ctx;
	int nrefs;

	// URL
	char url[PATH_MAX];

	// Configuration
	char keytab[PATH_MAX];

	// Kerberos Context
	krb5_context krb5_ctx;
};

static int pakfire_buildservice_xfer_create(struct pakfire_xfer** xfer,
	struct pakfire_buildservice* service, const char* url, ...) __attribute__((format(printf, 3, 4)));

static int pakfire_buildservice_xfer_create(struct pakfire_xfer** xfer,
		struct pakfire_buildservice* service, const char* url, ...) {
	struct pakfire_xfer* x = NULL;
	va_list args;
	int r;

	// Create a new xfer
	va_start(args, url);
	r = pakfire_xfer_create(&x, service->ctx, url, args);
	va_end(args);
	if (r < 0)
		goto ERROR;

	// Set the base URL
	r = pakfire_xfer_set_baseurl(x, service->url);
	if (r < 0)
		goto ERROR;

	// Success
	*xfer = pakfire_xfer_ref(x);

ERROR:
	if (x)
		pakfire_xfer_unref(x);

	return r;
}

static int pakfire_buildservice_setup_auth(struct pakfire_buildservice* service) {
	const char* error = NULL;
	int r;

	// Setup a Kerberos context
	r = krb5_init_context(&service->krb5_ctx);
	if (r) {
		error = krb5_get_error_message(service->krb5_ctx, r);

		ERROR(service->ctx, "Could not initialize Kerberos: %s\n", error);
		krb5_free_error_message(service->krb5_ctx, error);

		goto ERROR;
	}

ERROR:
	return r;
}

static void pakfire_buildservice_free(struct pakfire_buildservice* service) {
	if (service->krb5_ctx)
		krb5_free_context(service->krb5_ctx);
	if (service->ctx)
		pakfire_ctx_unref(service->ctx);

	free(service);
}

int pakfire_buildservice_create(
		struct pakfire_buildservice** service, struct pakfire_ctx* ctx, const char* url) {
	struct pakfire_buildservice* s = NULL;
	int r;

	// Allocate some memory
	s = calloc(1, sizeof(*s));
	if (!s)
		return -errno;

	// Store a reference to the context
	s->ctx = pakfire_ctx_ref(ctx);

	// Initialize the reference counter
	s->nrefs = 1;

	// Store the URL
	r = pakfire_string_set(s->url, url);
	if (r < 0)
		goto ERROR;

	DEBUG(s->ctx, "Pakfire Build Service initialized for %s\n",
		pakfire_buildservice_get_url(s));

	// Return the pointer
	*service = s;

	return 0;

ERROR:
	pakfire_buildservice_free(s);

	return r;
}

struct pakfire_buildservice* pakfire_buildservice_ref(
		struct pakfire_buildservice* service) {
	++service->nrefs;

	return service;
}

struct pakfire_buildservice* pakfire_buildservice_unref(
		struct pakfire_buildservice* service) {
	if (--service->nrefs > 0)
		return service;

	pakfire_buildservice_free(service);
	return NULL;
}

const char* pakfire_buildservice_get_url(struct pakfire_buildservice* service) {
	return service->url;
}

// Build

int pakfire_buildservice_build(struct pakfire_buildservice* service, const char* upload,
		const char* repo, const char** arches, int flags) {
	struct pakfire_xfer* xfer = NULL;
	int r;

	// Create a new xfer
	r = pakfire_buildservice_xfer_create(&xfer, service, "/api/v1/builds");
	if (r)
		goto ERROR;

	// Enable authentication
	r = pakfire_xfer_auth(xfer);
	if (r)
		goto ERROR;

	// Add the upload parameter
	r = pakfire_xfer_add_param(xfer, "upload", "%s", upload);
	if (r)
		goto ERROR;

	// Add the repo parameter
	if (repo) {
		r = pakfire_xfer_add_param(xfer, "repo", "%s", repo);
		if (r)
			goto ERROR;
	}

	// Add any arches
	if (arches) {
		for (const char** arch = arches; *arch; arch++) {
			r = pakfire_xfer_add_param(xfer, "arch", "%s", *arch);
			if (r)
				goto ERROR;
		}
	}

	// Disable tests?
	if (flags & PAKFIRE_BUILDSERVICE_DISABLE_TESTS) {
		r = pakfire_xfer_add_param(xfer, "disable_test_builds", "%s", "yes");
		if (r)
			goto ERROR;
	}

	// Send the request
	r = pakfire_xfer_run_api_request(xfer, NULL);
	if (r)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);

	return r;
}

static int pakfire_buildservice_api_response_string(struct pakfire_buildservice* self,
		struct json_object* response, const char* key, char** value) {
	struct json_object* object = NULL;
	const char* s = NULL;
	char* buffer = NULL;
	int r;

	// Fetch the object at key
	r = json_object_object_get_ex(response, key, &object);
	if (r == 0) {
		ERROR(self->ctx, "Could not fetch '%s' from response\n", key);
		r = -EBADMSG;
		goto ERROR;
	}

	// Read the string
	s = json_object_get_string(object);
	if (!s) {
		ERROR(self->ctx, "Could not fetch value from '%s'\n", key);
		r = -EBADMSG;
		goto ERROR;
	}

	// Copy the value
	buffer = strdup(s);
	if (!buffer) {
		ERROR(self->ctx, "Could not copy the string: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Return the value
	*value = buffer;
	return 0;

ERROR:
	if (buffer)
		free(buffer);

	return r;
}

// Uploads

static int pakfire_buildservice_create_upload(struct pakfire_buildservice* service,
		const char* path, const char* filename, FILE* f, char** url, char** uuid) {
	struct pakfire_hashes hashes = {};
	struct pakfire_xfer* xfer = NULL;
	struct json_object* response = NULL;
	char* hexdigest_blake2b512 = NULL;
	struct stat stat;
	int r;

	const int fd = fileno(f);

	// Stat the file
	r = fstat(fd, &stat);
	if (r) {
		ERROR(service->ctx, "Could not stat %s: %s\n", path, strerror(errno));
		goto ERROR;
	}

	// Compute the digest
	r = pakfire_hash_file(service->ctx, f, PAKFIRE_HASH_BLAKE2B512, &hashes);
	if (r < 0) {
		ERROR(service->ctx, "Could not compute the checksum of %s: %s\n",
			path, strerror(-r));
		goto ERROR;
	}

	// Convert the digest into hex format
	r = pakfire_hashes_get_hex(&hashes, PAKFIRE_HASH_BLAKE2B512, &hexdigest_blake2b512);
	if (r < 0)
		goto ERROR;

	// Create a new xfer
	r = pakfire_buildservice_xfer_create(&xfer, service, "/api/v1/uploads");
	if (r)
		goto ERROR;

	// Enable authentication
	r = pakfire_xfer_auth(xfer);
	if (r)
		goto ERROR;

	// Add the filename parameter
	r = pakfire_xfer_add_param(xfer, "filename", "%s", filename);
	if (r)
		goto ERROR;

	// Add the size parameter
	r = pakfire_xfer_add_param(xfer, "size", "%jd", stat.st_size);
	if (r)
		goto ERROR;

	// Add the hexdigest parameter
	r = pakfire_xfer_add_param(xfer, "hexdigest_blake2b512", "%s", hexdigest_blake2b512);
	if (r)
		goto ERROR;

	// Send the request
	r = pakfire_xfer_run_api_request(xfer, &response);
	if (r)
		goto ERROR;

	// Fetch the URL
	if (url) {
		r = pakfire_buildservice_api_response_string(service, response, "url", url);
		if (r < 0)
			goto ERROR;
	}

	// Fetch the ID
	if (uuid) {
		r = pakfire_buildservice_api_response_string(service, response, "id", uuid);
		if (r < 0)
			goto ERROR;
	}

	// Success
	r = 0;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (response)
		json_object_put(response);
	if (hexdigest_blake2b512)
		free(hexdigest_blake2b512);

	return r;
}

static int pakfire_buildservice_upload_payload(struct pakfire_buildservice* service,
		const char* filename, const char* url, FILE* f) {
	struct pakfire_xfer* xfer = NULL;
	int r;

	// Create a new xfer
	r = pakfire_buildservice_xfer_create(&xfer, service, "%s", url);
	if (r)
		goto ERROR;

	// Set the title
	r = pakfire_xfer_set_title(xfer, filename);
	if (r)
		goto ERROR;

	// Enable authentication
	r = pakfire_xfer_auth(xfer);
	if (r)
		goto ERROR;

	// Set source file
	r = pakfire_xfer_set_input(xfer, f);
	if (r)
		goto ERROR;

	// Send the request
	r = pakfire_xfer_run_api_request(xfer, NULL);
	if (r)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);

	return r;
}

int pakfire_buildservice_upload(struct pakfire_buildservice* service,
		const char* path, const char* filename, char** uuid) {
	char basename[NAME_MAX];
	char* url = NULL;
	FILE* f = NULL;
	int r;

	// Compute the basename
	r = pakfire_path_basename(basename, path);
	if (r)
		goto ERROR;

	// Set the basename as default filename
	if (!filename)
		filename = basename;

	// Open the source file
	f = fopen(path, "r");
	if (!f) {
		ERROR(service->ctx, "Could not open file for upload %s: %m\n", path);
		return -errno;
	}

	// Create a new upload
	r = pakfire_buildservice_create_upload(service, path, filename, f, &url, uuid);
	if (r)
		goto ERROR;

	DEBUG(service->ctx, "Created a new upload (%s)\n", *uuid);

	// Send the payload
	r = pakfire_buildservice_upload_payload(service, filename, url, f);
	if (r)
		goto ERROR;

ERROR:
	if (r) {
		if (*uuid)
			free(*uuid);

		*uuid = NULL;
	}
	if (url)
		free(url);
	if (f)
		fclose(f);

	return r;
}

int pakfire_buildservice_list_uploads(
		struct pakfire_buildservice* service, struct json_object** p) {
	struct pakfire_xfer* xfer = NULL;
	struct json_object* response = NULL;
	struct json_object* uploads = NULL;
	int r;

	// Create a new xfer
	r = pakfire_buildservice_xfer_create(&xfer, service, "/api/v1/uploads");
	if (r)
		goto ERROR;

	// Enable authentication
	r = pakfire_xfer_auth(xfer);
	if (r)
		goto ERROR;

	// Send the request
	r = pakfire_xfer_run_api_request(xfer, &response);
	if (r)
		goto ERROR;

	// Fetch the uploads
	uploads = json_object_object_get(response, "uploads");
	if (!uploads) {
		ERROR(service->ctx, "Malformed response\n");
		r = -EBADMSG;
		goto ERROR;
	}

	// Return the pointer
	*p = json_object_get(uploads);

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (response)
		json_object_put(response);

	return r;
}

int pakfire_buildservice_delete_upload(
		struct pakfire_buildservice* service, const char* uuid) {
	struct pakfire_xfer* xfer = NULL;
	struct json_object* response = NULL;
	int r;

	// Create a new xfer
	r = pakfire_buildservice_xfer_create(&xfer, service, "/api/v1/uploads/%s", uuid);
	if (r)
		goto ERROR;

	// Ask to DELETE
	r = pakfire_xfer_set_method(xfer, PAKFIRE_METHOD_DELETE);
	if (r)
		goto ERROR;

	// Enable authentication
	r = pakfire_xfer_auth(xfer);
	if (r)
		goto ERROR;

	// Send the request
	r = pakfire_xfer_run_api_request(xfer, &response);
	if (r)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (response)
		json_object_put(response);

	return r;
}

// Repositories

int pakfire_buildservice_list_repos(struct pakfire_buildservice* service,
		const char* distro, struct json_object** p) {
	struct pakfire_xfer* xfer = NULL;
	struct json_object* response = NULL;
	struct json_object* repos = NULL;
	int r;

	// Check inputs
	if (!distro)
		return -EINVAL;

	// Create a new xfer
	r = pakfire_buildservice_xfer_create(&xfer, service, "/api/v1/repos/%s", distro);
	if (r)
		goto ERROR;

	// Enable authentication
	r = pakfire_xfer_auth(xfer);
	if (r)
		goto ERROR;

	// Send the request
	r = pakfire_xfer_run_api_request(xfer, &response);
	if (r)
		goto ERROR;

	// Fetch the repos
	if (!json_object_object_get_ex(response, "repos", &repos)) {
		ERROR(service->ctx, "Malformed response\n");
		r = -EBADMSG;
		goto ERROR;
	}

	// Return the pointer
	*p = json_object_get(repos);

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (response)
		json_object_put(response);

	return r;
}

int pakfire_buildservice_get_repo(struct pakfire_buildservice* service,
		const char* distro, const char* name, struct json_object** p) {
	struct pakfire_xfer* xfer = NULL;
	struct json_object* response = NULL;
	int r;

	// Check inputs
	if (!distro || !name)
		return -EINVAL;

	// Create a new xfer
	r = pakfire_buildservice_xfer_create(&xfer, service, "/api/v1/repos/%s/%s", distro, name);
	if (r)
		goto ERROR;

	// Enable authentication
	r = pakfire_xfer_auth(xfer);
	if (r)
		goto ERROR;

	// Send the request
	r = pakfire_xfer_run_api_request(xfer, &response);
	if (r)
		goto ERROR;

	// Return the pointer
	*p = json_object_get(response);

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (response)
		json_object_put(response);

	return r;
}

int pakfire_buildservice_create_repo(struct pakfire_buildservice* service,
		const char* distro, const char* name, const char* description, struct json_object** p) {
	struct pakfire_xfer* xfer = NULL;
	struct json_object* response = NULL;
	int r;

	// Check inputs
	if (!distro)
		return -EINVAL;

	// Create a new xfer
	r = pakfire_buildservice_xfer_create(&xfer, service, "/api/v1/repos/%s", distro);
	if (r)
		goto ERROR;

	// Enable authentication
	r = pakfire_xfer_auth(xfer);
	if (r)
		goto ERROR;

	// Set name
	r = pakfire_xfer_add_param(xfer, "name", "%s", name);
	if (r)
		goto ERROR;

	// Set description
	if (description) {
		r = pakfire_xfer_add_param(xfer, "description", "%s", description);
		if (r)
			goto ERROR;
	}

	// Send the request
	r = pakfire_xfer_run_api_request(xfer, &response);
	if (r)
		goto ERROR;

	// Return the pointer
	if (p)
		*p = json_object_get(response);

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);
	if (response)
		json_object_put(response);

	return r;
}

int pakfire_buildservice_delete_repo(struct pakfire_buildservice* service,
		const char* distro, const char* name) {
	struct pakfire_xfer* xfer = NULL;
	int r;

	// Create a new xfer
	r = pakfire_buildservice_xfer_create(&xfer, service, "/api/v1/repos/%s/%s", distro, name);
	if (r)
		goto ERROR;

	// Ask to DELETE
	r = pakfire_xfer_set_method(xfer, PAKFIRE_METHOD_DELETE);
	if (r)
		goto ERROR;

	// Enable authentication
	r = pakfire_xfer_auth(xfer);
	if (r)
		goto ERROR;

	// Send the request
	r = pakfire_xfer_run_api_request(xfer, NULL);
	if (r)
		goto ERROR;

ERROR:
	if (xfer)
		pakfire_xfer_unref(xfer);

	return r;
}
